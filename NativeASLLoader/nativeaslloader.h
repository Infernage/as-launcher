#ifndef NATIVEASLLOADER_H
#define NATIVEASLLOADER_H

#include "nativeaslloader_global.h"
#include <loader.h>
#include <jni.h>

class NATIVEASLLOADERSHARED_EXPORT NativeASLLoader : public Loader
{

public:
    NativeASLLoader();
    ~NativeASLLoader();
    bool load(QString workingDir);
    int quit();
    bool waitForTermination();
    QString getErrorStr();
    static void addFileToRemove(QString path);

private:
    JavaVM* vm;
    JNIEnv* env;
    jobject nativeClassLoader;
    jmethodID loadClass;
    jclass bootstrap;
    QString error;
    QString nativeASLLoader;
    QString bootstrapASLloader;
    static QStringList files;

    bool checkException();
    void destroyFiles();
};

extern "C"{
    NATIVEASLLOADERSHARED_EXPORT Loader *createLoader();

    NATIVEASLLOADERSHARED_EXPORT char *getVersion();
}

#endif // NATIVEASLLOADER_H
