#include "javaproxyclasses.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDebug>
#include <QBuffer>

AES::AES(const char *type)
{
    this->type = QString(type);
}

void AES::setPassword(const char *password)
{
    this->password = QString::fromUtf8(password);
}

QString AES::getPassword()
{
    return password;
}

QByteArray AES::manipulateData(QString passwordHash, const char *data, bool isEncrypt, int salt)
{
    QByteArray bdata(data);
    if (isEncrypt){
        QCA::InitializationVector iv(16);
        QCA::SymmetricKey key(passwordHash.toUtf8());
        QCA::Cipher cipher(type, QCA::Cipher::CBC, QCA::Cipher::DefaultPadding, QCA::Encode, key, iv);
        QCA::SecureArray u = cipher.update(bdata);
        if (!cipher.ok()) throw QString("Failed during encoder cipher");
        QByteArray ciphered = u.append(cipher.final()).toByteArray();
        QByteArray res;
        QDataStream stream(&res, QIODevice::WriteOnly);
        stream << salt << type.toUtf8() << iv.toByteArray() << ciphered;
        return res.toHex();
    } else{
        bdata = QByteArray::fromHex(bdata);
        QDataStream stream(&bdata, QIODevice::ReadOnly);
        int storedSalt;
        QByteArray storedType;
        QByteArray storedIV;
        QByteArray storedData;
        stream >> storedSalt >> storedType >> storedIV >> storedData;
        if (storedType != type){
            throw QString("Expected ") + type + QString(" but found ") + storedType;
        }
        QCA::InitializationVector iv(storedIV);
        QCA::SymmetricKey key(passwordHash.toUtf8());
        QCA::Cipher cipher(type, QCA::Cipher::CBC, QCA::Cipher::DefaultPadding, QCA::Decode, key, iv);
        QByteArray res = cipher.process(storedData).toByteArray();
        if (!cipher.ok()) throw QString("Failed during decode cipher");
        return res;
    }
}

const char *AES::manipulateFile(QString passwordHash, const char *srcPath, const char *dstPath, bool isEncrypt, int salt)
{
    if (isEncrypt){
        QCA::InitializationVector iv(16);
        QCA::SymmetricKey key(passwordHash.toUtf8());
        QCA::Cipher cipher(type, QCA::Cipher::CBC, QCA::Cipher::DefaultPadding, QCA::Encode, key, iv);
        QFile src;
        src.setFileName((QString(srcPath)));
        if (!src.open(QFile::ReadOnly)){
            throw QString("Failed to open ") + QString(srcPath);
        }
        QFile dst;
        dst.setFileName((QString(dstPath)));
        if (!dst.open(QFile::WriteOnly)){
            throw QString("Failed to open ") + QString(dstPath);
        }
        dst.write(iv.toByteArray());
        QDataStream stream(&dst);
        stream << salt << type.toUtf8();
        QString hashing;
        QCA::Hash hash("sha1");
        hash.update(&src);
        hashing = hash.final().toByteArray();
        stream << (QString("FileExtension=") + QFileInfo(QString(srcPath)).fileName()
                   .mid(QFileInfo(QString(srcPath)).fileName().lastIndexOf(".") + 1,
                        QFileInfo(QString(srcPath)).fileName().size()) + QString("::") + hashing).toUtf8();
        src.seek(0);
        while(!src.atEnd()){
            QCA::SecureArray u = cipher.update(QCA::SecureArray(src.read(256 * 1024)));
            if (!cipher.ok()){
                src.close();
                dst.close();
                throw QString("Failed during encoder at file reading");
            }
            dst.write(u.toByteArray());
        }
        QCA::SecureArray f = cipher.final();
        dst.write(f.toByteArray());
        src.close();
        dst.close();
        return NULL;
    } else{
        QFile src;
        src.setFileName((QString(srcPath)));
        if (!src.open(QFile::ReadOnly)){
            throw QString("Failed to open ") + QString(srcPath);
        }
        QDataStream stream(&src);
        QByteArray storedIV, storedType, ext;
        storedIV = src.read(16);
        stream.skipRawData(4);
        stream >> storedType >> ext;
        if (storedType != type){
            src.close();
            throw QString("Expected ") + type + QString(" but found ") + storedType;
        }
        QString target = QDir(QString(dstPath)).absoluteFilePath(QFileInfo(QFileInfo(QString(srcPath)).fileName())
                                                                 .baseName() + QString(".") + QString(ext).split("=")
                                                                 .at(1).split("::").at(0));
        QFile dst(target);
        if (!dst.open(QFile::ReadWrite)){
            throw QString("Failed to open ") + target;
        }
        QCA::InitializationVector iv(storedIV);
        QCA::SymmetricKey key(passwordHash.toUtf8());
        QCA::Cipher cipher(type, QCA::Cipher::CBC, QCA::Cipher::DefaultPadding, QCA::Decode, key, iv);
        while(!src.atEnd()){
            QCA::SecureArray u = cipher.update(QCA::SecureArray(src.read(256 * 1024)));
            if (!cipher.ok()){
                src.close();
                dst.close();
                dst.remove();
                throw QString("Failed during decoder at file reading");
            }
            dst.write(u.toByteArray());
        }
        QCA::SecureArray f = cipher.final();
        dst.write(f.toByteArray());
        dst.seek(0);
        QCA::Hash hash("sha1");
        hash.update(&dst);
        dst.close();
        if (QString(ext).split("::").at(1) != hash.final().toByteArray()){
            src.close();
            dst.remove();
            throw QString("The stored hash doesn't match with the calculated one")
                          + QString(".\nThe file can have been encrypted or decrypted incorreclty.");
        }
        src.close();
        char *res = new char[target.size()];
        strcpy(res, target.toStdString().c_str());
        return res;
    }
}
