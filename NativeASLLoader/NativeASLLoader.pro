#-------------------------------------------------
#
# Project created by QtCreator 2014-07-30T12:51:46
#
#-------------------------------------------------

QT       -= gui
QT       += network

TARGET = ASLL
TEMPLATE = lib

CONFIG += crypto

DEFINES += NATIVEASLLOADER_LIBRARY

SOURCES += nativeaslloader.cpp \
    jnicaller.cpp \
    javaproxyclasses.cpp \
    xz/xz_crc32.c \
    xz/xz_crc64.c \
    xz/xz_dec_bcj.c \
    xz/xz_dec_lzma2.c \
    xz/xz_dec_stream.c

HEADERS += nativeaslloader.h\
        nativeaslloader_global.h \
    ../ASLExecutable/loader.h \
    jnicaller.h \
    javaproxyclasses.h \
    xz/xz.h \
    xz/xz_lzma2.h \
    xz/xz_private.h \
    xz/xz_stream.h

RESOURCES += \
    jarLauncher.qrc

win32{
    contains(QMAKE_TARGET.arch, x86_64){
        CONFIG(debug, debug|release){
            CONFIG   += console
            CONFIG   -= app_bundle
            LIBS += -L$$PWD/../../../../qca-build/lib/ -lqcad
            INCLUDEPATH += $$PWD/../../../../qca/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-build
            DEPENDPATH += $$PWD/../../../../qca/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-build
        }
        CONFIG(release, debug|release){
            CONFIG -= console
            CONFIG += app_bundle
            LIBS += -L$$PWD/../../../../qca-release-build/lib/ -lqca
            INCLUDEPATH += $$PWD/../../../../qca-release/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-release-build
            DEPENDPATH += $$PWD/../../../../qca-release/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-release-build
        }
    }

    contains(QMAKE_TARGET.arch, x86){
        CONFIG(debug, debug|release){
            CONFIG   += console
            CONFIG   -= app_bundle
            LIBS += -L$$PWD/../../../../qca-build-x86/lib/ -lqcad
            INCLUDEPATH += $$PWD/../../../../qca/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-build-x86
            DEPENDPATH += $$PWD/../../../../qca/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-build-x86
        }
        CONFIG(release, debug|release){
            CONFIG -= console
            CONFIG += app_bundle
            LIBS += -L$$PWD/../../../../qca-release-build-x86/lib/ -lqca
            INCLUDEPATH += $$PWD/../../../../qca-release/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-release-build-x86
            DEPENDPATH += $$PWD/../../../../qca-release/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-release-build-x86
        }
    }
}

win32 {
    contains(QMAKE_TARGET.arch, x86){
        INCLUDEPATH += "C:/Program Files (x86)/Java/jdk1.7.0_67/include"
        INCLUDEPATH += "C:/Program Files (x86)/Java/jdk1.7.0_67/include/win32"
        DEPENDPATH += "C:/Program Files (x86)/Java/jdk1.7.0_67/include"
        DEPENDPATH += "C:/Program Files (x86)/Java/jdk1.7.0_67/include/win32"
        LIBS += -L"C:/Program Files (x86)/Java/jdk1.7.0_67/lib" -ljvm
        LIBS += -L"C:/Program Files (x86)/Java/jdk1.7.0_67/lib" -ljawt
    }
    contains(QMAKE_TARGET.arch, x86_64){
        INCLUDEPATH += "C:/Program Files/Java/jdk1.7.0_67/include"
        INCLUDEPATH += "C:/Program Files/Java/jdk1.7.0_67/include/win32"
        DEPENDPATH += "C:/Program Files/Java/jdk1.7.0_67/include"
        DEPENDPATH += "C:/Program Files/Java/jdk1.7.0_67/include/win32"
        LIBS += -L"C:/Program Files/Java/jdk1.7.0_67/lib" -ljvm
        LIBS += -L"C:/Program Files/Java/jdk1.7.0_67/lib" -ljawt
    }
}

INCLUDEPATH += "../ASLExecutable"
