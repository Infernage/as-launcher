#include "nativeaslloader.h"
#include <QStringList>
#include <QFile>
#include <QDir>
#include <iostream>
#include <QThread>
#include <QDebug>
#include <QtCrypto>
#include <QProcess>

#ifdef QT_DEBUG
#define JVMARGS 45
#else
#define JVMARGS 9
#endif

#ifdef Q_OS_WIN
#define SEPARATOR ";"
#else
#define SEPARATOR ":"
#endif

QStringList NativeASLLoader::files;

void NativeASLLoader::destroyFiles(){
    // Truncate bootstrap loader
    QFile f(bootstrapASLloader);
    f.open(QFile::WriteOnly);
    f.write(0, f.size());
    f.close();
    f.remove();

    // Truncate native loader
    QFile fi(nativeASLLoader);
    fi.open(QFile::WriteOnly);
    fi.write(0, fi.size());
    fi.close();
    fi.remove();
}

NativeASLLoader::NativeASLLoader()
{
    error = "";
}

NativeASLLoader::~NativeASLLoader()
{
    quit();
    QFile(bootstrapASLloader).remove();
    QFile(nativeASLLoader).remove();
    if (!QProcess::startDetached("ASLEC.clib", files)){
        foreach (QString file, files) {
            QFileInfo info(file);
            if (info.isDir() && info.exists()){
                QDir dir(file);
                dir.removeRecursively();
            } else if (info.isFile() && info.exists()){
                QFile fi(file);
                fi.remove();
            }
        }
    }
}

bool NativeASLLoader::load(QString workingDir)
{
    nativeASLLoader = workingDir + QDir::separator() + "ASLNativeLoader.jar";
    bootstrapASLloader = workingDir + QDir::separator() + "ASLBootstrapLoader.jar";

    // Read lib launcher...
    QFile clib(workingDir + QDir::separator() + QString("ASLR.clib"));
    if (!clib.open(QFile::ReadOnly)){
        error = "Failed to open lib resources.";
        return false;
    }
    QCA::SymmetricKey key(QCA::SecureArray("AntiSiriLoader"));
    QCA::InitializationVector iv(clib.read(16));
    QCA::Cipher cipher(QString("aes256"), QCA::Cipher::CBC, QCA::Cipher::DefaultPadding, QCA::Decode, key, iv);
    QByteArray data = cipher.update(QCA::SecureArray(clib.readAll())).toByteArray();
    data.append(cipher.final().toByteArray());
    clib.close();
    jsize size = (long) data.size();

    // Extract native loader
    QFile loader(":/ASLNativeLoader.jar");
    QFile output(nativeASLLoader);
    loader.open(QFile::ReadOnly);
    output.open(QFile::WriteOnly);
    output.write(loader.readAll());
    loader.close();
    output.close();

    // Extract bootstrap loader
    QFile bootstraploader(":/ASLBootstrapLoader.jar");
    QFile outputBootstrap(bootstrapASLloader);
    bootstraploader.open(QFile::ReadOnly);
    outputBootstrap.open(QFile::WriteOnly);
    outputBootstrap.write(bootstraploader.readAll());
    bootstraploader.close();
    outputBootstrap.close();

    // Init JVM
    // JVM args: http://stas-blogspot.blogspot.com.es/2011/07/most-complete-list-of-xx-options-for.html
    JavaVMInitArgs args;
    JavaVMOption options[JVMARGS];
    // Product options
    options[0].optionString = "-Djava.compiler=NONE";
    QString boot = QString("-Xbootclasspath/p:") + bootstrapASLloader;
    char *bootPath = new char[boot.size() + 1];
    strcpy(bootPath, boot.toStdString().c_str());
    options[1].optionString = bootPath;
    QString path = QString("-Djava.class.path=") + nativeASLLoader;
    char *javaPath = new char[path.size() + 1];
    strcpy(javaPath, path.toStdString().c_str());
    options[2].optionString = javaPath;
    QString lib = QString("-Djava.library.path=") + workingDir;
    char *libPath = new char[lib.size() + 1];
    strcpy(libPath,  lib.toStdString().c_str());
    options[3].optionString = libPath;
    options[4].optionString = "-Xms5m";
    options[5].optionString = "-Xmx100m";
    options[6].optionString = "-XX:+UseCompressedOops";
    options[7].optionString = "-XX:+UnlockExperimentalVMOptions";
    options[8].optionString = "-XX:+UseG1GC";

#ifdef QT_DEBUG
    // Debug options
    options[9].optionString = "-verbose:jni";
    options[10].optionString = "-XX:+PrintVMQWaitTime";
    options[11].optionString = "-XX:+PrintJNIResolving";
    options[12].optionString = "";//-XX:-UseCompilerSafepoints";
    options[13].optionString = "-XX:+ProfileVM";
    options[14].optionString = "-XX:+ProfileIntervals";
    options[15].optionString = "-XX:+AllowJNIEnvProxy";
    options[16].optionString = "-XX:+CheckJNICalls";
    options[17].optionString = "-Xdebug";
    options[18].optionString = "-XX:+PrintVMOptions";
    options[19].optionString = "-XX:+DisplayVMOutputToStdout";
    options[20].optionString = "-XX:+PerfAllowAtExitRegistration";
    options[21].optionString = "";
    options[22].optionString = "-XX:+DTraceMethodProbes";
    // Develop options
    options[23].optionString = "-XX:+DeoptimizeALot";
    options[24].optionString = "-XX:+Debugging";
    options[25].optionString = "-XX:+ShowSafepointMsgs";
    options[26].optionString = "";//"-XX:+SafepointTimeout";
    options[27].optionString = "-XX:+PrintMiscellaneous";
    options[28].optionString = "-XX:+WizardMode";
    options[29].optionString = "-XX:+TraceVMOperation";
    options[30].optionString = "-XX:+PrintDebugInfo";
    options[31].optionString = "-XX:+TraceJNICalls";
    options[32].optionString = "-XX:+TraceJNIHandleAllocation";
    options[33].optionString = "-XX:+TraceThreadEvents";
    options[34].optionString = "-XX:+TraceSafepoint";
    options[35].optionString = "-XX:+VerifyJNIFields";
    options[36].optionString = "-XX:+TraceDeoptimization";
    options[37].optionString = "-XX:+DebugDeoptimization";
    // Notproduct options
    options[38].optionString = "-XX:+TraceRuntimeCalls";
    options[39].optionString = "-XX:+TraceJVMCalls";
    options[40].optionString = "-XX:+VerifyJNIEnvThread";
    // Diagnostic options
    options[41].optionString = "-XX:+VerifyBeforeExit";
    options[42].optionString = "-XX:+DebugNonSafepoints";
    options[43].optionString = "-XX:+LogVMOutput";
    options[44].optionString = "-Xcheck:jni";
    //options[42].optionString = "-XX:SelfDestructTimer=2"; // <- Destroy JVM when X minutes has passed. (Product opt)
#endif
    args.version = JNI_VERSION_1_6;
    args.nOptions = JVMARGS;
    args.options = options;
    args.ignoreUnrecognized = JNI_TRUE;
    // Create JVM
    int res = JNI_CreateJavaVM(&vm, (void**)&env, &args);
    delete javaPath;
    delete bootPath;
    if (res != JNI_OK){
        destroyFiles();
        QString str("Failed to create JVM: ");
        error = str.append("ERROR ").append(QString::number(res));
        return false;
    }

    /*
     * Once the JVM is created, the current thread becomes the Java main thread and must not be killed or stopped
     * with or without reason. If this thread dies, the JVM too, leaving all resources and memory in an unbehavior
     * state.
     */

    // Init classloader
    jclass customClassLoaderClass = env->FindClass("org/infernage/aslauncher/core/loaders/NativeLoader");
    if (customClassLoaderClass == NULL){
        destroyFiles();
        error = "Failed to load native ClassLoader";
        return false;
    }
    jmethodID constructor = env->GetMethodID(customClassLoaderClass, "<init>", "([B)V");
    if (constructor == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        error = "Failed to construct native ClassLoader";
        return false;
    }
    jbyteArray byteData = env->NewByteArray(size);
    if (byteData == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        error = "Failed to read native ByteArray";
        return false;
    }
    env->SetByteArrayRegion(byteData, 0, size, reinterpret_cast<jbyte*>(data.data()));
    nativeClassLoader = env->NewObject(customClassLoaderClass, constructor, byteData);
    if (nativeClassLoader == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        error = "Failed to create native ClassLoader";
        return false;
    }
    loadClass = env->GetMethodID(customClassLoaderClass, "loadClass", "(Ljava/lang/String;)Ljava/lang/Class;");
    if (loadClass == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        error = "Failed to get loadClass method from NativeClassLoader";
        return false;
    }

    // Init JNI-JVM bootstrap
    /*
     * This method initializes all security policies and waits for the Java System exit call.
     * All cleanup and startup is done with the ShutdownVMThread
     */
    jstring bootstrapPath = env->NewStringUTF("org.infernage.jni.BootstrapVM");
    if (bootstrapPath == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        error = "Failed to allocate String from bootstrap";
        return false;
    }
    bootstrap = (jclass) env->CallObjectMethod(nativeClassLoader, loadClass, bootstrapPath);
    if (bootstrap == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        env->DeleteLocalRef(bootstrapPath);
        error = "Failed to load bootstrap class";
        return false;
    }
    jmethodID initMain = env->GetStaticMethodID(bootstrap, "initMain",
                                                "(ZLjava/lang/ClassLoader;Ljava/lang/String;[Ljava/lang/Object;)V");
    if (initMain == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        env->DeleteLocalRef(bootstrapPath);
        env->DeleteLocalRef(bootstrap);
        error = "Failed to get initializer method from bootstrap";
        return false;
    }
    jclass string = env->FindClass("java/lang/String");
    jclass object = env->FindClass("java/lang/Object");
    if (string == NULL || object == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        env->DeleteLocalRef(bootstrapPath);
        env->DeleteLocalRef(bootstrap);
        if (string != NULL) env->DeleteLocalRef(string);
        if (object != NULL) env->DeleteLocalRef(object);
        error = "Failed to get java lang classes";
        return false;
    }
    jobjectArray objArguments = env->NewObjectArray(1, object, NULL);
    jobjectArray arguments = env->NewObjectArray(0, string, NULL);
    if (objArguments == NULL || arguments == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        env->DeleteLocalRef(bootstrapPath);
        env->DeleteLocalRef(bootstrap);
        env->DeleteLocalRef(string);
        env->DeleteLocalRef(object);
        if (objArguments != NULL) env->DeleteLocalRef(objArguments);
        if (arguments != NULL) env->DeleteLocalRef(arguments);
        error = "Failed to create java arguments";
        return false;
    }
    env->SetObjectArrayElement(objArguments, 0, (jobject) arguments);
    jstring asloaderPath = env->NewStringUTF("org.infernage.aslauncher.core.loaders.ASLoader");
    if (asloaderPath == NULL){
        destroyFiles();
        env->DeleteLocalRef(customClassLoaderClass);
        env->DeleteLocalRef(byteData);
        env->DeleteLocalRef(nativeClassLoader);
        env->DeleteLocalRef(bootstrapPath);
        env->DeleteLocalRef(bootstrap);
        env->DeleteLocalRef(string);
        env->DeleteLocalRef(object);
        env->DeleteLocalRef(objArguments);
        env->DeleteLocalRef(arguments);
        error = "Failed to allocate path to the launcher";
        return false;
    }
    try {
        env->CallStaticVoidMethod(bootstrap, initMain, JNI_FALSE, nativeClassLoader, asloaderPath, objArguments);
    } catch (...) {
    }
    // Clean native references
    env->DeleteLocalRef(bootstrapPath);
    env->DeleteLocalRef(asloaderPath);
    env->DeleteLocalRef(string);
    env->DeleteLocalRef(arguments);
    env->DeleteLocalRef(objArguments);
    env->DeleteLocalRef(object);
    env->DeleteLocalRef(byteData);
    env->DeleteLocalRef(customClassLoaderClass);
    destroyFiles();
    QThread::msleep(50);
    return checkException();
}

int NativeASLLoader::quit()
{
    if (vm == NULL) return 0;
    int res = vm->DestroyJavaVM(); // Destroy JVM
    vm = NULL;
    env = NULL;
    return res;
}

bool NativeASLLoader::waitForTermination()
{
    if (vm == NULL) return true;
    // This method simply calls the Java waitForShuttingdown method in BootstrapVM.class
    // All cleanup is done in Java.
    jmethodID wait = env->GetStaticMethodID(bootstrap, "waitForShuttingdown", "()V");
    try {
        env->CallStaticVoidMethod(bootstrap, wait);
    } catch (...) {
    }
    QThread::msleep(50);
    return checkException();
}

QString NativeASLLoader::getErrorStr()
{
    return error;
}

void NativeASLLoader::addFileToRemove(QString path)
{
    files.append(path);
}

bool NativeASLLoader::checkException()
{
    bool success;
    if (env->ExceptionCheck() == JNI_TRUE){ // Exception happened, bad Java program initialization.
        jthrowable cause = env->ExceptionOccurred();
        env->ExceptionClear();
        jclass e = env->FindClass("java/lang/Exception");
        QString exc;
        if (e != NULL){
            jmethodID toString = env->GetMethodID(e, "toString", "()Ljava/lang/String;");
            if (toString != NULL){
                jstring str = (jstring) env->CallObjectMethod(cause, toString);
                if (str != NULL){
                    const char *chars = env->GetStringUTFChars(str, NULL);
                    exc.append(chars); // Get the exception message
                    std::cerr << chars << std::endl; // Only for debugging!
                    env->ReleaseStringUTFChars(str, chars);
                } else{
                    exc = "Exception catched and cleaned, but method toString returned null";
                }
                env->DeleteLocalRef(str);
            } else{
                exc = "Exception catched and cleaned, but no toString method found";
            }
            env->DeleteLocalRef(e);
        } else{
            exc.append("java/lang/Exception not found!");
        }
        env->DeleteLocalRef(cause);
        env->DeleteLocalRef(bootstrap);
        env->DeleteLocalRef(nativeClassLoader); // Clean native loader reference
        quit(); // This method will return false, so we destroy the JVM here.
        error = QString("Exception during JVM Bootstrap: ").append(exc);
        success = false;
    } else{
        success = true;
    }
    return success;
}


Loader *createLoader()
{
    return dynamic_cast<Loader*>(new NativeASLLoader);
}


char *getVersion()
{
    return "0.6.0";
}
