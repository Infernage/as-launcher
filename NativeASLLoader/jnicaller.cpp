#include "jnicaller.h"
#include "javaproxyclasses.h"
#include "nativeaslloader.h"
#include "xz/xz.h"
#include <QCoreApplication>
#include <QFile>
#include <QDir>
#include <QString>
#include <QFileInfo>
#include <QtCrypto>
#include <QByteArray>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QBuffer>
#include <QDebug>

jfieldID getHandleFieldID(JNIEnv *env, jobject obj)
{
    jclass cl = env->GetObjectClass(obj);
    return env->GetFieldID(cl, "nativeHandle", "J");
}

template<typename P>
P *getHandle(JNIEnv *env, jobject obj)
{
    return reinterpret_cast<P*>(env->GetLongField(obj, getHandleFieldID(env, obj)));
}

template<typename P>
void setHandle(JNIEnv *env, jobject obj, P *p)
{
    env->SetLongField(obj, getHandleFieldID(env, obj), reinterpret_cast<jlong>(p));
}

void Java_org_infernage_aslauncher_util_IO_delete(JNIEnv *env, jclass cls, jstring file)
{
    Q_UNUSED(cls)
    const char *rawfilepath = env->GetStringUTFChars(file, NULL);
    QString filePath(rawfilepath);
    QFileInfo info(filePath);
    if (info.isDir() && info.exists()){
        QDir dir(filePath);
        if (!dir.removeRecursively()){
            NativeASLLoader::addFileToRemove(filePath);
        }
    } else if (info.isFile() && info.exists()){
        QFile file(filePath);
        if (!file.remove()){
            NativeASLLoader::addFileToRemove(filePath);
        }
    }
    env->ReleaseStringUTFChars(file, rawfilepath);
}

bool copyRecursive(QString src, QString dst){
    QFileInfo info(src);
    if (info.isDir() && info.exists()){
        QDir dir(dst);
        dir.cdUp();
        if (!dir.mkdir(QFileInfo(dst).fileName())) return false;
        QDir srcDir(src);
        QStringList files = srcDir.entryList(QDir::Files | QDir::Dirs | QDir::NoDotAndDotDot | QDir::Hidden
                                             | QDir::System);
        foreach (QString file, files) {
            QString srcEntry = src + QDir::separator() + file;
            QString dstEntry = dst + QDir::separator() + file;
            if (!copyRecursive(srcEntry, dstEntry)) return false;
        }
    } else if (info.exists()){
        return QFile::copy(src, dst);
    }
    return false;
}

void Java_org_infernage_aslauncher_util_IO_copy(JNIEnv *env, jclass cls, jstring src, jstring dst)
{
    Q_UNUSED(cls)
    const char *rawsrc = env->GetStringUTFChars(src, NULL), *rawdst = env->GetStringUTFChars(dst, NULL);
    QString srcFile(rawsrc);
    QString dstFile(rawdst);
    copyRecursive(srcFile, dstFile);
    env->ReleaseStringUTFChars(src, rawsrc);
    env->ReleaseStringUTFChars(dst, rawdst);
}

void Java_org_infernage_aslauncher_util_IO_move(JNIEnv *env, jclass cls, jstring src, jstring dst)
{
    Q_UNUSED(cls)
    const char *rawsrc = env->GetStringUTFChars(src, NULL), *rawdst = env->GetStringUTFChars(dst, NULL);
    QDir dir;
    dir.rename(QString(rawsrc), QString(rawdst));
    env->ReleaseStringUTFChars(src, rawsrc);
    env->ReleaseStringUTFChars(dst, rawdst);
}

jboolean Java_org_infernage_aslauncher_util_IO_check(JNIEnv *env, jclass cls, jstring src, jstring dst)
{
    Q_UNUSED(cls)
    const char *rawsrc = env->GetStringUTFChars(src, NULL), *rawdst = env->GetStringUTFChars(dst, NULL);
    bool res = QFileInfo(QString(rawsrc)) == QFileInfo(QString(rawdst));
    env->ReleaseStringUTFChars(src, rawsrc);
    env->ReleaseStringUTFChars(dst, rawdst);
    return res ? JNI_TRUE : JNI_FALSE;
}


void Java_org_infernage_aslauncher_core_crypto_CAES_create(JNIEnv *env, jobject thisObj, jstring str)
{
    const char *type = env->GetStringUTFChars(str, NULL);
    AES *aes = new AES(type);
    setHandle(env, thisObj, aes);
    env->ReleaseStringUTFChars(str, type);
}

jstring Java_org_infernage_aslauncher_core_crypto_CAES_manipulateData(JNIEnv *env, jobject thisObj, jstring data,
                                                                      jint salt, jboolean isEncrypt)
{
    AES *aes = getHandle<AES>(env, thisObj);
    if (aes == NULL){
        jclass exc = env->FindClass("java/lang/NullPointerException");
        if (exc != NULL){
            env->ThrowNew(exc, "Handle is undefined");
        }
        return NULL;
    }
    QCA::Initializer init;
    Q_UNUSED(init)
    QString password = aes->getPassword() + QString::number(salt);
    QString passwordHash = QCA::Hash("sha256").hashToString(password.toUtf8());
    const char *cdata = env->GetStringUTFChars(data, NULL);
    QByteArray manipulated;
    try {
        manipulated = aes->manipulateData(passwordHash, cdata, isEncrypt == JNI_TRUE ? true : false, salt);
    } catch (QString e) {
        jclass exc = env->FindClass("java/lang/Exception");
        if (exc != NULL){
            env->ThrowNew(exc, e.toStdString().c_str());
        }
        return NULL;
    }
    char *d = new char[manipulated.size()];
    strcpy(d, manipulated.constData());
    env->ReleaseStringUTFChars(data, cdata);
    return env->NewStringUTF(d);
}

jstring Java_org_infernage_aslauncher_core_crypto_CAES_manipulateFile(JNIEnv *env, jobject thisObj, jstring src,
                                                                      jstring dst, jint salt, jboolean isEncrypt)
{
    AES *aes = getHandle<AES>(env, thisObj);
    if (aes == NULL){
        jclass exc = env->FindClass("java/lang/NullPointerException");
        if (exc != NULL){
            env->ThrowNew(exc, "Handle is undefined");
        }
        return NULL;
    }
    QCA::Initializer init;
    Q_UNUSED(init)
    QString password = aes->getPassword() + QString::number(salt);
    QString passwordHash = QCA::Hash("sha256").hashToString(password.toUtf8());
    const char *srcFile = env->GetStringUTFChars(src, NULL);
    const char *dstFile = env->GetStringUTFChars(dst, NULL);
    const char *resFile = NULL;
    try {
        resFile = aes->manipulateFile(passwordHash, srcFile, dstFile, isEncrypt == JNI_TRUE ? true : false, salt);
    } catch (QString e) {
        jclass exc = env->FindClass("java/lang/Exception");
        if (exc != NULL){
            env->ThrowNew(exc, e.toStdString().c_str());
        }
        return NULL;
    }
    env->ReleaseStringUTFChars(src, srcFile);
    env->ReleaseStringUTFChars(dst, dstFile);
    jstring res = env->NewStringUTF(resFile);
    return res;
}

void Java_org_infernage_aslauncher_core_crypto_CAES_setPassword(JNIEnv *env, jobject thisObj, jstring password)
{
    AES *aes = getHandle<AES>(env, thisObj);
    if (aes == NULL){
        jclass exc = env->FindClass("java/lang/NullPointerException");
        if (exc != NULL){
            env->ThrowNew(exc, "Handle is undefined");
        }
        return;
    }
    const char *pass = env->GetStringUTFChars(password, NULL);
    aes->setPassword(pass);
    env->ReleaseStringUTFChars(password, pass);
}

jbyteArray Java_org_infernage_aslauncher_core_crypto_CAES_getPassword(JNIEnv *env, jobject thisObj)
{
    AES *aes = getHandle<AES>(env, thisObj);
    if (aes == NULL){
        jclass exc = env->FindClass("java/lang/NullPointerException");
        if (exc != NULL){
            env->ThrowNew(exc, "Handle is undefined");
        }
        return NULL;
    }
    QString password = aes->getPassword();
    jbyteArray jpassword = env->NewByteArray(password.toUtf8().size());
    env->SetByteArrayRegion(jpassword, 0, password.toUtf8().size(), (const jbyte*) password.toUtf8().constData());
    return jpassword;
}

jint Java_org_infernage_aslauncher_core_crypto_CAES_getSalt__Ljava_lang_String_2(JNIEnv *env, jobject thisObj,
                                                                                 jstring data)
{
    Q_UNUSED(thisObj)
    const char *cdata = env->GetStringUTFChars(data, NULL);
    QByteArray ba = QByteArray::fromHex(QByteArray(cdata));
    QDataStream str(&ba, QIODevice::ReadOnly);
    int salt;
    str >> salt;
    env->ReleaseStringUTFChars(data, cdata);
    return salt;
}

jint Java_org_infernage_aslauncher_core_crypto_CAES_getSalt__Ljava_nio_file_Path_2(JNIEnv *env, jobject thisObj,
                                                                                   jobject file)
{
    Q_UNUSED(thisObj)
    jclass exc = env->FindClass("java/lang/Exception");
    if (exc == NULL) return 0;
    jclass pathCls = env->GetObjectClass(file);
    if (pathCls == NULL){
        env->ThrowNew(exc, "Failed to get class from object");
        return 0;
    }
    jmethodID toString = env->GetMethodID(pathCls, "toString", "()Ljava/lang/String;");
    if (toString == NULL){
        env->ThrowNew(exc, "Failed to get method toString from Path class");
        return 0;
    }
    jstring path = (jstring) env->CallObjectMethod(file, toString);
    if (path == NULL){
        env->ThrowNew(exc, "Failed to get path from file");
        return 0;
    }
    const char *pathChar = env->GetStringUTFChars(path, NULL);
    QFile src;
    src.setFileName((QString(pathChar)));
    if (!src.open(QFile::ReadOnly)){
        env->ReleaseStringUTFChars(path, pathChar);
        env->DeleteLocalRef(path);
        env->ThrowNew(exc, "Failed to open file");
        return 0;
    }
    QDataStream str(&src);
    str.skipRawData(16);
    int salt;
    str >> salt;
    src.close();
    env->ReleaseStringUTFChars(path, pathChar);
    env->DeleteLocalRef(path);
    return salt;
}

void Java_org_infernage_aslauncher_core_crypto_CAES_closeHandle(JNIEnv *env, jobject thisObj)
{
    AES *aes = getHandle<AES>(env, thisObj);
    setHandle<int>(env, thisObj, NULL);
    delete aes;
}


jstring Java_org_infernage_aslauncher_core_CoreConfiguration_getCurrentApplicationPath(JNIEnv *env, jclass cls)
{
    Q_UNUSED(cls)
    QString app = QCoreApplication::applicationFilePath();
    char* appPath = new char[app.toUtf8().size()];
    strcpy(appPath, app.toStdString().c_str());
    return env->NewStringUTF(appPath);
}


jobject Java_org_infernage_aslauncher_mcsystem_forge_ForgeList_fetchForgeList(JNIEnv *env, jclass cls, jstring json)
{
    Q_UNUSED(cls)
    jclass exc = env->FindClass("java/lang/Exception");
    if (!exc){
        return NULL;
    }
    jclass remoteForgeCls = env->FindClass("org/infernage/aslauncher/mcsystem/forge/RemoteMinecraftForge");
    if (!remoteForgeCls){
        env->ThrowNew(exc, "Failed to find RemoteForge class");
        return NULL;
    }
    jmethodID remoteForgeConstr = env->GetMethodID(remoteForgeCls, "<init>",
                                                   "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    if (!remoteForgeConstr){
        env->ThrowNew(exc, "Failed to get RemoteForge constructor");
        return NULL;
    }
    jclass arrayListCls = env->FindClass("java/util/ArrayList");
    if (!arrayListCls){
        env->ThrowNew(exc, "Failed to find ArrayList class");
        return NULL;
    }
    jmethodID arrayListConstr = env->GetMethodID(arrayListCls, "<init>", "()V");
    if (!arrayListConstr){
        env->ThrowNew(exc, "Failed to get ArrayList constructor");
        return NULL;
    }
    jmethodID add = env->GetMethodID(arrayListCls, "add", "(Ljava/lang/Object;)Z");
    if (!add){
        env->ThrowNew(exc, "Failed to get add method from ArrayList class");
        return NULL;
    }
    jobject arrayList = env->NewObject(arrayListCls, arrayListConstr);
    if (!arrayList){
        env->ThrowNew(exc, "Failed to create ArrayList object");
        return NULL;
    }
    const char* cjson = env->GetStringUTFChars(json, NULL);
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray(cjson));
    if (!doc.isObject()){
        env->ReleaseStringUTFChars(json, cjson);
        env->ThrowNew(exc, "Failed to parse JSON document");
        return NULL;
    }
    QJsonObject root = doc.object();
    if (!root.value("builds").isArray()){
        env->ReleaseStringUTFChars(json, cjson);
        env->ThrowNew(exc, "Failed to get builds from JSON object");
        return NULL;
    }
    QJsonArray builds = root.value("builds").toArray();
    for (int i = 0; i < builds.size(); ++i) {
        if (!builds[i].isObject()) continue;
        QJsonObject obj = builds[i].toObject();
        QJsonArray files = obj.value("files").toArray();
        for (int j = 0; j < files.size(); ++j) {
            if (!files[j].isObject()) continue;
            QJsonObject file = files[j].toObject();
            QString buildType = file.value("buildtype").toString();
            if (buildType == "universal" || buildType == "client"){
                QString v = obj.value("version").toString();
                QString mcv = file.value("mcver").toString();
                QString u = file.value("url").toString();
                char *ver = new char[v.toUtf8().size()];
                char *mcver = new char[mcv.toUtf8().size()];
                char *downloadUrl = new char[u.toUtf8().size()];
                strcpy(ver, v.toUtf8().constData());
                strcpy(mcver, mcv.toUtf8().constData());
                strcpy(downloadUrl, u.toUtf8().constData());
                jobject remoteForge = env->NewObject(remoteForgeCls, remoteForgeConstr, env->NewStringUTF(ver),
                                                     env->NewStringUTF(mcver), env->NewStringUTF(downloadUrl));
                if (remoteForge){
                    env->CallBooleanMethod(arrayList, add, remoteForge);
                }
                break;
            }
        }
    }
    env->ReleaseStringUTFChars(json, cjson);
    return arrayList;
}


jobject Java_org_infernage_aslauncher_mcsystem_forge_ForgeList_fetchMavenForgeList(JNIEnv *env, jclass cls, jstring json)
{
    Q_UNUSED(cls)
    jclass exc = env->FindClass("java/lang/Exception");
    if (!exc){
        return NULL;
    }
    jclass remoteForgeCls = env->FindClass("org/infernage/aslauncher/mcsystem/forge/RemoteMinecraftForge");
    if (!remoteForgeCls){
        env->ThrowNew(exc, "Failed to find RemoteForge class");
        return NULL;
    }
    jmethodID remoteForgeConstr = env->GetMethodID(remoteForgeCls, "<init>",
                                                   "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V");
    if (!remoteForgeConstr){
        env->ThrowNew(exc, "Failed to get RemoteForge constructor");
        return NULL;
    }
    jclass arrayListCls = env->FindClass("java/util/ArrayList");
    if (!arrayListCls){
        env->ThrowNew(exc, "Failed to find ArrayList class");
        return NULL;
    }
    jmethodID arrayListConstr = env->GetMethodID(arrayListCls, "<init>", "()V");
    if (!arrayListConstr){
        env->ThrowNew(exc, "Failed to get ArrayList constructor");
        return NULL;
    }
    jmethodID add = env->GetMethodID(arrayListCls, "add", "(Ljava/lang/Object;)Z");
    if (!add){
        env->ThrowNew(exc, "Failed to get add method from ArrayList class");
        return NULL;
    }
    jobject arrayList = env->NewObject(arrayListCls, arrayListConstr);
    if (!arrayList){
        env->ThrowNew(exc, "Failed to create ArrayList object");
        return NULL;
    }
    const char* cjson = env->GetStringUTFChars(json, NULL);
    QJsonDocument doc = QJsonDocument::fromJson(QByteArray(cjson));
    if (!doc.isObject()){
        env->ReleaseStringUTFChars(json, cjson);
        env->ThrowNew(exc, "Failed to parse JSON document");
        return NULL;
    }
    QJsonObject root = doc.object();
    QString webpath = root.value("webpath").toString();
    QString artifact = root.value("artifact").toString();
    QJsonObject num = root.value("number").toObject();
    foreach (QString key, num.keys()) {
        QJsonObject obj = num.value(key).toObject();
        QString branch = obj.value("branch").toString();
        QJsonArray files = obj.value("files").toArray();
        for (int i = 0; i < files.size(); ++i) {
            QJsonArray file = files.at(i).toArray();
            if (file.size() < 3) continue;
            QString type = file.at(1).toString();
            if (type == "universal"){
                QString version = obj.value("version").toString();
                QString mcversion = obj.value("mcversion").toString();
                QString versionPath = mcversion + QString("-") + version;
                if (!branch.isEmpty()){
                    versionPath += QString("-") + branch;
                }
                QString fileName = artifact + QString("-") + versionPath + QString("-") + type + QString(".")
                        + file.at(0).toString();
                QString url = webpath + QString("/") + versionPath + QString("/") + fileName;
                char *ver = new char[version.toUtf8().size()];
                char *mcver = new char[mcversion.toUtf8().size()];
                char *downloadUrl = new char[url.toUtf8().size()];
                strcpy(ver, version.toUtf8().constData());
                strcpy(mcver, mcversion.toUtf8().constData());
                strcpy(downloadUrl, url.toUtf8().constData());
                jobject remoteForge = env->NewObject(remoteForgeCls, remoteForgeConstr, env->NewStringUTF(ver),
                                                     env->NewStringUTF(mcver), env->NewStringUTF(downloadUrl));
                if (remoteForge){
                    env->CallBooleanMethod(arrayList, add, remoteForge);
                }
                break;
            }
        }
    }
    env->ReleaseStringUTFChars(json, cjson);
    return arrayList;
}


jbyteArray Java_org_infernage_aslauncher_core_concurrent_ForgeEngine_readFromNative(JNIEnv *env, jobject thisObj,
                                                                                    jstring file)
{
    jclass exc = env->FindClass("java/lang/Exception");
    const char *path = env->GetStringUTFChars(file, NULL);
    QString fPath(path);
    QFile f(fPath);
    if (!f.open(QFile::ReadOnly)){
        if (exc) env->ThrowNew(exc, "Failed to open XZ file");
        env->ReleaseStringUTFChars(file, path);
        return NULL;
    }
    QBuffer buf;
    buf.open(QIODevice::WriteOnly);
    uint8_t in[256 * 1024];
    uint8_t out[256 * 1024];
    xz_crc32_init();
    xz_crc64_init();
    struct xz_dec *s;
    struct xz_buf b;
    enum xz_ret ret;
    s = xz_dec_init(XZ_DYNALLOC, 1 << 26);
    if (s == nullptr){
        xz_dec_end(s);
        if (exc){
            env->ThrowNew(exc, "Failed to init XZ engine");
        }
        f.close();
        env->ReleaseStringUTFChars(file, path);
        return NULL;
    }
    b.in = in;
    b.in_pos = 0;
    b.in_size = 0;
    b.out = out;
    b.out_pos = 0;
    b.out_size = 256 * 1024;
    bool success = false;
    while (!success){
        if (b.in_pos == b.in_size){
            b.in_size = f.read((char*)in, sizeof(in));
            b.in_pos = 0;
        }
        ret = xz_dec_run(s, &b);
        if (b.out_pos == sizeof(out)){
            if (buf.write((char*)out, b.out_pos) != b.out_pos){
                xz_dec_end(s);
                if (exc) env->ThrowNew(exc, "Failed: XZ write error");
                f.close();
                env->ReleaseStringUTFChars(file, path);
                return NULL;
            }
            b.out_pos = 0;
        }
        if (ret == XZ_OK || ret == XZ_UNSUPPORTED_CHECK) continue;
        if (buf.write((char*)out, b.out_pos) != b.out_pos){
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Failed: XZ write error");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        }
        switch (ret) {
        case XZ_STREAM_END:
            xz_dec_end(s);
            success = true;
            break;
        case XZ_MEM_ERROR:
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Failed to allocate memory for XZ engine");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        case XZ_MEMLIMIT_ERROR:
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Failed: Memory limit reached");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        case XZ_FORMAT_ERROR:
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Format error. Not a XZ file");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        case XZ_OPTIONS_ERROR:
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Unsupported options in XZ header");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        case XZ_DATA_ERROR:
        case XZ_BUF_ERROR:
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Failed: File corrupted");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        default:
            xz_dec_end(s);
            if (exc) env->ThrowNew(exc, "Failed: Unknown error");
            f.close();
            env->ReleaseStringUTFChars(file, path);
            return NULL;
        }
    }
    QByteArray data = buf.buffer();
    jbyteArray jdata = env->NewByteArray(data.size());
    env->SetByteArrayRegion(jdata, 0, data.size(), (const jbyte*) data.constData());
    return jdata;
}
