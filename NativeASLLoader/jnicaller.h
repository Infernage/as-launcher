#include <jni.h>

#ifndef JNICALLER_H
#define JNICALLER_H
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Signatures:
 * boolean = Z
 * byte = B
 * char = C
 * short = S
 * int = I
 * long = J
 * float = F
 * double = D
 * void = V
 * any class = Lfullclassname;
 * array = [arraytype
 */

// Put methods from Java native class here. Skeleton:
/*
 * JNIEXPORT {returntype} JNICALL Java_{package}_{classname}_{methodname}{if is an overloaded function add:"__{parameter
 * signatures_without_dots}"}(JNIEnv *env, {if static: "jclass cl", if non static: "jobject this_obj"}, {parameters});
 */

 /* Hello world example:
 * Class: JNICaller
 * Method: sayHello
 * Signature: (Ljava/lang/String;)V
 * JNIEXPORT void JNICALL Java_org_infernage_aslauncher_core_JNICaller_sayHello(JNIEnv *env, jclass cl, jstring msg);
 */

// IO.class
JNIEXPORT void JNICALL Java_org_infernage_aslauncher_util_IO_delete(JNIEnv *env, jclass cls, jstring file);

JNIEXPORT void JNICALL Java_org_infernage_aslauncher_util_IO_copy(JNIEnv *env, jclass cls, jstring src, jstring dst);

JNIEXPORT void JNICALL Java_org_infernage_aslauncher_util_IO_move(JNIEnv *env, jclass cls, jstring src, jstring dst);

JNIEXPORT jboolean JNICALL Java_org_infernage_aslauncher_util_IO_check(JNIEnv *env, jclass cls, jstring src, jstring dst);

//Crypto package
JNIEXPORT void JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_create(JNIEnv *env, jobject thisObj, jstring str);

JNIEXPORT jstring JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_manipulateData(JNIEnv *env, jobject thisObj,
                                                                                        jstring data, jint salt,
                                                                                        jboolean isEncrypt);

JNIEXPORT jstring JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_manipulateFile(JNIEnv *env, jobject thisObj,
                                                                                        jstring src, jstring dst,
                                                                                        jint salt, jboolean isEncrypt);

JNIEXPORT void JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_setPassword(JNIEnv *env, jobject thisObj,
                                                                                  jstring password);

JNIEXPORT jbyteArray JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_getPassword(JNIEnv *env, jobject thisObj);

JNIEXPORT jint JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_getSalt__Ljava_lang_String_2(JNIEnv *env,
                                                                                                   jobject thisObj,
                                                                                                   jstring data);

JNIEXPORT jint JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_getSalt__Ljava_nio_file_Path_2(JNIEnv *env,
                                                                                                     jobject thisObj,
                                                                                                     jobject file);

JNIEXPORT void JNICALL Java_org_infernage_aslauncher_core_crypto_CAES_closeHandle(JNIEnv *env, jobject thisObj);

//CoreConfiguration.class
JNIEXPORT jstring JNICALL Java_org_infernage_aslauncher_core_CoreConfiguration_getCurrentApplicationPath(JNIEnv *env,
                                                                                                         jclass cls);

//ForgeList.class
JNIEXPORT jobject JNICALL Java_org_infernage_aslauncher_mcsystem_forge_ForgeList_fetchForgeList(JNIEnv *env, jclass cls,
                                                                                                jstring json);

JNIEXPORT jobject JNICALL Java_org_infernage_aslauncher_mcsystem_forge_ForgeList_fetchMavenForgeList(JNIEnv *env,
                                                                                                     jclass cls,
                                                                                                     jstring json);

//ForgeEngine.class
JNIEXPORT jbyteArray JNICALL Java_org_infernage_aslauncher_core_concurrent_ForgeEngine_readFromNative(JNIEnv *env,
                                                                                                      jobject thisObj,
                                                                                                      jstring file);

#ifdef __cplusplus
}
#endif

// Helper methods
jfieldID getHandleFieldID(JNIEnv *env, jobject obj);

template<typename P>
P *getHandle(JNIEnv *env, jobject obj);

template<typename P>
void setHandle(JNIEnv *env, jobject obj, P *p);

#endif // JNICALLER_H
