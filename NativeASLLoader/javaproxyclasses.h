#ifndef JAVAPROXYCLASSES_H
#define JAVAPROXYCLASSES_H

#include <jni.h>
#include <QObject>
#include <QtCrypto>

class AES : public QObject
{
public:
    AES(const char *type);
    void setPassword(const char *password);
    QString getPassword();
    QByteArray manipulateData(QString passwordHash, const char *data, bool isEncrypt, int salt);
    const char *manipulateFile(QString passwordHash, const char *srcPath, const char *dstPath, bool isEncrypt, int salt);
private:
    QString password;
    QString type;
};

#endif // JAVAPROXYCLASSES_H
