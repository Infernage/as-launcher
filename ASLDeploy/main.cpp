#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#ifdef WIN32
#include <windows.h>
#pragma comment(lib,"user32.lib")
#include <tchar.h>
#include <strsafe.h>
#elif defined(linux)
#include <dirent.h>
#elif defined(mac)
#include <dirent.h>
#endif
using namespace std;

#define IDXSIZE 102400

/*!
 * \brief Checks if the executable is ready to deploy.
 * \return {\code true} or {\code false}
 */
bool isReadyToDeploy(){
    char exePath[MAX_PATH];
#ifdef WIN32
    GetModuleFileNameA(NULL, exePath, MAX_PATH);
#elif defined(linux)
    char id[MAX_PATH];
    sprintf(id, "/proc/%d/exe", getpid());
    exePath[readlink(id, exePath, MAX_PATH-1)] = '\0';
#elif defined(mac)
    uint32_t size = sizeof(exePath);
    _NSGetExecutablePath(exePath, &size);
#endif
    ifstream input(exePath, ios::binary);
    if (input.fail()){
        exit(-1);
    }
    input.seekg(0, ios::end);
    streamoff mark = input.tellg();
    mark -= IDXSIZE;
    input.seekg(mark);
    char *idx = new char[5];
    input.read(idx, 5);
    input.close();
    if (!strncmp(idx, "*IDX*", 5)){
        delete idx;
        return true;
    } else{
        delete idx;
        return false;
    }
}

void prepareRecursive(ofstream &output, string &index, string &rootdir, string &currentdir){
#ifdef WIN32
    HANDLE dir;
    WIN32_FIND_DATAA data;
    if ((dir = FindFirstFileA((currentdir + "\\*").c_str(), &data)) == INVALID_HANDLE_VALUE) return;
    do{
        string fileName = data.cFileName;
        string fullPath = currentdir + "\\" + fileName;
        if (fileName[0] == '.' || fileName.substr(0, fileName.find_last_of('.')) == "ASLDeploy" ||
                fileName == "ASL") continue;
        if ((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0){
            prepareRecursive(output, index, rootdir, fullPath);
            continue;
        }
        ifstream in(fullPath, ios::binary);
        if (in.fail()){
            cout << fileName << " fail!" << endl;
        }
        fullPath.replace(fullPath.find(rootdir + "\\"), rootdir.length() + 1, "");
        in.seekg(0, ios::end);
        streamoff size = in.tellg();
        in.seekg(0, ios::beg);
        cout << "ADDING " << fullPath << endl;
        char *binData = new char[size];
        in.read(binData, size);
        output.write(binData, size);
        output.flush();
        if (output.fail()){
            cerr << "FAIL" << endl;
        }
        in.close();
        delete binData;
        index.append(fullPath).append("\t").append(to_string(size)).append("\n");
    } while(FindNextFileA(dir, &data));
    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;
    dir = opendir(currentdir);
    while ((ent = readdir(dir)) != NULL) {
        string file_name = ent->d_name;
        string full_file_name = directory + "/" + file_name;
        if (file_name[0] == '.' || stat(full_file_name.c_str(), &st) == -1) continue;
        if ((st.st_mode & S_IFDIR) != 0){
            deployRecursive(output, index, rootdir, full_file_name);
            continue;
        }
        ifstream in(full_file_name, ios::binary);
        if (in.fail()){
            cout << file_name << " fail!" << endl;
            continue;
        }
        full_file_name.replace(full_file_name.find(rootdir + "/"), rootdir.length() + 1, "");
        in.seekg(0, ios::end);
        unsigned long size = in.tellg();
        in.seekg(0, ios::beg);
        cout << "ADDING " << full_file_name << endl;
        char *binData = new char[size];
        in.read(binData, size);
        output.write(binData, size);
        output.flush();
        if (output.fail()){
            cerr << "FAIL" << endl;
            exit(1);
        }
        in.close();
        delete binData;
        index.append(file_name).append("\t").append(to_string(size)).append("\n");
    }
    closedir(dir);
#endif
}

/*!
 * \brief Deploys all files attached to the working folder.
 */
void deploy(int argc, char *argv[]){
    char exePath[MAX_PATH];
    string workingDir;
    // Get the current executable path and the working folder. Also prepare the environment to load libraries.
#ifdef WIN32
    GetModuleFileNameA(NULL, exePath, MAX_PATH);
    workingDir = string(getenv("APPDATA")) + "\\ASL";
    CreateDirectoryA(workingDir.c_str(), NULL);
    workingDir.append("\\");
#elif defined(linux)
    char id[MAX_PATH];
    sprintf(id, "/proc/%d/exe", getpid());
    exePath[readlink(id, exePath, MAX_PATH-1)] = '\0';
    workingDir = getenv("HOME") + "/.ASL";
    mkdir(workingDir.c_str(), 0777);
    workingDir.append("/");
#elif defined(mac)
    char exePath[MAX_PATH];
    uint32_t size = sizeof(exePath);
    _NSGetExecutablePath(exePath, &size);
    workingDir = getenv("HOME") + "/Library/Application Support/.ASL";
    mkdir(workingDir.c_str(), 0777);
    workingDir.append("/");
#endif
    // Start to reads itself.
    ifstream src(exePath, ios::binary);
    if (src.fail()){
        exit(-5);
    }
    src.seekg(0, ios::end);
    streamoff indexSeek = src.tellg();
    indexSeek -= IDXSIZE; // Substract 100KB, the index size.
    src.seekg(indexSeek);
    char *index = new char[IDXSIZE];
    src.read(index, IDXSIZE); // File pointer position: EOF
    string idx((char*)(index + strlen("*IDX*\n"))); // Get the stored index removing the first *IDX*\n string
    delete index;
    unsigned long origSize = atol(idx.substr(0, idx.find("\n")).c_str()); // Get the original size of the executable
    src.seekg(origSize); // File pointer position: EOExecutableCode
    idx.erase(0, idx.find("\n") + 1); // Removes the original size from the index
    // Read index to extract all files. The stream is seeked to the original size of the executable.
    while(true){
        if (!idx.compare(idx.substr(0, strlen("*EIDX*")).c_str())) break; // EOIndex
        size_t len;
        string filename = idx.substr(0, (len = idx.find("\t")));
        idx.erase(0, len + 1);
        unsigned long fSize = atol(idx.substr(0, (len = idx.find("\n"))).c_str());
        idx.erase(0, len + 1);
        if (!ifstream(workingDir + filename)){ // If the file NOT exists.
            // Check if the index file is prepended with a directory. In that case, create it.
            cout << "Deploying " << filename << endl;
#ifdef WIN32
            if (filename.find("\\") != string::npos){
                string d(workingDir);
                d.append(filename.substr(0, filename.find_last_of("\\")));
                CreateDirectoryA(d.c_str(), NULL);
            }
#else
            if (filename.find("/") != string.npos){
                string d(workingDir);
                d.append(filename.substr(0, filename.find_last_of("/")));
                mkdir(d.c_str(), 0777);
            }
#endif
            // Extract the file to the working folder.
            ofstream output((workingDir + filename).c_str(), ios::binary);
            if (output.fail()){
                cerr << filename << " fail!" << endl;
                continue;
            }
            char *file = new char[fSize];
            src.read(file, fSize);
            output.write(file, fSize);
            output.flush();
            output.close();
            delete file;
        }
    }
    src.close();
    cout << "Changing exe image..." << endl;
    // All files are deployed, time to execute ASL
#ifdef WIN32
    STARTUPINFOA si;
    PROCESS_INFORMATION pi;
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);
    ZeroMemory(&pi, sizeof(pi));
    if (!CreateProcessA((workingDir + "ASL").c_str(), exePath, NULL, NULL, FALSE, NORMAL_PRIORITY_CLASS,
                       NULL, workingDir.substr(0, workingDir.length() - 1).c_str(), &si, &pi)){
        cerr << "Failed to change exe image!" << endl;
        cerr << "ERROR " << GetLastError() << endl;
        system("pause");
        return;
    }
    cout << "New exe image: PPID=" << pi.dwProcessId << ", MTPID=" << pi.dwThreadId;
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
#elif defined(linux) || defined(mac)
    pid_t pid;
    pid = fork();
    if (pid < 0){
        cerr << "Failed to change exe image!" << endl;
        cerr << "ERROR " << errno;
        system("pause");
        return;
    }
    if (pid != 0){
        return;
    }
    execl((workingDir + "ASL").c_str(), exePath);
#endif
}

/*!
 * \brief This is used only one time: At the executable creation.
 */
void prepareToDeploy(){
    cout << "Preparing binary to deploy phase..." << endl;
    string index("*IDX*\n");
    char exePath[MAX_PATH];
    // Get current executable path
#ifdef WIN32
    GetModuleFileNameA(NULL, exePath, MAX_PATH);
#elif defined(linux)
    char id[MAX_PATH];
    sprintf(id, "/proc/%d/exe", getpid());
    exePath[readlink(id, exePath, MAX_PATH-1)] = '\0';
#elif defined(mac)
    uint32_t size = sizeof(exePath);
    _NSGetExecutablePath(exePath, &size);
#endif
    // Get suffix from executable
    string suffix(exePath);
    suffix = suffix.substr(suffix.find_last_of("."), suffix.length());
    // Create the output file
    ofstream output("ASL" + suffix, ios::binary);
    if (output.fail()){
        exit(-1);
    }
    // Reads itself and stores it into the new executable
    ifstream input(exePath, ios::binary);
    if (input.fail()){
        exit(-1);
    }
    input.seekg(0, ios::end);
    streamoff size = input.tellg();
    cout << "ADDING " << exePath << endl;
    index.append(to_string(size)).append("\n");
    input.seekg(0, ios::beg);
    char *dataExe = new char[size];
    input.read(dataExe, size);
    output.write(dataExe, size);
    output.flush();
    input.close();
    delete dataExe;
    // Get the current folder
    string currentDir = string(exePath).substr(0, string(exePath).find_last_of("\\/"));
#ifdef WIN32
    // Get all files in the current folder, including the subfolders and its files.
    HANDLE dir;
    WIN32_FIND_DATAA data;
    if ((dir = FindFirstFileA((currentDir + "\\*").c_str(), &data)) == INVALID_HANDLE_VALUE){
        exit(-2);
    }
    do{
        string fileName = data.cFileName;
        string fullPath = currentDir + "\\" + fileName;
        if (fileName[0] == '.' || fileName.substr(0, fileName.find_last_of('.')) == "ASLDeploy" ||
                fileName == "ASL" + suffix) continue;
        if ((data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0){
            prepareRecursive(output, index, currentDir, fullPath);
            continue;
        }
        ifstream in(fullPath, ios::binary);
        if (in.fail()){
            cout << fileName << " fail!" << endl;
            continue;
        }
        in.seekg(0, ios::end);
        size = in.tellg();
        in.seekg(0, ios::beg);
        cout << "ADDING " << fileName << endl;
        char *binData = new char[size];
        in.read(binData, size);
        output.write(binData, size);
        output.flush();
        if (output.fail()){
            cerr << "FAIL" << endl;
            exit(1);
        }
        in.close();
        delete binData;
        index.append(fileName).append("\t").append(to_string(size)).append("\n");
    } while(FindNextFileA(dir, &data));
    FindClose(dir);
#else
    DIR *dir;
    class dirent *ent;
    class stat st;

    dir = opendir(currentDir);
    while ((ent = readdir(dir)) != NULL) {
        string file_name = ent->d_name;
        string full_file_name = directory + "/" + file_name;

        if (file_name[0] == '.' || stat(full_file_name.c_str(), &st) == -1) continue;

        if ((st.st_mode & S_IFDIR) != 0){
            prepareRecursive(output, index, currentDir, full_file_name);
            continue;
        }

        ifstream in(full_file_name, ios::binary);
        if (in.fail()){
            cout << file_name << " fail!" << endl;
            continue;
        }
        in.seekg(0, ios::end);
        size = in.tellg();
        in.seekg(0, ios::beg);
        cout << "ADDING " << file_name << endl;
        char *binData = new char[size];
        in.read(binData, size);
        output.write(binData, size);
        output.flush();
        if (output.fail()){
            cerr << "FAIL" << endl;
            exit(1);
        }
        in.close();
        delete binData;
        index.append(file_name).append("\t").append(to_string(size)).append("\n");
    }
    closedir(dir);
#endif
    // Once all files has been readed and stored, resize the index to 100KB and store into the new executable at the end
    index.append("*EIDX*");
    index.resize(IDXSIZE);
    output.write(index.c_str(), index.length());
    output.flush();
    output.close(); // All right, close it.
    cout << "Finished";
}

int main(int argc, char *argv[])
{
    // Checks if the executable is ready to deploy all libs or not.
    if (isReadyToDeploy()){
        deploy(argc, argv);
    } else{
        prepareToDeploy();
    }
}
