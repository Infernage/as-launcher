TEMPLATE = app
CONFIG -= qt
CONFIG(debug, debug|release){
    CONFIG += console
    CONFIG -= app_bundle
}
CONFIG(release, debug|release){
    CONFIG += console
    CONFIG -= app_bundle
}
RC_ICONS = MineCraft.ico

SOURCES += main.cpp
