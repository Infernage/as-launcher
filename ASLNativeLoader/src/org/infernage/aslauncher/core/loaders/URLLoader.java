package org.infernage.aslauncher.core.loaders;

import java.net.URLStreamHandler;
import java.net.URLStreamHandlerFactory;
import java.util.HashMap;
import java.util.Map;
import org.infernage.aslauncher.core.loaders.asl.Handler;

/**
 * @author Infernage
 */
public class URLLoader implements URLStreamHandlerFactory{
    private final Map<String, URLStreamHandler> handlers = new HashMap<>();
    
    public URLLoader(Map<String, byte[]> resources){
        handlers.put("asl", new Handler(resources));
    }

    @Override
    public URLStreamHandler createURLStreamHandler(String protocol) {
        if (handlers.containsKey(protocol)) return handlers.get(protocol);
        return null;
    }
    
}
