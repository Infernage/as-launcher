package org.infernage.aslauncher.core.loaders.asl;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;
import java.util.Map;

/**
 * @author Infernage
 */
public class Handler extends URLStreamHandler{
    private final Map<String, byte[]> resources;

    public Handler(Map<String, byte[]> r) {
        resources = r;
    }

    @Override
    protected URLConnection openConnection(URL u) throws IOException {
        return new ASLConnection(u, resources.get(u.getPath()));
    }
    
}
