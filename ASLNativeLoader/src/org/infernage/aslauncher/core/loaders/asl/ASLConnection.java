package org.infernage.aslauncher.core.loaders.asl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * @author Infernage
 */
public class ASLConnection extends URLConnection{
    private final byte[] content;
    
    public ASLConnection(URL u, byte[] data){
        super(u);
        content = data;
    }

    @Override
    public long getContentLengthLong() {
        return content.length;
    }

    @Override
    public InputStream getInputStream() throws IOException {
        return new ByteArrayInputStream(content);
    }

    @Override
    public void connect() throws IOException {
        // Nothing
    }
    
}
