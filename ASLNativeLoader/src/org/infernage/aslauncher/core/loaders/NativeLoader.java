package org.infernage.aslauncher.core.loaders;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

/**
 * @author Infernage
 */
public class NativeLoader extends ClassLoader {

    private final Map<String, byte[]> classes;
    private final Map<String, byte[]> resources;
    private final Map<String, Class<?>> definedClasses;

    public NativeLoader(byte[] data) throws IOException {
        super(NativeLoader.class.getClassLoader());
        Map<String, byte[]> c = new HashMap<>(), r = new HashMap<>();
        try (JarInputStream input = new JarInputStream(new ByteArrayInputStream(data))) {
            JarEntry entry;
            while ((entry = input.getNextJarEntry()) != null) {
                String name = entry.getName();
                if (name.endsWith(".class")) {
                    byte[] classBytes = readClass(input);
                    String canonicalName = name.replaceAll("/", "\\.")
                            .replaceAll(".class", "");
                    c.put(canonicalName, classBytes);
                } else if (!name.startsWith("META-INF")) {
                    r.put(name, readClass(input));
                }
            }
        }
        definedClasses = new HashMap<>();
        classes = Collections.unmodifiableMap(c);
        resources = Collections.unmodifiableMap(r);
        URL.setURLStreamHandlerFactory(new URLLoader(resources));
        Thread.currentThread().setContextClassLoader(this);
    }

    private byte[] readClass(InputStream input) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int read;
        byte[] buffer = new byte[1024 * 256];
        while ((read = input.read(buffer)) != -1) {
            baos.write(buffer, 0, read);
        }
        return baos.toByteArray();
    }

    @Override
    public Class loadClass(String name) throws ClassNotFoundException {
        try {
            return this.getParent().loadClass(name);
        } catch (Exception e) {
            return findClass(name);
        }
    }

    @Override
    public Class<?> findClass(String name) throws ClassNotFoundException {
        if (!definedClasses.containsKey(name)) {
            byte[] classBytes = classes.get(name);
            Class<?> c = defineClass(name, classBytes, 0, classBytes.length);
            definedClasses.put(name, c);
            return c;
        } else{
            return definedClasses.get(name);
        }
    }

    @Override
    public Enumeration<URL> getResources(String name) throws IOException {
        List<String> list = new ArrayList<>();
        if (name.startsWith("classes:")) {
            for (String string : classes.keySet()) {
                if (string.startsWith(name) && !string.endsWith("/")) {
                    list.add(string);
                }
            }
        } else if (name.startsWith("resources:")) {
            for (String string : resources.keySet()) {
                if (string.startsWith(name)) {
                    list.add(string);
                }
            }
        }
        return list.isEmpty() ? super.getResources(name) : new Enum(list);
    }

    @Override
    protected URL findResource(String name) {
        try {
            return new URL("asl:" + name);
        } catch (MalformedURLException ex) {
            throw new RuntimeException(ex);
        }
    }

    private class Enum implements Enumeration<URL> {

        private List<String> l = new ArrayList<>();

        public Enum(List<String> list) {
            l = list;
        }

        @Override
        public boolean hasMoreElements() {
            return !l.isEmpty();
        }

        @Override
        public URL nextElement() {
            if (l.isEmpty()) {
                return null;
            }
            String s = l.remove(0);
            try {
                return new URL("asl:" + s);
            } catch (MalformedURLException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
