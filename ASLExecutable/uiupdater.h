#pragma once

#ifndef UIUPDATER_H
#define UIUPDATER_H

#include <QWidget>
#include <QResizeEvent>
#include <QMessageBox>
#include "notify.h"

namespace Ui {
class UIUpdater;
}

class UIUpdater : public QWidget
{
    Q_OBJECT

public:
    explicit UIUpdater(QWidget *parent = 0);
    ~UIUpdater();

public slots:
    void percentage(quint64 rec, quint64 total);
    void print(const char *msg);
    void print(QString msg);
    void showMessage(const char *title, const char *msg, Notifier *n);
    void showAskMessage(const char *title, const char *msg, Notifier *n);
    void forceClose();

protected:
    void resizeEvent(QResizeEvent *evt);
    void closeEvent(QCloseEvent *evt);

private:
    Ui::UIUpdater *ui;
    bool canClose;
};

#endif // UIUPDATER_H
