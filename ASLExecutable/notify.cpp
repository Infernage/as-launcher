#include "notify.h"

Notifier::Notifier()
{
    finish = false;
    button = QMessageBox::NoButton;
}

Notifier::~Notifier()
{
    close();
}

void Notifier::showMessageBox(const char *title, const char *msg)
{
    finish = false;
    emit msgBox(title, msg, this);
    wait();
}

QMessageBox::StandardButton Notifier::showMessageAsk(const char *title, const char *msg)
{
    finish = false;
    emit askUserBox(title, msg, this);
    wait();
    return button;
}

void Notifier::print(const char *msg){
    emit write(msg);
}

void Notifier::print(QString msg)
{
    emit write(msg);
}
void Notifier::close(){
    emit quit();
}

void Notifier::wait()
{
    m.lock();
    while(!finish) c.wait(&m);
    m.unlock();
}

void Notifier::wake(QMessageBox::StandardButton clicked)
{
    m.lock();
    button = clicked;
    finish = true;
    m.unlock();
    c.wakeAll();
}
