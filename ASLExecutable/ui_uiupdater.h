/********************************************************************************
** Form generated from reading UI file 'uiupdater.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UIUPDATER_H
#define UI_UIUPDATER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_UIUpdater
{
public:
    QLabel *label;
    QLabel *label_2;
    QProgressBar *progressBar;

    void setupUi(QWidget *UIUpdater)
    {
        if (UIUpdater->objectName().isEmpty())
            UIUpdater->setObjectName(QStringLiteral("UIUpdater"));
        UIUpdater->resize(480, 351);
        label = new QLabel(UIUpdater);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(-220, 0, 931, 351));
        label->setPixmap(QPixmap(QString::fromUtf8(":/LoadingCircle_finalani.gif")));
        label_2 = new QLabel(UIUpdater);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(160, 200, 181, 16));
        label_2->setStyleSheet(QStringLiteral("color: rgb(0, 149, 190);"));
        label_2->setAlignment(Qt::AlignCenter);
        progressBar = new QProgressBar(UIUpdater);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(170, 220, 151, 23));
        progressBar->setValue(0);
        progressBar->setTextVisible(false);

        retranslateUi(UIUpdater);

        QMetaObject::connectSlotsByName(UIUpdater);
    } // setupUi

    void retranslateUi(QWidget *UIUpdater)
    {
        UIUpdater->setWindowTitle(QApplication::translate("UIUpdater", "Form", 0));
        label->setText(QString());
        label_2->setText(QApplication::translate("UIUpdater", "Starting...", 0));
    } // retranslateUi

};

namespace Ui {
    class UIUpdater: public Ui_UIUpdater {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UIUPDATER_H
