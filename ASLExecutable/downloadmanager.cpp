#include "downloadmanager.h"
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QCryptographicHash>
#include <QDebug>

#ifdef Q_OS_WIN
#ifdef Q_PROCESSOR_X86_64
#define OS "windows64"
#elif defined(Q_PROCESSOR_X86_32)
#define OS "windows32"
#endif
#elif defined(Q_OS_LINUX)
#ifdef Q_PROCESSOR_X86_64
#define OS "linux64"
#elif defined(Q_PROCESSOR_X86_32)
#define OS "linux32"
#endif
#elif defined(Q_OS_MAC)
#define OS "osx"
#endif

DownloadManager::DownloadManager(QString path, QString version)
{
    targetPath = path;
    this->version = version;
    currentDownload = NULL;
}

DownloadManager::~DownloadManager()
{
    if (!versionMap.isEmpty()){
        currentDownload->abort();
        currentDownload->deleteLater();
    }
    versionMap.clear();
}

void DownloadManager::download(QString url, QString name)
{
    QUrl urlObject(url);
    if (versionMap.isEmpty()){
        gui->print(QString("Downloading ") + name.mid(0, name.lastIndexOf('.')));
        currentDownload = manager.get(QNetworkRequest(urlObject));
        connect(currentDownload, SIGNAL(downloadProgress(qint64,qint64)), gui, SIGNAL(percentage(quint64,quint64)));
    }
    versionMap.insert(url, name);
}

void DownloadManager::execute()
{
    gui->print("Fetching updater...");
    connect(&manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(fetch(QNetworkReply*)));
    QUrl remote("https://www.copy.com/s/gTbobYTAt1wWorfz/Versions.json");
    remote.setPort(443);
    manager.get(QNetworkRequest(remote));
}

void DownloadManager::downloadFinished(QNetworkReply *reply)
{
    disconnect(reply, SIGNAL(downloadProgress(qint64,qint64)), gui, SIGNAL(percentage(quint64,quint64)));
    if (reply->error()){
        gui->print(QString("Failed to download ") + versionMap.value(reply->url().toString()));
        exit();
    } else{
        QString name = versionMap.value(reply->url().toString());
        if (name.contains("/")){
            QDir(targetPath).mkdir(name.split("/").at(0));
        }
        QFile output(QDir(targetPath).absoluteFilePath(name));
        if (!output.open(QFile::WriteOnly)){
            exit();
        }
        output.write(reply->readAll());
        output.close();
        versionMap.remove(reply->url().toString());
        if (!versionMap.isEmpty()){
            gui->print(QString("Downloading ") + versionMap.first().mid(0, versionMap.first().lastIndexOf('.')));
            currentDownload = manager.get(QNetworkRequest(QUrl(versionMap.firstKey())));
            connect(currentDownload, SIGNAL(downloadProgress(qint64,qint64)), gui, SIGNAL(percentage(quint64,quint64)));
        }
    }
    reply->deleteLater();
    if (versionMap.isEmpty()) emit finish();
}

void DownloadManager::exit()
{
    if (!versionMap.isEmpty()){
        currentDownload->abort();
        currentDownload->deleteLater();
    }
    versionMap.clear();
    error = true;
    emit finish();
}

bool compare(QString remote, QString local){
    if (remote.split('.').size() == local.split('.').size()){
        QStringList r = remote.split('.');
        QStringList l = local.split('.');
        for (int i = 0; i < r.size(); ++i) {
            if (r.at(i).toInt() > l.at(i).toInt()){
                return true;
            } else if (r.at(i).toInt() < l.at(i).toInt()){
                return false;
            }
        }
        return true;
    } else{
        return remote.split('.').size() > local.split('.').size();
    }
}

void DownloadManager::fetch(QNetworkReply *reply)
{
    disconnect(&manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(fetch(QNetworkReply*)));
    if (reply->error()){
        gui->print("Fetch failed");
        qDebug() << reply->errorString();
        reply->deleteLater();
        error = true;
        emit finish();
    } else{
        QByteArray t = reply->readAll();
        reply->close();
        reply->deleteLater();
        gui->print("Fetch complete");
        QJsonDocument json(QJsonDocument::fromJson(t));
        connect(&manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(downloadFinished(QNetworkReply*)));
        QJsonObject map = json.object();
        QJsonArray versions = map.value(OS).toArray();
        for (int i = 0; i < versions.size(); ++i) {
            QJsonObject remoteversion = versions[i].toObject();
            bool update = compare(remoteversion["version"].toString(), version);
            gui->print("Downloading necessary files...");
            if (update && remoteversion["version"] != version && !version.isEmpty()){
                QMessageBox::StandardButton button = gui->showMessageAsk("Update found", "Do you want to update?");
                if (button == QMessageBox::Yes){
                    QVariantMap files = remoteversion["files"].toObject().toVariantMap();
                    foreach (QString name, files.keys()) {
                        QString localLib = targetPath + QDir::separator() + name;
                        QFile f(localLib);
                        if (f.exists()){
                            if (!f.open(QFile::ReadOnly)){
                                error = true;
                                emit finish();
                            }
                            QString rChecksum = files.value(name).toMap().value("checksum").toString();
                            QString lChecksum(QCryptographicHash::hash(f.readAll(), QCryptographicHash::Sha1).toHex());
                            f.close();
                            if (rChecksum != lChecksum){
                                download(files.value(name).toMap().value("url").toString(), name);
                            }
                        } else{
                            download(files.value(name).toMap().value("url").toString(), name);
                        }
                    }
                } else break;
            } else if (update){
                QVariantMap files = remoteversion["files"].toObject().toVariantMap();
                foreach (QString name, files.keys()) {
                    QString localLib = targetPath + QDir::separator() + name;
                    QFile f(localLib);
                    if (f.exists()){
                        if (!f.open(QFile::ReadOnly)){
                            error = true;
                            emit finish();
                        }
                        QString rChecksum = files.value(name).toMap().value("checksum").toString();
                        QString lChecksum(QCryptographicHash::hash(f.readAll(), QCryptographicHash::Sha1).toHex());
                        f.close();
                        if (rChecksum != lChecksum){
                            download(files.value(name).toMap().value("url").toString(), name);
                        }
                    } else{
                        download(files.value(name).toMap().value("url").toString(), name);
                    }
                }
            }
        }
        if (versionMap.isEmpty()) emit finish();
    }
}
