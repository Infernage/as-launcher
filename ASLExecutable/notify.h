#pragma once

#ifndef NOTIFY_H
#define NOTIFY_H

#include <QObject>
#include <QWaitCondition>
#include <QMessageBox>

class Notifier : public QObject
{
    Q_OBJECT
public:
    Notifier();
    ~Notifier();
    void showMessageBox(const char *title, const char *msg);
    QMessageBox::StandardButton showMessageAsk(const char *title, const char *msg);
    void print(const char *msg);
    void print(QString msg);
    void close();
    void wait();
    void wake(QMessageBox::StandardButton clicked = QMessageBox::NoButton);

signals:
    void write(const char *msg);
    void write(QString msg);
    void quit();
    void msgBox(const char *title, const char *msg, Notifier *n);
    void askUserBox(const char *title, const char *msg, Notifier *n);
    void percentage(quint64 bRec, quint64 bTotal);

private:
    QMutex m;
    QWaitCondition c;
    bool finish;
    QMessageBox::StandardButton button;
};

#endif // NOTIFY_H
