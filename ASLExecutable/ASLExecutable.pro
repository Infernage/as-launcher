#-------------------------------------------------
#
# Project created by QtCreator 2014-08-19T18:22:53
#
#-------------------------------------------------

QT       += core network widgets concurrent

RC_ICONS = MineCraft.ico

TARGET = ASLS

CONFIG += crypto
CONFIG += app_bundle
CONFIG -= console

TEMPLATE = app

SOURCES += \
    notify.cpp \
    uiupdater.cpp \
    downloadmanager.cpp \
    main.cpp

HEADERS +=\
    notify.h \
    uiupdater.h \
    loader.h \
    downloadmanager.h

include($$PWD/../../../../qt-solutions-qt-solutions/qtsingleapplication/src/qtsingleapplication.pri)

win32{
    contains(QMAKE_TARGET.arch, x86_64){
        CONFIG(debug, debug|release){
            LIBS += -L$$PWD/../../../../qca-build/lib/ -lqcad
            INCLUDEPATH += $$PWD/../../../../qca/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-build
            DEPENDPATH += $$PWD/../../../../qca/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-build
        }
        CONFIG(release, debug|release){
            LIBS += -L$$PWD/../../../../qca-release-build/lib/ -lqca
            INCLUDEPATH += $$PWD/../../../../qca-release/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-release-build
            DEPENDPATH += $$PWD/../../../../qca-release/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-release-build
        }
    }

    contains(QMAKE_TARGET.arch, x86){
        CONFIG(debug, debug|release){
            LIBS += -L$$PWD/../../../../qca-build-x86/lib/ -lqcad
            INCLUDEPATH += $$PWD/../../../../qca/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-build-x86
            DEPENDPATH += $$PWD/../../../../qca/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-build-x86
        }
        CONFIG(release, debug|release){
            LIBS += -L$$PWD/../../../../qca-release-build-x86/lib/ -lqca
            INCLUDEPATH += $$PWD/../../../../qca-release/include/QtCrypto
            INCLUDEPATH += $$PWD/../../../../qca-release-build-x86
            DEPENDPATH += $$PWD/../../../../qca-release/include/QtCrypto
            DEPENDPATH += $$PWD/../../../../qca-release-build-x86
        }
    }
}

RESOURCES += \
    resource.qrc

FORMS += \
    uiupdater.ui

OTHER_FILES +=
