#include "uiupdater.h"
#include "ui_uiupdater.h"
#include <QMovie>

UIUpdater::UIUpdater(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::UIUpdater),
    canClose(false)
{
    setAutoFillBackground(false);
    setAttribute(Qt::WA_TranslucentBackground);
    setWindowFlags(Qt::FramelessWindowHint);
    ui->setupUi(this);
    this->setWindowTitle("AS Launcher bootstrap");
    QMovie *m = new QMovie(":/LoadingCircle_finalani.gif", QByteArray(), this);
    ui->label->setMovie(m);
    m->start();
    setMinimumSize(size());
    setMaximumSize(size());
    setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    ui->progressBar->hide();
    ui->progressBar->setAttribute(Qt::WA_TranslucentBackground);
    ui->progressBar->setAutoFillBackground(false);
}

UIUpdater::~UIUpdater()
{
    delete ui;
}

void UIUpdater::percentage(quint64 rec, quint64 total)
{
    if (!ui->progressBar->isVisible()) ui->progressBar->show();
    if (rec == total){
        ui->progressBar->hide();
        ui->progressBar->setValue(0);
        return;
    }
    if (total == -1){
        ui->progressBar->setMaximum(0);
        ui->progressBar->setMinimum(0);
    } else{
        ui->progressBar->setMinimum(0);
        ui->progressBar->setMaximum(100);
        double perc = (rec / total) * 100;
        ui->progressBar->setValue(perc);
    }
}

void UIUpdater::print(const char *msg)
{
    ui->label_2->setText(QString(msg));
}

void UIUpdater::print(QString msg)
{
    ui->label_2->setText(msg);
}

void UIUpdater::showMessage(const char *title, const char *msg, Notifier *n)
{
    QMessageBox box(QMessageBox::Critical, QString(title), QString(msg));
    box.exec();
    n->wake();
}

void UIUpdater::showAskMessage(const char *title, const char *msg, Notifier *n)
{
    QMessageBox box(QMessageBox::Question, QString(title), QString(msg));
    box.exec();
    n->wake(box.standardButton(box.clickedButton()));
}

void UIUpdater::forceClose()
{
    canClose = true;
    close();
}

void UIUpdater::resizeEvent(QResizeEvent *evt)
{
    evt->ignore();
}

void UIUpdater::closeEvent(QCloseEvent *evt)
{
    if (!canClose)evt->ignore();
    else evt->accept();
}
