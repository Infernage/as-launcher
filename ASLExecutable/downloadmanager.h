#ifndef DOWNLOADMANAGER_H
#define DOWNLOADMANAGER_H

#include <QObject>
#include <QUrl>
#include <QIODevice>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QList>
#include <QMap>
#include "uiupdater.h"
#include "notify.h"

bool compare(QString remote, QString local);

class DownloadManager : public QObject
{
    Q_OBJECT
public:
    DownloadManager(QString path, QString version);
    ~DownloadManager();

    bool error;
    Notifier *gui;

public slots:
    void execute();

signals:
    void finish();

private:
    QNetworkAccessManager manager;
    QNetworkReply *currentDownload;
    QString targetPath;
    QString version;
    QMap<QString, QString> versionMap;

    void exit();
    void download(QString url, QString name);

private slots:
    void fetch(QNetworkReply *reply);
    void downloadFinished(QNetworkReply *reply);
};

#endif // DOWNLOADMANAGER_H
