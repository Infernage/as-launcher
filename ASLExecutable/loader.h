#ifndef LOADER_H
#define LOADER_H

#include <QString>
#include <QStringList>

class Loader
{
public:

    virtual ~Loader(){}
    virtual bool load(QString workingDir) = 0;
    virtual int quit() = 0;
    virtual bool waitForTermination() = 0;
    virtual QString getErrorStr() = 0;
};

#endif // LOADER_H
