#include "uiupdater.h"
#include "notify.h"
#include "downloadmanager.h"
#include "loader.h"
#include <QtSingleApplication>
#include <QMessageBox>
#include <QFuture>
#include <QFutureWatcher>
#include <QtConcurrent>
#include <QSslSocket>
#include <QtCrypto>
#ifdef Q_OS_WIN
#include <Windows.h>
#endif

class JavaVersionWrapper{
public:
    JavaVersionWrapper(int jre, int version){
        this->jre = jre;
        this->version = version;
    }

    int jre;
    int version;
};

JavaVersionWrapper getJavaVersion(){
    QProcess process;
    process.setReadChannel(QProcess::StandardError);
    process.start("java", QStringList() << "-version");
    if (!process.waitForStarted()){
        return JavaVersionWrapper(-1, -1);
    }
    if (!process.waitForReadyRead()){
        return JavaVersionWrapper(-1, -1);
    }
    QByteArray data = process.readAll(); // Output-> java version "1.X.0_Y"
    process.close();
    QString raw(data);
    QStringList list = raw.split("\"", QString::SkipEmptyParts);
    QString raw_version = list.at(1);
    if (!raw_version.contains("_")) return JavaVersionWrapper(-1, -1); // No java installed.
    list = raw_version.split("_", QString::SkipEmptyParts);
    QString jre_version = list.at(0), version = list.at(1);
    list = jre_version.split(".");
    int jre = list.at(1).toInt(), jreVersion = version.toInt();
    switch(jre){
    case 6: return JavaVersionWrapper(6, jreVersion);
    case 7: return JavaVersionWrapper(7, jreVersion);
    case 8: return JavaVersionWrapper(8, jreVersion);
    case 9: return JavaVersionWrapper(9, jreVersion);
    default: return JavaVersionWrapper(-1, -1);
    }
}

#ifdef Q_OS_WIN
int checkDir(QDir &dir){
    QString pathEnv(qgetenv("PATH"));
    QString path1 = dir.absolutePath().replace(QString("/"), QString("\\"));
    QString path2;
#ifdef Q_OS_WIN64
    if (!dir.cd("server")){
        return -1;
    }
#else
    if (!dir.cd("client")){
        return -1;
    }
#endif
    path2 = dir.absolutePath().replace(QString("/"), QString("\\"));
    if (!pathEnv.contains(path1)){
        pathEnv.append(";").append(path1);
    }
    if (!pathEnv.contains(path2)){
        pathEnv.append(";").append(path2);
    }
    qputenv("PATH", pathEnv.toUtf8());
    //AddDllDirectory(path1.toStdWString().c_str());
    //AddDllDirectory(path2.toStdWString().c_str());
    return 0;
}
#endif

int checkJava(){
    JavaVersionWrapper wrapper = getJavaVersion();
    int jre_version = wrapper.version;
    int jre = wrapper.jre;
    if (jre > 7 || (jre == 7 && jre_version >= 67)){
#ifdef Q_OS_WIN
        /*
     * Explanation to check Java path in Windows:
     * 1->Check in the registry for the key HKEY_LOCAL_MACHINE\SOFTWARE\JavaSoft\Java Runtime Environment.
     *    Get the CurrentVersion key and check for the version in JavaHome key.
     * 2->Check in the registry for the key HKEY_CLASSES_ROOT\jarfile\shell\open\command and get the default
     *    key data. Ex: "C:\Program Files (x86)\Java\jre7\bin\javaw.exe" -jar "%1" %* in my key.
     * 3->Check in the registry for the key HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\App Paths\javaws.exe
     *    for the default key.
     * 4->Check the JAVA_HOME environment.
     * 5->Check the path in Program files directory.
     */
        //->1
        QString tmp = QString("HKEY_LOCAL_MACHINE\\SOFTWARE\\JavaSoft\\Java Runtime Environment\\1.") +
                QString::number(jre) + QString(".0_") + QString::number(jre_version);
        QSettings option_1(tmp, QSettings::NativeFormat);
        QString path = option_1.value("JavaHome").toString();
        if (path.isEmpty()){
            //->2
            QSettings option_2(QString("HKEY_CLASSES_ROOT\\jarfile\\shell\\open\\command"), QSettings::NativeFormat);
            path = option_2.value("Default").toString().split("\"", QString::SkipEmptyParts).at(0);
            if (path.isEmpty()){
                //->3
                path = QString(qgetenv("JAVA_HOME"));
                if (path.isEmpty()){
                    //->4
                    path = QString(qgetenv("PROGRAMFILES"));
                    if (path.isEmpty()){
                        return -1;
                    }
                    QDir dir(path);
                    if (!dir.cd("Java")){
                        return -1;
                    }
                    if (!dir.cd(QString("jre") + jre)){
                        return -1;
                    }
                    if (!dir.cd("bin")){
                        return -1;
                    }
                    return checkDir(dir);
                }
                QDir dir(path);
                if (!dir.cd("bin")){
                    return -1;
                }
                return checkDir(dir);
            }
            return checkDir(QFileInfo(path).absoluteDir());
        }
        QDir dir(path);
        if (!dir.cd("bin")){
            return -1;
        }
        return checkDir(dir);
#elif defined(Q_OS_LINUX) || defined(Q_OS_UNIX) || defined(Q_OS_MAC)
        QString path = getenv("JAVA_HOME");
        // TODO
        return -1;
#else
#error "Operative system unknown"
#endif
    }
    return jre == -1 ? -1 : 1;
}

void start(UIUpdater *gui){
    Notifier *notify = new Notifier();
    QObject::connect(notify, SIGNAL(write(const char*)), gui, SLOT(print(const char*)));
    QObject::connect(notify, SIGNAL(write(QString)), gui, SLOT(print(QString)));
    QObject::connect(notify, SIGNAL(quit()), gui, SLOT(forceClose()));
    QObject::connect(notify, SIGNAL(msgBox(const char*,const char*,Notifier*)), gui,
                     SLOT(showMessage(const char*,const char*,Notifier*)));
    QObject::connect(notify, SIGNAL(askUserBox(const char*,const char*,Notifier*)), gui,
                     SLOT(showAskMessage(const char*,const char*,Notifier*)));
    QObject::connect(notify, SIGNAL(percentage(quint64,quint64)), gui, SLOT(percentage(quint64,quint64)));
    notify->print("Checking for installed Java...");
    int cj = checkJava();
    if (cj == -1){
        notify->showMessageBox("Java not found",
                               "Java wasn't found in your system. Please, install at least the JRE7_67");
        delete notify;
        return;
    } else if (cj == 1){
        notify->showMessageBox("Old java found",
                               "Your java version is older than the required. Please, install at least the JRE7_67");
        delete notify;
        return;
    }

    // Gets the working folder.
    QString workingDir;
#ifdef Q_OS_WIN
    workingDir = QString(qgetenv("APPDATA")) + QDir::separator() + QString("ASL");
#elif defined(Q_OS_LINUX)
    workingDir = QString(qgetenv("HOME")) + QDir::separator() + QString(".ASL");
#elif defined(Q_OS_MAC)
    workingDir = QString(qgetenv("HOME")) + QDir::separator() + QString("Library") + QDir::separator() +
            QString("Application Support") + QDir::separator() + QString(".ASL");
#else
#error "Non supported system!"
#endif

    // Get native loader version
#ifdef Q_OS_WIN
    QString libName = "ASLL.dll";
#elif defined(Q_OS_LINUX)
    QString libName = "ASLL.so";
#elif defined(Q_OS_MAC)
    QString libName = "ASLL.dylib";
#endif
    notify->print("Getting installed version...");
    QString version;
    if (QFileInfo(workingDir + QDir::separator() + libName).exists()){
        QLibrary libV(workingDir + QDir::separator() + libName);
        if (libV.load()){
            typedef char* (*GetVersion)();
            GetVersion func = (GetVersion) libV.resolve("getVersion");
            if (func){
                char *c = func();
                if (c){
                    version = QString(c);
                    libV.unload();
                } else libV.unload();
            } else libV.unload();
        } else{
            qDebug() << libV.errorString();
#ifdef QT_DEBUG
            notify->showMessageBox("FAIL", libV.errorString().toStdString().c_str());
#endif
        }
    }

    // Check updates
    notify->print("Checking for updates...");
    DownloadManager *manager = new DownloadManager(workingDir, version);
    manager->gui = notify;
    QEventLoop loop;
    QObject::connect(manager, SIGNAL(finish()), &loop, SLOT(quit()));
    QTimer::singleShot(10, manager, SLOT(execute()));
    loop.exec();
    delete manager;

    // Load native loader
    notify->print("Loading native ASL...");
    QLibrary lib(workingDir + QDir::separator() + libName);
    if (!lib.load()){
        QString error = lib.errorString();
#ifdef QT_DEBUG
        qDebug() << error;
#endif
        notify->showMessageBox("Lib loader failed", (QString("Failed to load native loader!\nError: ") +
                                                     error).toStdString().c_str());
        delete notify;
        return;
    }
    typedef Loader* (*CreateLoader)();
    CreateLoader func = (CreateLoader) lib.resolve("createLoader");
    if (!func){
        notify->showMessageBox("Lib loader failed", (QString("Failed to resolve native loader!\nError: ") + lib
                                                     .errorString()).toStdString().c_str());
        delete notify;
        return;
    }
    Loader* loader = func();

    if (loader){
        if (!loader->load(workingDir)){
            notify->showMessageBox("Lib loader failed", (QString("Failed to execute native loader!\nError: ")
                                                         .append(loader->getErrorStr())).toStdString().c_str());
            delete loader;
            delete notify;
            return;
        } else{
            notify->close();
            if (!loader->waitForTermination()){
                notify->showMessageBox("Failed to load the launcher", loader->getErrorStr().toStdString().c_str());
            }
            loader->quit();
            delete notify;
            delete loader;
        }
    } else{
        QMessageBox box(QMessageBox::Critical, "Lib loader failed",
                        QString("Failed to create native loader!\nError: ") + lib.errorString());
        box.exec();
        return;
    }
}

int main(int argc, char *argv[])
{
    QCA::Initializer init;
    QtSingleApplication a("ASL", argc, argv);
    if (a.isRunning()){
        QMessageBox box(QMessageBox::Critical, "Application already running",
                        "ASL is already running, please, be patient.");
        box.exec();
        return 0;
    }

    // Add library path for QCA
#ifdef Q_OS_WIN
    QCoreApplication::addLibraryPath(QString(qgetenv("APPDATA")) + QDir::separator() + QString("ASL"));
#elif defined(Q_OS_LINUX)
    QCoreApplication::addLibraryPath((QString(qgetenv("HOME")) + QDir::separator() + QString(".ASL"));
#elif defined(Q_OS_MAC)
    QCoreApplication::addLibraryPath((QString(qgetenv("HOME")) + QDir::separator() + QString("Library") + QDir::separator() +
                     QString("Application Support") + QDir::separator() + QString(".ASL"));
#endif
    QCA::scanForPlugins();
    if (!QCA::isSupported("aes256-cbc-pkcs7")){
        QMessageBox box(QMessageBox::Critical, "QCA failure",
                        "Failed to open qca. AES256 PKCS7 not supported");
        box.exec();
        return 1;
    }

    UIUpdater gui;
    gui.show();

    QFutureWatcher<void> watcher;
    QObject::connect(&watcher, SIGNAL(finished()), &a, SLOT(quit()));
    QFuture<void> future = QtConcurrent::run(start, &gui);
    watcher.setFuture(future);
    return a.exec();
}

