#include <QCoreApplication>
#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QThread>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    Q_UNUSED(a)
    QThread::msleep(500);
    for (int i = 1; i < argc; ++i) {
        QString str(argv[i]);
        QFileInfo info(str);
        if (info.isDir() && info.exists()){
            QDir dir(str);
            dir.removeRecursively();
        } else if (info.isFile() && info.exists()){
            QFile file(str);
            file.remove();
        }
    }
    return 0;
}
