/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infernage.aslauncher.tests;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import javax.swing.JProgressBar;
import org.infernage.aslauncher.core.concurrent.DefaultEngine;
import org.infernage.aslauncher.core.concurrent.DownloadJob;
import org.infernage.aslauncher.core.concurrent.Downloader;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alberto
 */
public class NetworkTest {

    public NetworkTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    public void portScannerTCP() throws Exception {
        final ExecutorService es = Executors.newFixedThreadPool(50);
        final String ip = "127.0.0.1";
        final int timeout = 200;
        final List<Future<Pair>> futures = new ArrayList<>();
        for (int port = 1; port <= 65535; port++) {
            futures.add(portIsOpen(es, ip, port, timeout));
        }
        es.shutdown();
        for (final Future<Pair> f : futures) {
            if (f.get().open) {
                try {
                    ServerSocket test = new ServerSocket(f.get().port);
                    test.close();
                    System.out.println("Port " + f.get().port + " available to use!");
                } catch (Exception e) {
                    System.out.println("Port " + f.get().port + " open but is already in use!");
                }
            }
        }
    }

    private static class Pair {

        public boolean open;
        public int port;
    }

    public static Future<Pair> portIsOpen(final ExecutorService es, final String ip, final int port, final int timeout) {
        return es.submit(new Callable<Pair>() {
            @Override
            public Pair call() {
                Pair p = new Pair();
                p.port = port;
                try {
                    Socket socket = new Socket();
                    socket.connect(new InetSocketAddress(ip, port), timeout);
                    socket.close();
                    p.open = true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    p.open = false;
                }
                return p;
            }
        });
    }

    @Test
    public void getExternalIP() throws Exception {
        URLConnection conn = new URL("http://whatsmyip.net").openConnection();
        int length = Integer.valueOf(conn.getHeaderField("Content-Length"));
        byte[] buf = new byte[length];
        InputStream in = conn.getInputStream();
        in.read(buf);
        System.out.println(new String(buf).replaceAll("(?s).*?(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}).*", "$1"));
    }

    public void copyDownload() throws Exception {
        Downloader dw = new DefaultEngine(new URL("https://copy.com/MTeA80TrGznG"), new DownloadJob("Test", new JProgressBar()), Paths.get("E:", "animelist_1408408220_-_2771699.xml.gz"), false);
        System.out.println(dw.call());
    }

    @Test
    public void FTBurl() {
        String file = "maven/";
        String resolved = "http://ftb.cursecdn.com/FTB2/" + file;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(resolved).openConnection();
            connection.setRequestProperty("Cache-Control", "no-transform");
            connection.setRequestMethod("HEAD");
            Map<String, String> t = new HashMap<>();
            t.put("test", "http://ftb.cursecdn.com");
            t.put("test1", "ftb.cursecdn.com");
            t.put("test2", "new.creeperrepo.net");
            t.put("test3", "http://new.creeperrepo.net");
            for (String server : t.values()) {
                if (connection.getResponseCode() == 200) {
                    break;
                }
                if (!server.contains("creeper")) {
                    file = file.replaceAll("%5E", "/");
                }
                resolved = "http://" + server + "/FTB2/" + file;
                connection = (HttpURLConnection) new URL(resolved).openConnection();
                connection.setRequestProperty("Cache-Control", "no-transform");
                connection.setRequestMethod("HEAD");
            }
        } catch (IOException e) {
        }
        connection.disconnect();
        System.out.println(resolved);
    }
}
