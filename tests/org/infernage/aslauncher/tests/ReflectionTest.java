/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.infernage.aslauncher.tests;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import org.infernage.aslauncher.core.crypto.SuiteCrypto;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Alberto
 */
public class ReflectionTest {

    public ReflectionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void newInstanceTest() throws Exception {
        /*Constructor<SuiteCrypto> constructor = SuiteCrypto.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        SuiteCrypto test = constructor.newInstance();
        Method init = SuiteCrypto.class.getDeclaredMethod("init", String.class);
        init.setAccessible(true);
        init.invoke(test, "Root");*/
    }

    public void classLoader() throws Exception {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        Enumeration<URL> resources = loader.getResources("ass/aslauncher");
        List<Path> dirs = new ArrayList<>();
        while (resources.hasMoreElements()) {
            dirs.add(new File(URLDecoder.decode(resources.nextElement().getFile(), "UTF-8")).toPath());
        }
        List<Class> classes = new ArrayList<>();
        for (Path path : dirs) {
            classes.addAll(findClasses(path, "ass.aslauncher"));
        }
        for (Class c : classes) {
            System.out.println(c.getCanonicalName());
        }
    }
    
    @Test
    public void checkMethodAccessible() throws Exception{
        Check c = new Check();
        Method m1 = Check.class.getDeclaredMethod("doSomething"), m2 = Check.class
                .getDeclaredMethod("doSomething");
        m2.setAccessible(true);
        m2.invoke(c);
    }

    private List<Class> findClasses(Path directory, String packageName) throws Exception {
        List<Class> classes = new ArrayList<>();
        if (Files.notExists(directory)) {
            return classes;
        }
        for (Path path : Files.newDirectoryStream(directory)) {
            if (Files.isDirectory(path)) {
                classes.addAll(findClasses(path, packageName + "." + path.getFileName().toString()));
            } else if (path.toString().endsWith(".class")){
                classes.add(Class.forName(packageName + "." + path.getFileName().toString().substring(0, path.getFileName().toString().length() - 6)));
            }
        }
        return classes;
    }
    
    private class Check{
        private void doSomething(){
            System.out.println("Something!");
        }
    }
}
