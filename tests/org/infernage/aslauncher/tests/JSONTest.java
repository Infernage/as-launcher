/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.tests;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.infernage.aslauncher.core.CoreConfiguration.OS;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Infernage
 */
public class JSONTest {
    
    public JSONTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void toMap(){
        Map<OS, List<Value>> m = new HashMap<>();
        List<Value> l = new ArrayList<>();
        l.add(new Value("0.0.0", "url"));
        l.add(new Value("0.0.1", "url"));
        m.put(OS.windows, l);
        l = new ArrayList<>();
        l.add(new Value("0.1.0", "url"));
        m.put(OS.linux, l);
        System.out.println(new GsonBuilder().serializeNulls().serializeSpecialFloatingPointValues()
                .setPrettyPrinting().create().toJson(m, new TypeToken<Map<OS, Value>>(){}.getType()));
    }
    
    private class Value{
        public String version;
        public String url;
        
        public Value(String v, String u){
            version = v;
            url = u;
        }
    }
}
