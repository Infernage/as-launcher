/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.tests;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Infernage
 */
public class CryptoTest {
    
    public CryptoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void testQStringtoJString() throws IOException{
        Path p = Paths.get("C:\\Users\\Alberto\\AppData\\Roaming\\ASL\\test");
        byte[] content = Files.readAllBytes(p);
        byte[] size = new byte[4];
        System.arraycopy(content, 0, size, 0, 4);
        byte[] hello = new byte[content.length - 4];
        System.arraycopy(content, 4, hello, 0, hello.length);
        System.out.println(ByteBuffer.wrap(size).getInt());
        System.out.println(new String(hello, "UTF-8"));
    }
}
