/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.exceptions;

/**
 * Exception used to abstract the exception type produced at the initialization module step.
 * @author Infernage
 */
public class ModuleInitializationException extends Exception {
    private final String logMessage;
    private final Throwable throwHolder;
    
    /**
     * Default constructor.
     * @param message The message desired to show.
     * @param cause The throwable cause.
     */
    public ModuleInitializationException(String message, Throwable cause){
        logMessage = message;
        throwHolder = cause;
    }

    /**
     * Gets the message to show.
     * @return A String with the message.
     */
    public String getLogMessage() {
        return logMessage;
    }

    /**
     * Gets the throwable cause.
     * @return A Throwable object with the cause.
     */
    public Throwable getThrowHolder() {
        return throwHolder;
    }
}
