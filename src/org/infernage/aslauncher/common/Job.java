/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.common;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import javax.swing.JProgressBar;

/**
 * Interface used to abstract multiples Tasks or long Tasks.
 *
 * @author Infernage
 * @param <I> The Input type.
 * @param <O> The Output type.
 */
public interface Job<I extends Callable<?>, O> {

    /**
     * Gets the Job name.
     *
     * @return A String with the name.
     */
    public String getName();

    /**
     * Adds a Task to this job.
     *
     * @param task The Task to add.
     */
    public void addTask(I task);

    /**
     * Adds a Task Collection to this job.
     *
     * @param jobs The Collection to add.
     */
    public void addTaskCollection(Collection<I> jobs);

    /**
     * Starts this Job.
     *
     * @return A List with Future objects. The result typt of the Future objects
     * is specified with the template.
     * @throws Exception
     */
    public List<Future<O>> startJob() throws Exception;

    /**
     * Sets a progress bar to this Job.
     *
     * @param bar The progress bar to set.
     */
    public void setProgressBar(JProgressBar bar);

    /**
     * Gets the progress bar previously set.
     *
     * @return The JProgressBar set or null if any wasn't set.
     */
    public JProgressBar getProgressBar();

    /**
     * Checks if this Job has finished successfully or not.
     *
     * @return {@code true} if the Job has finished correctly, or {@code false}
     * if the Job hasn't finalized or hasn't finished correctly.
     */
    public boolean isSuccessfull();

    /**
     * Checks if this Job has finished.
     *
     * @return {@code true} if the Job has finished or {@code false} if not.
     */
    public boolean isDone();

    /**
     * Gets the current progress of this Job.
     *
     * @return The progress or -1 if the progress cannot be determined.
     */
    public BigDecimal getJobProgress();

    /**
     * Gets the computed total progress of this Job.
     *
     * @return The total progress.
     */
    public BigDecimal getTotalProgress();

    /**
     * Obtains the number of task that have failed.
     *
     * @return The number of failed tasks.
     */
    public int getFailTasksCount();
}
