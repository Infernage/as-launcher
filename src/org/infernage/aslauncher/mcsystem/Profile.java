/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.infernage.aslauncher.core.CoreConfiguration;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.mcsystem.authenticator.ProfileResponse;
import org.infernage.aslauncher.mcsystem.authenticator.SystemType;

/**
 * Class used to abstract a Minecraft Profile. A Profile can have 0 or more
 * Minecraft Instances.
 *
 * @author Infernage TODO Apply SuiteCrypto to each profile.
 */
public class Profile implements Serializable {

    public static final String[] actions = {"Close launcher", "Reopen launcher", "Keep launcher open"};
    private static final long serialVersionUID = 100264319;
    private static final double TIMEOUT = 1.8e12D;

    private String userName;
    private String accessToken;
    private String sessionid;
    private String key;
    private boolean autoLogin = false;
    private SystemType type;
    private volatile double played;
    private String instancesPath;
    private List<MCInstance> instances;
    private String maxRam;
    private String profileName;
    private String action;
    private boolean advanced;
    private String javaExec;
    private String javaArgs;
    private Map<String, Boolean> internalModules;
    private transient boolean logged = autoLogin;
    private transient boolean alreadyLogged = false;

    public Profile(ProfileResponse selected, String key, String session, String access, Path path, SystemType type) {
        String user = selected == null ? "Player" : selected.getName();
        this.type = type;
        this.key = key;
        userName = user;
        profileName = user;
        instancesPath = (path == null) ? CoreHandler.getConfiguration()
                .getMapValue(CoreConfiguration.ConfigKeys.ProfilesDir) + File.separator + user
                : path.toString();
        instances = new ArrayList<>();
        maxRam = "1 Gbyte";
        action = actions[0];
        sessionid = session;
        accessToken = access;
        javaExec = System.getProperty("java.home") + File.separator + "bin"
                + File.separator + (CoreHandler.getConfiguration().getOS()
                .equals(CoreConfiguration.OS.windows) ? "javaw" : "java");
        javaArgs = "";
        autoLogin = false;
        advanced = false;
        internalModules = new TreeMap<>();
        played = Double.NaN;
    }

    public Profile(String user, Path path) {
        this(null, null, null, "NULL", path, SystemType.LEGACY);
        profileName = user;
        played = 0D;
    }

    public final void improve(Profile demo) {
        if (demo == null) {
            throw new RuntimeException("Copy is null");
        }
        type = demo.type;
        key = demo.key;
        userName = demo.userName;
        profileName = demo.profileName;
        instancesPath = demo.instancesPath;
        instances = demo.instances;
        maxRam = demo.maxRam;
        action = demo.action;
        sessionid = demo.sessionid;
        accessToken = demo.accessToken;
        javaExec = demo.javaExec;
        javaArgs = demo.javaArgs;
        autoLogin = demo.autoLogin;
        advanced = demo.advanced;
        internalModules = demo.internalModules;
        played = demo.played;
    }

    public void addPlayed(double play) {
        played += play;
    }

    public double getPlayed() {
        return played;
    }

    public boolean isTimeout() {
        return played >= TIMEOUT;
    }

    public synchronized void changeDir(Path newPath) {
        instancesPath = newPath.toString();
    }

    public synchronized void addInstance(MCInstance instance) {
        instances.add(instance);
    }

    public synchronized void removeInstance(MCInstance instance) {
        instances.remove(instance);
    }

    public synchronized void changeName(String newUser) {
        profileName = newUser;
    }

    public void changeKey(String newKey) {
        if (isPremium()) {
            synchronized (this) {
                key = newKey;
            }
        }
    }

    public synchronized void setMaxRam(String ram) {
        maxRam = ram;
    }

    public synchronized void setAction(int index) {
        action = actions[index];
    }

    public void setAccessToken(String token) {
        if (!isPremium()) {
            return;
        }
        if (token == null) {
            token = "NULL";
        }
        synchronized (this) {
            accessToken = token;
        }
    }

    public void setType(SystemType type) {
        if (isPremium()) {
            synchronized (this) {
                this.type = type == null ? SystemType.LEGACY : type;
            }
        }
    }

    public synchronized SystemType getType() {
        return type;
    }

    public synchronized Path getPath() {
        return Paths.get(instancesPath);
    }

    public synchronized List<MCInstance> getList() {
        return Collections.unmodifiableList(instances);
    }

    public synchronized MCInstance getInstance(MCInstance instance) {
        MCInstance element = null;
        for (MCInstance each : instances) {
            if (each.equals(instance)) {
                element = new MCInstance(each);
                break;
            }
        }
        return element;
    }

    public String getUsername() {
        return userName;
    }

    public synchronized String getKey() {
        return key;
    }

    public synchronized String getMaxRam() {
        return maxRam;
    }

    public synchronized String getProfilename() {
        return profileName;
    }

    public synchronized String getAction() {
        return action;
    }

    public String getSessionID() {
        return sessionid;
    }

    public synchronized String getAccessToken() {
        return accessToken;
    }

    public synchronized boolean isPremium() {
        return !(accessToken.equalsIgnoreCase("NULL") || sessionid == null);
    }

    public synchronized String getSessionToken() {
        if (accessToken.equalsIgnoreCase("NULL")) {
            return "";
        }
        return accessToken;

    }

    /**
     * @return the autoLogin
     */
    public synchronized boolean isAutoLogin() {
        return autoLogin || !isPremium();
    }

    /**
     * @param autoLogin the autoLogin to set
     */
    public void setAutoLogin(boolean autoLogin) {
        if (isPremium()) {
            synchronized (this) {
                this.autoLogin = autoLogin;
            }
        }
    }

    /**
     * @return the advanced
     */
    public synchronized boolean isAdvanced() {
        return advanced;
    }

    /**
     * @param advanced the advanced to set
     */
    public synchronized void setAdvanced(boolean advanced) {
        this.advanced = advanced;
    }

    /**
     * @return the javaExec
     */
    public synchronized String getJavaExec() {
        return javaExec;
    }

    /**
     * @param javaExec the javaExec to set
     */
    public synchronized void setJavaExec(String javaExec) {
        this.javaExec = javaExec;
    }

    /**
     * @return the javaArgs
     */
    public synchronized String getJavaArgs() {
        return javaArgs;
    }

    /**
     * @param javaArgs the javaArgs to set
     */
    public synchronized void setJavaArgs(String javaArgs) {
        this.javaArgs = javaArgs;
    }

    /**
     * @return the internalModules
     */
    public Map<String, Boolean> getInternalModules() {
        return internalModules;
    }

    /**
     * @return the logged
     */
    public synchronized boolean isLogged() {
        return logged || !isPremium();
    }

    /**
     * @param logged the logged to set
     */
    public void setLogged(boolean logged) {
        if (isPremium()) {
            synchronized (this) {
                this.logged = logged;
                if (!logged) {
                    alreadyLogged = false;
                }
            }
        }
    }

    /**
     * @return If this Profile is already logged for a while or recently.
     */
    public synchronized boolean isAlreadyLogged() {
        if (isLogged() && !alreadyLogged) {
            alreadyLogged = true;
            return false;
        } else if (isLogged() && alreadyLogged) {
            return true;
        }
        return false;
    }
}
