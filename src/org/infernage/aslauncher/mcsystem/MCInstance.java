/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.GuiEventController;
import org.infernage.aslauncher.core.concurrent.DownloadJob;
import org.infernage.aslauncher.core.concurrent.Downloader;
import org.infernage.aslauncher.core.concurrent.MD5Engine;
import org.infernage.aslauncher.core.concurrent.SHA1Engine;
import org.infernage.aslauncher.mcsystem.forge.CompleteLocalMinecraftForge;
import org.infernage.aslauncher.mcsystem.versions.AssetHashList;
import org.infernage.aslauncher.mcsystem.versions.CompleteVersion;
import org.infernage.aslauncher.util.References;

/**
 * Class used to abstract a Minecraft instance. An instance has 1 version, 1
 * Forge version (Optional) and a mod list (Optional).
 *
 * @author Infernage
 */
public class MCInstance implements Serializable {

    private static final Logger log = Logger.getLogger(MCInstance.class.getName());
    private static final long serialVersionUID = 100236791;
    public final static short JAR = 1, CORE = 2, MOD = 3;

    private String name, info;
    private String path;
    private List<String> modList, coremodList, jarmodList;
    private CompleteLocalMinecraftForge forge;
    private CompleteVersion mineVersion;

    /**
     * Default constructor.
     *
     * @param name The instance name.
     * @param version The Minecraft version.
     * @param path The instance path.
     */
    public MCInstance(String name, Path path, CompleteVersion version) {
        this.name = name;
        mineVersion = version;
        this.path = path.toString();
        info = null;
        modList = new ArrayList<>();
        coremodList = new ArrayList<>();
        jarmodList = new ArrayList<>();
        forge = null;
    }

    /**
     * Constructor used to copy an instance.
     *
     * @param copy The instance to copy.
     */
    public MCInstance(MCInstance copy) {
        name = copy.getName();
        mineVersion = copy.getVersion();
        path = copy.path;
        info = null;
        modList = copy.modList;
        coremodList = copy.coremodList;
        jarmodList = copy.jarmodList;
    }

    /**
     * Changes the instance path.
     *
     * @param newPath The new path.
     */
    public void changePath(Path newPath) {
        path = newPath.toString();
    }

    /**
     * Renames the instance.
     *
     * @param newName The new name.
     */
    public void rename(String newName) {
        name = newName;
    }

    /**
     * Assigns an info to the instance.
     *
     * @param sequence The information.
     */
    public void setInfo(String sequence) {
        info = sequence;
    }

    /**
     * Adds a mod to the instance.
     *
     * @param title The mod name.
     * @param type The type of the mod.
     */
    public void addMod(String title, short type) {
        switch (type) {
            case JAR:
                jarmodList.add(title);
                break;
            case MOD:
                modList.add(title);
                break;
            case CORE:
                coremodList.add(title);
                break;
        }
    }

    /**
     * Removes a mod from the instance.
     *
     * @param title The mod name.
     * @param type The type of the mod.
     */
    public void removeMod(String title, short type) {
        switch (type) {
            case CORE:
                coremodList.remove(title);
                break;
            case JAR:
                jarmodList.remove(title);
                break;
            case MOD:
                modList.remove(title);
                break;
        }
    }

    /**
     * Adds a MC Forge to the instance.
     *
     * @param forg The MC Forge to add.
     */
    public void addForge(CompleteLocalMinecraftForge forg) {
        forge = forg;
    }

    /**
     * Removes the MC Forge installed. Is removed only in the object.
     */
    public void removeForge() {
        try {
            if (forge != null) {
                Files.deleteIfExists(forge.getPath());
            }
        } catch (IOException ex) {
            log.log(Level.CONFIG,
                    "Failed to delete MinecraftForge file. Ignoring and setting Object to NULL", ex);
        }
        forge = null;
    }

    /**
     *
     * @return
     */
    public boolean isLower1_6() {
        try {
            String[] split = mineVersion.getId().split("\\.");
            int v = Integer.parseInt(split[1]);
            return split[0].equals("1") && v < 6;
        } catch (NumberFormatException e) {
            //Ignore
        }
        return false;
    }

    public void cleanNatives() {
        IOFileFilter filter = new AgeFileFilter(System.currentTimeMillis() - 3600L);
        for (File version : new File(path).listFiles((FileFilter) DirectoryFileFilter.DIRECTORY)) {
            for (File folder : version.listFiles((FileFilter) FileFilterUtils.and(new IOFileFilter[]{new PrefixFileFilter(version.getName() + "-natives-"), filter}))) {
                FileUtils.deleteQuietly(folder);
            }
        }
    }
    
    public void requestNecessaryFiles()
            throws IOException, MalformedURLException, InterruptedException, NoSuchAlgorithmException{
        GuiEventController.Result versions = requestDownloadVersion();
        GuiEventController.Result resources = requestDownloadResources();
        versions.get();
        resources.get();
    }

    private GuiEventController.Result requestDownloadVersion()
            throws MalformedURLException, IOException, InterruptedException {
        DownloadJob job = new DownloadJob("Version " + name);
        if (forge != null && !isLower1_6()) {
            job.addTaskCollection(forge.getRequiredDownloads(Paths.get(path).getParent().getParent(), job));
        } else {
            job.addTaskCollection(mineVersion.getRequiredDownloads(Paths.get(path).getParent().getParent(),
                    job));
        }
        if (!isLower1_6() || forge == null) {
            job.addTask(new MD5Engine(new URL(References.MINECRAFT_DOWNLOAD_BASE + "versions/"
                    + mineVersion.getId() + "/" + mineVersion.getId() + ".jar"), job,
                    Paths.get(path, mineVersion.getId() + ".jar"), false));
        }
        return CoreHandler.getGuiController().addJob(job, "Downloading version",
                CoreHandler.Priority.High);
    }

    private GuiEventController.Result requestDownloadResources()
            throws IOException, NoSuchAlgorithmException, InterruptedException {
        migrateAssets();
        DownloadJob job = new DownloadJob("Resources");
        job.addTaskCollection(getResources(job));
        return CoreHandler.getGuiController().addJob(job, "Downloading resources",
                CoreHandler.Priority.High);
    }

    private Set<Downloader> getResources(DownloadJob job) throws MalformedURLException, IOException {
        Set<Downloader> res = new HashSet<>();
        Path assets = Paths.get(Paths.get(path).getParent().getParent().toString(), "assets");
        Path objects = Paths.get(assets.toString(), "objects");
        Path index = Paths.get(assets.toString(), "indexes");
        String name = mineVersion.getAssets();
        if (name == null) {
            name = "legacy";
        }
        Path indexFile = Paths.get(index.toString(), name + ".json");
        URL url = new URL(References.MINECRAFT_DOWNLOAD_BASE + "indexes/" + name + ".json");
        String json;
        try (InputStream stream = url.openConnection().getInputStream()) {
            json = IOUtils.toString(stream);
        }
        FileUtils.writeStringToFile(indexFile.toFile(), json);
        AssetHashList hashList = new Gson().fromJson(json, AssetHashList.class);
        for (AssetHashList.AssetPair pair : hashList.getValues()) {
            Path file = Paths.get(objects.toString(), pair.getHash().substring(0, 2), pair.getHash());
            if (!Files.isRegularFile(file) || Files.size(file) != pair.getSize()) {
                Downloader download = new SHA1Engine(new URL(References.MINECRAFT_RESOURCES
                        + pair.getHash().substring(0, 2) + "/" + pair.getHash()), job, file,
                        false, pair.getHash(), (int) pair.getSize());
                download.setSize(pair.getSize());
                res.add(download);
            }
        }
        return res;
    }

    private String getDigest(Path path, String alg, int length)
            throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance(alg);
        try (FileChannel input = FileChannel.open(path, StandardOpenOption.READ)) {
            ByteBuffer bb = ByteBuffer.allocate(256 * 1024);
            while (input.read(bb) > 0) {
                bb.flip();
                digest.update(bb);
                bb.compact();
            }
        }
        return String.format("%1$0" + length + "x", new Object[]{new BigInteger(1, digest.digest())});
    }

    private void migrateAssets() throws IOException, NoSuchAlgorithmException {
        Path src = Paths.get(path, "assets");
        Path objects = Paths.get(src.toString(), "objects");
        if (!Files.isDirectory(src)) {
            return;
        }
        IOFileFilter filter = FileFilterUtils.notFileFilter(FileFilterUtils.or(new IOFileFilter[]{FileFilterUtils.nameFileFilter("indexes"), FileFilterUtils.nameFileFilter("objects"),
            FileFilterUtils.nameFileFilter("virtual"), FileFilterUtils.nameFileFilter("skins")}));
        for (File file : new TreeSet<>(FileUtils.listFiles(src.toFile(), TrueFileFilter.TRUE, filter))) {
            String hash = getDigest(file.toPath(), "SHA-1", 40);
            Path target = Paths.get(objects.toString(), hash.substring(0, 2), hash);
            if (Files.notExists(target)) {
                Files.copy(file.toPath(), target,
                        StandardCopyOption.COPY_ATTRIBUTES);
            }
            Files.deleteIfExists(file.toPath());
        }
        for (Path p : Files.newDirectoryStream(src)) {
            if (!p.getFileName().toString().equals("indexes") && !p.getFileName().toString()
                    .equals("objects") && !p.getFileName().toString().equals("virtual")
                    && !p.getFileName().toString().equals("skins")) {
                Files.deleteIfExists(p);
            }
        }
    }

    public Path getPath() {
        return Paths.get(path);
    }

    public String getName() {
        return name;
    }

    public CompleteVersion getVersion() {
        return mineVersion;
    }

    public String getInfo() {
        return info;
    }

    private Map<String, Path> copyMap(Map<String, String> copy) {
        Map<String, Path> res = new HashMap<>();
        for (Map.Entry<String, String> entry : copy.entrySet()) {
            res.put(entry.getKey(), Paths.get(entry.getValue()));
        }
        return res;
    }

    public List<String> getModMap() {
        return Collections.unmodifiableList(modList);
    }

    public List<String> getJarModMap() {
        return Collections.unmodifiableList(jarmodList);
    }

    public List<String> getCoreModMap() {
        return Collections.unmodifiableList(coremodList);
    }

    public CompleteLocalMinecraftForge getForge() {
        return forge;
    }

    @Override
    public String toString() {
        return "Minecraft instance " + name + " V" + mineVersion.getId() + " with path: " + path;
    }
}
