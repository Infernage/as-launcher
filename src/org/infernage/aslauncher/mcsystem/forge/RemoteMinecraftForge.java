/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.forge;

/**
 * Class used to abstract all Minecraft Forge versions.
 *
 * @author Infernage
 */
public class RemoteMinecraftForge {

    private String version;
    private String mc_version_for;
    private String url;

    public RemoteMinecraftForge(String ver, String mc, String url) {
        version = ver;
        mc_version_for = mc;
        this.url = url;
    }

    public String getForgeVersion() {
        return version;
    }

    public String getMCVersion() {
        return mc_version_for;
    }

    public String getDownloadURL() {
        return url;
    }
}
