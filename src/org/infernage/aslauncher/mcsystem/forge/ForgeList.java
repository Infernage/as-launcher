/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.forge;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.logging.Logger;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.util.NetworkUtils;
import org.infernage.aslauncher.util.References;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.tidy.Tidy;

/**
 * Class used to get all Forge versions.
 *
 * @author Infernage
 */
public final class ForgeList {

    static {
        try {
            System.loadLibrary("ASLL");
            useNatives = true;
        } catch (Throwable e) {
            Logger.getLogger(ForgeList.class.getName()).throwing(ForgeList.class.getName(), "<cinit>", e);
            useNatives = false;
        }
    }

    private static List<RemoteMinecraftForge> remote = null;
    private static volatile boolean ready = true, useNatives;

    public static List<RemoteMinecraftForge> getForgeList() throws Exception {
        if (!ready) {
            while (!ready) {
                Thread.yield();
            }
        }
        if (remote == null) {
            ready = false;
            if (useNatives) {
                remote = fetchForgeList(NetworkUtils
                        .requestGetMethod("http://files.minecraftforge.net/minecraftforge/json"));
                remote.addAll(fetchMavenForgeList(NetworkUtils
                        .requestGetMethod("http://files.minecraftforge.net/maven/net/minecraftforge/"
                                + "forge/json")));
            } else {
                remote = getCompleteForgeList();
            }
            if (remote == null) {
                remote = getCompleteForgeList();
            }
            Collections.sort(remote, new ForgeSort());
            ready = true;
        }
        return Collections.unmodifiableList(remote);
    }

    private static native List<RemoteMinecraftForge> fetchForgeList(String json) throws Exception;

    private static native List<RemoteMinecraftForge> fetchMavenForgeList(String json) throws Exception;

    @Deprecated
    private static List<RemoteMinecraftForge> getPartialForgeList(String remoteUrl, String att, String buildType)
            throws IOException {
        String response = NetworkUtils.requestGetMethod(remoteUrl);
        Tidy parser = new Tidy();
        parser.setInputEncoding("UTF-8");
        parser.setOutputEncoding("UTF-8");
        parser.setWraplen(Integer.MAX_VALUE);
        parser.setPrintBodyOnly(true);
        parser.setXmlOut(true);
        parser.setSmartIndent(true);
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        ByteArrayInputStream input = new ByteArrayInputStream(response.getBytes("UTF-8"));
        Document doc = parser.parseDOM(input, output);
        doc.getDocumentElement().normalize();
        List<RemoteMinecraftForge> result = new ArrayList<>();
        NodeList divList = doc.getElementsByTagName("div");
        for (int i = 0; i < divList.getLength(); i++) {
            Node div = divList.item(i);
            if (div.getNodeType() == 1) {
                Element divElement = (Element) div;
                if (!divElement.getAttribute(att).equals(buildType)) {
                    continue;
                }
                Node table = divElement.getElementsByTagName("table").item(0);
                if (table == null) {
                    continue;
                }
                if (table.getNodeType() == 1) {
                    Element tableElement = (Element) table;
                    NodeList trList = tableElement.getElementsByTagName("tr");
                    for (int j = 0; j < trList.getLength(); j++) {
                        Node tr = trList.item(j);
                        if (tr.getNodeType() == 1) {
                            Element build = (Element) tr;
                            NodeList data = build.getElementsByTagName("td");
                            if (data == null) {
                                continue;
                            }
                            Node td0 = data.item(0), td1 = data.item(1), td3 = data.item(3);
                            if (td0 == null || td1 == null || td3 == null) {
                                continue;
                            }
                            NodeList child0 = td0.getChildNodes(), child1 = td1.getChildNodes();
                            if (child0 == null || child1 == null) {
                                continue;
                            }
                            Node version = child0.item(0), mc_version = child1.item(0);
                            if (version == null || mc_version == null) {
                                continue;
                            }
                            String url = null;
                            if (td3.getNodeType() == 1) {
                                Element urls = (Element) td3;
                                NodeList refs = urls.getElementsByTagName("a");
                                for (int k = 0; k < refs.getLength(); k++) {
                                    String text = refs.item(k).getChildNodes().item(0).getNodeValue();
                                    if (text.equalsIgnoreCase("universal") || text
                                            .equalsIgnoreCase("client")) {
                                        String tmp = ((Element) refs.item(k + 1)).getAttribute("href");
                                        if (tmp.contains("files.minecraftforge.net")) {
                                            url = tmp;
                                        }
                                    }
                                }
                            }
                            result.add(new RemoteMinecraftForge(version.getNodeValue(), mc_version
                                    .getNodeValue(), url));
                        }
                    }
                }
            }
        }
        return result;
    }

    /**
     * Obtains the entire forge list.
     *
     * @return A list with all forge versions.
     * @throws Exception
     * @deprecated Use {@code fetchForgeList} instead.
     */
    private static List<RemoteMinecraftForge> getCompleteForgeList() throws Exception {
        List<ForgeCallable> callables = new ArrayList<>();
        callables.add(new ForgeCallable(References.MINECRAFT_FORGE, "class", "builds"));
        callables.add(new ForgeCallable(References.MINECRAFT_FORGE_OLD_VERSIONS, "id", "all_builds"));
        List<Future<List<RemoteMinecraftForge>>> list
                = CoreHandler.INSTANCE.executeCollectionTaskBlocking(callables);
        List<RemoteMinecraftForge> res = new ArrayList<>();
        for (Future<List<RemoteMinecraftForge>> future : list) {
            res.addAll(future.get());
        }
        return res;
    }

    private static class ForgeSort implements Comparator<RemoteMinecraftForge> {

        @Override
        public int compare(RemoteMinecraftForge o1, RemoteMinecraftForge o2) {
            int _1 = Integer.parseInt(o1.getForgeVersion().split("\\.")[3]),
                    _2 = Integer.parseInt(o2.getForgeVersion().split("\\.")[3]);
            return _1 > _2 ? -1 : _1 < _2 ? 1 : 0;

        }
    }

    @Deprecated
    private static class ForgeCallable implements Callable<List<RemoteMinecraftForge>> {

        private final String url, att, build;

        public ForgeCallable(String url, String att, String build) {
            this.url = url;
            this.att = att;
            this.build = build;
        }

        @Override
        public List<RemoteMinecraftForge> call() throws Exception {
            return getPartialForgeList(url, att, build);
        }

    }
}
