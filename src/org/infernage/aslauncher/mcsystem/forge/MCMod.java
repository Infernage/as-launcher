/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.forge;

import java.util.List;

/**
 * Class used to identify a Minecraft mod from http://modlist.mcf.li/
 *
 * @author Infernage
 */
public final class MCMod {

    private String name;
    private String other;
    private String link;
    private String desc;
    private List<String> author;
    private List<String> type;
    private List<String> dependencies;
    private List<String> version;

    public String getName() {
        return name;
    }

    public String getOther() {
        return other;
    }

    public String getLink() {
        return link;
    }

    public String getDesc() {
        return desc;
    }

    public List<String> getAuthor() {
        return author;
    }

    public List<String> getType() {
        return type;
    }

    public List<String> getDependencies() {
        return dependencies;
    }

    public List<String> getVersion() {
        return version;
    }
}
