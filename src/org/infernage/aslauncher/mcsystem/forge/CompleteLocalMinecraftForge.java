/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.forge;

import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.infernage.aslauncher.mcsystem.versions.CompleteVersion;
import org.infernage.aslauncher.mcsystem.versions.Library;

/**
 * Class used to abstract an installed Forge.
 *
 * @author Infernage
 */
public class CompleteLocalMinecraftForge extends CompleteVersion implements Serializable {

    private static final long serialVersionUID = 105974102;

    private String path;
    private String version, mc_version_for;

    private transient Path toPath = null;

    public CompleteLocalMinecraftForge(CompleteVersion cv, Path p, String v, String mc,
            List<Library> library, String main) {
        toPath = p;
        path = p.toString();
        version = v;
        mc_version_for = mc;
        libraries = library;
        mainClass = main;
        if (cv != null) {
            minecraftArguments = cv.getMinecraftArguments();
            minimumLauncherVersion = cv.getMinimumLauncherVersion();
            incompatibilityReason = cv.getIncompatibilityReason();
            assets = cv.getAssets();
            rules = cv.getRules();
        }
    }

    public CompleteLocalMinecraftForge(CompleteLocalMinecraftForge copy, Path newPath) {
        toPath = newPath;
        path = newPath.toString();
        version = copy.version;
        mc_version_for = copy.mc_version_for;
        libraries = copy.libraries;
        mainClass = copy.mainClass;
        minecraftArguments = copy.getMinecraftArguments();
        minimumLauncherVersion = copy.getMinimumLauncherVersion();
        incompatibilityReason = copy.getIncompatibilityReason();
        assets = copy.getAssets();
        rules = copy.getRules();
    }

    @Override
    public Collection<Library> getRelevantLibraries() {
        Collection<Library> libs = super.getRelevantLibraries();
        for (Library lib : new ArrayList<>(libs)) {
            if (lib.hasOwnURL() && lib.getURL().startsWith("http://files.minecraftforge.net/maven/")) {
                libs.remove(lib);
            }
        }
        return libs;
    }

    public Path getPath() {
        if (toPath == null) {
            toPath = Paths.get(path);
        }
        return toPath;
    }

    public String getForgeVersion() {
        return version;
    }

    public String getMCVersion() {
        return mc_version_for;
    }
}
