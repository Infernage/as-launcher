/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.forge;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.GuiEventController;
import org.infernage.aslauncher.core.concurrent.DefaultEngine;
import org.infernage.aslauncher.core.concurrent.DownloadJob;
import org.infernage.aslauncher.core.concurrent.Downloader;
import org.infernage.aslauncher.core.concurrent.ForgeEngine;
import org.infernage.aslauncher.core.concurrent.SingleEngine;
import org.infernage.aslauncher.gui.MainFrame;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.MinecraftLoader;
import org.infernage.aslauncher.mcsystem.versions.CompleteVersion;
import org.infernage.aslauncher.mcsystem.versions.Library;
import org.infernage.aslauncher.util.MessageControl;

/**
 * Class used to install Forge.
 *
 * @author Infernage
 */
public final class Installer {

    private final static Logger log = Logger.getLogger(Installer.class.getName());

    public final static void installForge(RemoteMinecraftForge forge, MCInstance instance) {
        if (!forge.getMCVersion().equals(instance.getVersion().getId())) {
            MessageControl.showErrorMessage("The selected Forge is only available for MC"
                    + " version " + forge.getMCVersion() + ".\nYou are trying to install"
                    + " in MC version " + instance.getVersion().getId() + ".", "Required"
                    + " version different");
            return;
        }
        MainFrame.getWindow().setBusy(true);
        if (instance.isLower1_6()) {
            log.info("Adapting forge");
            log.config("Instance lower than 1.6 detected");
            Path original = Paths.get(instance.getPath().toString(), instance.getVersion().getId()
                    + ".jar");
            Path copy = Paths.get(original.getParent().toString(), instance.getVersion().getId()
                    + "-copy.jar");
            try {
                if (Files.notExists(copy)) {
                    log.config("Exporting...");
                    Files.createDirectories(copy.getParent());
                    Files.copy(original, copy, StandardCopyOption.COPY_ATTRIBUTES,
                            StandardCopyOption.REPLACE_EXISTING);
                }
            } catch (Exception e) {
                log.throwing(Installer.class.getName(), "installForge", e);
                copy = original;
            }
            try {
                log.info("Removing META-INF");
                ZipFile zip = new ZipFile(original.toFile());
                List<FileHeader> headers = zip.getFileHeaders();
                List<String> metainf = new ArrayList<>();
                for (FileHeader header : headers) {
                    if (header.getFileName().contains("META-INF/")) {
                        metainf.add(header.getFileName());
                    }
                }
                for (String string : metainf) {
                    zip.removeFile(string);
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "Failed to delete META-INF from MC", e);
                try {
                    if (!original.getFileName().toString().equals(copy.getFileName().toString())) {
                        Files.deleteIfExists(original);
                        Files.move(copy, original, StandardCopyOption.COPY_ATTRIBUTES);
                    }
                } catch (Exception ex) {
                    log.throwing(Installer.class.getName(), "installForge", ex);
                }
                MainFrame.getWindow().setBusy(false);
                return;
            }
        }
        String[] split = forge.getMCVersion().split("\\.");
        log.info("Preparing to download forge");
        try {
            DownloadJob job = new DownloadJob("Forge");
            instance.removeForge();
            log.info("Creating required folders");
            Path mods = Paths.get(instance.getPath().toString(), "mods");
            if (Files.notExists(mods)) {
                Files.createDirectories(mods);
            }
            int version = Integer.parseInt(split[1]);
            if (Files.notExists(instance.getPath())) {
                Files.createDirectories(instance.getPath());
            }
            if (split[0].equals("1") && version < 6) {
                Path jars = Paths.get(instance.getPath().toString(), "jars"),
                        core = Paths.get(instance.getPath().toString(), "coremods");
                if (Files.notExists(jars)) {
                    Files.createDirectories(jars);
                }
                if (Files.notExists(core)) {
                    Files.createDirectories(core);
                }
                job.addTask(new DefaultEngine(new URL(forge.getDownloadURL()), job, instance.getPath(),
                        true));
                GuiEventController.Result result = CoreHandler.getGuiController().addJob(job,
                        "Downloading forge", CoreHandler.Priority.Normal);
                List<Path> list = result.get();
                if (!job.isSuccessfull()) {
                    throw new Exception("Download job unsuccessfull");
                }
                Path forgeFile = list.get(0);
                instance.addForge(new CompleteLocalMinecraftForge(instance.getVersion(), forgeFile,
                        forge.getForgeVersion(), forge.getMCVersion(), null,
                        MinecraftLoader.class.getCanonicalName()));
                log.info("Forge installed");
            } else {
                log.info("Acquiring forge");
                Downloader forgeLib = new SingleEngine(new URL(forge.getDownloadURL()),
                        instance.getPath(), true);
                List<Downloader> lst = new ArrayList<>();
                lst.add(forgeLib);
                List<Future<Path>> future = CoreHandler.INSTANCE.executeCollectionTask(lst);
                Path forgeRawFile = future.get(0).get(); // To store!!
                try {
                    log.info("Retrieving internal libraries");
                    ZipFile zip = new ZipFile(forgeRawFile.toFile());
                    List<FileHeader> headers = zip.getFileHeaders();
                    boolean exist = false;
                    for (FileHeader header : headers) {
                        if (header.getFileName().equals("version.json")) {
                            zip.extractFile(header, forgeRawFile.getParent().toString());
                            exist = true;
                            break;
                        }
                    }
                    if (exist) {
                        Files.deleteIfExists(Paths.get(forgeRawFile.getParent().toString(),
                                forgeRawFile.getFileName().toString().replace(".jar", ".json")));
                        Files.move(Paths.get(forgeRawFile.getParent().toString(), "version.json"),
                                Paths.get(forgeRawFile.getParent().toString(), forgeRawFile.getFileName()
                                        .toString().replace(".jar", ".json")));
                    } else {
                        log.warning("Internal libraries not found");
                    }
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Failed to obtain the version info from forge", e);
                    try {
                        Files.deleteIfExists(forgeRawFile);
                    } catch (Exception ex) {
                        log.throwing(Installer.class.getName(), "installForge", ex);
                        // Ignore
                    }
                    MainFrame.getWindow().setBusy(false);
                    return;
                }
                log.info("Retrieving JSON objects");
                Path forgeJson = Paths.get(forgeRawFile.getParent().toString(), forgeRawFile
                        .getFileName().toString().replace(".jar", ".json"));
                CompleteVersion info;
                try (BufferedReader bf = new BufferedReader(new FileReader(forgeJson.toFile()))) {
                    info = new Gson().fromJson(bf, CompleteVersion.class);
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Failed to obtain the version info from forge", e);
                    MainFrame.getWindow().setBusy(false);
                    return;
                }
                log.info("Adding libraries");
                List<Library> forgeLibs = new ArrayList<>();
                for (Library lib : info.getLibraries()) {
                    job.addTask(new ForgeEngine(job, instance, lib));
                    forgeLibs.add(lib);
                }
                GuiEventController.Result res = CoreHandler.getGuiController().addJob(job,
                        "Downloading forge libraries", CoreHandler.Priority.Normal);
                res.get();
                if (!job.isSuccessfull()) {
                    MessageControl.showErrorMessage(job.getFailTasksCount() + 
                            " forge libraries couldn't be downloaded.\nThey will be downloaded at "
                            + "Minecraft start.", "Download failed");
                }
                instance.addForge(new CompleteLocalMinecraftForge(info, forgeRawFile,
                        forge.getForgeVersion(), forge.getMCVersion(), forgeLibs,
                        info.getMainClass()));
                log.info("Forge installed");
                try {
                    Files.deleteIfExists(forgeJson);
                } catch (Exception e) {
                    // Ignore
                }
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to install forge", e);
            instance.removeForge();
        }
        MainFrame.getWindow().setBusy(false);
    }

}
