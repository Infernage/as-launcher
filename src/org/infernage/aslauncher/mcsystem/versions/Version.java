/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

/**
 * Class used to abstract the default Minecraft version behaviors.
 *
 * @author Infernage
 */
public class Version {

    protected String id;
    protected String time;
    protected String releaseTime;
    protected ReleaseType type;

    public String getId() {
        return id;
    }

    public String getTime() {
        return time;
    }

    public String getReleaseTime() {
        return releaseTime;
    }

    public ReleaseType getType() {
        return type;
    }
}
