/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

import org.infernage.aslauncher.core.CoreConfiguration.OS;
import org.infernage.aslauncher.core.CoreHandler;

/**
 * Rule to apply to an OS.
 *
 * @author Infernage
 */
public class Rule {

    public static enum Action {

        allow, disallow
    }

    private Action action = Action.allow;
    private OSAction os;

    public Action getAction() {
        if (os != null && os.name != CoreHandler.getConfiguration().getOS()) {
            return null;
        }
        return action;
    }

    private class OSAction {

        private OS name;
        private String version;
    }
}
