/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class used as JSON to get all Minecraft versions availables.
 *
 * @author Infernage
 */
public class VersionList {

    private List<Version> versions = new ArrayList<>();
    private Map<String, String> latest = new HashMap<>();

    public List<Version> getVersionList() {
        return versions;
    }

    public Map<String, String> getLatestVersion() {
        return latest;
    }
}
