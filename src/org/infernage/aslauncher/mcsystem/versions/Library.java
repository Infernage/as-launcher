/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

import java.util.List;
import java.util.Map;
import org.infernage.aslauncher.core.CoreConfiguration.OS;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.util.MCUtils;
import org.infernage.aslauncher.util.References;

/**
 * Class used to abstract a Minecraft library.
 *
 * @author Infernage
 */
public class Library {

    private String name;
    private List<Rule> rules;
    private Map<OS, String> natives;
    private ExtractRules extract;
    private String url;
    private String[] checksums;
    private String comment;
    private boolean serverreq;
    private boolean clientreq;

    /**
     * @return the checksums
     */
    public String[] getChecksums() {
        return checksums;
    }

    /**
     * @return the comment
     */
    public String getComment() {
        return comment;
    }

    /**
     * @return the serverreq
     */
    public boolean isServerreq() {
        return serverreq;
    }

    /**
     * @return the clientreq
     */
    public boolean isClientreq() {
        return clientreq;
    }

    public String getName() {
        return name;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public Map<OS, String> getNatives() {
        return natives;
    }

    public ExtractRules getExtractRules() {
        return extract;
    }

    public String getURL() {
        return url;
    }

    public String getMCURL(boolean forge) {
        if (natives != null) {
            String nativ = natives.get(CoreHandler.getConfiguration().getOS());
            if (nativ != null) {
                return forge ? References.MINECRAFT_FORGE_BASE + "maven/" + getPath(nativ) :
                        MCUtils.getURLLibraryFile(getPath(nativ));
            }
            return null;
        } else {
            return forge ? References.MINECRAFT_FORGE_BASE + "maven/" + getPath() :
                        MCUtils.getURLLibraryFile(getPath());
        }
    }

    public boolean hasOwnURL() {
        return url != null;
    }

    public boolean appliesToCurrentEnvironment() {
        if (rules == null) {
            return true;
        }
        Rule.Action lastAction = Rule.Action.disallow;
        for (Rule rule : rules) {
            Rule.Action action = rule.getAction();
            if (action != null) {
                lastAction = action;
            }
        }
        return lastAction == Rule.Action.allow;
    }

    public String getBaseDir() {
        String[] names = name.split(":");
        return String.format("%s/%s/%s", new Object[]{names[0].replaceAll("\\.", "/"), names[1], names[2]});
    }

    public String getPath() {
        return String.format("%s/%s", new Object[]{getBaseDir(), getFilename()});
    }

    public String getPath(String classifier) {
        classifier = classifier.replace("${arch}", System.getProperty("sun.arch.data.model"));
        return String.format("%s/%s", new Object[]{getBaseDir(), getFilename(classifier)});
    }

    public String getFilename() {
        String[] names = name.split(":");
        return String.format("%s-%s.jar", new Object[]{names[1], names[2]});
    }

    public String getFilename(String classifier) {
        String[] names = name.split(":");
        return String.format("%s-%s-%s.jar", new Object[]{names[1], names[2], classifier});
    }
}
