/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.infernage.aslauncher.core.CoreConfiguration.OS;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.concurrent.ChecksumEngine;
import org.infernage.aslauncher.core.concurrent.DownloadJob;
import org.infernage.aslauncher.core.concurrent.Downloader;
import org.infernage.aslauncher.core.concurrent.DualURLEngine;
import org.infernage.aslauncher.mcsystem.forge.CompleteLocalMinecraftForge;

/**
 * Class used to abstract a Minecraft version.
 *
 * @author Infernage
 */
public class CompleteVersion extends Version {

    protected String minecraftArguments;
    protected List<Library> libraries;
    protected String mainClass;
    protected int minimumLauncherVersion;
    protected String incompatibilityReason;
    protected String assets;
    protected List<Rule> rules;

    public int getMinimumLauncherVersion() {
        return minimumLauncherVersion;
    }

    public List<Library> getLibraries() {
        return libraries;
    }

    public List<Rule> getRules() {
        return rules;
    }

    public String getMinecraftArguments() {
        return minecraftArguments;
    }

    public String getMainClass() {
        return mainClass;
    }

    public String getIncompatibilityReason() {
        return incompatibilityReason;
    }

    public String getAssets() {
        return assets;
    }

    public Collection<Library> getRelevantLibraries() {
        List relevant = new ArrayList();
        for (Library library : libraries) {
            if (library.appliesToCurrentEnvironment()) {
                relevant.add(library);
            }
        }
        return relevant;
    }

    public Collection<Path> getClassPath(Path base) {
        Collection<Library> libraries = getRelevantLibraries();
        List<Path> classPaths = new ArrayList();
        for (Library library : libraries) {
            if (library.getNatives() == null) {
                classPaths.add(base.resolve("libraries").resolve(library.getPath()));
            }
        }
        return classPaths;
    }

    public Set<Downloader> getRequiredDownloads(Path target, DownloadJob job) {
        Set downloads = new HashSet();
        for (Library library : getRelevantLibraries()) {
            String file = null;
            if (library.getNatives() != null) {
                String natives = library.getNatives().get(CoreHandler.getConfiguration().getOS());
                if (natives != null) {
                    file = library.getPath(natives);
                }
            } else {
                file = library.getPath();
            }
            if (file != null) {
                Path local = Paths.get(target.toString(), "libraries", file);
                try {
                    if (!Files.isRegularFile(local) || !library.hasOwnURL()) {
                        downloads.add(
                                new DualURLEngine(new ChecksumEngine(new URL(library
                                                        .getMCURL(this instanceof CompleteLocalMinecraftForge)),
                                                job, local, false), new ChecksumEngine(new URL(library
                                                        .getMCURL(!(this instanceof CompleteLocalMinecraftForge))),
                                                job, local, false), job, local, false)
                        );
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return downloads;
    }

    public boolean appliesToCurrentEnvironment() {
        if (rules == null) {
            return true;
        }
        Rule.Action lastAction = Rule.Action.disallow;
        for (Rule rule : rules) {
            Rule.Action action = rule.getAction();
            if (action != null) {
                lastAction = action;
            }
        }
        return lastAction == Rule.Action.allow;
    }
}
