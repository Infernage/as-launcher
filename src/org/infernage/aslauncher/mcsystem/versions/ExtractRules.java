/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

import java.util.ArrayList;
import java.util.List;

/**
 * Class used to abstract the extraction rules for Minecraft libraries.
 *
 * @author Infernage
 */
public class ExtractRules {

    private List<String> excludes = new ArrayList<>();

    public List<String> getRules() {
        return excludes;
    }

    public boolean shouldExtract(String path) {
        if (excludes != null) {
            for (String exclude : excludes) {
                if (path.startsWith(exclude)) {
                    return false;
                }
            }
        }
        return true;
    }
}
