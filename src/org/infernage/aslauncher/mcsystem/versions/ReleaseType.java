/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

/**
 * Enum used to identify all Minecraft version types.
 *
 * @author Infernage
 */
public enum ReleaseType {

    snapshot("snapshot"),
    release("release"),
    old_beta("old-beta"),
    old_alpha("old-alpha");

    private final String name;

    private ReleaseType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
