/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.versions;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class used as JSON for the Minecraft assets.
 *
 * @author Infernage
 */
public class AssetHashList {

    private Map<String, AssetPair> objects;
    private boolean virtual;

    public AssetHashList() {
        objects = new HashMap<>();
    }

    public Map<String, AssetPair> getIndex() {
        return objects;
    }

    public Set<AssetPair> getValues() {
        return new HashSet<>(objects.values());
    }

    public boolean isVirtual() {
        return virtual;
    }

    public class AssetPair {

        private String hash;
        private long size;

        public String getHash() {
            return hash;
        }

        public long getSize() {
            return size;
        }
    }
}
