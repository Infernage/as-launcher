/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;
import org.infernage.aslauncher.gui.MainFrame;
import org.infernage.aslauncher.util.IO;
import org.infernage.aslauncher.util.MessageControl;
import org.infernage.aslauncher.core.Systray;
import org.infernage.aslauncher.gui.ExternalConsole;

/**
 * Class used to execute a callback after the execution of Minecraft.
 *
 * @author Infernage
 */
public class MinecraftEndAction {

    private static final double TIMEOUT = 1.8e12D;
    private static final double WARNING = 3e11D;

    private Profile profile;
    private Process minecraft_process;
    private final TimerTask task;
    private long start = 0;
    private Path nativeDir;

    /**
     * Default constructor.
     *
     * @param prof The profile where the Minecraft will be executed.
     */
    public MinecraftEndAction(Profile prof) {
        profile = prof;
        task = new TimerTask() {
            @Override
            public void run() {
                profile.addPlayed(1e9D);
                if (profile.isTimeout() || profile.getPlayed() + (start - System.nanoTime()) >= TIMEOUT) {
                    minecraft_process.destroy();
                }
                if (profile.getPlayed() % WARNING == 0) {
                    BigDecimal restant = new BigDecimal(TIMEOUT)
                            .subtract(new BigDecimal(profile.getPlayed()));
                    BigDecimal min = restant.divide(new BigDecimal(6e10D), 0, RoundingMode.DOWN);
                    BigDecimal sec = new BigDecimal(restant.toBigInteger().mod(new BigDecimal(6e10D).toBigInteger()));
                    Systray.imprMessage("Time left", "The demo will finish in " + min + ":" + sec);
                }
            }
        };
    }

    /**
     * Sets the Minecraft process to the callback.
     *
     * @param process The Minecraft process.
     * @param natives The Path to the natives.
     */
    public void setProcess(Process process, Path natives) {
        start = System.nanoTime();
        minecraft_process = process;
        nativeDir = natives;
        Systray.setProcess(process);
        if (!profile.isPremium()) {
            Timer timer = new Timer(false);
            timer.scheduleAtFixedRate(task, 0, 1000);
        }
    }

    /**
     * Starts the callback listening. This function will block the current
     * thread.
     */
    public void startListening() {
        if (minecraft_process == null) {
            throw new NullPointerException("Process is null");
        }
        if (!profile.getAction().equals(Profile.actions[2])) {
            MainFrame.getWindow().setVisible(false);
            ExternalConsole.getConsole().setVisible(true);
        }
        boolean finish = false;
        while (!finish) {
            try {
                minecraft_process.waitFor();
                finish = true;
            } catch (Exception e) {
                Logger.getLogger(this.getClass().getName()).throwing(this.getClass().getName(),
                        "startListening", e);
                // Interrupted?
                if (Thread.currentThread().isInterrupted()){
                    finish = true;
                    minecraft_process.destroy();
                }
            }
        }
        task.cancel();
        if (profile.isTimeout()) {
            MessageControl.showInfoMessage("The demo has been finished. If you want to"
                    + " play without limits, buy the game.", "Demo finished");
        }
        try {
            Thread.sleep(500);
        } catch (Exception e) {
            // Ignore
        }
        try {
            IO.delete(nativeDir);
        } catch (Exception e) {
            Logger.getLogger(this.getClass().getName()).throwing(this.getClass().getName(),
                    "startListening", e);
        }
        if (profile.getAction().equals(Profile.actions[0])) {
            try {
                System.exit(0);
            } catch (SecurityException e) {
                // Ignore, special halting in course
            }
        } else if (profile.getAction().equals(Profile.actions[1])) {
            MainFrame.getWindow().setVisible(true);
            ExternalConsole.getConsole().setVisible(false);
        }
    }
}
