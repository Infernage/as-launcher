/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

/**
 * Class used to request a login through the Yggdrassil method.
 *
 * @author Infernage
 */
class LoginRequest {

    private Agent agent;
    private String username;
    private String password;
    private String clientToken;

    public LoginRequest(String user, String password, String token) {
        agent = Agent.MINECRAFT;
        username = user;
        clientToken = token;
        this.password = password;
    }
}
