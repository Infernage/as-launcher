/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

/**
 * Enum used to differentiate the system types.
 *
 * @author Infernage
 */
public enum SystemType {

    LEGACY("legacy"), MOJANG("mojang");

    private final String name;

    private SystemType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
