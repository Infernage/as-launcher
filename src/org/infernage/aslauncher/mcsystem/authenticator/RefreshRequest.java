/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

/**
 * Class used to refresh a login through Yggdrassil method.
 *
 * @author Infernage
 */
class RefreshRequest {

    private String clientToken;
    private String accessToken;
    private ProfileResponse selectedProfile;

    public RefreshRequest(String client, String access, ProfileResponse profile) {
        clientToken = client;
        accessToken = access;
        selectedProfile = profile;
    }
}
