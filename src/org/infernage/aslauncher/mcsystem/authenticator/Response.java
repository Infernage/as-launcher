/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

/**
 * Response from the Mojang server.
 *
 * @author Infernage
 */
class Response {

    private String accessToken;
    private String clientToken;
    private ProfileResponse selectedProfile;
    private ProfileResponse[] availableProfiles;
    private User user;

    public String getAccessToken() {
        return accessToken;
    }

    public String getClientToken() {
        return clientToken;
    }

    public ProfileResponse getSelectedProfile() {
        return selectedProfile;
    }

    public ProfileResponse[] getAvailableProfiles() {
        return availableProfiles;
    }

    public User getUser(){ return user; }
}
