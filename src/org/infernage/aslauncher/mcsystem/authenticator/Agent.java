/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

/**
 * Class used in the authentication.
 * @author Infernage
 */
class Agent {
    public static final Agent MINECRAFT = new Agent("Minecraft", 1);
    private final String name;
    private final int version;
    
    public Agent(String n, int v){
        name = n;
        version = v;
    }
    
    public String getName(){ return name; }
    
    public int getVersion(){ return version; }
}
