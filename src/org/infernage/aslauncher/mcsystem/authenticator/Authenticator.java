/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.crypto.Crypto;
import org.infernage.aslauncher.core.crypto.SuiteCrypto;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.util.References;

/**
 * Class used to authenticate in Minecraft.
 *
 * @author Infernage
 */
public final class Authenticator {
    private static final Logger log = Logger.getLogger(Authenticator.class.getName());

    /**
     * Overloaded method.
     *
     * @param profile Profile to login.
     * @throws Exception If it couldn't be authenticated.
     */
    public static void login(Profile profile) throws Exception {
        log.config("Decoding pass");
        Crypto aes = SuiteCrypto.getSuite(SuiteCrypto.Type.AES256);
        aes.setPassword(profile.getUsername() + "_" + profile.getSessionID());
        String key = aes.decryptData(profile.getKey());
        profile.setAccessToken(login(profile.getUsername(), key, profile.getPath())
                .getAccessToken());
    }

    /**
     * Login method.
     *
     * @param username The Minecraft user.
     * @param key The password.
     * @param instances The profile path.
     * @return The profile created.
     * @throws Exception If it couldn't be authenticated.
     */
    public static Profile login(String username, String key, Path instances) throws Exception {
        try {
            log.config("Authenticating...");
            LoginRequest request = new LoginRequest(username, key, CoreHandler.getConfiguration()
                    .getUUID().toString());
            Response response = makeRequest(new URL(References.AUTHENTICATION_URL), request,
                    Response.class);
            if (response == null) {
                throw new RuntimeException("Response is null!");
            }
            log.config("Assigning profile responses");
            ProfileResponse profileGame = response.getSelectedProfile();
            if (profileGame == null && response.getAvailableProfiles().length <= 0) {
                throw new RuntimeException("User has no ProfileResponse!");
            } else if (profileGame == null) {
                profileGame = response.getAvailableProfiles()[0];
            }
            log.config("Dumping crypted key to profile");
            Crypto crypto = SuiteCrypto.getSuite(SuiteCrypto.Type.AES256);
            crypto.setPassword(profileGame.getName() + "_" + profileGame.getId());
            String data = crypto.encryptData(key);
            System.out.println(data);
            System.out.println(crypto.encryptData(key));
            return new Profile(profileGame, data, profileGame.getId(),
                    response.getAccessToken(), instances, profileGame.isLegacy() ? SystemType.LEGACY :
                            SystemType.MOJANG);
        } catch (Exception e) {
            log.throwing(Authenticator.class.getName(), "login", e);
            if (e.toString().contains("403 for URL: https://authserver.mojang.com/authenticate")){
                throw new Exception("Username or password incorrect", e);
            }
            throw new Exception("Login failed", e);
        }
    }

    /**
     * Refresh an existing profile. (Only with Yggdrassil method.)
     *
     * @param profile The profile to refresh.
     * @throws Exception If an error happens.
     */
    public static void refresh(Profile profile) throws Exception {
        if (!profile.getSessionToken().isEmpty()) {
            RefreshRequest request = new RefreshRequest(CoreHandler.getConfiguration().getUUID()
                    .toString(), profile.getAccessToken(), null);
            Response response = makeRequest(new URL(References.AUTHENTICATION_REFRESH), request,
                    Response.class);
            if (!response.getClientToken().equals(CoreHandler.getConfiguration().getUUID().toString())) {
                throw new Exception("Server requested to change client UUID");
            }
            profile.setAccessToken(response.getAccessToken());
        } else {
            login(profile);
        }
    }

    private static Response makeRequest(URL url, Object input, Class type) throws IOException {
        Gson gson = new Gson();
        String post = doPost(url, gson.toJson(input), "application/json");
        return (Response) gson.fromJson(post, type);
    }

    private static String doPost(URL url, String json, String content) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        byte[] parameters = json.getBytes(Charset.forName("UTF-8"));
        connection.setConnectTimeout(15000);
        connection.setReadTimeout(15000);
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", content + "; charset=utf-8");
        connection.setRequestProperty("Content-Lenght", "" + parameters.length);
        connection.setRequestProperty("Content-Language", "en-US");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        try (DataOutputStream writer = new DataOutputStream(connection.getOutputStream())) {
            writer.write(parameters);
            writer.flush();
        }
        if (connection.getResponseCode() != 200){
            StringBuilder error = new StringBuilder();
            try(BufferedReader reader = new BufferedReader(new InputStreamReader(connection
                    .getErrorStream()))){
                String lane;
                while((lane = reader.readLine()) != null){
                    error.append(lane).append("\r");
                }
                log.log(Level.CONFIG, "Refresh error: {0}", error);
            }
        }
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String lane;
            while ((lane = reader.readLine()) != null) {
                builder.append(lane).append('\r');
            }
        }
        return builder.toString();
    }
}
