/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.mcsystem.authenticator;

/**
 * The game profile of a premium account.
 *
 * @author Infernage
 */
public class ProfileResponse {
    
    private final String id;
    private final String name;
    private boolean legacy;

    ProfileResponse(String i, String n) {
        if (i != null && !i.isEmpty()) {
            int length = i.length();
            int j = 0;
            while (j < length) {
                if (!Character.isWhitespace(i.charAt(j))) {
                    break;
                }
                j++;
            }
            if (j == length) {
                throw new RuntimeException("Profile whitespaced!");
            }
        }
        if (n != null && !n.isEmpty()) {
            int length = n.length();
            int j = 0;
            while (j < length) {
                if (!Character.isWhitespace(n.charAt(j))) {
                    break;
                }
                j++;
            }
            if (j == length) {
                throw new RuntimeException("Profile whitespaced!");
            }
        }
        id = i;
        name = n;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    
    public boolean isLegacy(){
        return legacy;
    }
}
