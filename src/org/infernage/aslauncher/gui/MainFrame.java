/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.gui;

import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.FutureCallback;
import com.google.gson.GsonBuilder;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.event.HyperlinkEvent;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.events.LogEvent;
import org.infernage.aslauncher.core.events.TweenEvent;
import org.infernage.aslauncher.core.loaders.MCLoader;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.MineFont;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.mcsystem.authenticator.Authenticator;
import org.infernage.aslauncher.mcsystem.forge.ForgeList;
import org.infernage.aslauncher.mcsystem.forge.MCMod;
import org.infernage.aslauncher.mcsystem.forge.RemoteMinecraftForge;
import org.infernage.aslauncher.util.ASLUtils;
import org.infernage.aslauncher.util.IO;
import org.infernage.aslauncher.util.MCUtils;
import org.infernage.aslauncher.util.MessageControl;
import org.infernage.aslauncher.util.References;
import org.infernage.aslauncher.core.Systray;
import org.jdesktop.swingx.painter.BusyPainter;

/**
 * Main GUI.
 *
 * @author Infernage
 */
public class MainFrame extends javax.swing.JFrame {

    private static MainFrame singleton = null;
    private static final Logger log = Logger.getLogger(MainFrame.class.getName());

    private final Object lock = new Object();
    private volatile int busyCounter = 0;
    private volatile boolean reverse = true;
    private Image icon;
    private Profile selected = null;
    private MCInstance instance = null;
    private SimpleAttributeSet attrs = new SimpleAttributeSet();
    private MainFrameController controller = new MainFrameController();
    private volatile boolean isRefreshing = false, working;

    /**
     * Creates new form MainFrame
     */
    private MainFrame(String version) throws IOException {
        this.icon = new ImageWrapper("/org/infernage/aslauncher/resources/bg_authenticator.png").get();
        initComponents();
        jMenu1.setVisible(false);
        log.config("Components initialized");
        setTitle("AS Launcher " + version);
        setIconImage(new ImageWrapper("/org/infernage/aslauncher/resources/5547.png").get());
        setLocationRelativeTo(null);
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                // Init MC blog.
                log.config("Requesting MC blog");
                try {
                    page.setPage(References.MINECRAFT_BLOG);
                } catch (Exception e) {
                    log.throwing(this.getClass().getName(), "run", e);
                    page.setText("<html><body><font color=\"#808080\"><br><br><br><br><br><br><br><center>"
                            + "<h1>Failed to get page</h1><br>" + e.toString() + "</center></font></body></html>");
                }
                // Init changelog.
                log.config("Requesting changelog");
                StringBuilder builder = new StringBuilder();
                try {
                    HttpURLConnection connection = (HttpURLConnection) new URL("https://dl.dropbox.com/s/v7lh0cr7ogus0fk/Changelog.txt").openConnection();
                    connection.setRequestMethod("GET");
                    try (BufferedReader bf = new BufferedReader(new InputStreamReader(connection.getInputStream(),
                            "iso-8859-1"))) {
                        String line = bf.readLine();
                        while (line != null) {
                            builder.append("\n").append(line);
                            line = bf.readLine();
                        }
                    }
                } catch (Exception e) {
                    builder.delete(0, builder.length());
                    builder.append("Failed to load changelog.\nReason: ").append(e.toString());
                    log.throwing(this.getClass().getName(), "run", e);
                }
                changelog.setText(builder.toString());
            }
        });
        init();
        ((DefaultCaret) guiConsole.getCaret()).setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
        printMsg("Ready");
        CoreHandler.INSTANCE.subscribe(this);
    }

    /**
     * Gets this gui JProgressBar.
     *
     * @return The main JProgressBar.
     */
    public JProgressBar getProgressBar() {
        return mainBar;
    }

    public void setWorking(boolean value) {
        working = value;
        if (working) {
            jTabbedPane1.setEnabled(false);
            createInstance.setEnabled(false);
            playButton.setEnabled(false);
            importButton.setEnabled(false);
            exportButton.setEnabled(false);
            savesButton.setEnabled(false);
            editNameButton.setEnabled(false);
            moreButton.setEnabled(false);
            deleteButton.setEnabled(false);
            instanceList.setEnabled(false);
            jTabbedPane2.setEnabled(false);
            modsList.setEnabled(false);
            addModsFolderButton.setEnabled(false);
            removeModsFolderButton.setEnabled(false);
            coremodsList.setEnabled(false);
            addCoremodsButton.setEnabled(false);
            removeCoremodsButton.setEnabled(false);
            jarList.setEnabled(false);
            addJarModsButton.setEnabled(false);
            removeJarModsButton.setEnabled(false);
            forgeIButton.setEnabled(false);
            forgeAButton.setEnabled(false);
            forgeTable.setEnabled(false);
        } else {
            jTabbedPane1.setEnabled(true);
            createInstance.setEnabled(true);
            playButton.setEnabled(true);
            importButton.setEnabled(true);
            exportButton.setEnabled(true);
            savesButton.setEnabled(true);
            editNameButton.setEnabled(true);
            moreButton.setEnabled(true);
            deleteButton.setEnabled(true);
            instanceList.setEnabled(true);
            jTabbedPane2.setEnabled(true);
            modsList.setEnabled(true);
            addModsFolderButton.setEnabled(true);
            removeModsFolderButton.setEnabled(true);
            coremodsList.setEnabled(true);
            addCoremodsButton.setEnabled(true);
            removeCoremodsButton.setEnabled(true);
            jarList.setEnabled(true);
            addJarModsButton.setEnabled(true);
            removeJarModsButton.setEnabled(true);
            forgeIButton.setEnabled(true);
            forgeAButton.setEnabled(true);
            forgeTable.setEnabled(true);
        }
    }

    /**
     * Requests an full gui refresh.
     *
     * @param prof The profile to select.
     */
    public void refresh(Profile prof) {
        if (prof == null || isRefreshing) {
            return;
        }
        isRefreshing = true;
        selected = prof;
        internalRefresh(prof);
        refreshProfiles();
        profileBox.setSelectedItem(prof.getProfilename());
        isRefreshing = false;
    }

    /**
     * Refresh the profiles combo box.
     */
    public void refreshProfiles() {
        log.fine("Refreshing profiles");
        profileBox.removeAllItems();
        profileBox.addItem("Select a profile");
        for (Profile prof : CoreHandler.getConfiguration().getProfiles()) {
            profileBox.addItem(prof.getProfilename());
        }
    }

    /**
     * Changes the selected tab.
     *
     * @param index The index to set.
     */
    public void setSelectedTab(int index) {
        jTabbedPane1.setSelectedIndex(index);
    }

    /**
     * Shows a message into the busy label.
     *
     * @param msg The message to show.
     */
    public void printMsg(String msg) {
        if (msg.isEmpty()) {
            msg = "Ready";
        }
        statusLabel.setForeground(Color.green);
        statusLabel.setText(msg);
        statusLabel.setToolTipText(msg);
    }

    /**
     * Shows an error into the busy label.
     *
     * @param error The error to show.
     */
    public void printErrorMsg(String error) {
        if (error.isEmpty()) {
            error = "Error";
        }
        statusLabel.setForeground(Color.red);
        statusLabel.setText(error);
        statusLabel.setToolTipText(error);
    }

    /**
     * Shows a warning into the busy label.
     *
     * @param msg The warning to show.
     */
    public void printWarningMsg(String msg) {
        if (msg.isEmpty()) {
            msg = "Warning";
        }
        statusLabel.setForeground(Color.yellow);
        statusLabel.setText(msg);
        statusLabel.setToolTipText(msg);
    }

    /**
     * Sets the gui as busy or not.
     *
     * @param value {@code true} or {@code false}
     */
    public void setBusy(boolean value) {
        if (value && statusLabel.isBusy()) {
            busyCounter++;
        } else if (!value && statusLabel.isBusy()) {
            if (busyCounter > 0) {
                busyCounter--;
            } else {
                statusLabel.setBusy(value);
            }
        } else {
            statusLabel.setBusy(value);
        }
    }

    /**
     * Creates the singleton Object.
     *
     * @param version The program version.
     * @return The MainFrame created.
     */
    public static MainFrame create(String version) throws IOException {
        if (singleton == null) {
            singleton = new MainFrame(version);
            singleton.dynamicResize();
        }
        try {
            Thread.sleep(800);
        } catch (Exception e) {
            // Simply ignore
        }
        return singleton;
    }

    /**
     * Obtains the MainFrame instance created.
     *
     * @return The MainFrame instance.
     */
    public static MainFrame getWindow() {
        return singleton;
    }

    /**
     * Shows a log into the gui console.
     *
     * @param event The event log to display.
     */
    @Subscribe
    public void doLog(LogEvent event) {
        synchronized (this) {
            Color tmp = null;
            switch (event.type.intValue()) {
                case 800:
                    StyleConstants.setBold(attrs, true);
                    break;
                case 1000:
                    tmp = StyleConstants.getForeground(attrs);
                    StyleConstants.setForeground(attrs, Color.red);
                    StyleConstants.setBold(attrs, true);
                    break;
                case 900:
                    tmp = StyleConstants.getForeground(attrs);
                    StyleConstants.setForeground(attrs, Color.orange);
                    StyleConstants.setBold(attrs, true);
                    break;
            }
            try {
                guiConsole.getStyledDocument().insertString(guiConsole.getStyledDocument().getLength(),
                        event.log, attrs);
            } catch (BadLocationException ex) {
                log.throwing(this.getClass().getName(), "doLog", ex);
            }
            switch (event.type.intValue()) {
                case 800:
                    StyleConstants.setBold(attrs, false);
                    break;
                case 1000:
                    StyleConstants.setForeground(attrs, tmp);
                    StyleConstants.setBold(attrs, false);
                    break;
                case 900:
                    StyleConstants.setBold(attrs, false);
                    StyleConstants.setForeground(attrs, tmp);
                    break;
            }
        }
    }

    private void dynamicResize() {
        if (showConsoleButton.getText().contains("Show")) {
            showConsoleButton.setText("Hide console");
        } else {
            showConsoleButton.setText("Show console");
        }
        CoreHandler.INSTANCE.sendEvent(new TweenEvent(reverse));
        reverse = !reverse;
    }

    private void init() {
        jXHyperlink1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        statusLabel.setToolTipText("Loading events");
        BusyPainter painter = new BusyPainter(new Rectangle2D.Float(0, 0, 4.0F, 4.0F),
                new Rectangle2D.Float(2.25F, 2.25F, 13.5F, 13.5F));
        painter.setTrailLength(4);
        painter.setPoints(8);
        painter.setFrame(-1);
        painter.setBaseColor(Color.cyan);
        painter.setHighlightColor(Color.orange);
        statusLabel.setIcon(new ImageIcon(new BufferedImage(19, 19, BufferedImage.TYPE_INT_ARGB)));
        statusLabel.setBusyPainter(painter);
        log.config("Dumping init method");
        jTabbedPane1.remove(4);
        jTabbedPane1.remove(3);
        for (int i = 0; i < 4; i++) {
            jTabbedPane2.remove(1);
        }
        for (Profile prof : CoreHandler.getConfiguration().getProfiles()) {
            profileBox.addItem(prof.getProfilename());
        }
        controller.requestForgeList(jLabel19);
        moduleList.setModel(new DefaultListModel());
        instanceList.setModel(new DefaultListModel());
        jarList.setModel(new DefaultListModel());
        modsList.setModel(new DefaultListModel());
        coremodsList.setModel(new DefaultListModel());
        mcModList.setModel(new DefaultListModel());
    }

    private void internalRefresh(Profile pro) {
        log.config("Refreshing internal data");
        if (pro.isLogged() && !working) {
            if (!pro.isAlreadyLogged()) {
                jTabbedPane1.add("Profile Editor", profileEditorPanel);
                jTabbedPane1.add("MC Editor", mcEditorPanel);
            }
            manualLogginButton.setEnabled(!pro.isPremium());
            selectedProfileLabel.setText(selected.getProfilename());
            ramBox.setSelectedItem(selected.getMaxRam());
            gameDirField.setText(selected.getPath().toAbsolutePath().toString());
            profileNameField.setText(selected.getProfilename());
            actionBox.setSelectedItem(selected.getAction());
            autoLogin.setSelected(selected.isAutoLogin());
            if (selected.isAdvanced()) {
                advancedSettings.setSelected(true);
                javaExecField.setEnabled(true);
                javaArgsField.setEnabled(true);
                javaExecField.setText(selected.getJavaExec());
                javaArgsField.setText(selected.getJavaArgs());
                DefaultListModel model = (DefaultListModel) moduleList.getModel();
                model.removeAllElements();
                for (String key : selected.getInternalModules().keySet()) {
                    model.addElement(key);
                }
                moduleList.setModel(model);
            }
            DefaultListModel model = (DefaultListModel) instanceList.getModel();
            model.removeAllElements();
            for (MCInstance inst : selected.getList()) {
                model.addElement(inst.getName());
            }
            instanceList.setModel(model);
            importButton.setEnabled(true);
        } else if(!pro.isLogged()) {
            disableMCComponents();
        }
    }

    private void disableMCComponents() {
        if (working) return;
        jTabbedPane1.setSelectedIndex(2);
        if (jTabbedPane1.getComponentCount() >= 4) {
            jTabbedPane1.remove(4);
            jTabbedPane1.remove(3);
        }
        manualLogginButton.setEnabled(false);
        selectedProfileLabel.setText("");
        gameDirField.setText("");
        profileNameField.setText("");
        autoLogin.setSelected(false);
        advancedSettings.setSelected(false);
        javaArgsField.setEnabled(false);
        javaExecField.setEnabled(false);
        javaExecField.setText("");
        javaArgsField.setText("");
        DefaultListModel model = (DefaultListModel) moduleList.getModel();
        model.removeAllElements();
        moduleList.setModel(model);
        model = (DefaultListModel) instanceList.getModel();
        model.removeAllElements();
        instanceList.setModel(model);
        instanceInfo.setText("");
        playButton.setEnabled(false);
        savesButton.setEnabled(false);
        importButton.setEnabled(false);
        exportButton.setEnabled(false);
        deleteButton.setEnabled(false);
        infoButton.setEnabled(false);
        editNameButton.setEnabled(false);
        moreButton.setEnabled(false);
        int cc = jTabbedPane2.getComponentCount();
        for (int i = 0; i < cc - 1; i++) {
            jTabbedPane2.remove(1);
        }
        DefaultTableModel fmodel = (DefaultTableModel) forgeTable.getModel();
        fmodel.setRowCount(0);
        forgeTable.setModel(fmodel);
        jTabbedPane2.setEnabled(false);
    }

    private void login() {
        if (profileBox.getSelectedItem().equals("Select a profile") || new String(passwordField
                .getPassword()).isEmpty()) {
            return;
        }
        Profile pro = controller.login((String) profileBox.getSelectedItem(),
                new String(passwordField.getPassword()));
        passwordField.setText("");
        if (pro == null) {
            return;
        }
        refresh(pro);
        selected = pro;
    }

    private void logout() {
        if (controller.isBusy()) {
            return;
        }
        if (selected == null) {
            log.fine("No profile selected");
            return;
        }
        log.fine("Logged out");
        selected.setLogged(false);
        internalRefresh(selected);
        selected = null;
        instance = null;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        additionalOpts = new javax.swing.JPopupMenu();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenuItem4 = new javax.swing.JMenuItem();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        mcBlogPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        page = new javax.swing.JTextPane();
        changelogPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        changelog = new javax.swing.JTextArea();
        authenticatorPanel = new Background(new ImageWrapper("/org/infernage/aslauncher/resources/bg_authenticator.png").get());
        jPanel5 = new javax.swing.JPanel();
        profileBox = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        loginButton = new javax.swing.JButton();
        createProfileButton = new javax.swing.JButton();
        profileEditorPanel = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        selectedProfileLabel = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        ramBox = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        gameDirField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        profileNameField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        actionBox = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        advancedSettings = new javax.swing.JCheckBox();
        javaExecField = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        javaArgsField = new javax.swing.JTextField();
        autoLogin = new javax.swing.JCheckBox();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        moduleList = new javax.swing.JList();
        activeOption = new javax.swing.JCheckBox();
        logoutButton = new javax.swing.JButton();
        deleteProfButton = new javax.swing.JButton();
        saveSettingsButton = new javax.swing.JButton();
        manualLogginButton = new javax.swing.JButton();
        mcEditorPanel = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        instanceList = new javax.swing.JList();
        createInstance = new javax.swing.JButton();
        jSeparator3 = new javax.swing.JSeparator();
        jScrollPane5 = new javax.swing.JScrollPane();
        instanceInfo = new javax.swing.JTextArea();
        playButton = new javax.swing.JButton();
        importButton = new javax.swing.JButton();
        savesButton = new javax.swing.JButton();
        infoButton = new javax.swing.JButton();
        deleteButton = new javax.swing.JButton();
        jLabel21 = new javax.swing.JLabel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        forgePanel = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        forgeTable = new javax.swing.JTable();
        jLabel19 = new javax.swing.JLabel();
        forgeIButton = new javax.swing.JButton();
        jLabel20 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        forgeAButton = new javax.swing.JButton();
        mcModListPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jXHyperlink1 = new org.jdesktop.swingx.JXHyperlink();
        jScrollPane11 = new javax.swing.JScrollPane();
        mcModList = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        authorLabel = new javax.swing.JLabel();
        typeLabel = new javax.swing.JLabel();
        urlLabel = new org.jdesktop.swingx.JXHyperlink();
        descButton = new javax.swing.JButton();
        modsPanel = new javax.swing.JPanel();
        jScrollPane9 = new javax.swing.JScrollPane();
        modsList = new javax.swing.JList();
        addModsFolderButton = new javax.swing.JButton();
        removeModsFolderButton = new javax.swing.JButton();
        jarPanel = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jarList = new javax.swing.JList();
        addJarModsButton = new javax.swing.JButton();
        removeJarModsButton = new javax.swing.JButton();
        coremodsPanel = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        coremodsList = new javax.swing.JList();
        addCoremodsButton = new javax.swing.JButton();
        removeCoremodsButton = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel23 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        editNameButton = new javax.swing.JButton();
        exportButton = new javax.swing.JButton();
        moreButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        showConsoleButton = new javax.swing.JButton();
        aboutPanel = new javax.swing.JPanel();
        jScrollPane10 = new javax.swing.JScrollPane();
        guiConsole = new javax.swing.JTextPane();
        mainBar = new javax.swing.JProgressBar();
        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        statusLabel = new org.jdesktop.swingx.JXBusyLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        jMenuItem3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/duplicate.png"))); // NOI18N
        jMenuItem3.setToolTipText("Duplicate the selected instance");
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem3ActionPerformed(evt);
            }
        });
        additionalOpts.add(jMenuItem3);

        jMenuItem4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/clean_up.png"))); // NOI18N
        jMenuItem4.setToolTipText("Cleans up the environment for this profile");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        additionalOpts.add(jMenuItem4);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(710, 479));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jTabbedPane1.setMinimumSize(new java.awt.Dimension(650, 351));

        page.setEditable(false);
        page.setBackground(java.awt.Color.darkGray);
        page.setContentType("text/html"); // NOI18N
        page.setText("<html><body><font color=\\\"#808080\\\"><br><br><br><br><br><br><br><center><h1>Loading page..</h1></center></font></body></html>");
        page.setMargin(null);
        page.addHyperlinkListener(new javax.swing.event.HyperlinkListener() {
            public void hyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {
                pageHyperlinkUpdate(evt);
            }
        });
        jScrollPane1.setViewportView(page);

        javax.swing.GroupLayout mcBlogPanelLayout = new javax.swing.GroupLayout(mcBlogPanel);
        mcBlogPanel.setLayout(mcBlogPanelLayout);
        mcBlogPanelLayout.setHorizontalGroup(
            mcBlogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 679, Short.MAX_VALUE)
        );
        mcBlogPanelLayout.setVerticalGroup(
            mcBlogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("MC Blog", mcBlogPanel);

        changelog.setEditable(false);
        changelog.setColumns(20);
        changelog.setLineWrap(true);
        changelog.setRows(5);
        changelog.setWrapStyleWord(true);
        jScrollPane2.setViewportView(changelog);

        javax.swing.GroupLayout changelogPanelLayout = new javax.swing.GroupLayout(changelogPanel);
        changelogPanel.setLayout(changelogPanelLayout);
        changelogPanelLayout.setHorizontalGroup(
            changelogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 679, Short.MAX_VALUE)
        );
        changelogPanelLayout.setVerticalGroup(
            changelogPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 342, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Changelog", changelogPanel);

        authenticatorPanel.setOpaque(false);

        jPanel5.setOpaque(false);

        profileBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select a profile" }));
        profileBox.setOpaque(false);
        profileBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                profileBoxActionPerformed(evt);
            }
        });

        jLabel7.setText("Profile:");

        jLabel9.setText("Password:");

        passwordField.setOpaque(false);
        passwordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordFieldActionPerformed(evt);
            }
        });

        loginButton.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        loginButton.setText("Login");
        loginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginButtonActionPerformed(evt);
            }
        });

        createProfileButton.setText("Create new profile");
        createProfileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createProfileButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(profileBox, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel9)
                .addGap(14, 14, 14)
                .addComponent(passwordField))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addComponent(loginButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(createProfileButton)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(profileBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(passwordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(loginButton)
                    .addComponent(createProfileButton)))
        );

        javax.swing.GroupLayout authenticatorPanelLayout = new javax.swing.GroupLayout(authenticatorPanel);
        authenticatorPanel.setLayout(authenticatorPanelLayout);
        authenticatorPanelLayout.setHorizontalGroup(
            authenticatorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, authenticatorPanelLayout.createSequentialGroup()
                .addContainerGap(376, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(113, 113, 113))
        );
        authenticatorPanelLayout.setVerticalGroup(
            authenticatorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(authenticatorPanelLayout.createSequentialGroup()
                .addGap(99, 99, 99)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(168, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Authenticator", authenticatorPanel);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel8.setText("Selected profile: ");

        selectedProfileLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel11.setText("Max RAM allowed:");

        ramBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "512 Mbytes", "1 Gbyte", "2 Gbytes", "3 Gbytes", "4 Gbytes", "5 Gbytes", "6 Gbytes", "7 Gbytes", "8 Gbytes" }));
        ramBox.setSelectedIndex(1);
        ramBox.setMaximumSize(new java.awt.Dimension(179, 20));
        ramBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ramBoxActionPerformed(evt);
            }
        });

        jLabel12.setText("Game directory:");

        gameDirField.setMaximumSize(new java.awt.Dimension(179, 20));
        gameDirField.setMinimumSize(new java.awt.Dimension(179, 20));
        gameDirField.setPreferredSize(new java.awt.Dimension(179, 20));
        gameDirField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                gameDirFieldKeyTyped(evt);
            }
        });

        jLabel13.setText("Profile name:");

        profileNameField.setMaximumSize(new java.awt.Dimension(179, 20));
        profileNameField.setMinimumSize(new java.awt.Dimension(179, 20));
        profileNameField.setPreferredSize(new java.awt.Dimension(179, 20));
        profileNameField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                profileNameFieldKeyTyped(evt);
            }
        });

        jLabel14.setText("Launcher action:");

        actionBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Close launcher", "Reopen launcher", "Keep launcher open" }));
        actionBox.setMaximumSize(new java.awt.Dimension(179, 20));
        actionBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                actionBoxActionPerformed(evt);
            }
        });

        jLabel15.setText("Java executable:");

        advancedSettings.setText("Allow advanced settings");
        advancedSettings.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advancedSettingsActionPerformed(evt);
            }
        });

        javaExecField.setEnabled(false);
        javaExecField.setMaximumSize(new java.awt.Dimension(179, 20));
        javaExecField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                javaExecFieldKeyTyped(evt);
            }
        });

        jLabel16.setText("Java arguments:");

        javaArgsField.setEnabled(false);
        javaArgsField.setMaximumSize(new java.awt.Dimension(179, 20));
        javaArgsField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                javaArgsFieldKeyTyped(evt);
            }
        });

        autoLogin.setText("Allow auto login");
        autoLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                autoLoginActionPerformed(evt);
            }
        });

        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/steve.png"))); // NOI18N

        jLabel18.setText("Internal modules:");

        moduleList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        moduleList.setEnabled(false);
        moduleList.setMaximumSize(new java.awt.Dimension(179, 85));
        jScrollPane3.setViewportView(moduleList);

        activeOption.setText("Active?");
        activeOption.setEnabled(false);

        logoutButton.setText("Logout");
        logoutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                logoutButtonActionPerformed(evt);
            }
        });

        deleteProfButton.setText("Delete profile");
        deleteProfButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteProfButtonActionPerformed(evt);
            }
        });

        saveSettingsButton.setText("Save changes");
        saveSettingsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveSettingsButtonActionPerformed(evt);
            }
        });

        manualLogginButton.setText("Manual login");
        manualLogginButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manualLogginButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout profileEditorPanelLayout = new javax.swing.GroupLayout(profileEditorPanel);
        profileEditorPanel.setLayout(profileEditorPanelLayout);
        profileEditorPanelLayout.setHorizontalGroup(
            profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(profileEditorPanelLayout.createSequentialGroup()
                .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(profileEditorPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(selectedProfileLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                        .addComponent(autoLogin)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(logoutButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(saveSettingsButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(manualLogginButton))
                                    .addComponent(advancedSettings)
                                    .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                            .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel15, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel14, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(profileNameField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(gameDirField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(ramBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(actionBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(javaExecField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(javaArgsField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                    .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel18)
                                            .addComponent(activeOption))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(42, 42, 42)
                                        .addComponent(deleteProfButton)))
                                .addGap(0, 22, Short.MAX_VALUE))))
                    .addComponent(jSeparator2))
                .addGap(6, 6, 6)
                .addComponent(jLabel17)
                .addContainerGap())
        );
        profileEditorPanelLayout.setVerticalGroup(
            profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(profileEditorPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(profileEditorPanelLayout.createSequentialGroup()
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(selectedProfileLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(ramBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(gameDirField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(profileNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel13))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(actionBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel14))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(autoLogin)
                            .addComponent(logoutButton)
                            .addComponent(saveSettingsButton)
                            .addComponent(manualLogginButton))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                .addComponent(advancedSettings)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel15)
                                    .addComponent(javaExecField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel16)
                                    .addComponent(javaArgsField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(profileEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(profileEditorPanelLayout.createSequentialGroup()
                                        .addComponent(jLabel18)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(activeOption)
                                        .addGap(46, 46, 46))
                                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))
                            .addComponent(deleteProfButton, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addComponent(jLabel17, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("Profile Editor", profileEditorPanel);

        instanceList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        instanceList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                instanceListMouseClicked(evt);
            }
        });
        jScrollPane4.setViewportView(instanceList);

        createInstance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/add minecraft.png"))); // NOI18N
        createInstance.setToolTipText("Create a Minecraft instance.");
        createInstance.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createInstanceActionPerformed(evt);
            }
        });

        jSeparator3.setOrientation(javax.swing.SwingConstants.VERTICAL);

        instanceInfo.setColumns(20);
        instanceInfo.setLineWrap(true);
        instanceInfo.setRows(5);
        instanceInfo.setEnabled(false);
        instanceInfo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                instanceInfoKeyReleased(evt);
            }
        });
        jScrollPane5.setViewportView(instanceInfo);

        playButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/play.png"))); // NOI18N
        playButton.setToolTipText("Execute the selected instance.");
        playButton.setEnabled(false);
        playButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playButtonActionPerformed(evt);
            }
        });

        importButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/import.png"))); // NOI18N
        importButton.setToolTipText("Import a minecraft instance.");
        importButton.setEnabled(false);
        importButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                importButtonActionPerformed(evt);
            }
        });

        savesButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/edit save.png"))); // NOI18N
        savesButton.setToolTipText("Edit the saves of the instance.");
        savesButton.setEnabled(false);
        savesButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                savesButtonActionPerformed(evt);
            }
        });

        infoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/information.png"))); // NOI18N
        infoButton.setToolTipText("Show all information available from the instance.");
        infoButton.setEnabled(false);
        infoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                infoButtonActionPerformed(evt);
            }
        });

        deleteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/document_delete.png"))); // NOI18N
        deleteButton.setToolTipText("Delete the selected instance. Can not be undone!");
        deleteButton.setEnabled(false);
        deleteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteButtonActionPerformed(evt);
            }
        });

        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("Instance editor");

        jTabbedPane2.setToolTipText("");
        jTabbedPane2.setEnabled(false);

        forgeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Forge version", "Minecraft version"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        forgeTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                forgeTableMouseClicked(evt);
            }
        });
        jScrollPane6.setViewportView(forgeTable);

        jLabel19.setText("Getting forge list. Please wait");

        forgeIButton.setText("Install");
        forgeIButton.setToolTipText("Install the selected Minecraft Forge");
        forgeIButton.setEnabled(false);
        forgeIButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                forgeIButtonActionPerformed(evt);
            }
        });

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("Installed:");

        forgeAButton.setText("Add");
        forgeAButton.setToolTipText("Add a Minecraft Forge file");
        forgeAButton.setEnabled(false);

        javax.swing.GroupLayout forgePanelLayout = new javax.swing.GroupLayout(forgePanel);
        forgePanel.setLayout(forgePanelLayout);
        forgePanelLayout.setHorizontalGroup(
            forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(forgePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(forgeAButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(forgeIButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        forgePanelLayout.setVerticalGroup(
            forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(forgePanelLayout.createSequentialGroup()
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(forgeIButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(forgeAButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(forgePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Forge", forgePanel);

        jLabel10.setText("Any mods not found?");

        jXHyperlink1.setText("Go here");
        jXHyperlink1.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N
        jXHyperlink1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jXHyperlink1MouseClicked(evt);
            }
        });

        mcModList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        mcModList.setMaximumSize(new java.awt.Dimension(281, 130));
        mcModList.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mcModListMouseClicked(evt);
            }
        });
        jScrollPane11.setViewportView(mcModList);

        jLabel2.setText("Authors:");

        jLabel3.setText("Description:");

        jLabel4.setText("Types:");

        jLabel5.setText("URL:");

        urlLabel.setText("");
        urlLabel.setFont(new java.awt.Font("Tahoma", 3, 11)); // NOI18N

        descButton.setText("Show");
        descButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                descButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout mcModListPanelLayout = new javax.swing.GroupLayout(mcModListPanel);
        mcModListPanel.setLayout(mcModListPanelLayout);
        mcModListPanelLayout.setHorizontalGroup(
            mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcModListPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mcModListPanelLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(authorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mcModListPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(typeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(mcModListPanelLayout.createSequentialGroup()
                        .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mcModListPanelLayout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(descButton))
                            .addGroup(mcModListPanelLayout.createSequentialGroup()
                                .addComponent(jLabel10)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jXHyperlink1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(mcModListPanelLayout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(urlLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addComponent(jScrollPane11, javax.swing.GroupLayout.DEFAULT_SIZE, 281, Short.MAX_VALUE)
        );
        mcModListPanelLayout.setVerticalGroup(
            mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mcModListPanelLayout.createSequentialGroup()
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(authorLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(descButton))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(typeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(urlLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(17, 17, 17)
                .addGroup(mcModListPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jXHyperlink1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Mod list", mcModListPanel);

        modsList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        modsList.setToolTipText("");
        jScrollPane9.setViewportView(modsList);

        addModsFolderButton.setText("Add");
        addModsFolderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addModsFolderButtonActionPerformed(evt);
            }
        });

        removeModsFolderButton.setText("Remove");
        removeModsFolderButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeModsFolderButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout modsPanelLayout = new javax.swing.GroupLayout(modsPanel);
        modsPanel.setLayout(modsPanelLayout);
        modsPanelLayout.setHorizontalGroup(
            modsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(modsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(modsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addGroup(modsPanelLayout.createSequentialGroup()
                        .addGroup(modsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(removeModsFolderButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addModsFolderButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        modsPanelLayout.setVerticalGroup(
            modsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(modsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane9, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addModsFolderButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeModsFolderButton)
                .addGap(0, 53, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Mods folder", modsPanel);

        jarList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jarList.setToolTipText("");
        jScrollPane7.setViewportView(jarList);

        addJarModsButton.setText("Add");
        addJarModsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addJarModsButtonActionPerformed(evt);
            }
        });

        removeJarModsButton.setText("Remove");
        removeJarModsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeJarModsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jarPanelLayout = new javax.swing.GroupLayout(jarPanel);
        jarPanel.setLayout(jarPanelLayout);
        jarPanelLayout.setHorizontalGroup(
            jarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jarPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addGroup(jarPanelLayout.createSequentialGroup()
                        .addGroup(jarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(removeJarModsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addJarModsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jarPanelLayout.setVerticalGroup(
            jarPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jarPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addJarModsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeJarModsButton)
                .addGap(0, 53, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Jar mods", jarPanel);

        coremodsList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        coremodsList.setToolTipText("");
        jScrollPane8.setViewportView(coremodsList);

        addCoremodsButton.setText("Add");
        addCoremodsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addCoremodsButtonActionPerformed(evt);
            }
        });

        removeCoremodsButton.setText("Remove");
        removeCoremodsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeCoremodsButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout coremodsPanelLayout = new javax.swing.GroupLayout(coremodsPanel);
        coremodsPanel.setLayout(coremodsPanelLayout);
        coremodsPanelLayout.setHorizontalGroup(
            coremodsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(coremodsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(coremodsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)
                    .addGroup(coremodsPanelLayout.createSequentialGroup()
                        .addGroup(coremodsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(removeCoremodsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(addCoremodsButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        coremodsPanelLayout.setVerticalGroup(
            coremodsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(coremodsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addCoremodsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(removeCoremodsButton)
                .addGap(0, 53, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Coremods folder", coremodsPanel);

        jSeparator4.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("Mods editor");

        editNameButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/edit.png"))); // NOI18N
        editNameButton.setToolTipText("Edit your instance name here.");
        editNameButton.setEnabled(false);
        editNameButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editNameButtonActionPerformed(evt);
            }
        });

        exportButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/export.png"))); // NOI18N
        exportButton.setToolTipText("Export a minecraft instance.");
        exportButton.setEnabled(false);
        exportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exportButtonActionPerformed(evt);
            }
        });

        moreButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/more.png"))); // NOI18N
        moreButton.setToolTipText("Show more options");
        moreButton.setEnabled(false);
        moreButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                moreButtonMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout mcEditorPanelLayout = new javax.swing.GroupLayout(mcEditorPanel);
        mcEditorPanel.setLayout(mcEditorPanelLayout);
        mcEditorPanelLayout.setHorizontalGroup(
            mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcEditorPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(createInstance, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mcEditorPanelLayout.createSequentialGroup()
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(importButton)
                            .addComponent(deleteButton)
                            .addComponent(playButton)
                            .addComponent(exportButton)
                            .addComponent(savesButton)
                            .addComponent(infoButton)
                            .addComponent(editNameButton)))
                    .addGroup(mcEditorPanelLayout.createSequentialGroup()
                        .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(mcEditorPanelLayout.createSequentialGroup()
                                .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel21, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jSeparator5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 264, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mcEditorPanelLayout.createSequentialGroup()
                                .addComponent(moreButton)
                                .addGap(2, 2, 2)))
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(mcEditorPanelLayout.createSequentialGroup()
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addComponent(jSeparator6)
                    .addGroup(mcEditorPanelLayout.createSequentialGroup()
                        .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        mcEditorPanelLayout.setVerticalGroup(
            mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(mcEditorPanelLayout.createSequentialGroup()
                .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mcEditorPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator3, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(mcEditorPanelLayout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addGroup(mcEditorPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane5)
                                    .addGroup(mcEditorPanelLayout.createSequentialGroup()
                                        .addComponent(playButton)
                                        .addGap(4, 4, 4)
                                        .addComponent(importButton)
                                        .addGap(4, 4, 4)
                                        .addComponent(exportButton)
                                        .addGap(4, 4, 4)
                                        .addComponent(savesButton)
                                        .addGap(4, 4, 4)
                                        .addComponent(infoButton)
                                        .addGap(4, 4, 4)
                                        .addComponent(editNameButton)
                                        .addGap(4, 4, 4)
                                        .addComponent(moreButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(deleteButton))))
                            .addGroup(mcEditorPanelLayout.createSequentialGroup()
                                .addComponent(jScrollPane4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(createInstance))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mcEditorPanelLayout.createSequentialGroup()
                                .addComponent(jLabel23)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 296, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );

        jTabbedPane1.addTab("MC Editor", mcEditorPanel);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/MC_logo.png"))); // NOI18N

        showConsoleButton.setText("Hide console");
        showConsoleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showConsoleButtonActionPerformed(evt);
            }
        });

        aboutPanel.setBackground(new java.awt.Color(0, 0, 0));

        guiConsole.setEditable(false);
        jScrollPane10.setViewportView(guiConsole);

        javax.swing.GroupLayout aboutPanelLayout = new javax.swing.GroupLayout(aboutPanel);
        aboutPanel.setLayout(aboutPanelLayout);
        aboutPanelLayout.setHorizontalGroup(
            aboutPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        aboutPanelLayout.setVerticalGroup(
            aboutPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane10, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );

        mainBar.setBackground(new java.awt.Color(0, 0, 0));
        mainBar.setForeground(new java.awt.Color(0, 204, 0));
        mainBar.setOpaque(true);
        mainBar.setPreferredSize(new java.awt.Dimension(146, 17));
        mainBar.setString("");
        mainBar.setStringPainted(true);

        jPanel2.setBackground(new java.awt.Color(168, 159, 156));
        jPanel2.setBorder(javax.swing.BorderFactory.createCompoundBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(0, 153, 51)), javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 153, 51))));

        jLabel6.setFont(MineFont.getFont(Font.BOLD, 12));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("AS Launcher created by Infernage");

        jSeparator1.setBackground(new java.awt.Color(0, 153, 51));
        jSeparator1.setForeground(new java.awt.Color(0, 153, 51));

        statusLabel.setForeground(new java.awt.Color(255, 255, 255));
        statusLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        statusLabel.setText("Initializing launcher");
        statusLabel.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        statusLabel.setMaximumSize(new java.awt.Dimension(177, 15));
        statusLabel.setMinimumSize(new java.awt.Dimension(177, 15));
        statusLabel.setPreferredSize(new java.awt.Dimension(177, 15));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator1)
            .addComponent(statusLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(statusLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jMenu1.setText("Modules");

        jMenuItem1.setText("Import module");
        jMenu1.add(jMenuItem1);

        jMenuItem2.setText("Modules window");
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 684, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(aboutPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainBar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(showConsoleButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addGap(7, 7, 7)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(mainBar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(showConsoleButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addComponent(aboutPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void showConsoleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showConsoleButtonActionPerformed
        dynamicResize();
    }//GEN-LAST:event_showConsoleButtonActionPerformed

    private void pageHyperlinkUpdate(javax.swing.event.HyperlinkEvent evt) {//GEN-FIRST:event_pageHyperlinkUpdate
        if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            try {
                Desktop.getDesktop().browse(evt.getURL().toURI());
            } catch (Exception e) {
                log.throwing(this.getClass().getName(), "Desktop browser not found", e);
            }
        }
    }//GEN-LAST:event_pageHyperlinkUpdate

    private void forgeTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_forgeTableMouseClicked
        if (working) return;
        controller.check(instance);
        String forgeVersion = (String) forgeTable.getValueAt(forgeTable.getSelectedRow(), 0);
        if (forgeVersion == null) {
            return;
        }
        jLabel19.setText("MCForge:" + forgeVersion);
        forgeIButton.setEnabled(true);
        forgeIButton.setText("Install");
    }//GEN-LAST:event_forgeTableMouseClicked

    private void forgeIButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_forgeIButtonActionPerformed
        if (controller.isBusy()) {
            return;
        }
        if (instance == null) {
            return;
        }
        FutureCallback<Void> callback = new FutureCallback<Void>() {

            @Override
            public void onSuccess(Void v) {
                jTabbedPane2.add("Mod list", mcModListPanel);
                jTabbedPane2.add("Mods folder", modsPanel);
                if (instance.isLower1_6()) {
                    jTabbedPane2.add("Coremods folder", coremodsPanel);
                    jTabbedPane2.add("Jarmods folder", jarPanel);
                }
                controller.getModList(mcModList, instance.getVersion().getId());
            }

            @Override
            public void onFailure(Throwable thrwbl) {
                // It won't throw any exception.
            }
        };
        controller.executeForgeAction(forgeIButton, jLabel22, instance,
                (String) forgeTable.getValueAt(forgeTable.getSelectedRow(), 0), callback);
    }//GEN-LAST:event_forgeIButtonActionPerformed

    private void addJarModsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addJarModsButtonActionPerformed
        controller.check(instance);
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Zip or Jar file(.zip, .jar)", "zip", "jar"));
        chooser.setMultiSelectionEnabled(true);
        int selection = chooser.showOpenDialog(this);
        if (selection != JFileChooser.APPROVE_OPTION) {
            return;
        }
        log.config("Getting selected files");
        File[] mods = chooser.getSelectedFiles();
        if (mods != null && mods.length > 0) {
            DefaultListModel model = new DefaultListModel();
            for (File file : mods) {
                Path target = Paths.get(instance.getPath().toString(),
                        "jars", file.getName());
                try {
                    log.log(Level.CONFIG, "Importing {0}", file.getAbsolutePath());
                    Files.copy(file.toPath(), target);
                    model.addElement(target.getFileName().toString());
                    instance.addMod(target.getFileName().toString(), MCInstance.JAR);
                } catch (Exception e) {
                    log.config("Jar mod failed to install. Skipping...");
                    log.throwing(this.getClass().getName(), "addJarModsButtonActionPerformed", e);
                    try {
                        Files.deleteIfExists(target);
                    } catch (Exception ex) {
                        // Ignore
                    }
                }
            }
            jarList.setModel(model);
        }
    }//GEN-LAST:event_addJarModsButtonActionPerformed

    private void removeJarModsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeJarModsButtonActionPerformed
        if (jarList.getSelectedValue() == null) {
            return;
        }
        controller.check(instance);
        String key = (String) jarList.getSelectedValue();
        Path target = Paths.get(instance.getPath().toString(), "jars", key);
        log.log(Level.CONFIG, "Removing jar mod {0} from {1}", new Object[]{key, target.toString()});
        instance.removeMod(key, MCInstance.JAR);
        DefaultListModel model = (DefaultListModel) jarList.getModel();
        model.removeElement(key);
        jarList.setModel(model);
        try {
            Files.deleteIfExists(target);
        } catch (Exception e) {
            // Ignore
        }
    }//GEN-LAST:event_removeJarModsButtonActionPerformed

    private void addCoremodsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addCoremodsButtonActionPerformed
        controller.check(instance);
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Zip or Jar file(.zip, .jar)", "zip", "jar"));
        chooser.setMultiSelectionEnabled(true);
        int selection = chooser.showOpenDialog(this);
        if (selection != JFileChooser.APPROVE_OPTION) {
            return;
        }
        log.config("Getting selected files");
        File[] mods = chooser.getSelectedFiles();
        if (mods != null && mods.length > 0) {
            DefaultListModel model = new DefaultListModel();
            for (File file : mods) {
                Path target = Paths.get(instance.getPath().toString(),
                        "coremods", file.getName());
                try {
                    log.log(Level.CONFIG, "Importing {0}", file.getAbsolutePath());
                    Files.copy(file.toPath(), target);
                    model.addElement(target.getFileName().toString());
                    instance.addMod(target.getFileName().toString(), MCInstance.CORE);
                } catch (Exception e) {
                    log.config("Core mod failed to install. Skipping...");
                    log.throwing(this.getClass().getName(), "addCoreModsButtonActionPerformed", e);
                    try {
                        Files.deleteIfExists(target);
                    } catch (Exception ex) {
                        // Ignore
                    }
                }
            }
            coremodsList.setModel(model);
        }
    }//GEN-LAST:event_addCoremodsButtonActionPerformed

    private void removeCoremodsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeCoremodsButtonActionPerformed
        if (coremodsList.getSelectedValue() == null) {
            return;
        }
        controller.check(instance);
        String key = (String) coremodsList.getSelectedValue();
        Path target = Paths.get(instance.getPath().toString(), "coremods", key);
        log.log(Level.CONFIG, "Removing core mod {0} from {1}", new Object[]{key, target.toString()});
        instance.removeMod(key, MCInstance.CORE);
        DefaultListModel model = (DefaultListModel) coremodsList.getModel();
        model.removeElement(key);
        coremodsList.setModel(model);
        try {
            Files.deleteIfExists(target);
        } catch (Exception e) {
            // Ignore
        }
    }//GEN-LAST:event_removeCoremodsButtonActionPerformed

    private void addModsFolderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addModsFolderButtonActionPerformed
        controller.check(instance);
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("Zip or Jar file(.zip, .jar)", "zip", "jar"));
        chooser.setMultiSelectionEnabled(true);
        int selection = chooser.showOpenDialog(this);
        if (selection != JFileChooser.APPROVE_OPTION) {
            return;
        }
        log.config("Getting selected files");
        File[] mods = chooser.getSelectedFiles();
        if (mods != null && mods.length > 0) {
            DefaultListModel model = new DefaultListModel();
            for (File file : mods) {
                Path target = Paths.get(instance.getPath().toString(),
                        "mods", file.getName());
                try {
                    log.log(Level.CONFIG, "Importing {0}", file.getAbsolutePath());
                    Files.copy(file.toPath(), target);
                    model.addElement(target.getFileName().toString());
                    instance.addMod(target.getFileName().toString(), MCInstance.MOD);
                } catch (Exception e) {
                    log.config("Default mod failed to install. Skipping...");
                    log.throwing(this.getClass().getName(), "addModsFolderButtonActionPerformed", e);
                    try {
                        Files.deleteIfExists(target);
                    } catch (Exception ex) {
                        // Ignore
                    }
                }
            }
            modsList.setModel(model);
        }
    }//GEN-LAST:event_addModsFolderButtonActionPerformed

    private void removeModsFolderButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeModsFolderButtonActionPerformed
        if (modsList.getSelectedValue() == null) {
            return;
        }
        controller.check(instance);
        String key = (String) modsList.getSelectedValue();
        Path target = Paths.get(instance.getPath().toString(), "mods", key);
        log.log(Level.CONFIG, "Removing mod {0} from {1}", new Object[]{key, target.toString()});
        instance.removeMod(key, MCInstance.MOD);
        DefaultListModel model = (DefaultListModel) modsList.getModel();
        model.removeElement(key);
        modsList.setModel(model);
        try {
            Files.deleteIfExists(target);
        } catch (Exception e) {
            // Ignore
        }
    }//GEN-LAST:event_removeModsFolderButtonActionPerformed

    private void loginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginButtonActionPerformed
        login();
    }//GEN-LAST:event_loginButtonActionPerformed

    private void createProfileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createProfileButtonActionPerformed
        if (controller.isBusy()) {
            return;
        }
        ProfileForm form = new ProfileForm(this, true);
        form.setLocationRelativeTo(this);
        form.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        form.setVisible(true);
    }//GEN-LAST:event_createProfileButtonActionPerformed

    private void logoutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_logoutButtonActionPerformed
        if (selected.isPremium()) {
            logout();
        } else {
            MessageControl.showInfoMessage("Only premium accounts can logout", "Base account tried to loggout");
        }
    }//GEN-LAST:event_logoutButtonActionPerformed

    private void gameDirFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_gameDirFieldKeyTyped
        if (controller.isBusy()) {
            return;
        }
        if (selected != null) {
            selected.changeDir(Paths.get(gameDirField.getText()));
        }
    }//GEN-LAST:event_gameDirFieldKeyTyped

    private void profileNameFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_profileNameFieldKeyTyped
        if (selected != null) {
            selected.changeName(profileNameField.getText());
            selectedProfileLabel.setText(profileNameField.getText());
        }
    }//GEN-LAST:event_profileNameFieldKeyTyped

    private void autoLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_autoLoginActionPerformed
        if (selected != null) {
            if (selected.isPremium()) {
                selected.setAutoLogin(autoLogin.isSelected());
            } else {
                Systray.imprMessage("Base account cannot loggout. Autologin is always activated.",
                        "Base account tried to remove autologin");
                autoLogin.setSelected(true);
            }
        }
    }//GEN-LAST:event_autoLoginActionPerformed

    private void advancedSettingsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_advancedSettingsActionPerformed
        if (selected != null) {
            selected.setAdvanced(advancedSettings.isSelected());
            if (advancedSettings.isSelected()) {
                javaExecField.setEnabled(true);
                javaExecField.setText(selected.getJavaExec());
                javaArgsField.setEnabled(true);
                javaArgsField.setText(selected.getJavaArgs());
                activeOption.setEnabled(true);
                moduleList.setEnabled(true);
            } else {
                javaExecField.setEnabled(false);
                javaExecField.setText("");
                javaArgsField.setEnabled(false);
                javaArgsField.setText("");
                activeOption.setEnabled(false);
                moduleList.setEnabled(true);
            }
        }
    }//GEN-LAST:event_advancedSettingsActionPerformed

    private void javaExecFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_javaExecFieldKeyTyped
        if (selected != null) {
            selected.setJavaExec(javaExecField.getText());
        }
    }//GEN-LAST:event_javaExecFieldKeyTyped

    private void javaArgsFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_javaArgsFieldKeyTyped
        if (selected != null) {
            selected.setJavaArgs(javaArgsField.getText());
        }
    }//GEN-LAST:event_javaArgsFieldKeyTyped

    private void createInstanceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createInstanceActionPerformed
        if (selected == null) {
            return;
        }
        InstanceForm form = new InstanceForm(this, true, selected);
        form.setLocationRelativeTo(this);
        form.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        form.setVisible(true);
    }//GEN-LAST:event_createInstanceActionPerformed

    private void instanceListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_instanceListMouseClicked
        if (selected == null || working) {
            return;
        }
        if (instanceList.getSelectedValue() == null || ((String) instanceList.getSelectedValue())
                .isEmpty()) {
            return;
        }
        instanceInfo.setEnabled(true);
        playButton.setEnabled(true);
        savesButton.setEnabled(true);
        exportButton.setEnabled(true);
        deleteButton.setEnabled(true);
        infoButton.setEnabled(true);
        editNameButton.setEnabled(true);
        moreButton.setEnabled(true);
        String instance = (String) instanceList.getSelectedValue();
        if (instance != null && !instance.isEmpty()) {
            List<MCInstance> list = selected.getList();
            boolean find = false;
            for (MCInstance instances : list) {
                if (instance.equals(instances.getName())) {
                    this.instance = instances;
                    if (this.instance.getInfo() != null) {
                        instanceInfo.setText(this.instance.getInfo());
                    } else {
                        instanceInfo.setText("");
                    }
                    find = true;
                    break;
                }
            }
            if (!find) {
                MessageControl.showErrorMessage("Instance '" + instance + "' doesn't exist", null);
                instanceInfo.setText("");
                return;
            }
        } else {
            this.instance = null;
            instanceInfo.setText("");
        }
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                synchronized (lock) {
                    try {
                        jLabel19.setText("Getting forge list. Please wait");
                        jTabbedPane2.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                        List<RemoteMinecraftForge> forge = ForgeList.getForgeList();
                        DefaultTableModel model1 = (DefaultTableModel) forgeTable.getModel();
                        model1.setRowCount(0);
                        for (RemoteMinecraftForge forg : forge) {
                            if (forg.getMCVersion().equals(MainFrame.getWindow().instance.getVersion().getId())) {
                                model1.addRow(new Object[]{forg.getForgeVersion(), forg.getMCVersion()});
                            }
                        }
                        if (model1.getRowCount() == 0) {
                            jLabel19.setText("No forge to " + MainFrame.getWindow().instance.getVersion()
                                    .getId() + " found.");
                        } else {
                            jLabel19.setText("");
                        }
                        forgeTable.setModel(model1);
                        jTabbedPane2.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
                    } catch (Exception e) {
                        log.throwing(this.getClass().getName(), "instanceListMouseClicked", e);
                        jLabel19.setText(e.getMessage());
                    }
                    if (MainFrame.getWindow().instance.getForge() != null) {
                        jLabel22.setText("MCForge " + MainFrame.getWindow().instance.getForge()
                                .getForgeVersion());
                        jTabbedPane2.setSelectedIndex(0);
                        jTabbedPane2.add("Mod list", mcModListPanel);
                        jTabbedPane2.add("Mods folder", modsPanel);
                        descButton.setVisible(false);
                        authorLabel.setText("");
                        typeLabel.setText("");
                        urlLabel.setText("");
                        urlLabel.setURI(null);
                        controller.getModList(mcModList, MainFrame.getWindow().instance.getVersion()
                                .getId());
                        jScrollPane11.getVerticalScrollBar().setValue(0);
                        if (MainFrame.getWindow().instance.isLower1_6()) {
                            jTabbedPane2.add("Coremods folder", coremodsPanel);
                            jTabbedPane2.add("Jarmods folder", jarPanel);
                        } else if (jTabbedPane2.getComponentCount() > 3) {
                            jTabbedPane2.remove(4);
                            jTabbedPane2.remove(3);
                        }
                    } else {
                        jLabel22.setText("N/A");
                        if (jTabbedPane2.getComponentCount() > 1) {
                            int cc = jTabbedPane2.getComponentCount();
                            for (int i = 0; i < cc - 1; i++) {
                                jTabbedPane2.remove(1);
                            }
                        }
                    }
                    jTabbedPane2.setEnabled(true);
                    DefaultListModel model = new DefaultListModel();
                    if (MainFrame.getWindow().instance.getModMap() != null) {
                        for (String mod : MainFrame.getWindow().instance.getModMap()) {
                            model.addElement(mod);
                        }
                    }
                    modsList.setModel(model);
                    model = new DefaultListModel();
                    if (MainFrame.getWindow().instance.getCoreModMap() != null) {
                        for (String mod : MainFrame.getWindow().instance.getCoreModMap()) {
                            model.addElement(mod);
                        }
                    }
                    coremodsList.setModel(model);
                    model = new DefaultListModel();
                    if (MainFrame.getWindow().instance.getJarModMap() != null) {
                        for (String mod : MainFrame.getWindow().instance.getJarModMap()) {
                            model.addElement(mod);
                        }
                    }
                    jarList.setModel(model);
                }
            }
        });
    }//GEN-LAST:event_instanceListMouseClicked

    private void playButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playButtonActionPerformed
        if (instance != null && !controller.isBusy()) {
            printMsg("Loading...");
            setBusy(true);
            controller.createCallback(CoreHandler.INSTANCE.executeTask(new MCLoader(selected, instance)));
        }
    }//GEN-LAST:event_playButtonActionPerformed

    private void savesButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_savesButtonActionPerformed
        if (instance == null || controller.isBusy()) {
            return;
        }
        SaveForm form = new SaveForm(this, true, instance);
        form.setLocationRelativeTo(this);
        form.setVisible(true);
    }//GEN-LAST:event_savesButtonActionPerformed

    private void deleteProfButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteProfButtonActionPerformed
        if (controller.isBusy()) {
            return;
        }
        if (selected == null) {
            return;
        }
        int i = MessageControl.showConfirmDialog("Are you sure to delete the selected profile? "
                + "It can not be undone!", "Delete profile", 0, 2);
        if (i != 0) {
            return;
        }
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                log.log(Level.INFO, "Removing profile {0}", selected.getProfilename());
                Profile prof = selected;
                selected = null;
                instance = null;
                prof.setLogged(false);
                disableMCComponents();
                CoreHandler.getConfiguration().removeProfile(prof);
                refreshProfiles();
                log.info("Removed");
            }
        });
    }//GEN-LAST:event_deleteProfButtonActionPerformed

    private void infoButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_infoButtonActionPerformed
        if (instance == null) {
            return;
        }
        JDialog dialog = new JDialog(this);
        dialog.setLocationRelativeTo(this);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setSize(360, 189);
        StringBuilder information = new StringBuilder();
        information.append("Instance name: ").append(instance.getName()).append("\n")
                .append("Minecraft version: ").append(instance.getVersion().getId()).append("\n")
                .append("Install path: ").append(instance.getPath().toAbsolutePath().toString())
                .append("\n").append("Forge installed: ").append(instance.getForge() != null
                        ? instance.getForge().getForgeVersion() : "N/A").append("\n")
                .append("Mods installed: ");
        if (instance.getModMap().isEmpty()) {
            information.append("No mods\n");
        } else {
            for (String mod : instance.getModMap()) {
                information.append("-->Mod name: ").append(mod).append("\n");
            }
            for (String mod : instance.getJarModMap()) {
                information.append("-->Jarmod name: ").append(mod).append("\n");
            }
            for (String mod : instance.getCoreModMap()) {
                information.append("-->Coremod name: ").append(mod).append("\n");
            }
        }
        information.append("Information stored: ").append(instance.getInfo());
        JTextArea infoArea = new JTextArea(information.toString());
        infoArea.setEditable(false);
        infoArea.setWrapStyleWord(true);
        infoArea.setLineWrap(true);
        JScrollPane pane = new JScrollPane();
        pane.setViewportView(infoArea);
        dialog.add(pane);
        dialog.setVisible(true);
    }//GEN-LAST:event_infoButtonActionPerformed

    private void deleteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteButtonActionPerformed
        if (controller.isBusy()) {
            return;
        }
        if (instance == null) {
            return;
        }
        int i = MessageControl.showConfirmDialog("Are you sure to delete " + instance.getName()
                + " instance? It can not be undone.", "Delete instance", 0, 2);
        if (i != 0) {
            return;
        }
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                setWorking(true);
                try {
                    Path path = instance.getPath();
                    log.log(Level.INFO, "Removing {0}", instance.getName());
                    if (Files.exists(path)) {
                        IO.delete(path);
                    }
                    selected.removeInstance(instance);
                    DefaultListModel model = (DefaultListModel) instanceList.getModel();
                    model.removeElement(instance.getName());
                    instanceList.setModel(model);
                    instanceInfo.setText("");
                    instanceInfo.setEnabled(false);
                    playButton.setEnabled(false);
                    exportButton.setEnabled(false);
                    savesButton.setEnabled(false);
                    infoButton.setEnabled(false);
                    editNameButton.setEnabled(false);
                    deleteButton.setEnabled(false);
                    moreButton.setEnabled(false);
                    DefaultTableModel model1 = (DefaultTableModel) forgeTable.getModel();
                    model1.setRowCount(0);
                    forgeTable.setModel(model1);
                    int cc = jTabbedPane2.getComponentCount();
                    for (int i = 0; i < cc - 1; i++) {
                        jTabbedPane2.remove(1);
                    }
                    jTabbedPane2.setEnabled(false);
                    jLabel22.setText("");
                    instance = null;
                    try {
                        ASLUtils.saveInstances(selected);
                    } catch (Exception e) {
                        log.throwing(this.getClass().getName(), "deleteButtonActionPerformed", e);
                    }
                    log.info("Instance removed");
                } catch (Exception e) {
                    log.log(Level.WARNING, "Failed to remove " + instance.getName(), e);
                }
                setWorking(false);
            }
        });
    }//GEN-LAST:event_deleteButtonActionPerformed

    private void saveSettingsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveSettingsButtonActionPerformed
        if (selected != null && !controller.isBusy()) {
            selected.changeDir(controller.checkPath(gameDirField.getText(), selected));
        }
    }//GEN-LAST:event_saveSettingsButtonActionPerformed

    private void importButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importButtonActionPerformed
        if (selected == null || controller.isBusy()) {
            return;
        }
        if (!selected.isPremium()) {
            MessageControl.showErrorMessage("Only users with Minecraft account can import"
                    + " instances!", "Base profile detected");
            return;
        }
        controller.importModPack(selected);
    }//GEN-LAST:event_importButtonActionPerformed

    private void profileBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_profileBoxActionPerformed
        if (controller.isBusy() || isRefreshing) {
            return;
        }
        String profileName = (String) profileBox.getSelectedItem();
        if (profileName == null || profileName.equals("Select a profile")) {
            return;
        }
        log.log(Level.CONFIG, "Searching profile {0}", profileName);
        Profile pro = null;
        for (Profile profile : CoreHandler.getConfiguration().getProfiles()) {
            if (profileName.equals(profile.getProfilename())) {
                pro = profile;
                break;
            }
        }
        if (pro == null) {
            MessageControl.showErrorMessage("Profile not found", "The profile " + profileName
                    + " isn't in the database.\nPlease, try with another one or create one.");
            profileBox.removeItem(profileName);
            return;
        }
        if (selected != null && !selected.isAutoLogin()) {
            selected.setLogged(false);
        }
        log.config("Refreshing GUI");
        refresh(pro);
        Systray.setProfile(pro);
    }//GEN-LAST:event_profileBoxActionPerformed

    private void ramBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ramBoxActionPerformed
        if (selected != null) {
            selected.setMaxRam((String) ramBox.getSelectedItem());
        }
    }//GEN-LAST:event_ramBoxActionPerformed

    private void actionBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_actionBoxActionPerformed
        if (selected != null) {
            selected.setAction(actionBox.getSelectedIndex());
        }
    }//GEN-LAST:event_actionBoxActionPerformed

    private void passwordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordFieldActionPerformed
        login();
    }//GEN-LAST:event_passwordFieldActionPerformed

    private void manualLogginButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manualLogginButtonActionPerformed
        if (selected == null) {
            return;
        }
        String password = MessageControl.showInputPassword("Input your password:");
        if (password == null || password.isEmpty()) {
            return;
        }
        try {
            if (selected.isPremium()) {
                Authenticator.login(selected);
            } else {
                selected.improve(Authenticator.login(selected.getProfilename(), password,
                        selected.getPath()));
                selected.setLogged(true);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to login", e);
            selected.setAccessToken(null);
        }
        refresh(selected);
    }//GEN-LAST:event_manualLogginButtonActionPerformed

    private void editNameButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editNameButtonActionPerformed
        if (controller.isBusy()) {
            return;
        }
        if (instance == null) {
            return;
        }
        String name = MessageControl.showInputMessage("Enter a new name for " + instance.getName()
                + " instance:", "Change name");
        if (name == null || name.isEmpty()) {
            return;
        }
        String path = null;
        try {
            path = ASLUtils.checkFileName(name, instance.getPath().getParent().getParent());
            path = Paths.get(instance.getPath().getParent().toString(), path).toString();
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to check the new name", e);
            return;
        }
        try {
            IO.copy(instance.getPath(), Paths.get(path));
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to move the instance", e);
            if (Files.exists(Paths.get(path))) {
                try {
                    IO.delete(Paths.get(path));
                } catch (Exception ex) {
                    // Ignore
                }
            }
            return;
        }
        try {
            IO.delete(instance.getPath());
        } catch (Exception e) {
            // Ignore, copy already done
        }
        instance.changePath(Paths.get(path));
        instance.rename(Paths.get(path).getFileName().toString());
        refresh(selected);
    }//GEN-LAST:event_editNameButtonActionPerformed

    private void exportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exportButtonActionPerformed
        if (instance == null || selected == null) {
            return;
        }
        if (!selected.isPremium()) {
            MessageControl.showErrorMessage("Only users with Minecraft account can export"
                    + " its instances!", "Base profile detected");
            return;
        }
        final JFileChooser chooser = new JFileChooser(new File(System.getProperty("user.home")));
        FileFilter filter = new FileFilter() {

            @Override
            public boolean accept(File f) {
                return f.isDirectory();
            }

            @Override
            public String getDescription() {
                return ".inst";
            }
        };
        chooser.addChoosableFileFilter(filter);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setFileFilter(filter);
        final int value = chooser.showSaveDialog(this);
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                if (value == JFileChooser.APPROVE_OPTION) {
                    log.info("Exporting, please wait...");
                    setWorking(true);
                    setBusy(true);
                    File destiny = chooser.getSelectedFile();
                    if (!destiny.getName().endsWith(".inst")) {
                        destiny = (destiny.isDirectory() ? new File(destiny.getAbsolutePath(),
                                instance.getName() + ".inst") : new File(destiny.getParent(),
                                        destiny.getName() + ".inst"));
                    }
                    try {
                        ZipFile zip = new ZipFile(destiny);
                        ZipParameters par = new ZipParameters();
                        par.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
                        par.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_FASTEST);
                        par.setEncryptFiles(true);
                        par.setEncryptionMethod(Zip4jConstants.ENC_METHOD_AES);
                        par.setAesKeyStrength(Zip4jConstants.AES_STRENGTH_256);
                        par.setPassword("ASL");
                        Files.write(Paths.get(selected.getPath().toString(), "INST"), new GsonBuilder()
                                .setPrettyPrinting().create().toJson(instance).getBytes(Charset
                                        .defaultCharset()), StandardOpenOption.CREATE,
                                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
                        //zip.createZipFile(new File(selected.getPath().toString(), "INST"), par);
                        zip.addFile(new File(selected.getPath().toString(), "INST"), par);
                        Files.deleteIfExists(Paths.get(selected.getPath().toString(), "INST"));
                        for (Path p : Files.newDirectoryStream(instance.getPath(),
                                new DirectoryStream.Filter<Path>() {

                                    @Override
                                    public boolean accept(Path entry) throws IOException {
                                        return !entry.toString().endsWith(".jar");
                                    }
                                })) {
                            if (Files.isDirectory(p)) {
                                zip.addFolder(p.toString(), par);
                            } else if (Files.isRegularFile(p)) {
                                zip.addFile(p.toFile(), par);
                            }
                        }
                        log.info("Instance exported");
                    } catch (Exception e) {
                        log.log(Level.SEVERE, "Failed to export the instance", e);
                        try {
                            Files.deleteIfExists(destiny.toPath());
                        } catch (Exception ex) {
                            log.throwing(this.getClass().getName(), "exportButtonActionPerformed", ex);
                        }
                        try {
                            Files.deleteIfExists(Paths.get(selected.getPath().toString(), "INST"));
                        } catch (Exception ex) {
                            log.throwing(this.getClass().getName(), "exportButtonActionPerformed", ex);
                        }
                    }
                    setBusy(false);
                    setWorking(false);
                }
            }
        });
    }//GEN-LAST:event_exportButtonActionPerformed

    private void instanceInfoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_instanceInfoKeyReleased
        if (instance != null) {
            instance.setInfo(instanceInfo.getText());
        }
    }//GEN-LAST:event_instanceInfoKeyReleased

    private void jXHyperlink1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jXHyperlink1MouseClicked
        try {
            Desktop.getDesktop().browse(new URL("http://www.minecraftforum.net/forum/51-minecraft-mods/")
                    .toURI());
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to open URL", e);
        }
    }//GEN-LAST:event_jXHyperlink1MouseClicked

    private void mcModListMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mcModListMouseClicked
        if (mcModList.getSelectedValue() == null || instance == null) {
            return;
        }
        try {
            List<MCMod> mods = MCUtils.getModList(instance.getVersion().getId());
            MCMod mod = null;
            String name = ((String) mcModList.getSelectedValue()).split(" - ")[0];
            for (MCMod mCMod : mods) {
                if (mCMod.getName().equals(name)) {
                    mod = mCMod;
                    break;
                }
            }
            if (mod.getAuthor() != null) {
                String author = "";
                for (String string : mod.getAuthor()) {
                    author += string + ", ";
                }
                authorLabel.setText(author.substring(0, author.length() - 2));
            } else {
                authorLabel.setText("No author specified");
            }
            descButton.setVisible(true);
            if (mod.getDesc() != null) {
                descButton.setEnabled(true);
                descButton.setToolTipText(mod.getDesc());
            } else {
                descButton.setEnabled(false);
            }
            if (mod.getType() != null) {
                String types = "";
                for (String string : mod.getType()) {
                    types += string + ", ";
                }
                typeLabel.setText(types.substring(0, types.length() - 2));
            } else {
                typeLabel.setText("None");
            }
            if (mod.getLink() != null) {
                urlLabel.setURI(new URL(mod.getLink()).toURI());
                urlLabel.setText("Click me");
            } else {
                urlLabel.setURI(null);
                urlLabel.setText("No URL specified");
            }
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "mcModListMouseClicked", e);
        }
    }//GEN-LAST:event_mcModListMouseClicked

    private void descButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_descButtonActionPerformed
        if (descButton.getToolTipText() == null || descButton.getToolTipText().isEmpty()) {
            return;
        }
        JDialog dialog = new JDialog(this);
        dialog.setLocationRelativeTo(this);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setSize(360, 189);
        JTextArea infoArea = new JTextArea(descButton.getToolTipText());
        infoArea.setEditable(false);
        infoArea.setWrapStyleWord(true);
        infoArea.setLineWrap(true);
        JScrollPane pane = new JScrollPane();
        pane.setViewportView(infoArea);
        dialog.add(pane);
        dialog.setVisible(true);
    }//GEN-LAST:event_descButtonActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (Systray.processAlive()) {
            try {
                Systray.killMC();
            } catch (Exception e) {
                e.printStackTrace();
                // Just return, user denied exit.
                return;
            }
        }
        System.exit(0);
    }//GEN-LAST:event_formWindowClosing

    private void moreButtonMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_moreButtonMouseClicked
        if (evt.getButton() == MouseEvent.BUTTON1) {
            additionalOpts.show(moreButton, evt.getX(), evt.getY() - 50);
        }
    }//GEN-LAST:event_moreButtonMouseClicked

    private void jMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem3ActionPerformed
        if (instance == null) {
            return;
        }
        final String name;
        try {
            name = ASLUtils.checkFileName(instance.getName(), selected.getPath());
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed checking the instance name", e);
            return;
        }
        final Path duplicate = instance.getPath().resolveSibling(name);
        try {
            if (Files.notExists(duplicate)) {
                Files.createDirectory(duplicate);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed creating the directory", e);
            return;
        }
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                setWorking(true);
                setBusy(true);
                log.log(Level.INFO, "Duplicating {0}", name);
                try {
                    IO.copy(instance.getPath(), duplicate);
                } catch (Exception e) {
                    log.log(Level.WARNING, "Failed to copy the instance", e);
                    try {
                        IO.delete(duplicate);
                    } catch (Exception ex) {
                        log.throwing(this.getClass().getName(), "jMenuItem3ActionPerformed", ex);
                    }
                    setBusy(false);
                    setWorking(false);
                    log.warning("Duplication failed");
                    return;
                }
                MCInstance dup = new MCInstance(instance);
                dup.changePath(duplicate);
                dup.rename(name);
                selected.addInstance(dup);
                try {
                    ASLUtils.saveInstances(selected);
                } catch (Exception e) {
                    log.throwing(this.getClass().getName(), "jMenuItem3ActionPerformed", e);
                }
                setBusy(false);
                setWorking(false);
                refresh(selected);
                log.info("Duplicated correclty");
            }
        });
    }//GEN-LAST:event_jMenuItem3ActionPerformed

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        if (selected == null) {
            return;
        }
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                setWorking(true);
                setBusy(true);
                log.info("Cleaning...");
                try {
                    IO.delete(selected.getPath().resolve("assets"));
                } catch (Exception e) {
                    log.throwing(this.getClass().getName(), "jMenuItem4ActionPerformed", e);
                }
                try {
                    IO.delete(selected.getPath().resolve("libraries"));
                } catch (Exception e) {
                    log.throwing(this.getClass().getName(), "jMenuItem4ActionPerformed", e);
                }
                setBusy(false);
                setWorking(false);
                log.info("Cleaned");
            }
        });
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel aboutPanel;
    private javax.swing.JComboBox actionBox;
    private javax.swing.JCheckBox activeOption;
    private javax.swing.JButton addCoremodsButton;
    private javax.swing.JButton addJarModsButton;
    private javax.swing.JButton addModsFolderButton;
    private javax.swing.JPopupMenu additionalOpts;
    private javax.swing.JCheckBox advancedSettings;
    private javax.swing.JPanel authenticatorPanel;
    private javax.swing.JLabel authorLabel;
    private javax.swing.JCheckBox autoLogin;
    private javax.swing.JTextArea changelog;
    private javax.swing.JPanel changelogPanel;
    private javax.swing.JList coremodsList;
    private javax.swing.JPanel coremodsPanel;
    private javax.swing.JButton createInstance;
    private javax.swing.JButton createProfileButton;
    private javax.swing.JButton deleteButton;
    private javax.swing.JButton deleteProfButton;
    private javax.swing.JButton descButton;
    private javax.swing.JButton editNameButton;
    private javax.swing.JButton exportButton;
    private javax.swing.JButton forgeAButton;
    private javax.swing.JButton forgeIButton;
    private javax.swing.JPanel forgePanel;
    private javax.swing.JTable forgeTable;
    private javax.swing.JTextField gameDirField;
    private javax.swing.JTextPane guiConsole;
    private javax.swing.JButton importButton;
    private javax.swing.JButton infoButton;
    private javax.swing.JTextArea instanceInfo;
    private javax.swing.JList instanceList;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem3;
    private javax.swing.JMenuItem jMenuItem4;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    private org.jdesktop.swingx.JXHyperlink jXHyperlink1;
    private javax.swing.JList jarList;
    private javax.swing.JPanel jarPanel;
    private javax.swing.JTextField javaArgsField;
    private javax.swing.JTextField javaExecField;
    private javax.swing.JButton loginButton;
    private javax.swing.JButton logoutButton;
    private javax.swing.JProgressBar mainBar;
    private javax.swing.JButton manualLogginButton;
    private javax.swing.JPanel mcBlogPanel;
    private javax.swing.JPanel mcEditorPanel;
    private javax.swing.JList mcModList;
    private javax.swing.JPanel mcModListPanel;
    private javax.swing.JList modsList;
    private javax.swing.JPanel modsPanel;
    private javax.swing.JList moduleList;
    private javax.swing.JButton moreButton;
    private javax.swing.JTextPane page;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JButton playButton;
    private javax.swing.JComboBox profileBox;
    private javax.swing.JPanel profileEditorPanel;
    private javax.swing.JTextField profileNameField;
    private javax.swing.JComboBox ramBox;
    private javax.swing.JButton removeCoremodsButton;
    private javax.swing.JButton removeJarModsButton;
    private javax.swing.JButton removeModsFolderButton;
    private javax.swing.JButton saveSettingsButton;
    private javax.swing.JButton savesButton;
    private javax.swing.JLabel selectedProfileLabel;
    private javax.swing.JButton showConsoleButton;
    private org.jdesktop.swingx.JXBusyLabel statusLabel;
    private javax.swing.JLabel typeLabel;
    private org.jdesktop.swingx.JXHyperlink urlLabel;
    // End of variables declaration//GEN-END:variables

    private class Background extends JPanel {

        private Image icon;

        public Background(Image i) {
            icon = i;
        }

        @Override
        public void paint(Graphics g) {
            g.drawImage(icon, 0, 0, null);
            super.paint(g);
        }
    }

    private class ImageWrapper {

        private ImageIcon img;

        public ImageWrapper(String path) {
            try {
                img = new ImageIcon(ImageIO.read(MainFrame.class.getResourceAsStream(path)));
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }

        public Image get() {
            return img.getImage();
        }
    }
}
