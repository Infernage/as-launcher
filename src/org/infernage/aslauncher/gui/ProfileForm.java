/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.gui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.awt.Color;
import java.awt.Desktop;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import org.infernage.aslauncher.core.CoreConfiguration;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.mcsystem.authenticator.Authenticator;
import org.infernage.aslauncher.util.IO;
import org.infernage.aslauncher.util.MessageControl;
import org.infernage.aslauncher.util.References;

/**
 * JDialog used to login with Minecraft and creates Profiles.
 *
 * @author Infernage
 */
public class ProfileForm extends javax.swing.JDialog {

    private final static Logger log = Logger.getLogger(ProfileForm.class.getName());

    /**
     * Creates new form ProfileForm
     */
    public ProfileForm(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        jProgressBar1.setVisible(false);
    }

    private Path checkPath() {
        if (instancePath.getText() == null || instancePath.getText().isEmpty()) return null;
        Path path = Paths.get(instancePath.getText().equals("Default") ? CoreHandler.getConfiguration()
                .getMapValue(CoreConfiguration.ConfigKeys.ProfilesDir) : instancePath.getText());
        try {
            log.log(Level.FINE, "Checking path {0}", path.toString());
            if (!path.isAbsolute()){
                status.setForeground(Color.red);
                status.setText("Invalid path! Must be an absolute path.");
                return null;
            }
            if (Files.notExists(path)) {
                Files.createDirectories(path);
            } else if (!Files.isDirectory(path)) {
                status.setForeground(Color.red);
                status.setText("Invalid path! Must be a directory.");
                return null;
            } else if (Files.isDirectory(path)) {
                if (Files.exists(Paths.get(path.toString(), ".Instance"))) {
                    return path;
                }
                path = Paths.get(path.toString(), userTextField.getText() + " MCGame dir");
                if (Files.notExists(path)) {
                    Files.createDirectories(path);
                }
            }
            if (Files.notExists(path)){
                status.setForeground(Color.red);
                status.setText("Invalid path! Must be a correct path.");
                return null;
            }
            log.config("Creating dump file");
            Path hide = Paths.get(path.toString(), ".Instance");
            if (Files.notExists(hide)) {
                Files.createFile(hide);
            }
            log.config("Setting attributes");
            if (CoreHandler.getConfiguration().getOS() == CoreConfiguration.OS.windows) {
                Files.setAttribute(hide, "dos:hidden", Boolean.TRUE, LinkOption.NOFOLLOW_LINKS);
            }
            Files.createDirectories(Paths.get(path.toString(), "instances"));
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to check instance path", e);
            return null;
        }
        return path;
    }

    private List<MCInstance> checkAlreadyCreatedProfile(Path path) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            StringBuilder builder = new StringBuilder();
            for (String str : Files.readAllLines(path, Charset.defaultCharset())) {
                builder.append(str).append("\n");
            }
            java.lang.reflect.Type type = new TypeToken<List<MCInstance>>() {
            }.getType();
            return gson.fromJson(builder.toString(), type);
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "checkAlreadyCreatedProfile", e);
            //Ignore
        }
        return null;
    }

    private void createPremiumProfile() {
        final JDialog dialog = this;
        jProgressBar1.setVisible(true);
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                status.setText("");
                status.setForeground(Color.black);
                Path instances = checkPath();
                if (instances == null) {
                    log.config("Aborting creation");
                    jProgressBar1.setVisible(false);
                    return;
                }
                String user = userTextField.getText();
                String pass = new String(passwordTextField.getPassword());
                passwordTextField.setText("");
                status.setText("Login... Please wait.");
                try {
                    Profile profile = Authenticator.login(user, pass, instances);
                    List<MCInstance> list = checkAlreadyCreatedProfile(Paths.get(instances.toString(),
                            ".Instance"));
                    log.fine("Checking already created profile");
                    if (list != null) {
                        for (MCInstance instance : list) {
                            if (profile.getInstance(instance) == null) {
                                profile.addInstance(instance);
                            }
                        }
                    }
                    CoreHandler.getConfiguration().addProfile(profile);
                    profile.setLogged(true);
                    MainFrame.getWindow().refresh(profile);
                    dialog.dispose();
                } catch (Exception e) {
                    jProgressBar1.setVisible(false);
                    log.throwing(this.getClass().getName(), "createPremiumProfile", e);
                    status.setForeground(Color.red);
                    status.setText("Failed! Cause: " + e.getMessage());
                    try {
                        IO.delete(instances);
                    } catch (Exception ex) {
                        log.throwing(this.getClass().getName(), "createPremiumProfile", e);
                    }
                }
            }
        });
    }

    private void createBaseProfile() {
        int i = MessageControl.showConfirmDialog("This will allow to play Minecraft for 30 minutes"
                + " only!", "Warning", 0, 2);
        if (i != 0) return;
        final JDialog dialog = this;
        jProgressBar1.setVisible(true);
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                status.setText("");
                status.setForeground(Color.black);
                Path instances = checkPath();
                if (instances == null) {
                    log.config("Aborting creation");
                    jProgressBar1.setVisible(false);
                    return;
                }
                Profile profile = new Profile(userTextField.getText(), instances);
                List<MCInstance> list = checkAlreadyCreatedProfile(Paths.get(instances.toString(),
                        ".Instance"));
                log.fine("Checking already created profile");
                if (list != null) {
                    for (MCInstance instance : list) {
                        if (profile.getInstance(instance) == null) {
                            profile.addInstance(instance);
                        }
                    }
                }
                CoreHandler.getConfiguration().addProfile(profile);
                profile.setLogged(true);
                MainFrame.getWindow().refresh(profile);
                dialog.dispose();
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        createButton = new javax.swing.JButton();
        jProgressBar1 = new javax.swing.JProgressBar();
        mc_logo = new javax.swing.JLabel();
        username = new javax.swing.JLabel();
        userTextField = new javax.swing.JTextField();
        password = new javax.swing.JLabel();
        passwordTextField = new javax.swing.JPasswordField();
        login = new javax.swing.JButton();
        register = new javax.swing.JButton();
        pathLabel = new javax.swing.JLabel();
        instancePath = new javax.swing.JTextField();
        status = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Profile creator");

        createButton.setText("Create demo profile");
        createButton.setToolTipText("Creates a demo profile with 30 minutes to access Minecraft");
        createButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createButtonActionPerformed(evt);
            }
        });

        jProgressBar1.setFocusable(false);
        jProgressBar1.setIndeterminate(true);

        mc_logo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        mc_logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/org/infernage/aslauncher/resources/MC_logo.png"))); // NOI18N

        username.setText("Username");
        username.setFocusable(false);

        userTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userTextFieldActionPerformed(evt);
            }
        });

        password.setText("Password");
        password.setFocusable(false);

        passwordTextField.setToolTipText("Password of your Minecraft account. Leave in blank if you prefer a basic profile");
        passwordTextField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userTextFieldActionPerformed(evt);
            }
        });

        login.setText("Create");
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        register.setText("Register");
        register.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                registerActionPerformed(evt);
            }
        });

        pathLabel.setText("Instance path");

        instancePath.setText("Default");
        instancePath.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                instancePathFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                instancePathFocusLost(evt);
            }
        });

        status.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(status, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jProgressBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(mc_logo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(login, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(createButton)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(register, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(username)
                                    .addComponent(password)
                                    .addComponent(pathLabel))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(userTextField)
                                    .addComponent(passwordTextField)
                                    .addComponent(instancePath, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(55, 55, 55))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(mc_logo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(status, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(userTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(username))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(passwordTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(password))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(pathLabel)
                    .addComponent(instancePath, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(login, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(register, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(createButton, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jProgressBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void registerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_registerActionPerformed
        try {
            Desktop.getDesktop().browse(new URI(References.MINECRAFT_REGISTER));
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "Desktop browser not found", e);
        }
    }//GEN-LAST:event_registerActionPerformed

    private void userTextFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userTextFieldActionPerformed
        if (passwordTextField.getPassword().length > 0) {
            createPremiumProfile();
        } else {
            createBaseProfile();
        }
    }//GEN-LAST:event_userTextFieldActionPerformed

    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed
        createPremiumProfile();
    }//GEN-LAST:event_loginActionPerformed

    private void createButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createButtonActionPerformed
        createBaseProfile();
    }//GEN-LAST:event_createButtonActionPerformed

    private void instancePathFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_instancePathFocusGained
        if (instancePath.getText().equals("Default")){
            instancePath.setText("");
        }
    }//GEN-LAST:event_instancePathFocusGained

    private void instancePathFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_instancePathFocusLost
        if (instancePath.getText().isEmpty()){
            instancePath.setText("Default");
        }
    }//GEN-LAST:event_instancePathFocusLost

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton createButton;
    private javax.swing.JTextField instancePath;
    private javax.swing.JProgressBar jProgressBar1;
    private javax.swing.JButton login;
    private javax.swing.JLabel mc_logo;
    private javax.swing.JLabel password;
    private javax.swing.JPasswordField passwordTextField;
    private javax.swing.JLabel pathLabel;
    private javax.swing.JButton register;
    private javax.swing.JLabel status;
    private javax.swing.JTextField userTextField;
    private javax.swing.JLabel username;
    // End of variables declaration//GEN-END:variables
}
