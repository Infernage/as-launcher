/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.gui;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.mcsystem.versions.CompleteVersion;
import org.infernage.aslauncher.mcsystem.versions.ReleaseType;
import org.infernage.aslauncher.mcsystem.versions.Version;
import org.infernage.aslauncher.mcsystem.versions.VersionList;
import org.infernage.aslauncher.util.ASLUtils;
import org.infernage.aslauncher.util.MCUtils;
import org.infernage.aslauncher.util.NetworkUtils;

/**
 * Gui used to install a new MC version.
 *
 * @author Infernage
 */
public class InstanceForm extends javax.swing.JDialog {

    private final static Logger log = Logger.getLogger(InstanceForm.class.getName());

    private Profile profile;
    private VersionList versions;
    //private ModPackList modpacks; TODO Modpack installer

    /**
     * Creates new form InstanceForm
     */
    public InstanceForm(java.awt.Frame parent, boolean modal, Profile profile) {
        super(parent, modal);
        initComponents();
        jTabbedPane1.setEnabledAt(1, false);
        this.profile = profile;
        versions = CoreHandler.getMCVersions();
        DefaultListModel model = new DefaultListModel();
        if (versions == null) {
            model.addElement("No versions found");
        } else {
            String latest = versions.getLatestVersion().get("release");
            model.addElement(latest);
            for (Version version : versions.getVersionList()) {
                if (version.getType().equals(ReleaseType.release) && !version.getId()
                        .equalsIgnoreCase(latest)) {
                    model.addElement(version.getId());
                }
            }
        }
        versionList.setModel(model);
        /*modpacks = Loader.getModPackList();
         model = new DefaultListModel();
         if (modpacks == null){
         model.addElement("No modpacks found");
         } else{
         if (modpacks.getLatestModPacks().isEmpty()) model.addElement("No modpacks found");
         else{
         for (String title : modpacks.getLatestModPacks().keySet()) {
         model.addElement(title + " V" + modpacks.getLatestModPacks().get(title).getVersion() + 
         " MC" + modpacks.getLatestModPacks().get(title).getMinecraftVersion());
         }
         }
         }
         modpackList.setModel(model);*/
    }

    private final static String firstHost = "2shared.com", secondHost = "sendspace.com",
            thirdHost = "dropbox.com", fouthHost = "mega.co.nz";

    private String getSignificantURL(String link) throws MalformedURLException, IOException {
        boolean stop = false;
        if (link.contains(firstHost)) {
            URL url = new URL(link);
            try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
                String lin;
                while (((lin = in.readLine()) != null) && !stop) {
                    if (lin.contains(firstHost + "/download") && lin.contains(".cxz")) {
                        StringTokenizer token = new StringTokenizer(lin, "'\"");
                        while (token.hasMoreTokens()) {
                            String te = token.nextToken();
                            if (te.contains(".cxz")) {
                                link = te;
                                stop = true;
                            }
                        }
                    }
                }
            }
        } else if (link.contains(secondHost)) {
            URL url = new URL(link);
            try (BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()))) {
                String lin;
                while (((lin = in.readLine()) != null) && !stop) {
                    if (lin.contains("download_button") && lin.contains(secondHost) && lin.contains(".cxz")) {
                        StringTokenizer token = new StringTokenizer(lin, "'\"");
                        while (token.hasMoreTokens()) {
                            String te = token.nextToken();
                            if (te.contains(".cxz") && te.contains(secondHost)) {
                                link = te;
                                stop = true;
                            }
                        }
                    }
                }
            }
        } else if (link.contains(thirdHost) || link.contains(fouthHost)) {
            stop = true;
        } else {
            return "";
        }
        return link;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        versionList = new javax.swing.JList();
        installVersion = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        modpackList = new javax.swing.JList();
        installModPack = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Instance installer");
        setResizable(false);

        versionList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(versionList);

        installVersion.setText("Install");
        installVersion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                installVersionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(installVersion)
                .addContainerGap(258, Short.MAX_VALUE))
            .addComponent(jScrollPane1)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(installVersion)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Minecraft versions", jPanel1);

        modpackList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(modpackList);

        installModPack.setText("Install");
        installModPack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                installModPackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(installModPack)
                .addContainerGap(258, Short.MAX_VALUE))
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 329, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(installModPack)
                .addContainerGap(32, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("ModPacks", jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void installVersionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_installVersionActionPerformed
        String mc_version = (String) versionList.getSelectedValue();
        if (mc_version == null) {
            return;
        }
        if (mc_version.equals("No versions found")) {
            return;
        }
        String name;
        log.log(Level.FINE, "Checking name {0}", mc_version);
        try {
            name = ASLUtils.checkFileName(mc_version, profile.getPath());
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed checking the instance name", e);
            return;
        }
        Path namePath = Paths.get(profile.getPath().toString(), "instances", name);
        try {
            if (Files.notExists(namePath)) {
                Files.createDirectories(namePath);
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed creating the instance directory", e);
            return;
        }
        CompleteVersion version;
        log.config("Requesting JSON object");
        try {
            String get = NetworkUtils.requestGetMethod(MCUtils.getJSONVersion(mc_version));
            version = new Gson().fromJson(get, CompleteVersion.class);
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed creating the instance", e);
            try {
                Files.deleteIfExists(namePath);
            } catch (Exception ex) {
                // Ignore
            }
            return;
        }
        final MCInstance inst = new MCInstance(namePath.getFileName().toString(), namePath, version);
        profile.addInstance(inst);
        try {
            ASLUtils.saveInstances(profile);
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to save instances", e);
            // Ignore
        }
        MainFrame.getWindow().refresh(profile);
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                MainFrame.getWindow().setWorking(true);
                try {
                    inst.requestNecessaryFiles();
                } catch (Exception e) {
                    log.log(Level.SEVERE, "Failed to download MC files", e);
                }
                MainFrame.getWindow().setWorking(false);
                log.log(Level.FINE, "Version {0} installed", inst.getVersion().getId());
            }
        });
        this.dispose();
    }//GEN-LAST:event_installVersionActionPerformed

    private void installModPackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_installModPackActionPerformed
        /*final String modpack = (String) modpackList.getSelectedValue();
         if (modpack == null) return;
         if (modpack.equals("No modpacks found")) return;
         String name = Util.checkFileName(modpack, profile.getPath());
         final File namePath = new File(profile.getPath(), name);
         namePath.mkdirs();
         ThreadPool.getInstance().execute(new Runnable() {

         @Override
         public void run() {
         try {
         Loader.getMainGui().getConsoleTab().println("Downloading ModPack. Please wait...");
         DownloadJob job = new DownloadJob("Modpack installer", Loader.getMainGui()
         .getProgressBar());
         String title;
         if (modpack.split(" ").length == 3){
         title = modpack.split(" ")[0];
         } else{
         String[] split = modpack.split(" ");
         int length = split.length - 2;
         title = split[0];
         for (int i = 1; i < length; i++) {
         title = title + " " + split[i];
         }
         }
         ModPack pack = modpacks.getLatestModPacks().get(title);
         String url = getSignificantURL(pack.getURL());
         job.addJob(new DefaultEngine(new URL(url), job, namePath, true));
         List<Future<File>> file = job.startJob();
         File decompressed = Compressor.secureDecompression(file.get(0).get(), null, true);
         decompressed.renameTo(namePath);
         file.get(0).get().delete();
         IO.deleteDirectory(decompressed);
         decompressed.delete();
         Instances instance = new Instances(modpack, namePath, new Gson().fromJson(Util
         .requestGetMethod(Util.getCompleteVersionJson(pack.getMinecraftVersion())),
         CompleteVersion.class));
         instance.setInfo(pack.getName() + " V" + pack.getVersion() + "\n" + 
         pack.getInformation());
         for (Map.Entry<String, String> entry : pack.getModPaths().entrySet()) {
         String key = entry.getKey();
         String value = entry.getValue();
         if (value.startsWith("jars")){
         instance.addMod(key, new File(namePath, value), Instances.JAR);
         } else if (value.startsWith("coremods")){
         instance.addMod(key, new File(namePath, value), Instances.CORE);
         } else{
         instance.addMod(key, new File(namePath, value), Instances.MOD);
         }
         }
         profile.addInstance(instance);
         Loader.getMainGui().notifyListeners();
         try {
         Util.saveInstances(profile);
         } catch (Exception e) {
         //Ignore
         }
         } catch (Exception e) {
         e.printStackTrace();
         IO.deleteDirectory(namePath);
         namePath.delete();
         }
         }
         });
         Loader.getMainGui().setSelectedTab(3);
         this.dispose();*/
    }//GEN-LAST:event_installModPackActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton installModPack;
    private javax.swing.JButton installVersion;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JList modpackList;
    private javax.swing.JList versionList;
    // End of variables declaration//GEN-END:variables
}
