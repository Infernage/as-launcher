/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.gui;

import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.gson.GsonBuilder;
import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.ParserConfigurationException;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.infernage.aslauncher.core.CoreConfiguration;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.concurrent.Downloader;
import org.infernage.aslauncher.core.concurrent.SingleEngine;
import org.infernage.aslauncher.core.crypto.Crypto;
import org.infernage.aslauncher.core.crypto.SuiteCrypto;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.MinecraftLoader;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.mcsystem.authenticator.Authenticator;
import org.infernage.aslauncher.mcsystem.forge.CompleteLocalMinecraftForge;
import org.infernage.aslauncher.mcsystem.forge.ForgeList;
import org.infernage.aslauncher.mcsystem.forge.Installer;
import org.infernage.aslauncher.mcsystem.forge.MCMod;
import org.infernage.aslauncher.mcsystem.forge.RemoteMinecraftForge;
import org.infernage.aslauncher.mcsystem.versions.CompleteVersion;
import org.infernage.aslauncher.mcsystem.versions.Library;
import org.infernage.aslauncher.util.ASLUtils;
import org.infernage.aslauncher.util.IO;
import org.infernage.aslauncher.util.MCUtils;
import org.infernage.aslauncher.util.MessageControl;
import org.infernage.aslauncher.util.NetworkUtils;
import org.infernage.aslauncher.util.References;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * MainFrame controller.
 *
 * @author Infernage
 */
public class MainFrameController {

    public static final int animation_resize = 2763;

    private static final Logger log = Logger.getLogger(MainFrameController.class.getName());

    void createCallback(ListenableFuture future) {
        final MainFrameController controller = this;
        FutureCallback<Void> callback = new FutureCallback<Void>() {

            @Override
            public void onSuccess(Void v) {
                synchronized (controller) {
                    busy = false;
                }
            }

            @Override
            public void onFailure(Throwable thrwbl) {
                synchronized (controller) {
                    busy = false;
                }
            }
        };
        Futures.addCallback(future, callback);
    }

    private volatile boolean busy = false;

    boolean isBusy() {
        return busy || CoreHandler.getGuiController().isBusy();
    }

    void setBusy(boolean b) {
        busy = b;
    }

    /**
     * Requests the Forge list.
     *
     * @param label The JLabel to set the status.
     */
    void requestForgeList(final JLabel label) {
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                try {
                    log.fine("Requesting forge list");
                    new ForgeList().getForgeList();
                    label.setText("Forge list ready");
                } catch (Exception e) {
                    log.log(Level.WARNING, "Failed to request forge list", e);
                }
            }
        });
    }

    /**
     * Logins with Minecraft server.
     *
     * @param username The username.
     * @param key The password.
     * @return A Profile Object.
     */
    Profile login(String username, String key) {
        log.log(Level.FINE, "Searching existing profile {0}", username);
        Profile pro = null;
        for (Profile profile : CoreHandler.getConfiguration().getProfiles()) {
            if (username.equals(profile.getProfilename())) {
                pro = profile;
                log.config("Found");
                break;
            }
        }
        if (pro == null) {
            MessageControl.showErrorMessage("Profile not found", "The profile " + username
                    + " isn't in the database.\nPlease, try with another one or create one.");
            return null;
        }
        if (pro.isLogged()) {
            log.config("Profile already logged");
            return null;
        }
        log.fine("Logging...");
        try {
            Crypto crypto = SuiteCrypto.getSuite(SuiteCrypto.Type.AES256);
            crypto.setPassword(pro.getUsername() + "_" + pro.getSessionID());
            System.out.println(pro.getKey());
            String storedPassword = crypto.decryptData(pro.getKey());
            if (!storedPassword.equals(key)) {
                MessageControl.showErrorMessage("Password missmatch", "The password is not correct.");
                return null;
            }
            Authenticator.login(pro);
            pro.setLogged(true);
            log.fine("Logged");
            return pro;
        } catch (Exception e) {
            if (e.toString().contains("503 for URL:")) {
                log.severe("Authentication failed! Username/Password incorrect");
                log.throwing(this.getClass().getName(), "login", e);
            } else {
                log.log(Level.SEVERE, "Authentication failed", e);
            }
        }
        return null;
    }

    /**
     * Checks the mods of the given instance.
     *
     * @param instance The instance to check.
     */
    void check(MCInstance instance) {
        if (isBusy()) {
            return;
        }
        log.config("Checking mods");
        Path mods = Paths.get(instance.getPath().toString(), "mods"),
                jar = Paths.get(instance.getPath().toString(), "jars"),
                core = Paths.get(instance.getPath().toString(), "coremods");
        try {
            if (Files.notExists(mods)) {
                Files.createDirectories(mods);
            }
            if (instance.isLower1_6()) {
                if (Files.notExists(jar)) {
                    Files.createDirectories(jar);
                }
                if (Files.notExists(core)) {
                    Files.createDirectories(core);
                }
            }
        } catch (Exception e) {
            log.throwing(this.getClass().getName(), "check", e);
        }
    }

    /**
     * Checks the given path for the selected profile.
     *
     * @param instancePath The path to the profile.
     * @param selected The selected profile.
     * @return
     */
    Path checkPath(String instancePath, Profile selected) {
        log.log(Level.FINE, "Checking instance path for {0}", instancePath);
        Path path = Paths.get(instancePath);
        try {
            if (Files.notExists(path)) {
                Files.createDirectories(path);
            } else if (!Files.isDirectory(path)) {
                return null;
            } else if (Files.isDirectory(path)) {
                if (Files.exists(Paths.get(instancePath, ".Instance"))) {
                    return path;
                }
                path = Paths.get(path.toString(), selected.getProfilename() + " MCGame dir");
                if (Files.notExists(path)) {
                    Files.createDirectories(path);
                }
            }
            log.config("Creating dump file");
            Path hide = Paths.get(path.toString(), ".Instance");
            if (Files.notExists(hide)) {
                Files.createFile(hide);
            }
            log.config("Setting attributes");
            if (CoreHandler.getConfiguration().getOS() == CoreConfiguration.OS.windows) {
                Files.setAttribute(hide, "dos:hidden", Boolean.TRUE, LinkOption.NOFOLLOW_LINKS);
            }
        } catch (Exception e) {
            log.log(Level.WARNING, "Failed to check instance path", e);
            return null;
        }
        return path;
    }

    /**
     * Executes the installer/uninstaller of Forge.
     *
     * @param forgeButton The Forge gui button.
     * @param label The status label.
     * @param instance The MC instance.
     * @param forgeVersion The Forge version.
     * @param callback A callback after the action.
     */
    void executeForgeAction(final JButton forgeButton, final JLabel label, final MCInstance instance,
            final String forgeVersion, FutureCallback callback) {
        ListenableFuture forge = CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                check(instance);
                if (forgeButton.getText().equals("Remove")) {
                    forgeButton.setEnabled(false);
                    forgeButton.setText("Install");
                    instance.removeForge();
                    label.setText("N/A");
                } else {
                    forgeButton.setEnabled(false);
                    log.info("Getting forge version");
                    if (forgeVersion == null) {
                        return;
                    }
                    RemoteMinecraftForge forge = null;
                    log.info("Checking forge into remote list");
                    try {
                        for (RemoteMinecraftForge remote : ForgeList.getForgeList()) {
                            if (remote.getForgeVersion().equals(forgeVersion)) {
                                forge = remote;
                                break;
                            }
                        }
                    } catch (Exception e) {
                        log.log(Level.WARNING, "Failed retrieving forge list", e);
                        return;
                    }
                    if (forge != null) {
                        Installer.installForge(forge, instance);
                    } else {
                        MessageControl.showErrorMessage("The forge version selected is not in the database",
                                "Forge not found");
                    }
                }
            }
        });
        Futures.addCallback(forge, callback);
    }

    /**
     * Gets the MC mod list of the version specified.
     *
     * @param list The Gui list to set the model.
     * @param version The MC version.
     */
    void getModList(JList list, String version) {
        try {
            List<MCMod> mods = MCUtils.getModList(version);
            DefaultListModel model = new DefaultListModel();
            for (MCMod mod : mods) {
                StringBuilder show = new StringBuilder(mod.getName());
                for (String string : mod.getDependencies()) {
                    show.append(" - ").append(string);
                }
                if (mod.getOther() != null) {
                    show.append(" - ").append(mod.getOther());
                }
                model.addElement(show.toString());
            }
            list.setModel(model);
            list.clearSelection();
        } catch (IOException ex) {
            log.log(Level.WARNING, "Failed to get MC mod list from version "
                    + version, ex);
        }
    }

    void importModPack(final Profile selected) {
        final JFileChooser chooser = new JFileChooser(System.getProperty("user.home"));
        FileFilter filter = new FileFilter() {

            @Override
            public boolean accept(File f) {
                return f.getName().endsWith(getDescription()) || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return ".inst";
            }
        };
        chooser.addChoosableFileFilter(filter);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setFileFilter(filter);
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        final int value = chooser.showOpenDialog(MainFrame.getWindow());
        CoreHandler.INSTANCE.executeTask(new Runnable() {

            @Override
            public void run() {
                if (value == JFileChooser.APPROVE_OPTION && selected.isPremium()) {
                    log.info("Importing, please wait...");
                    MainFrame.getWindow().setBusy(true);
                    MainFrame.getWindow().setWorking(true);
                    File src = chooser.getSelectedFile();
                    Path packjson = Paths.get(src.getAbsolutePath(), "minecraft", "pack.json");
                    if (Files.exists(packjson) && Files.isRegularFile(packjson)) {
                        importFTB(selected, src, packjson);
                    } else if (src.getName().equals(".minecraft") && src.isDirectory()) {
                        importNMC(selected, src);
                    } else if (src.getName().endsWith(".inst") && src.isFile()) {
                        importASL(selected, src);
                    } else {
                        MessageControl.showErrorMessage("The folder " + src.getPath()
                                + " is not a Minecraft modpack", "Not a minecraft file!");
                        MainFrame.getWindow().printMsg("Ready");
                    }
                    MainFrame.getWindow().setWorking(false);
                    MainFrame.getWindow().refresh(selected);
                    MainFrame.getWindow().setBusy(false);
                }
            }
        });
    }

    private void importFTB(Profile selected, File src, Path packjson) {
        MessageControl.showInfoMessage("FTB modpack detected.", null);
        MCInstance aslInstance = null;
        Path instPath = null;
        try {
            String strjson = IOUtils.toString(Files.newInputStream(packjson, StandardOpenOption.READ),
                    Charsets.UTF_8);
            CompleteVersion ftbversion = new GsonBuilder().create().fromJson(strjson,
                    CompleteVersion.class);
            Library forge = null;
            for (Library library : ftbversion.getLibraries()) {
                if (library.getName().contains("net.minecraftforge:")) {
                    forge = library;
                    break;
                }
            }
            instPath = Paths.get(selected.getPath().toString(), "instances",
                    ASLUtils.checkFileName(src.getName(), selected.getPath()));
            if (Files.notExists(instPath)) {
                Files.createDirectories(instPath);
            }
            log.config("Requesting JSON object for MC version");
            CompleteVersion mcversion = getMCVersionForFTB(src, References.FTBMODPACKS);
            if (mcversion == null) {
                // Check thirparty
                mcversion = getMCVersionForFTB(src, References.FTBTHIRDPARTY);
                if (mcversion == null) {
                    throw new Exception("Couldn't find MC version target for FTB modpack "
                            + src.getName());
                }
            }
            aslInstance = new MCInstance(src.getName(), instPath, mcversion);
            aslInstance.requestNecessaryFiles();
            MainFrame.getWindow().printMsg("Checking forge");
            if (aslInstance.isLower1_6()) {
                Files.createDirectories(instPath.resolve("jars"));
            }
            if (forge == null) {
                // Not possible to get forge, set as Unknown.
                Files.copy(Paths.get(src.getAbsolutePath(), "instMods", "MinecraftForge.zip"),
                        instPath.resolve("MinecraftForge.zip"));
                aslInstance.addForge(new CompleteLocalMinecraftForge(mcversion,
                        instPath.resolve("MinecraftForge.zip"), "Unknown", mcversion.getId(),
                        null, MinecraftLoader.class.getCanonicalName()));
                for (File jmod : new File(src, "instMods")
                        .listFiles((java.io.FileFilter) FileFileFilter.FILE)) {
                    if (!jmod.getName().equals("MinecraftForge.zip")) {
                        log.log(Level.FINE, "Importing jar mod {0}", jmod.getName());
                        Files.copy(jmod.toPath(), instPath.resolve("jars").resolve(jmod.getName()));
                        if (jmod.toString().endsWith(".jar") || jmod.toString().endsWith(".zip")) {
                            aslInstance.addMod(jmod.getName(), MCInstance.JAR);
                        }
                    }
                }
            } else {
                String fversion = forge.getName().replace(":", "").replace("net.minecraftforge", "")
                        .replace("minecraftforge", "").replace("forge", "").replace("universal", "")
                        .replace("client", "").replace(mcversion.getId() + "-", "");
                ftbversion.getLibraries().remove(forge);
                String[] name = forge.getName().split(":");
                String url = forge.hasOwnURL() ? forge.getURL() + (forge.getName()
                        .split(":").length > 3 ? forge.getPath(forge.getName().split(":")[3])
                        : forge.getPath()) : References.MINECRAFT_FORGE_BASE + "maven/" + name[0]
                        .replaceAll("\\.", "/") + "/forge/" + mcversion.getId() + "-" + name[2]
                        + "/forge-" + mcversion.getId() + "-" + name[2] + "-universal.jar";
                Downloader forgeDownload = new SingleEngine(new URL(url), instPath, true, "Forge import");
                Path forgePath = forgeDownload.call();
                aslInstance.addForge(new CompleteLocalMinecraftForge(ftbversion, forgePath, fversion,
                        mcversion.getId(), ftbversion.getLibraries(), ftbversion.getMainClass()));
            }
            for (File file : new File(src, "minecraft").listFiles((java.io.FileFilter) DirectoryFileFilter.DIRECTORY)) {
                log.log(Level.FINE, "Importing {0}", file.getName());
                IO.copy(file.toPath(), instPath.resolve(file.getName()));
                if (file.getName().equals("mods")) {
                    for (File mod : file.listFiles()) {
                        if (mod.toString().endsWith(".jar") || mod.toString().endsWith(".zip")) {
                            log.log(Level.FINE, "Importing mod {0}", mod.getName());
                            aslInstance.addMod(mod.getName(), MCInstance.MOD);
                        }
                    }
                }
                if (file.getName().equals("coremods")) {
                    for (File mod : file.listFiles()) {
                        if (mod.toString().endsWith(".jar") || mod.toString().endsWith(".zip")) {
                            log.log(Level.FINE, "Importing coremod {0}", mod.getName());
                            aslInstance.addMod(mod.getName(), MCInstance.CORE);
                        }
                    }
                }
            }
            if (Files.exists(instPath.resolve("bin")) && Files.exists(instPath.resolve("bin").resolve("Minecraft.jar"))) {
                Files.move(instPath.resolve("bin").resolve("Minecraft.jar"),
                        instPath.resolve(mcversion.getId() + ".jar"), StandardCopyOption.REPLACE_EXISTING);
            }
            selected.addInstance(aslInstance);
            ASLUtils.saveInstances(selected);
            MainFrame.getWindow().printMsg("Imported");
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to import FTB modpack", e);
            if (instPath != null) {
                try {
                    IO.delete(instPath);
                } catch (Exception ex) {
                    log.throwing(this.getClass().getName(), "importFTB", ex);
                }
            }
            if (aslInstance != null) {
                selected.removeInstance(aslInstance);
            }
        }
    }

    private CompleteVersion getMCVersionForFTB(File src, String url)
            throws IOException, SAXException, ParserConfigurationException {
        Document doc = new DocumentBuilderFactoryImpl().newDocumentBuilder()
                .parse(NetworkUtils.requestGetMethodToStream(url));
        NodeList modPacks = doc.getElementsByTagName("modpack");
        for (int i = 0; i < modPacks.getLength(); i++) {
            NamedNodeMap attrs = modPacks.item(i).getAttributes();
            try {
                if (attrs.getNamedItem("dir").getTextContent().equals(src.getName())) {
                    log.log(Level.CONFIG, "Modpack found. MC version target = {0}",
                            attrs.getNamedItem("mcVersion").getTextContent());
                    return new GsonBuilder().create().fromJson(NetworkUtils
                            .requestGetMethod(MCUtils.getJSONVersion(attrs.getNamedItem("mcVersion")
                                            .getTextContent())), CompleteVersion.class);
                }
            } catch (Exception e) {
                // Ignore, possible NullPointerException
            }
        }
        return null;
    }

    private static class MCProfileMapValue {

        public String name;
        public String lastVersionId;
    }

    private static class MCProfileJSON {

        public Map<String, MCProfileMapValue> profiles;
        public String selectedProfile;
    }

    private void importNMC(Profile selected, File src) {
        Path json = Paths.get(src.getAbsolutePath(), "launcher_profiles.json");
        Path instPath = null;
        MCInstance aslInstance = null;
        if (Files.exists(json)) {
            MessageControl.showInfoMessage("Minecraft folder detected. This will import your LAST selected profile\nfrom the Mojang launcher.", null);
            try {
                MCProfileJSON mcprofile = new GsonBuilder().create().fromJson(IOUtils
                        .toString(Files.newInputStream(json, StandardOpenOption.READ), Charsets.UTF_8),
                        MCProfileJSON.class);
                String lastId = mcprofile.profiles.get(mcprofile.selectedProfile).lastVersionId;
                if (lastId == null) {
                    lastId = CoreHandler.getMCVersions().getLatestVersion().get("release");
                }
                lastId = ASLUtils.checkFileName(lastId, selected.getPath());
                instPath = Paths.get(selected.getPath().toString(), "instances", lastId);
                if (Files.notExists(instPath)) {
                    Files.createDirectories(instPath);
                }
                log.log(Level.CONFIG, "Requesting JSON object from id {0}", lastId);
                CompleteVersion version = new GsonBuilder().create().fromJson(NetworkUtils
                        .requestGetMethod(MCUtils.getJSONVersion(lastId.split("-")[0])),
                        CompleteVersion.class);
                aslInstance = new MCInstance(lastId, instPath, version);
                if (mcprofile.selectedProfile.equals("Forge")) {
                    log.fine("Forge detected! Checking and installing...");
                    RemoteMinecraftForge remote = null;
                    for (RemoteMinecraftForge remoteMinecraftForge : ForgeList.getForgeList()) {
                        if (remoteMinecraftForge.getForgeVersion().equals(lastId.split("-")[1]
                                .replace("Forge", ""))) {
                            remote = remoteMinecraftForge;
                            break;
                        }
                    }
                    if (remote != null) {
                        Installer.installForge(remote, aslInstance);
                    } else {
                        throw new Exception("Unknown forge version detected!\nForge "
                                + lastId.split("-")[1].replace("Forge", ""));
                    }
                }
                for (File file : src.listFiles((java.io.FileFilter) DirectoryFileFilter.DIRECTORY)) {
                    if (!file.getName().equals("assets") && !file.getName().equals("libraries")
                            && !file.getName().equals("versions")) {
                        log.log(Level.FINE, "Importing {0}", file.getName());
                        IO.copy(file.toPath(), instPath.resolve(file.getName()));
                    }
                    if (file.getName().equals("mods")) {
                        for (File mod : file.listFiles()) {
                            if (mod.toString().endsWith(".jar") || mod.toString().endsWith(".zip")) {
                                log.log(Level.FINE, "Importing mod {0}", mod.getName());
                                aslInstance.addMod(mod.getName(), MCInstance.MOD);
                            }
                        }
                    }
                    if (file.getName().equals("coremods")) {
                        for (File mod : file.listFiles()) {
                            if (mod.toString().endsWith(".jar") || mod.toString().endsWith(".zip")) {
                                log.log(Level.FINE, "Importing coremod {0}", mod.getName());
                                aslInstance.addMod(mod.getName(), MCInstance.CORE);
                            }
                        }
                    }
                }
                aslInstance.requestNecessaryFiles();
                selected.addInstance(aslInstance);
                ASLUtils.saveInstances(selected);
                MainFrame.getWindow().printMsg("Imported");
            } catch (Exception e) {
                log.log(Level.SEVERE, "Failed to import MC folder", e);
                if (instPath != null) {
                    try {
                        IO.delete(instPath);
                    } catch (Exception ex) {
                        log.throwing(this.getClass().getName(), "importNMC", ex);
                    }
                }
                if (aslInstance != null) {
                    selected.removeInstance(aslInstance);
                }
            }
        }
    }

    private void importASL(Profile selected, File src) {
        Path newPath = null;
        try {
            ZipFile zipFile = new ZipFile(src);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword("ASL");
            }
            FileHeader inst = zipFile.getFileHeader("INST");
            if (inst != null) {
                MCInstance instance = new GsonBuilder().create().fromJson(IOUtils
                        .toString(zipFile.getInputStream(inst)), MCInstance.class);
                try {
                    Files.deleteIfExists(Paths.get(selected.getPath().toString(), "INST"));
                } catch (Exception e) {
                    log.throwing(this.getClass().getName(), "importModPack",
                            e);
                }
                instance.rename(ASLUtils.checkFileName(instance.getName(), selected.getPath()));
                newPath = Paths.get(selected.getPath().toString(), "instances",
                        instance.getName());
                zipFile.extractAll(instance.getPath().toString());
                try {
                    Files.deleteIfExists(Paths.get(instance.getPath().toString(), "INST"));
                } catch (Exception e) {
                    // Ignore
                }
                instance.changePath(newPath);
                if (instance.getForge() != null) {
                    RemoteMinecraftForge remote = null;
                    for (RemoteMinecraftForge remoteMinecraftForge : ForgeList.getForgeList()) {
                        if (remoteMinecraftForge.getForgeVersion().equals(instance.getForge().getForgeVersion())) {
                            remote = remoteMinecraftForge;
                            break;
                        }
                    }
                    if (remote != null) {
                        Installer.installForge(remote, instance);
                    } else {
                        throw new Exception("Unknown forge version detected!\nForge "
                                + instance.getForge().getForgeVersion());
                    }
                }
                selected.addInstance(instance);
                MainFrame.getWindow().refresh(selected);
                log.info("Instance imported");
            } else {
                throw new Exception("Corrupted instance. INST file not found");
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to import", e);
            try {
                Files.deleteIfExists(Paths.get(selected.getPath().toString(), "INST"));
            } catch (Exception ex) {
                log.throwing(this.getClass().getName(), "importButtonActionPerformed",
                        ex);
            }
            if (newPath != null) {
                try {
                    IO.delete(newPath);
                } catch (Exception ex) {
                    log.throwing(this.getClass().getName(), "importButtonActionPerformed",
                            ex);
                }
            }
        }
    }
}
