/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.util;

import java.util.Random;

/**
 * Class used to store all references.
 * @author Infernage
 */
public final class References {
    // Program
    public final static String ASL_VERSION = "0.6.0 Beta";
    
    public final static String ASL_AUTHOR = "Infernage";
    
    // Authentication
    public final static String AUTHENTICATION_URL = "https://authserver.mojang.com/authenticate";

    public final static String AUTHENTICATION_REFRESH = "https://authserver.mojang.com/refresh";

    // MC Mods
    public final static String MINECRAFT_FORGE_BASE = "http://files.minecraftforge.net/";
    
    @Deprecated
    public final static String MINECRAFT_FORGE = MINECRAFT_FORGE_BASE + "minecraftforge/";

    @Deprecated
    public final static String MINECRAFT_FORGE_OLD_VERSIONS
            = MINECRAFT_FORGE + "index_legacy.html";
    
    public final static String SUBMIT_MOD_URL = "http://modlist.mcf.li/submit/form";
    
    public final static String MINECRAFT_MODS_URL
            = "http://www.minecraftforum.net/forums/mapping-and-modding/minecraft-mods";

    // Minecraft
    public final static String MINECRAFT_RESOURCES = "http://resources.download.minecraft.net/";

    public final static String MINECRAFT_REGISTER = "https://account.mojang.com/register";

    public final static String MINECRAFT_DOWNLOAD_BASE = "https://s3.amazonaws.com/Minecraft.Download/";

    public final static String MINECRAFT_BLOG = "http://mcupdate.tumblr.com";

    public final static String MINECRAFT_VERSIONS_URL
            = "https://s3.amazonaws.com/Minecraft.Download/versions/versions.json";

    public final static String MINECRAFT_LIBRARIES = "https://libraries.minecraft.net/";
    
    // FTB Modpacks
    public final static String FTBTHIRDPARTY = new Random().nextBoolean() ?
            "http://ftb.cursecdn.com/FTB2/static/thirdparty.xml" :
            "http://new.creeperrepo.net/FTB2/static/thirdparty.xml";
    
    public final static String FTBMODPACKS = new Random().nextBoolean() ?
            "http://ftb.cursecdn.com/FTB2/static/modpacks.xml" :
            "http://new.creeperrepo.net/FTB2/static/modpacks.xml";
}
