/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.util;

import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;

/**
 * Class used to help in cryptographic operations.
 *
 * @author Infernage
 */
public final class CryptoUtils {

    /**
     * Obtains a local digest from a file.
     *
     * @param targetFile The local file.
     * @param algorithim The algorithim to use.
     * @param hashLength The hash length.
     * @return The local digest.
     * @throws java.lang.Exception
     */
    public static String getDigest(Path targetFile, String algorithim, String hashLength) throws Exception {
        try (FileChannel input = FileChannel.open(targetFile, StandardOpenOption.READ)) {
            ByteBuffer bb = ByteBuffer.allocate(1024 * 256);
            MessageDigest digest = MessageDigest.getInstance(algorithim);
            while (input.read(bb) > 0) {
                bb.flip();
                digest.update(bb);
                bb.compact();
            }
            return String.format("%1$0" + hashLength + "x", new Object[]{new BigInteger(1, digest
                .digest())});
        }
    }
}
