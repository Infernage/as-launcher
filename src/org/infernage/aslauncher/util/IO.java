/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.util;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Logger;

/**
 * This class implements all IO methods used.
 *
 * @author Infernage
 */
public class IO {

    private static boolean useNative;

    static {
        try {
            System.loadLibrary("ASLL");
            useNative = true;
        } catch (Throwable e) {
            useNative = false;
            Logger.getLogger(IO.class.getName()).throwing(IO.class.getName(), "<cinit>", e);
        }
    }

    /**
     * Removes a file or directory.
     *
     * @param file The path to the file or directory.
     */
    private static native void delete(String file);

    /**
     * Copies a file or directory.
     *
     * @param src The file or directory to be copied.
     * @param dst The target file or directory.
     */
    private static native void copy(String src, String dst);

    /**
     * Moves a file or directory.
     *
     * @param src The file or directory to be moved.
     * @param dst The target file or directory.
     */
    private static native void move(String src, String dst);

    /**
     * Checks if a file or directory is equal to another one.
     *
     * @param src The file or directory to check.
     * @param dst The file or directory to be checked.
     * @return {@code true} if the 2 files or directories are equals.
     */
    private static native boolean check(String src, String dst);

    /**
     * This method deletes a file or a directory.
     *
     * @param file The file or directory to delete
     * @throws IOException
     */
    public static void delete(Path file) throws IOException {
        if (useNative) {
            IO.delete(file.toAbsolutePath().toString());
        } else if (Files.exists(file) && Files.isDirectory(file)) {
            Files.walkFileTree(file, new DeleteVisitor());
        } else if (Files.exists(file) && Files.isRegularFile(file)) {
            Files.delete(file);
        }
    }

    /**
     * This method copies a source directory to a destiny directory.
     *
     * @param src The source directory
     * @param dst The destiny directory
     * @throws IOException
     */
    public static void copy(Path src, Path dst) throws IOException {
        if (useNative) {
            IO.copy(src.toAbsolutePath().toString(), dst.toAbsolutePath().toString());
        } else if (Files.exists(src) && Files.isDirectory(src)) {
            Files.walkFileTree(src, new CopyOrMoveVisitor(src, dst, true));
        } else if (Files.exists(src) && Files.isRegularFile(src)) {
            Files.copy(src, dst);
        }
    }

    /**
     * Moves a directory to another.
     *
     * @param src The directory to move.
     * @param dst The target directory.
     * @throws IOException
     */
    public static void move(Path src, Path dst) throws IOException {
        if (useNative) {
            IO.move(src.toAbsolutePath().toString(), dst.toAbsolutePath().toString());
        } else if (Files.exists(src) && Files.isDirectory(src)) {
            Files.walkFileTree(src, new CopyOrMoveVisitor(src, dst, false));
            Files.walkFileTree(src, new DeleteVisitor());
        } else if (Files.exists(src) && Files.isRegularFile(src)) {
            Files.move(src, dst);
        }
    }

    /**
     * Method to check if a file is equal to other one.
     *
     * @param src The source file
     * @param dst The destiny file
     * @return {@code true} if the two files are equals, and {@code false} in
     * otherwise
     */
    public static boolean check(Path src, Path dst) {
        if (useNative) {
            return check(src.toAbsolutePath().toString(), dst.toAbsolutePath().toString());
        } else {
            if (Files.notExists(src) || Files.notExists(dst)) {
                return false;
            }
            boolean res = false;
            try {
                long a = Files.size(src), b = Files.size(dst);
                String one = src.toAbsolutePath().toString(), two = dst.toAbsolutePath().toString();
                res = a == b && one.equals(two) && Files.isSameFile(src, dst);
            } catch (Exception e) {
                MessageControl.showExceptionMessage(e, "Error checking files");
            }
            return res;
        }
    }

    /**
     * This method checks if a directory is equal to another one.
     *
     * @param srcDir The source directory.
     * @param dstDir The destination directory.
     * @return {@code true} if the two directories are equals, and {@code false}
     * in otherwise
     */
    public static boolean checkDirectory(Path srcDir, Path dstDir) {
        if (useNative) {
            return check(srcDir.toAbsolutePath().toString(), dstDir.toAbsolutePath().toString());
        } else {
            boolean res = true;
            try {
                res = new CheckVisitor(srcDir, dstDir).check();
            } catch (Exception ex) {
                MessageControl.showExceptionMessage(ex, "Error checking files");
            }
            return res;
        }
    }

    private static class DeleteVisitor extends SimpleFileVisitor<Path> {

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
                throws IOException {
            Files.deleteIfExists(file);
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult postVisitDirectory(Path dir, IOException exc)
                throws IOException {
            if (exc == null) {
                Files.deleteIfExists(dir);
                return FileVisitResult.CONTINUE;
            } else {
                throw exc;
            }
        }
    }

    private static class CopyOrMoveVisitor extends SimpleFileVisitor<Path> {

        private Path src;
        private Path dst;
        private boolean copy;
        private StandardCopyOption option = StandardCopyOption.REPLACE_EXISTING;

        public CopyOrMoveVisitor(Path source, Path destiny, boolean isCopy) {
            src = source;
            dst = destiny;
            copy = isCopy;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException {
            Path target = dst.resolve(src.relativize(dir));
            if (Files.notExists(target)) {
                Files.createDirectories(target);
            }
            return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            if (copy) {
                Files.copy(file, dst.resolve(src.relativize(file)), option);
            } else {
                Files.move(file, dst.resolve(src.relativize(file)), StandardCopyOption.ATOMIC_MOVE);
            }
            return FileVisitResult.CONTINUE;
        }
    }

    private static class CheckVisitor extends SimpleFileVisitor<Path> {

        private Path src;
        private Path dst;
        private boolean res = true;

        public CheckVisitor(Path source, Path destiny) {
            src = source;
            dst = destiny;
        }

        public boolean check() throws IOException {
            Files.walkFileTree(src, this);
            return res;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            res = IO.check(file, dst.resolve(src.relativize(file)));
            return res ? FileVisitResult.CONTINUE : FileVisitResult.TERMINATE;
        }

    }
}
