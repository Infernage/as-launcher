/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import org.infernage.aslauncher.core.CoreConfiguration.OS;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.Profile;

/**
 * Class used to help in general ASL operations.
 *
 * @author Infernage
 */
public final class ASLUtils {

    /**
     * Rewrites the hidden file of a profile.
     *
     * @param profile
     * @throws IOException
     */
    public static void saveInstances(Profile profile) throws IOException {
        if (profile == null) {
            throw new NullPointerException();
        }
        Path hidden = Paths.get(profile.getPath().toString(), ".Instance");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        MCInstance[] list = profile.getList().toArray(
                new MCInstance[profile.getList().size()]);
        if (CoreHandler.getConfiguration().getOS() == OS.windows) {
            Files.setAttribute(hidden, "dos:hidden", Boolean.FALSE, LinkOption.NOFOLLOW_LINKS);
            try (PrintWriter pw = new PrintWriter(hidden.toFile())) {
                gson.toJson(list, pw);
            } finally {
                Files.setAttribute(hidden, "dos:hidden", Boolean.TRUE,
                        LinkOption.NOFOLLOW_LINKS);
            }
        } else {
            Path visible = Paths.get(profile.getPath().toString(), "Instance");
            try (PrintWriter pw = new PrintWriter(visible.toFile())) {
                Files.move(hidden, visible);
                gson.toJson(list, pw);
            } finally {
                Files.move(visible, hidden);
            }
        }
    }
    
    /**
     * Checks if an file name is repeated.
     *
     * @param name The name to check.
     * @param path The parent of the files.
     * @return The name checked.
     * @throws java.io.IOException
     */
    public static String checkFileName(String name, Path path) throws IOException {
        Path version = Paths.get(path.toString(), "instances");
        CheckVisitor checker = new CheckVisitor(name);
        return checker.check(version);
    }
    
    private static class CheckVisitor extends SimpleFileVisitor<Path> {

        private String name;
        private boolean finish = false;

        public CheckVisitor(String n) {
            name = n == null ? "Default" : n;
        }

        public String check(Path target) throws IOException {
            while (!finish) {
                Files.walkFileTree(target, this);
            }
            return name.endsWith("_") ? name.substring(0, name.length() - 1) : name;
        }

        @Override
        public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
            finish = checkFile(dir);
            return finish ? FileVisitResult.CONTINUE : FileVisitResult.TERMINATE;
        }

        @Override
        public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
            finish = checkFile(file);
            return finish ? FileVisitResult.CONTINUE : FileVisitResult.TERMINATE;
        }

        private boolean checkFile(Path file) {
            if (name.endsWith("_") && name.substring(0, name.length() - 1).contains("_")) {
                if (name.substring(0, name.length() - 1).equals(file.getFileName().toString())) {
                    int number = Integer.parseInt(name.substring(name.length() - 2, name.length() - 1));
                    name = name.substring(0, name.length() - 3) + "_" + (number + 1) + "_";
                    return false;
                }
            } else {
                if (name.equals(file.getFileName().toString())) {
                    name += "_1_";
                    return false;
                }
            }
            return true;
        }
    }
}
