/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.util;

import com.google.common.base.Charsets;
import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import java.io.IOException;
import java.io.StringReader;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

/**
 * Class used to help in JSON operations.
 *
 * @author Infernage
 */
public final class JSONUtils {

    private final static Gson gson = new GsonBuilder().serializeNulls()
            .serializeSpecialFloatingPointValues().setPrettyPrinting().create();

    /**
     * Read an Object from a JSON file. The File MUST exist.
     *
     * @param <T> The Object type.
     * @param file The JSON File.
     * @param token The TypeToken of the Object.
     * @return The JSON Object.
     * @throws IOException
     */
    public static <T> T readObjectFromJSONFile(Path file, TypeToken<T> token) throws IOException {
        JsonReader reader = new JsonReader(new StringReader(Joiner.on("\n")
                .join(Files.readAllLines(file, Charsets.UTF_16LE).toArray(new String[0]))));
        reader.setLenient(true);
        return gson.fromJson(reader, token.getType());
    }

    /**
     * Writes an Object to a JSON file.
     *
     * @param file The JSON File.
     * @param obj The JSON Object to store.
     * @throws IOException
     */
    public static void writeObjectToJSONFile(Path file, Object obj) throws IOException {
        if (Files.notExists(file)) Files.createFile(file);
        try (FileChannel channel = FileChannel.open(file, StandardOpenOption.TRUNCATE_EXISTING,
                StandardOpenOption.WRITE)) {
            channel.write(Charsets.UTF_16LE.encode(gson.toJson(obj)));
            channel.force(false);
        }
    }
}
