/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Class used to help in network operations.
 *
 * @author Infernage
 */
public final class NetworkUtils {

    /**
     * Transforms into an URL with GET query
     *
     * @param url The url.
     * @param params (Optional) All pairs "param=value" to use.
     * @return The URL encoded.
     * @throws UnsupportedEncodingException
     */
    public static String encodeURLGET(String url, String... params) throws UnsupportedEncodingException {
        if (params != null && params.length > 0) {
            String[] split = params[0].split("=");
            url += "?" + URLEncoder.encode(split[0], "utf-8") + "=" + URLEncoder.encode(split[1],
                    "utf-8");
            for (int i = 1; i < params.length; i++) {
                split = params[i].split("=");
                url += "&" + URLEncoder.encode(split[0], "utf-8") + "=" + URLEncoder.encode(split[1],
                        "utf-8");
            }
        }
        return url;
    }

    /**
     * Makes a GET method to an URL.
     *
     * @param url The URL to request.
     * @param params (Optional) All pairs "param=value" to use.
     * @return The response.
     * @throws MalformedURLException
     * @throws IOException
     */
    public static String requestGetMethod(String url, String... params) throws MalformedURLException, IOException {
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(
                requestGetMethodToStream(url, params)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line).append('\n');
            }
        }
        return builder.toString();
    }

    /**
     * Makes a GET method to an URL.
     *
     * @param url The URL to request.
     * @param params (Optional) All pairs "param=value" to use.
     * @return An InputStream to read.
     * @throws UnsupportedEncodingException
     * @throws IOException
     */
    public static InputStream requestGetMethodToStream(String url, String... params) throws UnsupportedEncodingException, IOException {
        url = encodeURLGET(url, params);
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("GET");
        return connection.getInputStream();
    }
}
