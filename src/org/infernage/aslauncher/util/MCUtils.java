/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.util;

import com.google.common.collect.ArrayListMultimap;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.util.List;
import org.infernage.aslauncher.mcsystem.forge.MCMod;

/**
 * Class used to help in Minecraft operations.
 *
 * @author Infernage
 */
public final class MCUtils {

    private static final ArrayListMultimap<String, MCMod> mods = ArrayListMultimap.create();
    private static final Object lock = new Object();

    /**
     * Gets a Minecraft mod list from http://modlist.mcf.li (Thanks to all for
     * create this!)
     *
     * @param version The version to get the list.
     * @return A mod list.
     * @throws IOException
     */
    public static final List<MCMod> getModList(String version) throws IOException {
        synchronized (lock) {
            if (mods.containsKey(version)) {
                return mods.get(version);
            }
        }
        String json = NetworkUtils.requestGetMethod("http://modlist.mcf.li/api/v3/" + version + ".json");
        TypeToken<List<MCMod>> type = new TypeToken<List<MCMod>>() {
        };
        List<MCMod> list = new GsonBuilder().serializeNulls().serializeSpecialFloatingPointValues()
                .create().fromJson(json, type.getType());
        synchronized (lock) {
            mods.putAll(version, list);
        }
        return list;
    }

    /**
     * Gets the URL to the JSON file of the Minecraft version.
     *
     * @param version The Minecraft version.
     * @return An URL to the JSON file.
     */
    public static final String getJSONVersion(String version) {
        return References.MINECRAFT_DOWNLOAD_BASE + "versions/" + version + "/" + version + ".json";
    }

    /**
     * Gets the Minecraft version URL.
     *
     * @param version The Minecraft version.
     * @return An URL to the Minecraft file.
     */
    public static final String getMinecraftJar(String version) {
        return getJSONVersion(version).replace(".json", ".jar");
    }

    /**
     * Gets the Minecraft library URL.
     *
     * @param lib The library name.
     * @return An URL to the Minecraft library.
     */
    public static final String getURLLibraryFile(String lib) {
        return References.MINECRAFT_LIBRARIES + lib;
    }
}
