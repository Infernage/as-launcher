/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core;

import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.util.MessageControl;

/**
 * Class used to show a System Tray icon.
 *
 * @author Infernage
 */
public class Systray {

    private static boolean init = false;
    private static TrayIcon icon;
    private static PopupMenu menu;
    private static Process mc_process = null;
    private static MenuItem killMenu;
    private static Profile profile = null;

    /**
     * Starts the System Tray icon.
     */
    public static void setUp() {
        if (!init) {
            if (SystemTray.isSupported()) {
                Image ic = Toolkit.getDefaultToolkit().getImage(Systray.class
                        .getResource("/org/infernage/aslauncher/resources/5548.png"));
                ActionListener kill = new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (mc_process != null) {
                            mc_process.destroy();
                        }
                    }
                };
                menu = new PopupMenu();
                killMenu = new MenuItem("Kill MC");
                killMenu.addActionListener(kill);
                menu.add(killMenu);
                icon = new TrayIcon(ic, "ASL Systray");
                icon.setPopupMenu(menu);
                icon.addMouseListener(new MouseAdapter() {
                    @Override
                    public void mouseReleased(MouseEvent e) {
                        if (e.isPopupTrigger()) {
                            if (mc_process != null) {
                                try {
                                    mc_process.exitValue();
                                    killMenu.setEnabled(false);
                                } catch (Exception ex) {
                                    killMenu.setEnabled(true);
                                }
                            } else {
                                killMenu.setEnabled(false);
                            }
                        }
                    }

                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (e.getButton() == MouseEvent.BUTTON1 && e.getClickCount() == 1) {
                            if (profile != null) {
                                if (profile.isPremium()) {
                                    imprMessage("Please, enjoy!",
                                            "Status");
                                } else {
                                    BigDecimal restant = new BigDecimal(1.8e12D)
                                            .subtract(new BigDecimal(profile.getPlayed()));
                                    BigDecimal min = restant.divide(new BigDecimal(6e10D),
                                            0, RoundingMode.DOWN);
                                    BigDecimal sec = profile.isTimeout() ? BigDecimal.ZERO
                                            : new BigDecimal(restant.toBigInteger()
                                                    .mod(new BigDecimal(6e10D).toBigInteger())).divide(
                                                    new BigDecimal(1e9D), 0, RoundingMode.HALF_UP);
                                    Systray.imprMessage("Time left", "The demo will finish in " + min
                                            + ":" + sec);
                                }
                            } else {
                                imprMessage("Profile not selected", "Select a profile first");
                            }
                        }
                    }
                });
                try {
                    SystemTray.getSystemTray().add(icon);
                } catch (Exception e) {
                    log.throwing(Systray.class.getName(), "setUp", e);
                }
            } else {
                MessageControl.showErrorMessage("SystemTray is not supported in your system.",
                        "SystemTray unsupported");
            }
            init = true;
        }
    }
    private static final Logger log = Logger.getLogger(Systray.class.getName());

    /**
     * Kills the current Minecraft process if exists.
     * @throws Exception If the user doesn't click on 'Yes' button.
     */
    public static void killMC() throws Exception {
        if (MessageControl.showConfirmDialog("All Minecraft unsaved data will be lost."
                + " Are you sure?", "Minecraft process still alive", 0, 2) == 0) {
            if (mc_process != null) mc_process.destroy();
        } else throw new Exception("User denied");
    }

    /**
     * Changes the selected Profile.
     *
     * @param prof The Profile to set.
     */
    public static void setProfile(Profile prof) {
        profile = prof;
    }

    /**
     * Sets the MC process.
     *
     * @param process The MC process.
     */
    public static void setProcess(Process process) {
        if (process == null) {
            return;
        }
        if (mc_process != null) {
            mc_process.exitValue();
        }
        mc_process = process;
    }

    /**
     * Checks if the MC process is alive.
     *
     * @return {@code true} if the process is alive.
     */
    public static boolean processAlive() {
        if (mc_process == null) {
            return false;
        }
        try {
            mc_process.exitValue();
            return false;
        } catch (Exception e) {
            return true;
        }
    }

    /**
     * Shows a balloon message.
     *
     * @param msg The message to show.
     * @param title The message title.
     */
    public static void imprMessage(String msg, String title) {
        if (SystemTray.isSupported() && init) {
            icon.displayMessage(title, msg, TrayIcon.MessageType.INFO);
        } else {
            MessageControl.showInfoMessage(msg, title);
        }
    }
}
