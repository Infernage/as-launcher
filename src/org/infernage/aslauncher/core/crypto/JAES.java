/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.crypto;

import com.google.common.base.Charsets;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.ArrayUtils;
import org.infernage.aslauncher.util.CryptoUtils;

/**
 * Class used to crypt with Java AES algorithm. (Simmetrical cipher) This class
 * ciphers the data without any asimetric support.
 *
 * @author Infernage
 */
class JAES implements Crypto {

    static class Pair implements Serializable {

        public byte[] data;
        public Date lastModified;
    }

    private static class Data {

        public String header;
        public byte[] fileData;
    }

    static final SecureRandom random = new SecureRandom();
    static final Map<Integer, Pair> hashes;
    private static final MessageDigest sha256;
    private static final Logger log = Logger.getLogger(JAES.class.getName());

    static {
        try {
            sha256 = MessageDigest.getInstance("SHA-256");
            Path sFile = Paths.get(System.getProperty("user.home"), ".skfx");
            if (Files.notExists(sFile) || Files.size(sFile) == 0) {
                Files.deleteIfExists(sFile);
                Files.createFile(sFile);
                hashes = new HashMap<>();
            } else {
                try (FileReader reader = new FileReader(sFile.toFile())) {
                    hashes = new GsonBuilder().serializeNulls().create().fromJson(reader,
                            new TypeToken<Map<Integer, Pair>>() {
                            }.getType());
                }
            }
        } catch (JsonIOException | JsonSyntaxException | IOException | NoSuchAlgorithmException ex) {
            log.log(Level.SEVERE, "Failed to create SHA", ex);
            throw new RuntimeException(ex);
        }
    }

    private byte[] password;
    private SecretKey sKey;
    private Cipher encrypt;
    private Cipher decrypt;
    private byte[] buffer = null;
    private String encoded;
    private final String code = "UTF-8";
    private final String ALG = "AES";

    public JAES() {
        buffer = new byte[1024 * 256];
        Arrays.fill(buffer, (byte) 0);
        password = null;
    }

    @Override
    public void setPassword(String password) {
        try {
            this.password = password.getBytes(code);
        } catch (UnsupportedEncodingException ex) {
            log.log(Level.WARNING, "Bytes not supported", ex);
        }
    }

    @Override
    public String encryptData(String data) throws Exception {
        byte[] salt = new byte[4];
        byte[] type = "aes128".getBytes(code);
        byte[] size = new byte[4];
        ByteBuffer.wrap(size).putInt(type.length);
        baseInit(true, salt);
        byte[] raw = stringToHex(encoded);
        SecretKeySpec skeyspec = new SecretKeySpec(raw, ALG);
        Cipher ciph = Cipher.getInstance(ALG);
        ciph.init(Cipher.ENCRYPT_MODE, skeyspec);
        byte[] crypted = ciph.doFinal(data.getBytes(code));
        byte[] message = new byte[crypted.length + 8 + type.length];
        System.arraycopy(salt, 0, message, 0, 4);
        System.arraycopy(size, 0, message, 4, 4);
        System.arraycopy(type, 0, message, 8, type.length);
        System.arraycopy(crypted, 0, message, 4, crypted.length);
        return hexToString(message);
    }

    @Override
    public String decryptData(String data) throws Exception {
        byte[] hex = stringToHex(data);
        byte[] size = new byte[4];
        System.arraycopy(hex, 4, size, 0, 4);
        byte[] type = new byte[ByteBuffer.wrap(size).getInt()];
        System.arraycopy(hex, 8, type, 0, type.length);
        String cryptType = new String(type, code);
        if (!cryptType.equals("aes128")) {
            throw new Exception("Expected aes128 but found " + cryptType);
        }
        byte[] message = new byte[hex.length - 8 - type.length];
        byte[] salt = new byte[4];
        System.arraycopy(hex, 0, salt, 0, 4);
        System.arraycopy(hex, 8 + type.length, message, 0, message.length);
        baseInit(false, salt);
        byte[] raw = stringToHex(encoded);
        SecretKeySpec skeyspec = new SecretKeySpec(raw, ALG);
        Cipher ciph = Cipher.getInstance(ALG);
        ciph.init(Cipher.DECRYPT_MODE, skeyspec);
        byte[] decrypted = ciph.doFinal(message);
        char[] aux = new String(decrypted).toCharArray();
        String res = "";
        for (int i = 0; i < aux.length; i++) {
            if ((aux[i] >= '!') && (aux[i] <= '¡') || aux[i] == ' ') {
                res += aux[i];
            }
        }
        return res;
    }

    @Override
    public Path encryptFile(Path src) throws Exception {
        Path dst = Paths.get(src.getParent().toString(), src.getFileName().toString().substring(0,
                src.getFileName().toString().lastIndexOf(".")) + ".casx");
        encryptFile(src, dst);
        return dst;
    }

    @Override
    public void encryptFile(Path src, Path dst) throws Exception {
        if (!dst.toString().endsWith(".casx")) {
            dst = Paths.get(src.getParent().toString(), src.getFileName().toString().substring(0,
                    src.getFileName().toString().lastIndexOf(".")) + ".casx");
        }
        try (FileChannel srcChannel = FileChannel.open(src, StandardOpenOption.READ); FileChannel dstChannel
                = FileChannel.open(dst, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING,
                        StandardOpenOption.WRITE)) {
            encrypt(srcChannel, dstChannel, src.getFileName().toString()
                    .substring(src.getFileName().toString().lastIndexOf(".") + 1,
                            src.getFileName().toString().length()) + "::"
                    + CryptoUtils.getDigest(src, "SHA-1", "40"));
        } catch (Exception ex) {
            try {
                Files.deleteIfExists(dst);
            } catch (IOException e) {
                log.throwing(this.getClass().getName(), "encryptFile", e);
            }
            throw ex;
        }
    }

    @Override
    public Path decryptFile(Path src) throws Exception {
        return decryptFile(src, src.getParent());
    }

    @Override
    public Path decryptFile(Path src, Path parent) throws Exception {
        Path target = null;
        FileChannel dstChannel = null;
        try (FileChannel srcChannel = FileChannel.open(src, StandardOpenOption.READ)) {
            Data cryptedInfo = getHeaderData(srcChannel);
            String storedHash = cryptedInfo.header.split("::")[1];
            target = Paths.get(parent.toString(), src.getFileName().toString().substring(0,
                    src.getFileName().toString().lastIndexOf(".")) + "." + cryptedInfo.header
                    .split("=")[1].split("::")[0]);
            dstChannel = FileChannel.open(target, StandardOpenOption.WRITE, StandardOpenOption.CREATE,
                    StandardOpenOption.TRUNCATE_EXISTING);
            dstChannel.write(ByteBuffer.wrap(cryptedInfo.fileData));
            if (srcChannel.position() < srcChannel.size()) {
                decrypt(srcChannel, dstChannel);
            } else {
                dstChannel.write(ByteBuffer.wrap(decrypt.doFinal()));
            }
            if (!storedHash.equals(CryptoUtils.getDigest(target, "SHA-1", "40"))) {
                throw new Exception("The stored hash doesn't match with the calculated one.\n"
                        + "The file can have been encrypted or decrypted incorreclty.");
            }
        } catch (Exception ex) {
            try {
                if (target != null) {
                    Files.deleteIfExists(target);
                }
            } catch (IOException e) {
                log.throwing(this.getClass().getName(), "decryptFile", e);
            }
            throw ex;
        } finally {
            if (dstChannel != null) {
                dstChannel.close();
            }
        }
        return target;
    }

    @Override
    public void close() throws IOException {  /* Nothing */    }

    /**
     * Core method to crypt.
     *
     * @param src The FileChannel of the source file.
     * @param dst The FileChannel of the destiny file.
     * @param data The data to add into the file, i.e the extension.
     * @throws Exception
     */
    private void encrypt(FileChannel in, FileChannel out, String extension)
            throws Exception {
        init(out, true);
        Arrays.fill(buffer, (byte) 0);
        byte[] info = ("FileExtension=" + extension + "::").getBytes(Charsets.UTF_8);
        byte[] size = new byte[4];
        ByteBuffer.wrap(size).putInt(info.length);
        byte[] bb = encrypt.update(size, 0, 1);
        out.write(ByteBuffer.wrap(bb));
        bb = encrypt.update(info, 0, info.length);
        out.write(ByteBuffer.wrap(bb));
        int read;
        while ((read = in.read(ByteBuffer.wrap(buffer))) >= 0) {
            bb = encrypt.update(buffer, 0, read);
            if (bb != null) {
                out.write(ByteBuffer.wrap(bb));
            }
        }
        byte[] end = encrypt.doFinal();
        if (end != null) {
            out.write(ByteBuffer.wrap(end));
        }
        in.close();
        out.close();
    }

    /**
     * Core method to decrypt. The streams will be closed.
     *
     * @param in The cipherinputstream of the source file.
     * @param out The outputstream of the destiny file.
     * @throws java.lang.Exception
     */
    private void decrypt(FileChannel in, FileChannel out) throws Exception {
        Arrays.fill(buffer, (byte) 0);
        try {
            int read;
            byte[] bb;
            while ((read = in.read(ByteBuffer.wrap(buffer))) >= 0) {
                bb = decrypt.update(buffer, 0, read);
                out.write(ByteBuffer.wrap(bb));
            }
            byte[] end = decrypt.doFinal();
            if (end != null) {
                out.write(ByteBuffer.wrap(end));
            }
        } finally {
            try {
                in.close();
            } catch (IOException e) {
                //Ignore
            }
            try {
                out.close();
            } catch (IOException e) {
                //Ignore
            }
        }
    }

    /**
     * Reads the header data stored into the crypted file.
     *
     * @param channel The input FileChannel.
     * @return The data stored.
     * @throws java.lang.Exception
     */
    private Data getHeaderData(FileChannel channel) throws Exception {
        init(channel, false);
        byte[] bb;
        int r = channel.read(ByteBuffer.wrap(buffer));
        if (r == -1) {
            bb = decrypt.doFinal();
        } else {
            bb = decrypt.update(buffer, 0, r);
        }
        byte[] info = new byte[bb[0] & 0xFF];
        if (info.length > bb.length) {
            List<Byte> bytearray = new ArrayList<>(Arrays.asList(ArrayUtils.toObject(bb)));
            do {
                r = channel.read(ByteBuffer.wrap(buffer));
                if (r == -1) {
                    bb = decrypt.doFinal();
                } else {
                    bb = decrypt.update(buffer, 0, r);
                }
                bytearray.addAll(Arrays.asList(ArrayUtils.toObject(bb)));
            } while (info.length > bytearray.size());
            bb = ArrayUtils.toPrimitive(bytearray.toArray(new Byte[0]));
        }
        System.arraycopy(bb, 1, info, 0, info.length);
        Data data = new Data();
        data.header = new String(info, Charsets.UTF_8);
        data.fileData = new byte[bb.length - (info.length + 1)];
        System.arraycopy(bb, info.length + 1, data.fileData, 0, data.fileData.length);
        return data;
    }

    /**
     * Common initialization.
     *
     * @throws Exception
     */
    private void baseInit(boolean bNew, byte[] b) throws Exception {
        Integer salt;
        if (bNew) {
            salt = setHashValue(password);
            ByteBuffer.wrap(b).putInt(salt);
            if (password == null) {
                password = hashes.get(salt).data;
            }
        } else {
            salt = ByteBuffer.wrap(b).getInt();
            if (password == null) {
                password = getHashValue(salt);
            }
        }
        ByteBuffer bb = ByteBuffer.allocate(password.length + 4);
        bb.put(password);
        bb.putInt(salt);
        sha256.update(bb);
        KeyGenerator key = KeyGenerator.getInstance(ALG);
        SecureRandom rand = new SecureRandom(sha256.digest());
        key.init(128, rand);
        sKey = key.generateKey();
        encoded = hexToString(sKey.getEncoded());
        password = null;
    }

    /**
     * Initializes the object to encrypt/decrypt.
     *
     * @throws Exception
     */
    private void init(FileChannel channel, boolean bNew) throws Exception {
        byte[] iv = new byte[16];
        Arrays.fill(iv, (byte) 0);
        if (channel.size() == 0L) {
            // New file
            SecureRandom rand = new SecureRandom();
            rand.nextBytes(iv);
            ByteBuffer bb = ByteBuffer.wrap(iv);
            channel.write(bb);
        } else {
            // Crypted file
            channel.position(0L);
            ByteBuffer bb = ByteBuffer.wrap(iv);
            channel.read(bb);
        }
        byte[] salt = new byte[4];
        if (!bNew) {
            channel.read(ByteBuffer.wrap(salt));
            byte[] size = new byte[4];
            channel.read(ByteBuffer.wrap(size));
            byte[] type = new byte[ByteBuffer.wrap(size).getInt()];
            channel.read(ByteBuffer.wrap(type));
            String cryptType = new String(type, code);
            if (!cryptType.equals("aes128")) {
                throw new Exception("Expected aes128 but found " + cryptType);
            }
        }
        baseInit(bNew, salt);
        if (bNew) {
            channel.write(ByteBuffer.wrap(salt));
            byte[] type = "aes128".getBytes(code);
            channel.write(ByteBuffer.allocate(4).putInt(type.length));
            channel.write(ByteBuffer.wrap(type));
        }
        IvParameterSpec paramSpec = new IvParameterSpec(iv);
        encrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        decrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        encrypt.init(Cipher.ENCRYPT_MODE, sKey, paramSpec);
        decrypt.init(Cipher.DECRYPT_MODE, sKey, paramSpec);
    }

    /**
     * Transforms a byte array of hexadecimal characters to a string.
     *
     * @param bytes The byte array of hexadecimal characters.
     * @return The string transformed.
     */
    static String hexToString(byte[] bytes) {
        String crypted = "";
        for (int i = 0; i < bytes.length; i++) {
            int aux = bytes[i] & 0xff;
            if (aux < 16) {
                crypted = crypted.concat("0");
            }
            crypted = crypted.concat(Integer.toHexString(aux));
        }
        return crypted;
    }

    /**
     * Transforms a string to a byte array of hexadecimal characters.
     *
     * @param crypted The string to transform.
     * @return The byte array of hexadecimal characters.
     */
    static byte[] stringToHex(String crypted) {
        byte[] bytes = new byte[crypted.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            int index = i * 2;
            String aux = crypted.substring(index, index + 2);
            int v = Integer.parseInt(aux, 16);
            bytes[i] = (byte) v;
        }
        return bytes;
    }

    static Integer setHashValue(byte[] password) throws FileNotFoundException {
        Integer salt = random.nextInt();
        Pair p = new Pair();
        if (password == null) {
            password = random.generateSeed(random.nextInt(256));
            p.data = password;
        } else {
            p.data = null;
        }
        p.lastModified = new Date();
        hashes.put(salt, p);
        refresh();
        return salt;
    }

    static byte[] getHashValue(Integer salt) throws FileNotFoundException {
        Pair p = hashes.get(salt);
        p.lastModified.setTime(System.currentTimeMillis());
        refresh();
        return p.data;
    }

    private static void refresh() throws FileNotFoundException {
        try (PrintWriter pw = new PrintWriter(System.getProperty("user.home") + File.separator
                + ".skfx")) {
            new GsonBuilder().setPrettyPrinting().serializeNulls().create().toJson(hashes, pw);
        }
    }
}
