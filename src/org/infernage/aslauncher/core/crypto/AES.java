/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.crypto;

import com.google.common.base.Charsets;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.ArrayUtils;

/**
 * Class used to crypt with AES algorithm. (Simmetrical cipher) This class
 * ciphers the data without any asimetric support.
 *
 * @author Infernage
 */
class AES extends CipherALG {

    private class Pair implements Serializable {

        public byte[] data;
        public Date lastModified;
    }

    private static final Logger log = Logger.getLogger(AES.class.getName());
    private static final MessageDigest sha256;
    private static final SecureRandom random = new SecureRandom();
    private static final Map<Integer, Pair> hashes;

    static {
        try {
            sha256 = MessageDigest.getInstance("SHA-256");
            Path sFile = Paths.get(System.getProperty("user.home"), ".skfx");
            if (Files.notExists(sFile) || Files.size(sFile) == 0) {
                Files.deleteIfExists(sFile);
                Files.createFile(sFile);
                hashes = new HashMap<>();
            } else {
                try (FileReader reader = new FileReader(sFile.toFile())) {
                    hashes = new GsonBuilder().serializeNulls().create().fromJson(reader,
                            new TypeToken<Map<Integer, Pair>>() {
                            }.getType());
                }
            }
        } catch (Exception ex) {
            log.log(Level.SEVERE, "Failed to create SHA", ex);
            throw new RuntimeException(ex);
        }
    }

    private native String encryptDataNative(String data);

    private native String decryptDataNative(String data);

    private final String ALG = "AES";

    /**
     * Default constructor.
     */
    public AES() throws Exception {
        super();
        for (Map.Entry<Integer, Pair> entry : hashes.entrySet()) {
            Calendar c = Calendar.getInstance(), c1 = Calendar.getInstance();
            c.setTime(entry.getValue().lastModified);
            c.add(Calendar.MONTH, 2);
            if (c.before(c1)) {
                hashes.remove(entry.getKey());
            }
        }
        //Default password
        /*password = new byte[]{(byte) 0x45, (byte) 0x73, (byte) 0x63, (byte) 0x6F, (byte) 0x72,
         (byte) 0x62, (byte) 0x75, (byte) 0x74, (byte) 0x6F, (byte) 0x5F, (byte) 0x4E,
         (byte) 0x65, (byte) 0x74, (byte) 0x77, (byte) 0x6F, (byte) 0x72, (byte) 0x6B};*/
    }

    @Override
    public String encryptData(String msg) throws Exception {
        if (useNatives) {
            return encryptDataNative(msg);
        } else {
            byte[] salt = new byte[4];
            baseInit(true, salt);
            byte[] raw = stringToHex(encoded);
            SecretKeySpec skeyspec = new SecretKeySpec(raw, ALG);
            Cipher ciph = Cipher.getInstance(ALG);
            ciph.init(Cipher.ENCRYPT_MODE, skeyspec);
            byte[] crypted = ciph.doFinal(msg.getBytes(code));
            byte[] message = new byte[crypted.length + 4];
            System.arraycopy(salt, 0, message, 0, 4);
            System.arraycopy(crypted, 0, message, 4, crypted.length);
            return hexToString(message);
        }
    }

    @Override
    public String decryptData(String msg) throws Exception {
        if (useNatives) {
            return decryptDataNative(msg);
        } else {
            byte[] salt = new byte[4];
            byte[] hex = stringToHex(msg);
            byte[] message = new byte[hex.length - 4];
            System.arraycopy(hex, 0, salt, 0, 4);
            System.arraycopy(hex, 4, message, 0, message.length);
            baseInit(false, salt);
            byte[] raw = stringToHex(encoded);
            SecretKeySpec skeyspec = new SecretKeySpec(raw, ALG);
            Cipher ciph = Cipher.getInstance(ALG);
            ciph.init(Cipher.DECRYPT_MODE, skeyspec);
            byte[] decrypted = ciph.doFinal(message);
            char[] aux = new String(decrypted).toCharArray();
            String res = "";
            for (int i = 0; i < aux.length; i++) {
                if ((aux[i] >= '!') && (aux[i] <= '¡') || aux[i] == ' ') {
                    res += aux[i];
                }
            }
            return res;
        }
    }

    @Override
    protected Data getHeaderData(FileChannel channel) throws Exception {
        init(channel, false);
        byte[] bb;
        int r = channel.read(ByteBuffer.wrap(buffer));
        if (r == -1) {
            bb = decrypt.doFinal();
        } else {
            bb = decrypt.update(buffer, 0, r);
        }
        byte[] info = new byte[bb[0] & 0xFF];
        if (info.length > bb.length) {
            List<Byte> bytearray = new ArrayList<>(Arrays.asList(ArrayUtils.toObject(bb)));
            do {
                r = channel.read(ByteBuffer.wrap(buffer));
                if (r == -1) {
                    bb = decrypt.doFinal();
                } else {
                    bb = decrypt.update(buffer, 0, r);
                }
                bytearray.addAll(Arrays.asList(ArrayUtils.toObject(bb)));
            } while (info.length > bytearray.size());
            bb = ArrayUtils.toPrimitive(bytearray.toArray(new Byte[0]));
        }
        System.arraycopy(bb, 1, info, 0, info.length);
        Data data = new Data();
        data.header = new String(info, Charsets.UTF_16LE);
        data.fileData = new byte[bb.length - (info.length + 1)];
        System.arraycopy(bb, info.length + 1, data.fileData, 0, data.fileData.length);
        return data;
    }

    @Override
    protected void encrypt(FileChannel in, FileChannel out, String extension)
            throws Exception {
        init(out, true);
        Arrays.fill(buffer, (byte) 0);
        byte[] info = ("FileExtension=" + extension + "::").getBytes(Charsets.UTF_16LE);
        buffer[0] = (byte) info.length;
        byte[] bb = encrypt.update(buffer, 0, 1);
        out.write(ByteBuffer.wrap(bb));
        bb = encrypt.update(info, 0, info.length);
        out.write(ByteBuffer.wrap(bb));
        int read;
        while ((read = in.read(ByteBuffer.wrap(buffer))) >= 0) {
            bb = encrypt.update(buffer, 0, read);
            if (bb != null) {
                out.write(ByteBuffer.wrap(bb));
            }
        }
        byte[] end = encrypt.doFinal();
        if (end != null) {
            out.write(ByteBuffer.wrap(end));
        }
        in.close();
        out.close();
    }

    /**
     * Core method to decrypt. The streams will be closed.
     *
     * @param in The cipherinputstream of the source file.
     * @param out The outputstream of the destiny file.
     * @throws java.lang.Exception
     */
    @Override
    protected void decrypt(FileChannel in, FileChannel out) throws Exception {
        Arrays.fill(buffer, (byte) 0);
        try {
            int read;
            byte[] bb;
            while ((read = in.read(ByteBuffer.wrap(buffer))) >= 0) {
                bb = decrypt.update(buffer, 0, read);
                out.write(ByteBuffer.wrap(bb));
            }
            byte[] end = decrypt.doFinal();
            if (end != null) {
                out.write(ByteBuffer.wrap(end));
            }
        } finally {
            try {
                in.close();
            } catch (Exception e) {
                //Ignore
            }
            try {
                out.close();
            } catch (Exception e) {
                //Ignore
            }
        }
    }

    /**
     * Transforms a byte array of hexadecimal characters to a string.
     *
     * @param bytes The byte array of hexadecimal characters.
     * @return The string transformed.
     */
    static String hexToString(byte[] bytes) {
        String crypted = "";
        for (int i = 0; i < bytes.length; i++) {
            int aux = bytes[i] & 0xff;
            if (aux < 16) {
                crypted = crypted.concat("0");
            }
            crypted = crypted.concat(Integer.toHexString(aux));
        }
        return crypted;
    }

    /**
     * Transforms a string to a byte array of hexadecimal characters.
     *
     * @param crypted The string to transform.
     * @return The byte array of hexadecimal characters.
     */
    static byte[] stringToHex(String crypted) {
        byte[] bytes = new byte[crypted.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            int index = i * 2;
            String aux = crypted.substring(index, index + 2);
            int v = Integer.parseInt(aux, 16);
            bytes[i] = (byte) v;
        }
        return bytes;
    }

    /**
     * Common initialization.
     *
     * @throws Exception
     */
    private void baseInit(boolean bNew, byte[] b) throws Exception {
        Integer salt;
        if (bNew) {
            salt = random.nextInt();
            ByteBuffer.wrap(b).putInt(salt);
            Pair p = new Pair();
            if (password == null) {
                password = random.generateSeed(random.nextInt(256));
                p.data = password;
            } else {
                p.data = null;
            }
            p.lastModified = new Date();
            hashes.put(salt, p);
            try (PrintWriter pw = new PrintWriter(System.getProperty("user.home") + File.separator
                    + ".skfx")) {
                new GsonBuilder().setPrettyPrinting().serializeNulls().create().toJson(hashes, pw);
            }
        } else {
            salt = ByteBuffer.wrap(b).getInt();
            Pair p = hashes.get(salt);
            if (password == null) {
                password = p.data;
            }
            p.lastModified.setTime(System.currentTimeMillis());
        }
        ByteBuffer bb = ByteBuffer.allocate(password.length + 4);
        bb.put(password);
        bb.putInt(salt);
        sha256.update(bb);
        KeyGenerator key = KeyGenerator.getInstance(ALG);
        SecureRandom rand = new SecureRandom(sha256.digest());
        key.init(128, rand);
        sKey = key.generateKey();
        encoded = hexToString(sKey.getEncoded());
        password = null;
    }

    /**
     * Initializes the object to encrypt/decrypt.
     *
     * @throws Exception
     */
    private void init(FileChannel channel, boolean bNew) throws Exception {
        byte[] iv = new byte[16];
        Arrays.fill(iv, (byte) 0);
        if (channel.size() == 0L) {
            // New file
            SecureRandom rand = new SecureRandom();
            rand.nextBytes(iv);
            ByteBuffer bb = ByteBuffer.wrap(iv);
            channel.write(bb);
        } else {
            // Crypted file
            channel.position(0L);
            ByteBuffer bb = ByteBuffer.wrap(iv);
            channel.read(bb);
        }
        byte[] salt = new byte[4];
        if (!bNew) {
            channel.read(ByteBuffer.wrap(salt));
        }
        baseInit(bNew, salt);
        if (bNew) {
            channel.write(ByteBuffer.wrap(salt));
        }
        IvParameterSpec paramSpec = new IvParameterSpec(iv);
        encrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        decrypt = Cipher.getInstance("AES/CBC/PKCS5Padding");
        encrypt.init(Cipher.ENCRYPT_MODE, sKey, paramSpec);
        decrypt.init(Cipher.DECRYPT_MODE, sKey, paramSpec);
    }
}
