/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.crypto;

import java.io.IOException;
import java.nio.file.Path;

/**
 *
 * @author Infernage
 */
public interface Crypto extends AutoCloseable{

    /**
     * Sets a new password.
     *
     * @param password The new password to set.
     */
    public void setPassword(String password);

    /**
     * Encrypts a message.
     *
     * @param data The message to crypt.
     * @return The message crypted.
     * @throws Exception
     */
    public String encryptData(String data) throws Exception;

    /**
     * Decrypts a message.
     *
     * @param data The message to decrypt.
     * @return The message decrypted.
     * @throws Exception
     */
    public String decryptData(String data) throws Exception;

    /**
     * Crypts a file without specified ubication.
     *
     * @param src The file to crypt.
     * @return The file crypted.
     * @throws Exception
     */
    public Path encryptFile(Path src) throws Exception;

    /**
     * Encrypts a file.
     *
     * @param src The file to crypt.
     * @param dst The file crypted.
     * @throws Exception
     */
    public void encryptFile(Path src, Path dst) throws Exception;

    /**
     * Decrypts a file without specified ubication.
     *
     * @param src The file to decrypt.
     * @return The file decrypted.
     * @throws Exception
     */
    public Path decryptFile(Path src) throws Exception;

    /**
     * Decrypts a file.
     *
     * @param src The file to decrypt.
     * @param parent The folder where will be decrypted.
     * @return The file decrypted.
     * @throws Exception
     */
    public Path decryptFile(Path src, Path parent) throws Exception;

    @Override
    public void close() throws Exception;
}
