/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.crypto;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;
import org.apache.commons.io.Charsets;

/**
 * Class used to crypt with C++ AES algorithm. (Simmetrical cipher) This class
 * ciphers the data without any asimetric support.
 *
 * @author Infernage
 */
final class CAES implements Crypto {

    private static volatile boolean nativeLib;
    private static final Logger log = Logger.getLogger(CAES.class.getName());

    static {
        try {
            System.loadLibrary("ASLL");
            nativeLib = true;
        } catch (Throwable e) {
            log.throwing(CAES.class.getName(), "<cinit>", e);
            nativeLib = false;
        }
    }

    private long nativeHandle;

    public CAES(String typeStr) throws Exception {
        if (!nativeLib) {
            throw new Exception("Native library could not be loaded, use AES 128 bits instead");
        }
        create(typeStr);
    }

    @Override
    public native void setPassword(String password);

    @Override
    public String encryptData(String data) throws Exception {
        Integer salt = JAES.setHashValue(getPassword());
        return manipulateData(data, salt, true);
    }

    @Override
    public String decryptData(String data) throws Exception {
        int salt = getSalt(data);
        if (getPassword().length == 0) {
            setPassword(new String(JAES.getHashValue(salt), Charsets.UTF_8));
        }
        return manipulateData(data, salt, false);
    }

    @Override
    public Path encryptFile(Path src) throws Exception {
        Path dst = Paths.get(src.getParent().toString(), src.getFileName().toString().substring(0,
                src.getFileName().toString().lastIndexOf(".")) + ".casx");
        encryptFile(src, dst);
        return dst;
    }

    @Override
    public void encryptFile(Path src, Path dst) throws Exception {
        Integer salt = JAES.setHashValue(getPassword());
        manipulateFile(src.toString(), dst.toString(), salt, true);
    }

    @Override
    public Path decryptFile(Path src) throws Exception {
        return decryptFile(src, src.getParent());
    }

    @Override
    public Path decryptFile(Path src, Path parent) throws Exception {
        int salt = getSalt(src);
        if (getPassword().length == 0) {
            setPassword(new String(JAES.getHashValue(salt), Charsets.UTF_8));
        }
        return Paths.get(manipulateFile(src.toString(), parent.toString(), salt, false));
    }

    @Override
    public void close() throws Exception {
        closeHandle();
    }

    @Override
    protected void finalize() throws Throwable {
        closeHandle();
        super.finalize();
    }

    private native void create(String type);

    private native String manipulateData(String data, int salt, boolean isEncrypt);

    private native String manipulateFile(String src, String dst, int salt, boolean isEncrypt);

    private native byte[] getPassword();

    private native int getSalt(String data);

    private native int getSalt(Path file);

    private native void closeHandle();
}
