/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.core.crypto;

/**
 *
 * @author Infernage
 */
public final class SuiteCrypto {
    // TODO Implement class SuiteCrypto
    public enum Type{
        AES128,
        AES192,
        AES256
    }
    
    public static Crypto getSuite(Type t) throws Exception{
        switch(t){
            case AES128: return new JAES();
            case AES192: return new CAES("aes192");
            case AES256: return new CAES("aes256");
            default: return null;
        }
    }
}
