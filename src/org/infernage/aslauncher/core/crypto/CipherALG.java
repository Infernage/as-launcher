/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.crypto;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import org.infernage.aslauncher.util.CryptoUtils;

/**
 * Class used to abstract the Cipher type.
 *
 * @author Infernage
 */
abstract class CipherALG implements Serializable {

    private static final Logger log = Logger.getLogger(AES.class.getName());
    private static final long serialVersionUID = 375582358;
    protected static boolean useNatives;

    static {
        try {
            System.loadLibrary("ASLL");
            useNatives = false;
        } catch (Throwable e) {
            useNatives = false;
            Logger.getLogger(CipherALG.class.getName()).throwing(CipherALG.class.getName(), "<cinit>", e);
        }
    }

    protected native void encrypt(Path src, Path dst);

    protected native Path decrypt(Path src, Path parent);

    protected class Data {

        public String header;
        public byte[] fileData;
    }

    protected byte[] password;
    protected SecretKey sKey;
    protected Cipher encrypt;
    protected Cipher decrypt;
    protected byte[] buffer = null;
    protected String encoded;
    protected String code = "UTF-16LE";

    protected CipherALG() {
        if (!useNatives) {
            buffer = new byte[1024 * 256];
            Arrays.fill(buffer, (byte) 0);
            password = null;
        }
    }

    /**
     * Sets a new password.
     *
     * @param password The new password to set.
     */
    public void setPassword(String password) {
        try {
            this.password = password.getBytes(code);
        } catch (UnsupportedEncodingException ex) {
            log.log(Level.WARNING, "Bytes not supported", ex);
        }
    }

    /**
     * Encrypts a message.
     *
     * @param data The message to crypt.
     * @return The message crypted.
     * @throws Exception
     */
    public abstract String encryptData(String data) throws Exception;

    /**
     * Decrypts a message.
     *
     * @param data The message to decrypt.
     * @return The message decrypted.
     * @throws Exception
     */
    public abstract String decryptData(String data) throws Exception;

    /**
     * Encrypts a file.
     *
     * @param src The file to crypt.
     * @param dst The file crypted.
     * @throws Exception
     */
    public void encryptFile(Path src, Path dst) throws Exception {
        if (!dst.toString().endsWith(".casx")) {
            dst = Paths.get(src.getParent().toString(), src.getFileName().toString().substring(0,
                    src.getFileName().toString().lastIndexOf(".")) + ".casx");
        }
        if (useNatives) {
            encrypt(src, dst);
        } else {
            try (FileChannel srcChannel = FileChannel.open(src, StandardOpenOption.READ); FileChannel dstChannel
                    = FileChannel.open(dst, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING,
                            StandardOpenOption.WRITE)) {
                encrypt(srcChannel, dstChannel, src.getFileName().toString()
                        .substring(src.getFileName().toString().lastIndexOf(".") + 1,
                                src.getFileName().toString().length()) + "::"
                        + CryptoUtils.getDigest(src, "SHA-1", "40"));
            } catch (Exception ex) {
                try {
                    Files.deleteIfExists(dst);
                } catch (IOException e) {
                    log.throwing(this.getClass().getName(), "encryptFile", e);
                }
                throw ex;
            }
        }
    }

    /**
     * Crypts a file without specified ubication.
     *
     * @param src The file to crypt.
     * @return The file crypted.
     * @throws Exception
     */
    public Path encryptFile(Path src) throws Exception {
        Path dst = Paths.get(src.getParent().toString(), src.getFileName().toString().substring(0,
                src.getFileName().toString().lastIndexOf(".")) + ".casx");
        encryptFile(src, dst);
        return dst;
    }

    /**
     * Decrypts a file without specified ubication.
     *
     * @param src The file to decrypt.
     * @return The file decrypted.
     * @throws Exception
     */
    public Path decryptFile(Path src) throws Exception {
        return decryptFile(src, src.getParent());
    }

    /**
     * Decrypts a file.
     *
     * @param src The file to decrypt.
     * @param parent The folder where will be decrypted.
     * @return The file decrypted.
     * @throws Exception
     */
    public Path decryptFile(Path src, Path parent) throws Exception {
        if (useNatives) {
            return decrypt(src, parent);
        } else {
            Path target = null;
            FileChannel dstChannel = null;
            try (FileChannel srcChannel = FileChannel.open(src, StandardOpenOption.READ)) {
                Data cryptedInfo = getHeaderData(srcChannel);
                String storedHash = cryptedInfo.header.split("::")[1];
                target = Paths.get(parent.toString(), src.getFileName().toString().substring(0,
                        src.getFileName().toString().lastIndexOf(".")) + "." + cryptedInfo.header
                        .split("=")[1].split("::")[0]);
                dstChannel = FileChannel.open(target, StandardOpenOption.WRITE, StandardOpenOption.CREATE,
                        StandardOpenOption.TRUNCATE_EXISTING);
                dstChannel.write(ByteBuffer.wrap(cryptedInfo.fileData));
                if (srcChannel.position() < srcChannel.size()) {
                    decrypt(srcChannel, dstChannel);
                } else {
                    dstChannel.write(ByteBuffer.wrap(decrypt.doFinal()));
                }
                if (!storedHash.equals(CryptoUtils.getDigest(target, "SHA-1", "40"))) {
                    log.warning("The stored hash doesn't match with the calculated one.\n"
                            + "The file can have been encrypted or decrypted incorreclty.");
                }
            } catch (Exception ex) {
                try {
                    if (target != null) {
                        Files.deleteIfExists(target);
                    }
                } catch (IOException e) {
                    log.throwing(this.getClass().getName(), "decryptFile", e);
                }
                throw ex;
            } finally {
                if (dstChannel != null) {
                    dstChannel.close();
                }
            }
            return target;
        }
    }

    /**
     * Reads the header data stored into the crypted file.
     *
     * @param channel The input FileChannel.
     * @return The data stored.
     * @throws java.lang.Exception
     */
    protected abstract Data getHeaderData(FileChannel channel) throws Exception;

    /**
     * Core method to crypt.
     *
     * @param src The FileChannel of the source file.
     * @param dst The FileChannel of the destiny file.
     * @param data The data to add into the file, i.e the extension.
     * @throws Exception
     */
    protected abstract void encrypt(FileChannel src, FileChannel dst, String data) throws Exception;

    /**
     * Core method to decrypt.
     *
     * @param src The FileChannel of the source file.
     * @param dst The FileChannel of the destiny file.
     * @throws java.lang.Exception
     */
    protected abstract void decrypt(FileChannel src, FileChannel dst) throws Exception;
}
