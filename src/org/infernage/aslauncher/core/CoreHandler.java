/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenAccessor;
import aurelienribon.tweenengine.TweenManager;
import com.google.common.base.Charsets;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import javax.swing.SwingUtilities;
import org.infernage.aslauncher.common.Job;
import org.infernage.aslauncher.core.CoreConfiguration.ConfigKeys;
import org.infernage.aslauncher.core.crypto.Crypto;
import org.infernage.aslauncher.core.crypto.SuiteCrypto;
import org.infernage.aslauncher.core.events.ShutdownEvent;
import org.infernage.aslauncher.core.events.TweenEvent;
import org.infernage.aslauncher.core.loaders.ModLoader.ModContainer;
import org.infernage.aslauncher.gui.ExternalConsole;
import org.infernage.aslauncher.gui.MainFrame;
import org.infernage.aslauncher.gui.MainFrameController;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.mcsystem.authenticator.Authenticator;
import org.infernage.aslauncher.mcsystem.versions.VersionList;
import org.infernage.aslauncher.util.JSONUtils;
import org.infernage.aslauncher.util.MessageControl;
import org.infernage.aslauncher.util.NetworkUtils;
import org.infernage.aslauncher.util.References;

/**
 * Class used to controll the program.
 *
 * @author Infernage
 */
public class CoreHandler {

    public static final CoreHandler INSTANCE = new CoreHandler();
    private static final Logger log = Logger.getLogger(CoreHandler.class.getName());

    private static VersionList mcversions = null;
    private static GuiEventController controller = null;
    private static CoreConfiguration configuration = null;
    private static Shutdown hookController = null;
    private static final TweenManager manager = new TweenManager();

    /**
     * Request to save the configuration.
     */
    public static void saveConfiguration() {
        if (hookController != null) {
            try {
                hookController.saveConfig();
            } catch (IOException e) {
                log.log(Level.SEVERE, "Failed to save the Core configuration", e);
            }
        }
    }

    /**
     * Gets the Minecraft version list.
     *
     * @return A VersionList object.
     */
    public static VersionList getMCVersions() {
        return mcversions;
    }

    /**
     * Gets the GuiEventController installed.
     *
     * @return The GuiEventController.
     */
    public static GuiEventController getGuiController() {
        return controller;
    }

    /**
     * Gets the CoreConfiguration object.
     *
     * @return The CoreConfiguration.
     */
    public static CoreConfiguration getConfiguration() {
        return configuration;
    }

    /**
     * Starts the CoreHandler system.
     *
     * @param os The Operative System host.
     * @return The Modules previosly installed.
     * @throws Exception
     */
    public static Map<String, ModContainer> start(String os) throws Exception {
        if (INSTANCE.initializationState != State.NotInit) {
            return null;
        }
        preInit();
        log.config("Starting Core configuration...");
        Path configFile = Paths.get(CoreConfiguration.getWorkingDir(os), "ASL.casx");
        if (Files.notExists(configFile) || Files.size(configFile) == 0L) {
            log.config("Creating new Core configuration file...");
            configFile = Paths.get(CoreConfiguration.getWorkingDir(os), "ASL.conf");
            if (Files.notExists(configFile.getParent())) {
                Files.createDirectories(configFile.getParent());
            }
            if (Files.notExists(configFile)) {
                Files.createFile(configFile);
            }
            configuration = new CoreConfiguration(os);
            log.config("New Core configuration file created successfully");
        } else {
            log.config("Loading Core configuration from file...");
            Crypto aes = SuiteCrypto.getSuite(SuiteCrypto.Type.AES256);
            aes.setPassword("ASLauncher");
            configFile = aes.decryptFile(configFile);
            try {
                log.config("Parsing with JSON parser");
                configuration = JSONUtils.
                        <CoreConfiguration>readObjectFromJSONFile(configFile,
                                new TypeToken<CoreConfiguration>() {
                                });
            } catch (IOException e) {
                log.throwing(CoreHandler.class.getName(), "start", e);
                log.severe("JSON parser has failed. Not possible to load the Core configuration");
                log.info("Dumping error to a file...");
                List<CharSequence> error = new ArrayList<>();
                error.add(e.toString());
                for (StackTraceElement st : e.getStackTrace()) {
                    error.add(st.toString());
                }
                try {
                    Files.write(Paths.get(CoreConfiguration.getCurrentJar().getParent()
                            .toAbsolutePath().toString(), "Dump" + new SecureRandom().nextLong()),
                            error, Charsets.UTF_8, StandardOpenOption.CREATE,
                            StandardOpenOption.WRITE);
                } catch (URISyntaxException | IOException exc) {
                    log.log(Level.SEVERE, "Failed to dump the error!", exc);
                }
                throw e;
            }
            log.config("Core configuration parsed successfully");
        }
        InternalConsole.addPath(configuration.getMapValue(ConfigKeys.Log));
        log.config("Creating hooks...");
        hookController = new Shutdown(configFile);
        Runtime.getRuntime().addShutdownHook(hookController);
        log.info("Attempting to refresh profiles...");
        if (!configuration.getProfiles().isEmpty()) {
            log.log(Level.INFO, "Found {0} profiles", configuration.getProfiles().size());
            for (Profile profile : configuration.getProfiles()) {
                if (profile.isAutoLogin() && profile.isPremium()) {
                    try {
                        log.log(Level.INFO, "Refreshing profile {0}", profile.getUsername());
                        //Try to authenticate with Yggdrassil method.
                        Authenticator.refresh(profile);
                        profile.setLogged(true);
                    } catch (Exception e) {
                        log.throwing(CoreHandler.class.getName(), "start", e);
                        log.info("Refresh failed. Authenticating again");
                        //If failed, login again.
                        try {
                            Authenticator.login(profile);
                            profile.setLogged(true);
                        } catch (Exception ex) {
                            log.log(Level.WARNING, "Authentication failed. Please,"
                                    + " login again manually", ex);
                            profile.setAccessToken(null);
                            profile.setLogged(false);
                        }
                    }
                }
                for (MCInstance instance : profile.getList()) {
                    if (Files.notExists(instance.getPath())) {
                        log.log(Level.INFO, "Instance {0} corrupted. Removing...", instance.getName());
                        profile.removeInstance(instance);
                    }
                }
            }
        } else {
            log.info("No profiles found");
        }
        if (System.getProperty("sun.arch.data.model").equals("32")) {
            MessageControl.showInfoMessage("It seems that you are running a 32-bit JVM architecture. "
                    + "If you want to\nexecute Minecraft with more than 1Gb, please install the 64-bit"
                    + " JVM version.", "32-bit JVM architecture detected!");
        }
        log.fine("Finishing initialization...");
        init();
        return configuration.modules;
    }

    /**
     * Preinitializes the CoreHandler class.
     */
    private static void preInit() {
        if (INSTANCE.initializationState != State.NotInit) {
            return;
        }
        INSTANCE.initializationState = State.PreInit;
        InternalConsole.setUp();
        log.config("Starting core system...");
        Systray.setUp();
        controller = new GuiEventController();
        controller.start();
        try {
            log.fine("Requesting MC versions...");
            Gson g = new Gson();
            mcversions = g.fromJson(NetworkUtils.requestGetMethod(References.MINECRAFT_VERSIONS_URL),
                    VersionList.class);
        } catch (IOException | JsonSyntaxException e) {
            log.log(Level.SEVERE, "Failed to obtain MC versions", e);
        }
    }

    /**
     * Finalizes the initialization of CoreHandler class.
     */
    private static void init() throws IOException {
        if (INSTANCE.initializationState != State.PreInit) {
            return;
        }
        INSTANCE.initializationState = State.Init;
        log.config("Starting auto-saver task");
        INSTANCE.scheduleTask(new AutoSaver(), true, Priority.High, TimeUnit.MINUTES, 10);
        log.config("Checking environment");
        if (Files.notExists(Paths.get(configuration.getMapValue(ConfigKeys.ConfigDir)))) {
            Files.createDirectory(Paths.get(configuration.getMapValue(ConfigKeys.ConfigDir)));
        }
        if (Files.notExists(Paths.get(configuration.getMapValue(ConfigKeys.ModsDir)))) {
            Files.createDirectory(Paths.get(configuration.getMapValue(ConfigKeys.ModsDir)));
        }
        if (Files.notExists(Paths.get(configuration.getMapValue(ConfigKeys.ProfilesDir)))) {
            Files.createDirectory(Paths.get(configuration.getMapValue(ConfigKeys.ProfilesDir)));
        }
        // Start Tween engine.
        new TweenThread().start();
        // Start frame.
        ExternalConsole.getConsole();
        MainFrame frame = MainFrame.create(References.ASL_VERSION);
        frame.setVisible(true);
        log.fine("Initialization complete");
    }

    /**
     * Gets the recommended configuration file.
     *
     * @param moduleName The module name.
     * @return The path to the configuration file.
     */
    public static Path getRecommendedConfigurationFile(String moduleName) {
        return Paths.get(configuration.getMapValue(ConfigKeys.ConfigDir), moduleName + ".conf");
    }

    /**
     * Gets the configuration Object from the configuration file.
     *
     * @param <T> The Object type to return.
     * @param configFile The configuration file.
     * @return The configuration Object.
     * @throws IOException
     */
    public static <T> T loadConfigurationFileContent(Path configFile) throws IOException {
        return JSONUtils.<T>readObjectFromJSONFile(configFile, new TypeToken<T>() {
        });
    }

    public static enum Priority {

        Low(1),
        Normal(2),
        High(3),
        Critical(4);

        private final int priority;

        public int getPriority() {
            return priority;
        }

        private Priority(int priority) {
            this.priority = priority;
        }
    }

    /**
     * Checks if the JVM is going to shutdown or not.
     *
     * @return {@code true} if the JVM is going to shutdown.
     */
    public boolean isGoingShutdown() {
        return goingShutdown;
    }

    /**
     * Sends an event.
     *
     * @param event The Object event.
     */
    public synchronized void sendEvent(Object event) {
        bus.post(event);
    }

    /**
     * Executes the given Runnable. This will use the GuiEventController to show
     * information to the user.
     *
     * @param target The Runnable to execute.
     * @param message The default message sent to the controller.
     */
    public void executeLongTask(Runnable target, String message) {
        RunnableAbstract runnable = new RunnableAbstract(target);
        LongTask job = new LongTask();
        job.addTask(runnable);
        controller.addJob(job, message, Priority.Normal);
    }

    /**
     * Executes the given Runnable. This object will be executed in a
     * ThreadPool.
     *
     * @param target The Runnable to execute.
     * @return A ListenableFuture to execute Callbacks functions.
     */
    public ListenableFuture<?> executeTask(Runnable target) {
        ListenableFuture f = executor.submit(target);
        Futures.addCallback(f, new FutureCallback<Void>() {

                @Override
                public void onSuccess(Void result) {
                    System.gc();
                }

                @Override
                public void onFailure(Throwable t) {
                    System.gc();
                }
            });
        return f;
    }

    /**
     * Executes the given Callable. This object will be executed in a ThreadPool
     *
     * @param <T> The result type of the computation.
     * @param target The Callable to execute.
     * @return A ListenableFuture object to access to the result and execute
     * Callbacks functions.
     */
    public <T> ListenableFuture<T> executeTask(Callable<T> target) {
        ListenableFuture<T> f = executor.submit(target);
        Futures.addCallback(f, new FutureCallback<T>() {

            @Override
            public void onSuccess(T result) {
                System.gc();
            }

            @Override
            public void onFailure(Throwable t) {
                System.gc();
            }
        });
        return f;
    }

    /**
     * Executes a Callable list. This method will block until all tasks are
     * complete.
     *
     * @param <T> The result type of the computation.
     * @param target The list with all Callable objects.
     * @return A List with Future objects to access to the results.
     * @throws InterruptedException
     */
    public <T> List<Future<T>> executeCollectionTaskBlocking(
            Collection<? extends Callable<T>> target) throws InterruptedException {
        List<Future<T>> list = executor.invokeAll(target);
        System.gc();
        return list;
    }

    /**
     * Executes a Callable list.
     *
     * @param <T> The result type of the computation.
     * @param target The list with all Callable objects.
     * @return A List with Future objects to access to the results.
     * @throws InterruptedException
     */
    public <T> List<Future<T>> executeCollectionTask(
            Collection<? extends Callable<T>> target) throws InterruptedException {
        List<Future<T>> futures = new ArrayList<>();
        for (Callable<T> callable : target) {
            ListenableFuture<T> f = executor.submit(callable);
            Futures.addCallback(f, new FutureCallback<T>() {

                @Override
                public void onSuccess(T result) {
                    System.gc();
                }

                @Override
                public void onFailure(Throwable t) {
                    System.gc();
                }
            });
            futures.add(f);
        }
        return futures;
    }

    /**
     * Waits until the given Runnable has finished its execution.
     *
     * @param target The Runnable to wait for.
     * @throws InterruptedException
     */
    public void waitForScheduledTask(Runnable target) throws InterruptedException {
        Task task = getExecutionTaskFromRunnable(target);
        if (task == null) {
            return;
        }
        waitForScheduledExecutionTask(task);
    }

    /**
     * Removes a task from the scheduler.
     *
     * @param target The Runnable target to remove.
     * @param waitForTermination {@code true} if this method will wait for the
     * Runnable to finish its execution.
     * @throws InterruptedException
     */
    public void removeScheduledTask(Runnable target, boolean waitForTermination)
            throws InterruptedException {
        Task key = getExecutionTaskFromRunnable(target);
        if (key == null) {
            return;
        }
        synchronized (this) {
            inExecutionScheduledTasks.put(key, false);
        }
        if (waitForTermination) {
            waitForScheduledExecutionTask(key);
        }
    }

    /**
     * Sends a Runnable to be scheduled.
     *
     * @param target The Runnable to schedule.
     * @param periodic {@code true} if the Runnable should be executed
     * periodically.
     * @param priority The task priority.
     * @param unit The TimeUnit to use. If periodic parameter is {@code false},
     * this will be discarted.
     * @param period The task period. The time amount will take the TimeUnit
     * specified in unit parameter. If periodic parameter is {@code false}, this
     * will be discarted.
     * @return {@code true} if the task was scheduled correctly or {@code false}
     * if the task couldn't be scheduled (Innsuficient space or task already
     * scheduled).
     */
    public synchronized boolean scheduleTask(Runnable target, boolean periodic, Priority priority,
            TimeUnit unit, long period) {
        if (inExecutionScheduledTasks.size() >= 15) {
            return false;
        }
        Task t = new Task(target, priority);
        if (inExecutionScheduledTasks.containsKey(t)) {
            return false;
        }
        inExecutionScheduledTasks.put(t, periodic);
        ScheduledFuture future;
        if (periodic) {
            future = scheduler.scheduleAtFixedRate(t, 0, period, unit);
        } else {
            future = scheduler.schedule(t, 1, TimeUnit.SECONDS);
        }
        return !future.isCancelled();
    }

    /**
     * Subscribes the Object to the core EventBus.
     *
     * @param obj The Object to subscribe.
     */
    public void subscribe(Object obj) {
        bus.register(obj);
    }

    private synchronized void waitForScheduledExecutionTask(Task t) throws InterruptedException {
        while (inExecutionScheduledTasks.containsKey(t)) {
            wait();
        }
    }

    private Task getExecutionTaskFromRunnable(Runnable target) {
        Task t = new Task(target, Priority.Normal);
        synchronized (this) {
            for (Task task : inExecutionScheduledTasks.keySet()) {
                if (task.equals(t)) {
                    return task;
                }
            }
        }
        return null;
    }

    private enum State {

        NotInit,
        PreInit,
        Init
    }

    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(3);
    private final ListeningExecutorService executor
            = MoreExecutors.listeningDecorator(Executors.newFixedThreadPool(7));
    private final Map<Task, Boolean> inExecutionScheduledTasks;
    private State initializationState;
    private EventBus bus;
    private volatile boolean goingShutdown = false;

    /**
     * Default constructor.
     */
    private CoreHandler() {
        initializationState = State.NotInit;
        inExecutionScheduledTasks = new TreeMap<>();
        bus = new EventBus("ASL Core");
    }

    /**
     * Class used to store Runnable tasks.
     */
    private class Task implements Runnable, Comparable<Task> {

        private final Runnable target;
        private final long id;
        private final Priority p;
        private final Object lock = new Object();

        private boolean inExecution = false;

        public Task(Runnable target, Priority priority) {
            this.target = target;
            p = priority;
            id = INSTANCE.inExecutionScheduledTasks.size();
        }

        public long getId() {
            return id;
        }

        public boolean isInExecution() {
            return inExecution;
        }

        @Override
        public void run() {
            synchronized (lock) {
                inExecution = true;
            }
            target.run();
            synchronized (lock) {
                inExecution = false;
            }
            synchronized (INSTANCE) {
                if (!INSTANCE.inExecutionScheduledTasks.get(this)) {
                    INSTANCE.inExecutionScheduledTasks.remove(this);
                    INSTANCE.notifyAll();
                }
            }
            System.gc();
        }

        @Override
        public int compareTo(Task o) {
            return ((p.priority - o.p.priority) + (hashCode() - o.hashCode())) * (equals(o) ? 0 : 1);
        }

        @Override
        public boolean equals(Object obj) {
            if (obj instanceof Task) {
                Task t = (Task) obj;
                return t.target.equals(target);
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 37 * hash + Objects.hashCode(this.target);
            hash = 37 * hash + Objects.hashCode(this.p);
            return hash;
        }
    }

    /**
     * Class used to save the Core configuration automatically.
     */
    private static class AutoSaver implements Runnable {

        @Override
        public void run() {
            log.config("Autosaving configuration...");
            saveConfiguration();
        }
    }

    /**
     * Class used to run a Shutdown hook at the end of JVM.
     */
    private static class Shutdown extends Thread {

        private final Path file;
        private FileChannel channel;
        private FileLock lock;

        public Shutdown(Path configFile) throws Exception {
            super("Shutdown");
            file = configFile;
            if (configFile == null) {
                return; // Only use a volatile configuration.
            }
            log.config("Creating JSON writer");
            channel = FileChannel.open(file, StandardOpenOption.CREATE,
                    StandardOpenOption.READ, StandardOpenOption.WRITE);
            lock = channel.lock();
        }

        public synchronized void saveConfig() throws IOException {
            if (file == null) {
                return;
            }
            channel.position(0);
            channel.write(Charsets.UTF_16LE.encode(new GsonBuilder().setPrettyPrinting()
                    .serializeNulls().serializeSpecialFloatingPointValues().create()
                    .toJson(configuration)));
            channel.force(false);
        }

        public void release() throws Exception {
            if (lock != null) {
                lock.release();
            }
            if (channel != null) {
                channel.close();
            }
        }

        @Override
        public void run() {
            synchronized (INSTANCE) {
                INSTANCE.goingShutdown = true; // Mark as going to shutdown.
            }
            try {
                log.config("System going shutdown!");
                controller.quit();
                INSTANCE.sendEvent(new ShutdownEvent());
                INSTANCE.executor.shutdown();
                INSTANCE.scheduler.shutdown();
                try {
                    saveConfig();
                    release();
                    Crypto aes = SuiteCrypto.getSuite(SuiteCrypto.Type.AES256);
                    aes.setPassword("ASLauncher");
                    aes.encryptFile(file);
                    Files.deleteIfExists(file);
                    try {
                        INSTANCE.executor.awaitTermination(1, TimeUnit.SECONDS);
                        INSTANCE.scheduler.awaitTermination(1, TimeUnit.SECONDS);
                    } catch (InterruptedException e) {
                        // Interrupted! Ignoring this wait!
                    }
                    if (!INSTANCE.executor.isShutdown()) {
                        INSTANCE.executor.shutdownNow();
                    }
                    if (!INSTANCE.scheduler.isShutdown()) {
                        INSTANCE.scheduler.shutdownNow();
                    }
                } catch (Exception e) {
                    log.log(Level.CONFIG, "Failed in Shutdown configuration", e);
                    e.printStackTrace(System.err);
                }
            } catch (Exception e) {
                log.throwing(this.getClass().getName(), "run", e);
            }
        }
    }

    /**
     * Class used to abstract a Runnable Object into a Callable one.
     */
    private class RunnableAbstract implements Callable<Void> {

        private final Runnable runnable;

        public RunnableAbstract(Runnable target) {
            runnable = target;
        }

        @Override
        public Void call() throws Exception {
            runnable.run();
            return null;
        }

    }

    /**
     * Class used to abstract a very long Task into a Job with GUI progress.
     */
    private class LongTask implements Job<RunnableAbstract, Void> {

        private RunnableAbstract target;
        private volatile boolean success = false, done = false;

        @Override
        public String getName() {
            return "Long task";
        }

        @Override
        public void addTask(RunnableAbstract task) {
            target = task;
        }

        @Override
        public void addTaskCollection(Collection<RunnableAbstract> jobs) {
            // This Job only executes one Task.
        }

        @Override
        public List<Future<Void>> startJob() throws Exception {
            try {
                target.call();
                synchronized (this) {
                    success = true;
                }
            } catch (Exception e) {
                log.log(Level.WARNING, "Long task failed to execute", e);
            }
            synchronized (this) {
                done = true;
            }
            return new ArrayList<>();
        }

        @Override
        public void setProgressBar(JProgressBar bar) {
            // Ignore.
        }

        @Override
        public JProgressBar getProgressBar() {
            return null;
        }

        @Override
        public boolean isSuccessfull() {
            return success;
        }

        @Override
        public boolean isDone() {
            return done;
        }

        @Override
        public BigDecimal getJobProgress() {
            return BigDecimal.valueOf(-1);
        }

        @Override
        public BigDecimal getTotalProgress() {
            return BigDecimal.valueOf(-1);
        }

        @Override
        public int getFailTasksCount() {
            return success ? 0 : 1;
        }

    }

    private static class MainFrameAccesor implements TweenAccessor<MainFrame> {

        @Override
        public int getValues(MainFrame target, int tweenType, float[] returnValues) {
            switch (tweenType) {
                case MainFrameController.animation_resize:
                    returnValues[0] = target.getWidth();
                    returnValues[1] = target.getHeight();
                    return 2;
                default:
                    return -1;
            }
        }

        @Override
        public void setValues(MainFrame target, int tweenType, float[] newValues) {
            switch (tweenType) {
                case MainFrameController.animation_resize:
                    target.setSize((int) newValues[0], (int) newValues[1]);
                    target.validate();
                    break;
            }
        }

    }

    // Tween internal engine
    public static class TweenThread extends Thread {

        private TweenThread() {
            super("Tween engine");
            setDaemon(true);
            Tween.registerAccessor(MainFrame.class, new MainFrameAccesor());
            INSTANCE.subscribe(this);
        }

        @Subscribe
        public void tween(TweenEvent event) {
            if (manager.containsTarget(MainFrame.getWindow(), MainFrameController.animation_resize)) {
                manager.killTarget(MainFrame.getWindow(), MainFrameController.animation_resize);
            }
            Tween.to(MainFrame.getWindow(), MainFrameController.animation_resize, 1500F)
                    .target(710, event.reverse ? 520 : 660).delay(2F).start(manager);
        }

        @Override
        public void run() {
            long last = System.currentTimeMillis();
            long delta = System.currentTimeMillis();
            while (true) {
                long newMili = System.currentTimeMillis();
                long sleep = 15 - (newMili - last);
                last = newMili;
                if (sleep > 1) {
                    try {
                        Thread.sleep(sleep);
                    } catch (InterruptedException e) {
                        break;
                    }
                }
                long deltaNew = System.currentTimeMillis();
                final float deltaResult = (deltaNew - delta);
                try {
                    SwingUtilities.invokeAndWait(new Runnable() {

                        @Override
                        public void run() {
                            manager.update(deltaResult);
                        }
                    });
                } catch (InterruptedException | InvocationTargetException e) {
                    if (e instanceof InvocationTargetException) {
                        continue;
                    } else {
                        break;
                    }
                }
                delta = newMili;
            }
            manager.pause();
            manager.killAll();
        }
    }
}
