/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.infernage.aslauncher.util.CryptoUtils;

/**
 * Downloader engine which uses MD5 verification.
 *
 * @author Infernage
 */
public class MD5Engine extends Downloader {

    private final static Logger log = Logger.getLogger(MD5Engine.class.getName());

    /**
     * Default constructor.
     *
     * @param url Link to the file.
     * @param j The job where is called.
     * @param target The local file where will be stored.
     * @param isDirectory {@code true} if the param target is a directory.
     * @throws MalformedURLException
     * @throws IOException
     */
    public MD5Engine(URL url, DownloadJob j, Path target, boolean isDirectory)
            throws MalformedURLException, IOException {
        super(url, j, target, isDirectory);
    }

    @Override
    protected void download() throws Exception {
        log.log(Level.FINE, "Target url: {0}\nTarget file: {1}",
                new Object[]{url.toString(), destiny.toString()});
        if (destiny.getParent() != null && !Files.isDirectory(destiny.getParent())
                && Files.notExists(destiny.getParent())) {
            Files.createDirectories(destiny.getParent());
        }
        log.fine("Getting md5...");
        String md5 = Files.isRegularFile(destiny) ? CryptoUtils.getDigest(destiny, "MD5", "32") : null;
        log.log(Level.FINE, "MD5: {0}", md5);
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDefaultUseCaches(false);
            connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
            connection.setRequestProperty("Expires", "0");
            connection.setRequestProperty("Pragma", "no-cache");
            if (md5 != null) {
                connection.setRequestProperty("If-None-Match", md5);
            }
            connection.connect();
            int response = connection.getResponseCode();
            int divided = response / 100;
            if (response == 304) {
                throw new RuntimeException("Used own copy as it matched etag");
            }
            if (divided == 2) {
                try (ReadableByteChannel input = Channels.newChannel(connection.getInputStream());
                        FileChannel output = FileChannel.open(destiny, StandardOpenOption.CREATE,
                                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.DSYNC,
                                StandardOpenOption.WRITE)) {
                    MessageDigest digest = MessageDigest.getInstance("MD5");
                    ByteBuffer bb = ByteBuffer.allocate(1024 * 256);
                    int read;
                    while ((read = input.read(bb)) > 0) {
                        job.addOffset(read);
                        bb.flip();
                        bb.mark();
                        digest.update(bb);
                        bb.reset();
                        output.write(bb);
                        bb.compact();
                    }
                    md5 = String.format("%1$032x", new Object[]{new BigInteger(1,
                        digest.digest())});
                }
                String etag = (connection.getHeaderField("ETag") == null) ? "-"
                        : connection.getHeaderField("ETag");
                if (etag.startsWith("\"") && etag.endsWith("\"")) {
                    etag = etag.substring(1,
                            etag.length() - 1);
                }
                if (etag.contains("-")) {
                    throw new RuntimeException("Etag not found");
                }
                if (etag.equalsIgnoreCase(md5)) {
                    return;
                }
            }
        } catch (Exception e) {
            // File already downloaded
            if (e.getMessage().equals("Used own copy as it matched etag")) {
                return;
            }
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    protected void start() throws Exception {
        log.config("Starting download with MD5Engine");
        if (directory) {
            if (Files.notExists(destiny)) {
                Files.createDirectories(destiny);
            }
            if (name == null) {
                name = url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
            }
            checkNameFile();
            destiny = Paths.get(destiny.toString(), name);
        } else if (Files.notExists(destiny.getParent())) {
            Files.createDirectories(destiny.getParent());
        }
        download();
    }
}
