/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Engine used to do a standalone download.
 * The functionality is the same as the DefaultEngine.
 * @author Infernage
 */
public class SingleEngine extends Downloader{
    private final static Logger log = Logger.getLogger(SingleEngine.class.getName());
    
    /**
     * Default constructor.
     * @param url Link to the file.
     * @param target The local file where will be stored.
     * @param isDirectory {@code true} if the param target is a directory.
     * @throws MalformedURLException
     * @throws IOException 
     */
    public SingleEngine(URL url, Path target, boolean isDirectory) 
            throws MalformedURLException, IOException{
        this(url, target, isDirectory, "Single Engine");
    }
    
    /**
     * Constructor with name.
     * @param url Link to the file.
     * @param target The local file where will be stored.
     * @param isDirectory {@code true} if the param target is a directory.
     * @param jobName The Job name.
     */
    public SingleEngine(URL url, Path target, boolean isDirectory, String jobName){
        super(url, new DownloadJob(jobName), target, isDirectory);
        job.addTask(this);
    }

    @Override
    protected void download() throws Exception {
        log.log(Level.FINE, "Target url: {0}\nTarget file: {1}",
                new Object[]{url.toString(), destiny.toString()});
        Exception ex = null;
        ReadableByteChannel input = null;
        FileChannel channel = FileChannel.open(destiny, StandardOpenOption.CREATE, 
                                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.DSYNC,
                                StandardOpenOption.WRITE);
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Range", "bytes=" + 0 + "-");
            conn.connect();
            input = Channels.newChannel(conn.getInputStream());
            ByteBuffer bb = ByteBuffer.allocate(1024 * 256);
            int read;
            while((read = input.read(bb)) > 0){
                bb.flip();
                channel.write(bb);
                job.addOffset(read);
                bb.compact();
            }
        } catch (Exception e) {
            e.printStackTrace();
            error = true;
            ex = e;
        }
        try {
            input.close();
        } catch (Exception e) {
            // Ignore
        }
        try {
            channel.close();
        } catch (Exception e) {
            // Ignore
        }
        if (error) throw ex;
    }

    @Override
    protected void start() throws Exception {
        log.config("Starting download with SingleEngine");
        if (directory){
            if (Files.notExists(destiny)) Files.createDirectories(destiny);
            if (name == null) name = url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
            checkNameFile();
            destiny = Paths.get(destiny.toString(), name);
        } else if(Files.notExists(destiny.getParent())) Files.createDirectories(destiny.getParent());
        download();
    }
    
    @Override
    public Path callImp() throws Exception{
        try {
            start();
            return destiny;
        } catch (Exception e) {
            try {
                Files.delete(destiny);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }
        return null;
    }
}
