/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.Callable;

/**
 * Abstract class used to download a file from an url.
 *
 * @author Infernage
 */
public abstract class Downloader implements Callable<Path> {

    protected DownloadJob job;
    protected URL url;
    protected Path destiny;
    protected volatile long size = -1L;
    protected String name = null;
    protected boolean error = false, directory, skip = false;
    private boolean started = false;

    /**
     * Default constructor.
     *
     * @param url Link to the file.
     * @param j The job where is called.
     * @param target The local file where will be stored.
     * @param isFolder {@code true} if the param target is a directory.
     */
    public Downloader(URL url, DownloadJob j, Path target, boolean isFolder) {
        this.url = url;
        job = j;
        destiny = target;
        directory = isFolder;
    }

    public void setSize(long size) {
        if (!started) {
            this.size = size;
        }
    }

    public void setName(String name) {
        if (!started) {
            this.name = name;
        }
    }

    public long getSize() {
        try {
            if (size <= 0) {
                size = url.openConnection().getContentLength();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Ignore
        }
        return size;
    }

    /**
     * Gets the URLs loaded.
     *
     * @return The URLs loaded.
     */
    public URL getUrl() {
        return url;
    }

    protected void checkNameFile() {
        if (name.contains("?dl=1")) {
            name = name.replace("?dl=1", "");
        }
        if (name.contains("/") || name.contains("\\")
                || name.contains(":") || name.contains("*")
                || name.contains("?") || name.contains("\"")
                || name.contains("<") || name.contains(">")
                || name.contains("|")) {
            name = "Download" + name.substring(destiny.toAbsolutePath().toString().lastIndexOf(".") + 1);
        }
        if (name.contains("%3B")) {
            name = name.replace("%3B", ";");
        }
    }

    protected void prepare() throws Exception {
        // Only for engines which need a preparation before download.
    }

    protected abstract void download() throws Exception;

    protected abstract void start() throws Exception;

    protected Path callImp() throws Exception {
        if (skip) {
            job.addDownloadedFile(this);
            return null;
        }
        try {
            start();
            job.addDownloadedFile(this);
            return destiny;
        } catch (Exception e) {
            try {
                Files.deleteIfExists(destiny);
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            job.addFailedDownload(this);
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Path call() throws Exception {
        started = true;
        return callImp();
    }
}
