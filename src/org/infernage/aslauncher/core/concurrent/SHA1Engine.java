/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.io.IOException;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Downloader engine which uses SHA1 verification.
 *
 * @author Infernage
 */
public class SHA1Engine extends Downloader {

    private final static Logger log = Logger.getLogger(SHA1Engine.class.getName());

    private String hash;
    private int size;

    /**
     * Default constructor.
     *
     * @param url Link to the file.
     * @param j The job where is called.
     * @param target The local file where will be stored.
     * @param isDirectory {@code true} if the param target is a directory.
     * @param expectedHash The expected hash for the file.
     * @param expectedSize The expected size for the file.
     * @throws MalformedURLException
     * @throws IOException
     */
    public SHA1Engine(URL url, DownloadJob j, Path target, boolean isDirectory, String expectedHash,
            int expectedSize)
            throws MalformedURLException, IOException {
        super(url, j, target, isDirectory);
        hash = expectedHash;
        size = expectedSize;
    }

    protected SHA1Engine(URL url, DownloadJob j, Path target, boolean isDirectory) {
        super(url, j, target, isDirectory);
    }

    protected String makeDownload() throws Exception {
        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setUseCaches(false);
            connection.setDefaultUseCaches(false);
            connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
            connection.setRequestProperty("Expires", "0");
            connection.setRequestProperty("Pragma", "no-cache");
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(30000);
            int response = connection.getResponseCode();
            int divided = response / 100;
            if (divided == 2) {
                String digest;
                try (ReadableByteChannel input = Channels.newChannel(connection.getInputStream());
                        FileChannel output = FileChannel.open(destiny, StandardOpenOption.CREATE,
                                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.DSYNC,
                                StandardOpenOption.WRITE)) {
                    digest = doDigest(input, output, job);
                }
                return digest;
            } else {
                throw new RuntimeException("Server response failed: " + response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @Override
    protected void download() throws Exception {
        log.config("Starting download with SHA1Engine");
        if (Files.isRegularFile(destiny) && Files.size(destiny) == size) {
            log.log(Level.CONFIG, "Skipping file {0}", destiny.toString());
            skip = true;
            return;
        }
        if (destiny.getParent() != null && !Files.isDirectory(destiny.getParent())
                && Files.notExists(destiny.getParent())) {
            Files.createDirectories(destiny.getParent());
        }
        log.log(Level.FINE, "Target url: {0}\nTarget file: {1}",
                new Object[]{url.toString(), destiny.toString()});
        String hash = makeDownload();
        log.log(Level.FINE, "Comparing hash {0} with stored {1}", new Object[]{hash, this.hash});
        if (!hash.equals(this.hash)) {
            throw new RuntimeException(
                    String.format("Hash did not match downloaded file (Expected %s,"
                            + " downloaded %s)", new Object[]{this.hash, hash}));
        }
    }

    @Override
    protected void start() throws Exception {
        if (directory) {
            if (Files.notExists(destiny)) {
                Files.createDirectories(destiny);
            }
            if (name == null) {
                name = url.getFile().substring(url.getFile().lastIndexOf("/") + 1);
            }
            checkNameFile();
            destiny = Paths.get(destiny.toString(), name);
        } else {
            if (Files.notExists(destiny.getParent())) {
                Files.createDirectories(destiny.getParent());
            }
        }
        download();
    }

    static String doDigest(ReadableByteChannel input, WritableByteChannel output, DownloadJob job)
            throws NoSuchAlgorithmException, IOException {
        MessageDigest digest = MessageDigest.getInstance("SHA");
        ByteBuffer bb = ByteBuffer.allocate(256 * 1024);
        while (input.read(bb) > 0) {
            job.addOffset(bb.position());
            bb.flip();
            bb.mark();
            digest.update(bb);
            bb.reset();
            output.write(bb);
            bb.compact();
        }
        input.close();
        output.close();
        return String.format("%1$040x", new Object[]{new BigInteger(1, digest.digest())});
    }
}
