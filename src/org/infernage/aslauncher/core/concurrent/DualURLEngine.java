/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.file.Path;

/**
 *
 * @author Infernage
 */
public class DualURLEngine extends Downloader {

    private Downloader prim, secn;
    private Downloader selected;

    public DualURLEngine(Downloader primary, Downloader secondary, DownloadJob job, Path target,
            boolean isDirectory) {
        super(null, job, target, isDirectory);
        prim = primary;
        secn = secondary;
    }

    @Override
    public long getSize() {
        try {
            URL primURL = prim.getUrl();
            HttpURLConnection conn = (HttpURLConnection) primURL.openConnection();
            if (conn.getResponseCode() == 200) {
                prim.job = job;
                prim.destiny = destiny;
                prim.directory = directory;
                selected = prim;
                return conn.getContentLength();
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            try {
                URL secnURL = secn.getUrl();
                HttpURLConnection conn = (HttpURLConnection) secnURL.openConnection();
                if (conn.getResponseCode() == 200) {
                    secn.job = job;
                    secn.destiny = destiny;
                    secn.directory = directory;
                    selected = secn;
                    return conn.getContentLength();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return size;
    }

    @Override
    protected Path callImp() throws Exception {
        return selected.call();
    }

    @Override
    protected void download() throws Exception {
    }

    @Override
    protected void start() throws Exception {
    }

}
