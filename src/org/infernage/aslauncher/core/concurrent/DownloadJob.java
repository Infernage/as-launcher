/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import org.infernage.aslauncher.common.Job;
import org.infernage.aslauncher.core.CoreHandler;

/**
 * Class used to download.
 *
 * @author Infernage
 */
public class DownloadJob implements Job<Downloader, Path>{

    private final static Logger log = Logger.getLogger(DownloadJob.class.getName());

    private List<Downloader> files = Collections.synchronizedList(new ArrayList<Downloader>());
    private Queue<Downloader> remainingFiles = new ConcurrentLinkedQueue<>();
    private List<Downloader> downloadedFiles = Collections.synchronizedList(new ArrayList<Downloader>());
    private List<Downloader> fails = Collections.synchronizedList(new ArrayList<Downloader>());
    private JProgressBar progress;
    String name;
    private boolean started = false, empty = false;
    private volatile BigDecimal offset = new BigDecimal(-1), totalSize = new BigDecimal(0);

    /**
     * Default constructor.
     *
     * @param name The name of the job.
     */
    public DownloadJob(String name) {
        progress = null;
        this.name = name;
    }

    public DownloadJob(String name, JProgressBar prog) {
        this(name);
        progress = prog;
    }

    /**
     * Adds a download to the job. It's not possible to add a download when the
     * job is running.
     *
     * @param job The download.
     */
    @Override
    public void addTask(Downloader job) {
        if (started) {
            throw new RuntimeException("Downloader job already started");
        }
        files.add(job);
        remainingFiles.offer(job);
    }

    /**
     * Adds a download collection to the job. It's not possible to add it when
     * the job is running.
     *
     * @param collection The downloads.
     */
    @Override
    public void addTaskCollection(Collection<Downloader> collection) {
        if (started) {
            throw new RuntimeException("Downloader job already started");
        }
        files.addAll(collection);
        remainingFiles.addAll(collection);
    }

    /**
     * Starts all scheduled downloads.
     *
     * @return A list of all files.
     * @throws InterruptedException
     */
    @Override
    public List<Future<Path>> startJob() throws Exception {
        log.log(Level.FINE, "Starting job {0}", name);
        if (files.isEmpty()) {
            empty = true;
        }
        long size = 0;
        log.fine("Getting size...");
        for (Downloader file : files) {
            try {
                file.prepare();
                size += file.getSize();
            } catch (Exception e) {
                if (!file.skip) {
                    throw e;
                } else {
                    addDownloadedFile(file);
                }
            }
        }
        totalSize = BigDecimal.valueOf(size);
        log.log(Level.FINE, "Size calculated on: {0} bytes", totalSize.toString());
        if (empty){
            log.log(Level.FINE, "Empty job {0}. Skipping...", name);
            return new ArrayList<>();
        }
        started = true;
        log.config("Invoking tasks");
        return CoreHandler.INSTANCE.executeCollectionTask(files);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setProgressBar(JProgressBar bar) {
        if (started){
            throw new RuntimeException("Downloader job already started");
        }
        progress = bar;
    }

    @Override
    public JProgressBar getProgressBar() {
        return progress;
    }

    /**
     * Looks if the download has finalized successfully.
     *
     * @return {@code true} if there aren't errors.
     */
    @Override
    public boolean isSuccessfull() {
        return fails.isEmpty() && isDone();
    }

    /**
     * Looks if the job has finalized or not.
     *
     * @return {@code true} if the job has finalized.
     */
    @Override
    public boolean isDone() {
        return remainingFiles.isEmpty();
    }

    @Override
    public BigDecimal getJobProgress() {
        return offset;
    }

    @Override
    public BigDecimal getTotalProgress() {
        return totalSize;
    }

    @Override
    public int getFailTasksCount() {
        return fails.size();
    }

    /**
     * Method used to add a finished download.
     *
     * @param download The download finished.
     */
    void addDownloadedFile(Downloader download) {
        synchronized (downloadedFiles) {
            downloadedFiles.add(download);
        }
        synchronized (remainingFiles) {
            remainingFiles.poll();
        }
    }

    /**
     * Method used to add a failed download.
     *
     * @param download The download failed.
     */
    void addFailedDownload(Downloader download) {
        fails.add(download);
        synchronized (remainingFiles) {
            remainingFiles.poll();
        }
    }

    void addOffset(long percentage) {
        offset = offset.add(new BigDecimal(percentage));
    }

    /**
     * Monitor of all downloads.
     */
    @SuppressWarnings("unused")
    private class ProgressMonitor implements Runnable {

        private final BigDecimal MBYTES = new BigDecimal(1048576);
        private volatile boolean finish = false;

        public boolean isFinished() {
            return finish;
        }

        @Override
        public void run() {
            log.log(Level.CONFIG, "Monitor calculation: Offset = {0}, Total = {1}",
                    new Object[]{offset.toString(), totalSize.toString()});
            if (progress == null) {
                consoleType();
            } else {
                guiType();
            }
            finish = true;
        }

        private void consoleType() {
            System.out.print("Total progress   :|");
            for (int i = 0; i < 10; i++) {
                System.out.print("=");
            }
            System.out.println("|\nFile size: " + totalSize.divide(MBYTES).doubleValue() + "MB");
            System.out.print("Transfer progress:|");
            int bar = 0;
            while (bar < 10) {
                if (offset.setScale(2, RoundingMode.HALF_UP).equals(totalSize
                        .setScale(2, RoundingMode.HALF_UP)) && isDone()) {
                    break; // Download already finished. Avoid infinite loop.
                }
                BigDecimal tmp = offset.divide(totalSize.intValue() == 0 ? BigDecimal.ONE : totalSize, 2,
                        RoundingMode.HALF_UP).multiply(BigDecimal.TEN);
                BigDecimal temp = tmp.subtract(new BigDecimal(bar));
                for (int i = 0; i < temp.intValue(); i++) {
                    System.out.print("=");
                }
                bar = tmp.intValue();
            }
            System.out.println("| Finished");
        }

        private void guiType() {
            progress.setValue(0);
            progress.setMinimum(0);
            progress.setMaximum(100);
            while (progress.getValue() < 100) {
                if (offset.setScale(2, RoundingMode.HALF_UP).equals(totalSize
                        .setScale(2, RoundingMode.HALF_UP)) && isDone()) {
                    break; // Download already finished. Avoid infinite loop.
                }
                BigDecimal tmp = offset.divide(totalSize.intValue() == 0 ? BigDecimal.ONE : totalSize, 2,
                        RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
                progress.setValue(tmp.intValue());
                progress.setString(tmp.intValue() + "%");
            }
        }
    }
}
