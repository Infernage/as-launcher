/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import com.google.common.hash.Hashing;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Pack200;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.versions.Library;
import org.infernage.aslauncher.util.References;
import org.infernage.aslauncher.xz_coder.xz.XZInputStream;

/**
 * Downloader engine used to download Forge files.
 *
 * @author Infernage
 */
public class ForgeEngine extends Downloader {

    private final static Logger log = Logger.getLogger(ForgeEngine.class.getName());
    private static boolean useNatives;
    
    static{
        try {
            System.loadLibrary("ASLL");
            useNatives = true;
        } catch (Throwable e) {
            log.throwing(ForgeEngine.class.getName(), "<cinit>", e);
            useNatives = false;
        }
    }
    
    private MCInstance instance;
    private Library lib;

    private List<String> checksums;
    private Path libPath;

    public ForgeEngine(DownloadJob j, MCInstance inst, Library forge) {
        super(null, j, null, false);
        instance = inst;
        lib = forge;
    }

    @Override
    protected void download() throws Exception {
        log.log(Level.FINE, "Target url: {0}\nTarget file: {1}",
                new Object[]{url.toString(), destiny.toString()});
        Exception ex = null;
        ReadableByteChannel input = null;
        FileChannel channel = FileChannel.open(destiny, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.DSYNC,
                StandardOpenOption.WRITE);
        try {
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            input = Channels.newChannel(conn.getInputStream());
            ByteBuffer bb = ByteBuffer.allocate(1024 * 256);
            int read;
            while ((read = input.read(bb)) > 0) {
                bb.flip();
                channel.write(bb);
                job.addOffset(read);
                bb.compact();
            }
        } catch (Exception e) {
            if (url.toString().startsWith("https://libraries.minecraft.net/")) {
                log.fine("Mirror file detected and failed. Skipping...");
                skip = true;
                throw new RuntimeException("Mirror file impossible to download. Skipping...");
            }
            e.printStackTrace();
            error = true;
            ex = e;
        }
        try {
            input.close();
        } catch (Exception e) {
            // Ignore
        }
        try {
            channel.close();
        } catch (Exception e) {
            // Ignore
        }
        if (error) {
            throw ex;
        }
    }

    @Override
    protected void start() throws Exception {
        log.config("Starting download with ForgeEngine");
        checkNameFile();
        if (directory) {
            if (Files.notExists(destiny)) {
                Files.createDirectories(destiny);
            }
            destiny = Paths.get(destiny.toString(), name);
        } else {
            if (Files.notExists(destiny.getParent())) {
                Files.createDirectories(destiny.getParent());
            }
        }
        download();
        postDownload();
    }

    private void postDownload() throws Exception {
        log.config("Checking checksums and signatures");
        Files.deleteIfExists(libPath);
        // TODO Export XZ engine to native code.
        byte[] decoded;
        if (useNatives){
            decoded = readFromNative(destiny.toAbsolutePath().toString());
        } else{
            decoded = readAllData(new XZInputStream(new FileInputStream(destiny.toFile())), true);
        }
        String end = new String(decoded, decoded.length - 4, 4);
        if (!end.equals("SIGN")) {
            throw new Exception("Signature missing " + end);
        }
        int x = decoded.length;
        int length = decoded[(x - 8)] & 0xFF | (decoded[(x - 7)] & 0xFF) << 8
                | (decoded[(x - 6)] & 0xFF) << 16 | (decoded[(x - 5)] & 0xFF) << 24;
        byte[] byteChecksums = Arrays.copyOfRange(decoded, decoded.length - length - 8,
                decoded.length - 8);
        try (FileOutputStream output = new FileOutputStream(libPath.toFile());
                JarOutputStream jos = new JarOutputStream(output)) {
            Pack200.newUnpacker().unpack(new ByteArrayInputStream(decoded), jos);
            jos.putNextEntry(new JarEntry("checksums.sha1"));
            jos.write(byteChecksums);
            jos.closeEntry();
        }
        Files.deleteIfExists(destiny);
        if (!checkChecksum()) {
            throw new Exception("Checksum verification failed");
        }
        destiny = libPath;
    }
    
    private native byte[] readFromNative(String file) throws Exception;

    private byte[] readAllData(InputStream input, boolean autoclose) throws IOException {
        byte[] buffer = new byte[256 * 1024];
        ByteArrayOutputStream entry = new ByteArrayOutputStream();
        int read;
        while ((read = input.read(buffer)) != -1) {
            if (read > 0) {
                entry.write(buffer, 0, read);
            }
        }
        if (autoclose) {
            input.close();
        }
        return entry.toByteArray();
    }

    private boolean checkChecksum() throws IOException {
        try {
            byte[] data = Files.readAllBytes(libPath);
            boolean valid = (checksums == null) || (checksums.isEmpty())
                    || (checksums.contains(Hashing.sha1().hashBytes(data).toString()));
            if ((!valid) && (libPath.toString().endsWith(".jar"))) {
            }
            Map<String, String> files = new HashMap<>();
            String[] hashes = null;
            try (JarInputStream jar = new JarInputStream(new ByteArrayInputStream(data))) {
                JarEntry entry = jar.getNextJarEntry();
                while (entry != null) {
                    byte[] eData = readAllData(jar, false);
                    if (entry.getName().equals("checksums.sha1")) {
                        hashes = new String(eData, Charset.forName("UTF-8")).split("\n");
                    }
                    if (!entry.isDirectory()) {
                        files.put(entry.getName(), Hashing.sha1().hashBytes(eData).toString());
                    }
                    entry = jar.getNextJarEntry();
                }
            }
            if (hashes != null) {
                boolean failed = !checksums.contains(files.get("checksums.sha1"));
                if (!failed) {
                    for (String hash : hashes) {
                        if ((!hash.trim().isEmpty()) && (hash.contains(" "))) {
                            String[] e = hash.split(" ");
                            String validChecksum = e[0];
                            String target = e[1];
                            String checksum = (String) files.get(target);
                            if ((!files.containsKey(target)) || (checksum == null)) {
                                log.log(Level.SEVERE, "{0}: missing", target);
                                failed = true;
                            } else if (!checksum.equals(validChecksum)) {
                                log.log(Level.SEVERE, "{0}: failed({1}, {2})",
                                        new Object[]{target, checksum, validChecksum});
                                failed = true;
                            }
                        }
                    }
                }
                if (!failed) {
                    log.log(Level.INFO, "{0} contents validated successfully", name);
                }
                return !failed;
            }
            log.config("checksums.sha1 was not found, validation failed");
            return false;
        } catch (IOException e) {
            log.throwing(this.getClass().getName(), "checkChecksum", e);
        }
        return false;
    }

    @Override
    protected void prepare() throws Exception {
        log.config("Preparing...");
        name = lib.getFilename();
        if (lib.getChecksums() != null) {
            checksums = Arrays.asList(lib.getChecksums());
        } else {
            checksums = new ArrayList<>();
        }
        if (lib.isClientreq()) {
            String[] parts = lib.getName().split(":");
            parts[0] = parts[0].replace('.', '/');
            String jar = parts[1] + "-" + parts[2] + ".jar";
            String path = parts[0] + "/" + parts[1] + "/" + parts[2] + "/" + jar;
            libPath = Paths.get(instance.getPath().getParent().getParent().toString(), "libraries",
                    path.replace("/", File.separator));
            String url = References.MINECRAFT_LIBRARIES;
            if (lib.hasOwnURL()) {
                url = lib.getURL();
            }
            log.log(Level.CONFIG, "Target url: {0}", url);
            if (Files.exists(libPath) && checkChecksum()) {
                log.config("File already downloaded, skipping");
                skip = true;
                throw new RuntimeException("Not required");
            }
            if (Files.notExists(libPath.getParent())) {
                Files.createDirectories(libPath.getParent());
            }
            url += path + ".pack.xz";
            Path pack = Paths.get(libPath.toString() + ".pack.xz");
            this.url = new URL(url);
            destiny = pack;
            log.log(Level.CONFIG, "Target pack file: {0}\nTarget file: {1}",
                    new Object[]{destiny.toString(), libPath.toString()});
        } else {
            log.config("Not requested, skipping");
            skip = true;
            throw new RuntimeException("Not required");
        }
    }
}
