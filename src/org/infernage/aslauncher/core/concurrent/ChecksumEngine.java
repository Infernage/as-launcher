/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.concurrent;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.infernage.aslauncher.util.CryptoUtils;

/**
 * Downloader engine which uses checksum verification.
 *
 * @author Infernage
 */
public class ChecksumEngine extends SHA1Engine {

    private final static Logger log = Logger.getLogger(ChecksumEngine.class.getName());

    private String checksum;

    public ChecksumEngine(URL url, DownloadJob j, Path target, boolean isDirectory) {
        super(url, j, target, isDirectory);
    }

    @Override
    protected void download() throws Exception {
        log.config("Starting download with ChecksumEngine");
        if (destiny.getParent() != null && !Files.isDirectory(destiny.getParent())
                && Files.notExists(destiny.getParent())) {
            Files.createDirectories(destiny.getParent());
        }
        log.log(Level.FINE, "Target url: {0}\nTarget file: {1}",
                new Object[]{url.toString(), destiny.toString()});
        Path checksumFile = Paths.get(destiny.toAbsolutePath().toString() + ".sha");
        String hash = Files.isRegularFile(destiny) ? CryptoUtils.getDigest(destiny, "SHA-1", "40") : null;
        log.log(Level.FINE, "Hash file calculated on {0}", hash);
        if (Files.isRegularFile(destiny) && Files.isRegularFile(checksumFile)) {
            log.config("Getting checksum from existing file");
            try {
                checksum = Files.readAllLines(checksumFile, Charset.defaultCharset()).get(0);
                if (checksum.length() == 0 || checksum.trim().equalsIgnoreCase(hash)) {
                    skip = true;
                    log.log(Level.FINE, "Skipping existing file {0}", destiny.getFileName().toString());
                    throw new RuntimeException("Already downloaded");
                }
            } catch (Exception e) {
                if (skip) {
                    return;
                }
                checksum = null;
                try {
                    Files.deleteIfExists(checksumFile);
                } catch (Exception ex) {
                }
            }
        }
        if (checksum == null) {
            log.config("Getting checksum file");
            try {
                HttpURLConnection connection = (HttpURLConnection) new URL(url.toString() + ".sha1")
                        .openConnection();
                connection.setUseCaches(false);
                connection.setDefaultUseCaches(false);
                connection.setRequestProperty("Cache-Control", "no-store,max-age=0,no-cache");
                connection.setRequestProperty("Expires", "0");
                connection.setRequestProperty("Pragma", "no-cache");
                connection.setConnectTimeout(5000);
                connection.setReadTimeout(30000);
                if (connection.getResponseCode() / 100 == 2) {
                    try (InputStream input = connection.getInputStream()) {
                        checksum = IOUtils.toString(input, Charsets.UTF_8);
                        FileUtils.writeStringToFile(checksumFile.toFile(), checksum);
                    } catch (Exception e) {
                        checksum = "";
                    }
                } else if (Files.isRegularFile(checksumFile)) {
                    try {
                        checksum = Files.readAllLines(checksumFile, Charset.defaultCharset()).get(0);
                    } catch (Exception e) {
                        checksum = "";
                    }
                } else {
                    checksum = "";
                }
            } catch (Exception e) {
                if (Files.isRegularFile(destiny)) {
                    try {
                        checksum = Files.readAllLines(checksumFile, Charset.defaultCharset()).get(0);
                    } catch (Exception ex) {
                        checksum = "";
                    }
                } else {
                    throw e;
                }
            }
        }
        try {
            log.log(Level.FINE, "Downloading file {0}", destiny.getFileName().toString());
            String digest = makeDownload();
            if (checksum == null || checksum.length() == 0) {
                skip = true;
                log.log(Level.FINE, "File {0} Downloaded successfully", destiny.getFileName().toString());
                return;
            }
            if (checksum.trim().equalsIgnoreCase(digest)) {
                return;
            }
            throw new RuntimeException("Checksum expected: " + checksum + ", downloaded: " + digest);
        } catch (Exception e) {
            if (Files.isRegularFile(destiny) && e.toString().contains("Server response")) {
                skip = true;
            }
            throw e;
        }
    }
}
