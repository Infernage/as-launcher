/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core;

import com.google.common.collect.ImmutableList;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.infernage.aslauncher.core.crypto.SuiteCrypto;
import org.infernage.aslauncher.core.loaders.ModLoader.ModContainer;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.util.IO;

/**
 * Configuration handler class used in Core Module.
 *
 * @author Infernage
 */
public class CoreConfiguration implements Serializable {

    private static final long serialVersionUID = 100112345;
    private static final Logger log = Logger.getLogger(CoreConfiguration.class.getName());

    public static enum ConfigKeys {

        WorkingDir,
        ConfigDir,
        ModsDir,
        ProfilesDir,
        MaxRam,
        Log
    }

    public static enum OS {

        windows("windows"),
        linux("linux"),
        osx("osx"),
        unknown("unknown");

        private final String name;

        private OS(String n) {
            name = n;
        }

        public String getName() {
            return name;
        }

        public boolean isSupported() {
            return this != unknown;
        }

        @Override
        public String toString() {
            return name;
        }
    }

    private UUID uuid = UUID.randomUUID();
    private OS currentOS;
    private Map<ConfigKeys, String> settings;
    private transient SuiteCrypto coreCrypto; // TODO Implement Configuration SuiteCrypto.
    private List<Profile> profiles;
    Map<String, ModContainer> modules;

    /**
     * Default constructor.
     *
     * @param os The OS which is hosting the program.
     */
    CoreConfiguration(String os) {
        currentOS = OS.valueOf(os);
        profiles = new ArrayList<>();
        settings = new HashMap<>();
        settings.put(ConfigKeys.WorkingDir, getWorkingDir(os));
        settings.put(ConfigKeys.ConfigDir, getWorkingDir(os) + File.separator + "config");
        settings.put(ConfigKeys.ModsDir, getWorkingDir(os) + File.separator + "mods");
        settings.put(ConfigKeys.ProfilesDir, getWorkingDir(os) + File.separator + "profiles");
        settings.put(ConfigKeys.Log, getWorkingDir(os) + File.separator + "ASL.log");
        modules = new ConcurrentHashMap<>();
        // initCrypto();
    }

    /**
     * Gets the OS.
     *
     * @return The OS.
     */
    public OS getOS() {
        return currentOS;
    }

    /**
     * Obtains the UUID.
     *
     * @return An UUID.
     */
    public UUID getUUID() {
        return uuid;
    }

    /**
     * Adds a Profile to the list.
     *
     * @param profile The Profile to add.
     */
    public synchronized void addProfile(Profile profile) {
        if (!profiles.contains(profile) && !contains(profile)) {
            profiles.add(profile);
        }
    }

    /**
     * Removes a Profile from the list.
     * <b>All connected with the Profile will be removed!</b>
     *
     * @param profile The Profile to remove.
     */
    public void removeProfile(Profile profile) {
        try {
            synchronized (this) {
                profiles.remove(profile);
                IO.delete(profile.getPath());
            }
        } catch (Exception e) {
            log.log(Level.SEVERE, "Failed to remove the profile " + profile.getProfilename(), e);
        }
    }

    /**
     * Gets an unmodifiable list of all profiles created.
     *
     * @return An unmodifiable list.
     */
    public List<Profile> getProfiles() {
        return ImmutableList.copyOf(profiles);
    }

    /**
     * Gets a value from the map.
     *
     * @param key The key in the map.
     * @return The value mapped.
     */
    public String getMapValue(ConfigKeys key) {
        return settings.get(key);
    }

    /**
     * Change a value from the map.
     *
     * @param key The key of the value. It has to be in the map.
     * @param value The new value.
     */
    public void changeMapValue(ConfigKeys key, String value) {
        if (settings.containsKey(key)) {
            settings.put(key, value);
        }
    }

    private boolean contains(Profile profile) {
        for (Profile baseProfile : profiles) {
            if (baseProfile.getUsername().equals(profile.getUsername())
                    || baseProfile.getProfilename().equals(profile.getProfilename())) {
                return true;
            }
        }
        return false;
    }

    private void initCrypto() throws Exception {
        Constructor<SuiteCrypto> constructor = SuiteCrypto.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        coreCrypto = constructor.newInstance();
        Method init = SuiteCrypto.class.getDeclaredMethod("init", String.class);
        init.setAccessible(true);
        init.invoke(coreCrypto, "Root");
    }

    private static native String getCurrentApplicationPath();

    /**
     * Gets the actual jar.
     *
     * @return The jar executed.
     * @throws URISyntaxException If something went wrong.
     * @deprecated Use getCurrentPath method instead.
     */
    static Path getCurrentJar() throws URISyntaxException {
        return Paths.get(CoreConfiguration.class.getProtectionDomain().getCodeSource().getLocation()
                .toURI());
    }

    /**
     * Gets the current path.
     *
     * @return The executable path.
     */
    static Path getCurrentPath() {
        return Paths.get(getCurrentApplicationPath());
    }

    /**
     * Gets the folder config path.
     *
     * @return The folder config path.
     */
    static String getWorkingDir(String OS) {
        switch (OS) {
            case "windows":
                return System.getenv("APPDATA") + "\\ASL";
            case "linux":
                return System.getProperty("user.home") + "/.ASL";
            case "macosx":
                return System.getProperty("user.home") + "/Library/Application Support/ASL";
            default:
                return System.getProperty("user.home") + "/ASL";
        }
    }
}
