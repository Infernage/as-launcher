/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.core;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;

/**
 * Formatter used in the log to format the output.
 * @author Infernage
 */
public final class LogFormatter extends Formatter {
    private static String format = "[%1$tb %1$td %1$tY] [%1$tl:%1$tM:%1$tS %1$Tp] [%2$s]%n[%4$s]:"
            + " %5$s%6$s%n";
    private Date date = new Date();
    
    @Override
    public String format(LogRecord record) {
        if (record instanceof LogHandler.MinecraftLogRecord){
            return formatMessage(record);
        }
        date.setTime(record.getMillis());
        String source;
        if (record.getSourceClassName() != null) {
            source = record.getSourceClassName();
            if (record.getSourceMethodName() != null) {
               source += " " + record.getSourceMethodName();
            }
        } else {
            source = record.getLoggerName();
        }
        String msg = formatMessage(record);
        String exc = "";
        if (record.getThrown() != null) {
            StringWriter sw = new StringWriter();
            try (PrintWriter pw = new PrintWriter(sw)) {
                pw.println();
                record.getThrown().printStackTrace(pw);
            }
            exc = sw.toString();
        }
        String res = String.format(format, date, source, record.getLoggerName(), record.getLevel()
                .getName(), msg, exc);
        return record.getLevel() == Level.CONFIG ? "[" + record.getThreadID() + "/" + Thread
                .currentThread().getName() + "] " + res : res;
    }

}
