/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.core;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Infernage
 */
final class InternalConsole {

    private final static Logger log = Logger.getLogger("org.infernage.aslauncher");

    static void setUp() {
        log.setLevel(Level.ALL);
        log.setUseParentHandlers(false);
        LogHandler.ConsoleLogHandler handler = new LogHandler.ConsoleLogHandler();
        handler.setFormatter(new LogFormatter());
        handler.setLevel(Level.ALL);
        log.addHandler(handler);
    }

    static void addPath(String logFile) {
        try {
            if (Files.notExists(Paths.get(logFile))) {
                Files.createDirectories(Paths.get(logFile).getParent());
                Files.createFile(Paths.get(logFile));
            }
            LogHandler.FileLogHandler handler = new LogHandler.FileLogHandler(logFile);
            handler.setFormatter(new LogFormatter());
            log.addHandler(handler);
        } catch (IOException | SecurityException e) {
            log.log(Level.WARNING, "Failed to open the File Handler", e);
        }
    }
}
