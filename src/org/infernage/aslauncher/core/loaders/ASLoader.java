/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.loaders;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.loaders.ModLoader.ModContainer;
import org.infernage.aslauncher.exceptions.ModuleInitializationException;

/**
 * Class used to load the program.
 *
 * @author Infernage
 * @version 0.6.0 Beta
 */
public final class ASLoader {

    /**
     * Sets the operative system.
     */
    private static String getOS() {
        String OS = System.getProperty("os.name").toLowerCase();
        if (OS.contains("win")) {
            OS = "windows";
        } else if (OS.contains("lin") || OS.contains("uni")) {
            OS = "linux";
        } else if (OS.contains("mac")) {
            OS = "osx";
        } else {
            OS = "unknown";
        }
        return OS;
    }

    private static void launch() {
        // Set JVM properties.
        System.setProperty("java.net.preferIPv4Stack", "true");
        System.setProperty("java.net.useSystemProxies", "true");
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException |
                UnsupportedLookAndFeelException e) {
            e.printStackTrace();
            try {
                UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
            } catch (ClassNotFoundException | IllegalAccessException | InstantiationException 
                    | UnsupportedLookAndFeelException ex) {
                // Exception again?
                e.printStackTrace();
            }
        }

        Map<String, ModContainer> map;

        // Initialize the system.
        try {
            map = CoreHandler.start(getOS());
        } catch (Exception e) {
            Logger.getLogger(ASLoader.class.getName()).log(Level.SEVERE, "Failed to load system", e);
            try {
                System.exit(1);
            } catch (Exception ex) {
                // Ignore
            }
            return;
        }

        if (map == null) {
            NullPointerException e = new NullPointerException("ModContainer map is NULL");
            Logger.getLogger(ASLoader.class.getName()).log(Level.SEVERE, "Failed to fetch modules", e);
            try {
                System.exit(1);
            } catch (Exception ex) {
                // Ignore
            }
            return;
        }

        /*
         Initialize program modules.
         */
        try {
            ModLoader.initModules(map);
        } catch (ModuleInitializationException e) {
            Logger.getLogger(ASLoader.class.getName()).log(Level.SEVERE,
                    "An error has been detected while ModLoader was loading the modules."
                    + "\nAll modules will be disabled. Cause:\n" + e.getLogMessage(),
                    e.getThrowHolder());
        }
    }

    /**
     * Main program function.
     *
     * @param args Program arguments
     */
    public static void main(String[] args) {
        launch();
    }
}
