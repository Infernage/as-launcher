/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.loaders;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.infernage.aslauncher.common.EventHandler;
import org.infernage.aslauncher.common.Module;
import org.infernage.aslauncher.core.CoreConfiguration;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.events.ASLEvent;
import org.infernage.aslauncher.core.events.ShutdownEvent;
import org.infernage.aslauncher.exceptions.ModuleInitializationException;

/**
 * Class used to load all program mods.
 *
 * @author Infernage
 */
public final class ModLoader {

    private static final Logger log = Logger.getLogger(ModLoader.class.getName());
    private static final ModLoader internalInstance = new ModLoader();

    private static URLClassLoader externalLoader;
    private static Map<String, ModContainer> modules;

    private ModLoader() {
    }

    @Subscribe
    public void stopModules(ShutdownEvent event) {
        for (ModContainer modContainer : modules.values()) {
            modContainer.shutdown(event);
        }
    }

    /**
     * Initializes all modules defined in settings.
     *
     * @throws ModuleInitializationException
     */
    static void initModules(Map<String, ModContainer> modules) throws ModuleInitializationException {
        CoreHandler.INSTANCE.subscribe(internalInstance);
        ModLoader.modules = modules;
        try {
            // Initialize internal modules.
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            Enumeration<URL> resources = cl
                    .getResources("classes:org/infernage/aslauncher");
            List<Class<?>> possibleInternalModules = new ArrayList<>();
            while(resources.hasMoreElements()){
                URL resource = resources.nextElement();
                String name = resource.getPath().replace('/', '.').replace('\\', '.')
                        .replace(".class", "");
                if (name != null){
                    possibleInternalModules.add(Class.forName(name));
                }
            }
            // Scan the classes for possible modules.
            for (Class<?> c : possibleInternalModules) {
                if (c.isAnnotationPresent(Module.class)) {
                    Module annotation = c.getAnnotation(Module.class);
                    if (modules.containsKey(annotation.id())) {
                        continue;
                    }
                    ModContainer container = new InternalModContainer(annotation, c);
                    container.getAnnotations();
                    container.instance = c.newInstance();
                    modules.put(container.modId, container);
                }
            }
        } catch (Exception e) {
            // ModuleLoader failed, mark all modules as disabled
            for (Map.Entry<String, ModContainer> entry : modules.entrySet()) {
                ModContainer modContainer = entry.getValue();
                modContainer.modLoaderEnabled = false;
            }
            throw new ModuleInitializationException("Failed during initialization of ASL internal modules",
                    e);
        }
        // Now initialize external modules.
        try {
            Path modsFolder = Paths.get(CoreHandler.getConfiguration().getMapValue(CoreConfiguration.ConfigKeys.ModsDir));
            if (Files.notExists(modsFolder)) {
                Files.createDirectories(modsFolder);
            }
            Table<Path, URL, List<String>> possibleClasses = HashBasedTable.create();
            // Scan all .jar files in the modules folder
            for (Path possibleMod : Files.newDirectoryStream(modsFolder, "*.jar")) {
                try (JarFile jar = new JarFile(possibleMod.toFile())) {
                    Enumeration<JarEntry> entries = jar.entries();
                    List<String> classes = new ArrayList<>();
                    possibleClasses.put(possibleMod, possibleMod.toUri().toURL(), classes);
                    while (entries.hasMoreElements()) {
                        JarEntry entry = entries.nextElement();
                        if (entry.getName().endsWith(".class")) {
                            String name = entry.getName().substring(0, entry.getName()
                                    .lastIndexOf(".class"));
                            if (name.lastIndexOf("/") != -1) {
                                name = name.replaceAll("/", ".");
                            }
                            if (name.lastIndexOf("\\") != -1) {
                                name = name.replaceAll("\\", ".");
                            }
                            if (name.startsWith("org.infernage.aslauncher.core")) {
                                // Don't allow to override Core classes
                                log.log(Level.CONFIG, "File {0} trying to override ASL Core classes",
                                        possibleMod.getFileName().toString());
                                possibleClasses.remove(possibleMod, possibleMod.toUri().toURL());
                                log.config("Removed from ModLoader");
                                break;
                            }
                            classes.add(name);
                        }
                    }
                } catch (Exception e) {
                    log.log(Level.WARNING, "Failed to scan module file "
                            + possibleMod.getFileName().toString(), e);
                    possibleClasses.remove(possibleMod, possibleMod.toUri().toURL());
                }
            }
            // Load all scanned classes for possible modules
            externalLoader = new URLClassLoader(possibleClasses.columnKeySet().toArray(new URL[0]));
            for (URL url : possibleClasses.columnKeySet()) {
                for (Map.Entry<Path, List<String>> entry : possibleClasses.column(url).entrySet()) {
                    Path path = entry.getKey();
                    List<String> list = entry.getValue();
                    for (String s : list) {
                        Class<?> c = Class.forName(s, true, externalLoader);
                        if (c.isAnnotationPresent(Module.class)) {
                            // Module found
                            Module a = c.getAnnotation(Module.class);
                            if (modules.containsKey(a.id()) && modules.get(a.id()).modName.equals(a.name())
                                    && modules.get(a.id()).version.equals(a.version())) {
                                // Same module inside mods folder. Not necessary to scan.
                                continue;
                            }
                            // Don't allow repeated modules. Possible override error.
                            if (modules.containsKey(a.id())) {
                                log.log(Level.WARNING, "The module {0}-{1} is trying to replace the"
                                        + " module\n{2}-{3}. It will be discarted.",
                                        new Object[]{a.name(), a.version(),
                                            modules.get(a.id()).modId,
                                            modules.get(a.id()).version});
                                continue;
                            }
                            ModContainer container = new InjectedModContainer(a, c, path);
                            container.getAnnotations();
                            container.instance = c.newInstance();
                            modules.put(container.modId, container);
                        }
                    }
                }
            }
        } catch (Exception e) {
            // ModuleLoader failed, mark all modules as disabled
            for (Map.Entry<String, ModContainer> entry : modules.entrySet()) {
                ModContainer modContainer = entry.getValue();
                modContainer.modLoaderEnabled = false;
            }
            throw new ModuleInitializationException("Failed during initialization of ASL external modules",
                    e);
        }
    }

    private static void findInternalJarClasses(String resPath, String pkgname, List<Class<?>> classes)
            throws Exception {
        String relPath = pkgname.replace('.', '/');
        String jarPath = resPath.replaceFirst("[.]jar[!].*", ".jar").replaceFirst("file:", "");
        JarFile jarFile = new JarFile(jarPath);
        Enumeration<JarEntry> entries = jarFile.entries();
        while (entries.hasMoreElements()) {
            JarEntry entry = entries.nextElement();
            String entryName = entry.getName();
            String className = null;
            if (entryName.endsWith(".class") && entryName.startsWith(relPath) && entryName.length()
                    > (relPath.length() + "/".length())) {
                className = entryName.replace('/', '.').replace('\\', '.').replace(".class", "");
            }
            if (className != null) {
                classes.add(Class.forName(className));
            }
        }
    }

    // Only for debugging purposes.
    private static void findInternalDirClasses(File directory, String pkgname, List<Class<?>> classes)
            throws ClassNotFoundException {
        // Get the list of the files contained in the package
        for (String fileName : directory.list()) {
            String className = null;
            // we are only interested in .class files
            if (fileName.endsWith(".class")) {
                // removes the .class extension
                className = pkgname + '.' + fileName.substring(0, fileName.length() - 6);
            }
            if (className != null) {
                classes.add(Class.forName(className));
            }
            File subdir = new File(directory, fileName);
            if (subdir.isDirectory()) {
                findInternalDirClasses(subdir, pkgname + '.' + fileName, classes);
            }
        }
    }

    public static abstract class ModContainer implements Serializable {

        protected Object instance;
        protected Class<?> classObject;
        protected String modId;
        protected String modName;
        protected String version;
        protected String description = "";
        protected List<String> authors;
        protected Set<String> requiredModules = new HashSet<>();
        protected List<String> dependencies = new ArrayList<>();
        protected EventBus bus;
        protected ListMultimap<Class<? extends ASLEvent>, Method> eventMethods;
        private boolean enabled = true;
        private transient boolean modLoaderEnabled = true;

        protected ModContainer(Module a, Class<?> c) {
            classObject = c;
            modId = a.id();
            modName = a.name();
            version = a.version();
            authors = Lists.newArrayList(a.authors());
            eventMethods = ArrayListMultimap.create();
        }

        public final void shutdown(ShutdownEvent event) {
            // TODO Send the event to all Modules using the internal EventBus.
        }

        public final void getAnnotations() {
            for (Method method : classObject.getDeclaredMethods()) {
                if (method.isAnnotationPresent(EventHandler.class)) {
                    if (method.getParameterTypes().length == 1
                            && ASLEvent.class.isAssignableFrom(method.getParameterTypes()[0])) {
                        method.setAccessible(true);
                        eventMethods.put((Class<? extends ASLEvent>) method.getParameterTypes()[0],
                                method);
                    }
                }
            }
        }

        public final boolean isEnabled() {
            return enabled && modLoaderEnabled;
        }

        final void enable() {
            if (modLoaderEnabled) {
                enabled = true;
            }
        }

        final void disable() {
            if (modLoaderEnabled) {
                enabled = false;
            }
        }
    }

    public static class InternalModContainer extends ModContainer {

        private static final long serialVersionUID = 316473284;

        public InternalModContainer(Module annotation, Class<?> cl) {
            super(annotation, cl);
        }
    }

    public static class InjectedModContainer extends ModContainer {

        private static final long serialVersionUID = 384739284;

        private String sourceFile;
        private String url = "";
        private String updateUrl = "";
        private String credits = "";

        public InjectedModContainer(Module annotation, Class<?> cl, Path file) {
            super(annotation, cl);
            sourceFile = file.toAbsolutePath().toString();
        }
    }
}
