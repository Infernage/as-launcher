/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.loaders;

import com.google.common.collect.ForwardingMultimap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import org.apache.commons.codec.binary.Base64;
import org.infernage.aslauncher.core.CoreConfiguration;
import org.infernage.aslauncher.core.CoreHandler;
import org.infernage.aslauncher.core.Systray;
import org.infernage.aslauncher.core.events.MinecraftLogEvent;
import org.infernage.aslauncher.gui.MainFrame;
import org.infernage.aslauncher.mcsystem.MCInstance;
import org.infernage.aslauncher.mcsystem.Profile;
import org.infernage.aslauncher.mcsystem.MinecraftEndAction;
import org.infernage.aslauncher.mcsystem.versions.AssetHashList;
import org.infernage.aslauncher.mcsystem.versions.CompleteVersion;
import org.infernage.aslauncher.mcsystem.versions.ExtractRules;
import org.infernage.aslauncher.mcsystem.versions.Library;
import org.infernage.aslauncher.util.IO;
import org.infernage.aslauncher.util.MessageControl;

/**
 * Class used to load Minecraft.
 *
 * @author Infernage
 */
public final class MCLoader implements Runnable {

    private static final Logger log = Logger.getLogger(MCLoader.class.getName());

    private Profile profile;
    private MCInstance instance;

    /**
     * Default constructor.
     *
     * @param profile The user profile.
     * @param instance The selected instance by the user.
     */
    public MCLoader(Profile profile, MCInstance instance) {
        MainFrame.getWindow().setWorking(true);
        this.profile = profile;
        this.instance = instance;
    }

    /**
     * Unpacks MC natives.
     *
     * @param version The version to unpack.
     * @param nativeDir The native directory.
     * @throws IOException
     */
    private void unpackLibraries(CompleteVersion version, Path nativeDir) throws IOException {
        try {
            Collection<Library> libs = version.getRelevantLibraries();
            for (Library lib : libs) {
                Map<CoreConfiguration.OS, String> natives = lib.getNatives();
                if (natives != null && natives.get(CoreHandler.getConfiguration().getOS()) != null) {
                    Path file = Paths.get(instance.getPath().getParent().getParent().toString(), "libraries",
                            lib.getPath(natives.get(CoreHandler.getConfiguration().getOS())));
                    log.log(Level.FINE, "Unpacking {0}", file.toString());
                    ZipFile zip = new ZipFile(file.toFile());
                    ExtractRules rules = lib.getExtractRules();
                    try {
                        Enumeration<? extends ZipEntry> entries = zip.entries();
                        while (entries.hasMoreElements()) {
                            ZipEntry entry = entries.nextElement();
                            if (rules == null || rules.shouldExtract(entry.getName())) {
                                Path target = Paths.get(nativeDir.toString(), entry.getName());
                                if (target.getParent() != null && Files.notExists(target.getParent())) {
                                    Files.createDirectories(target.getParent());
                                }
                                if (!entry.isDirectory()) {
                                    ReadableByteChannel input = Channels.newChannel(zip.getInputStream(entry));
                                    FileChannel output = FileChannel.open(target,
                                            StandardOpenOption.CREATE, StandardOpenOption.DSYNC,
                                            StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE);
                                    ByteBuffer bb = ByteBuffer.allocate(1024 * 256);
                                    try {
                                        while (input.read(bb) != -1) {
                                            bb.flip();
                                            output.write(bb);
                                            bb.compact();
                                        }
                                    } finally {
                                        input.close();
                                        output.close();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        log.throwing(this.getClass().getName(), "load", e);
                    } finally {
                        zip.close();
                    }
                }
            }
        } catch (IOException e) {
            try {
                IO.delete(nativeDir);
            } catch (Exception ex) {
                // Ignore
            }
            throw e;
        }
    }

    /**
     * Loads Minecraft.
     *
     * @throws IOException
     * @throws MalformedURLException
     * @throws InterruptedException
     * @throws NoSuchAlgorithmException
     */
    private void load()
            throws IOException, MalformedURLException, InterruptedException, NoSuchAlgorithmException {
        if (Systray.processAlive()) {
            throw new IOException("Minecraft is alive! Close it and try again");
        }
        String id = instance.getName();
        CompleteVersion version = instance.getVersion();
        MinecraftEndAction runnable = new MinecraftEndAction(profile);
        if (!version.appliesToCurrentEnvironment()) {
            MessageControl.showErrorMessage("Version " + id
                    + " is incompatible with your environment:\n" + version.getIncompatibilityReason()
                    == null ? "Version not compatible. Try another one, please."
                    : version.getIncompatibilityReason(), "Incompatibility found!");
            return;
        }
        log.info("Requesting necessary files");
        instance.requestNecessaryFiles();
        log.info("Cleaning natives");
        instance.cleanNatives();
        Path nativeDir = Paths.get(instance.getPath().toString(), version.getId()
                + "-natives" + System.nanoTime());
        if (Files.notExists(nativeDir) || !Files.isDirectory(nativeDir)) {
            Files.createDirectories(nativeDir);
        }
        log.info("Unpacking natives");
        unpackLibraries(version, nativeDir);
        log.info("Reconstructing assets");
        // Reconstruct assets from SHA-1 coder
        Path assets = Paths.get(instance.getPath().getParent().getParent().toString(), "assets");
        Path index = Paths.get(assets.toString(), "indexes");
        Path objects = Paths.get(assets.toString(), "objects");
        String ass_version = version.getAssets() == null ? "legacy" : version.getAssets();
        Path indexFile = Paths.get(index.toString(), ass_version + ".json");
        Path virtualFolder = Paths.get(assets.toString(), "virtual", ass_version); // <- Assets to use
        AssetHashList indexdb;
        if (Files.isRegularFile(indexFile)) {
            StringBuilder gson = new StringBuilder();
            for (String str : Files.readAllLines(indexFile, Charset.defaultCharset())) {
                gson.append(gson.length() != 0 ? "\n" + str : str);
            }
            indexdb = new Gson().fromJson(gson.toString(), AssetHashList.class);
            if (indexdb.isVirtual()) {
                log.config("Virtual assets found");
                for (Map.Entry<String, AssetHashList.AssetPair> entry : indexdb.getIndex().entrySet()) {
                    Path target = Paths.get(virtualFolder.toString(), entry.getKey());
                    Path orig = Paths.get(objects.toString(), entry.getValue().getHash()
                            .substring(0, 2), entry.getValue().getHash());
                    log.log(Level.CONFIG, "Decoding {0} to {1}", new Object[]{orig.toString(),
                        target.toString()});
                    if (!Files.isRegularFile(target)) {
                        Files.createDirectories(target.getParent());
                        Files.copy(orig, target,
                                StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES);
                    }
                }
            }
        } else {
            indexdb = new AssetHashList();
        }
        log.info("Building process");
        // Build process arguments.
        List<String> args = new ArrayList<>();
        String java = System.getProperty("java.home") + File.separator + "bin"
                + File.separator + (CoreHandler.getConfiguration().getOS()
                .equals(CoreConfiguration.OS.windows) ? "javaw" : "java");
        args.add(java);
        if (CoreHandler.getConfiguration().getOS() == CoreConfiguration.OS.osx) {
            args.add("-Xdock:icon=" + Paths.get(objects.toString(), indexdb.getIndex()
                    .get("icons/minecraft.icns").getHash().substring(0, 2), indexdb.getIndex()
                    .get("icons/minecraft.icns").getHash()));
            args.add("-Xdock:name=" + instance.getName());
        } else if (CoreHandler.getConfiguration().getOS() == CoreConfiguration.OS.windows) {
            args.add("-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe"
                    + ".heapdump");
        }
        args.add("-d" + System.getProperty("sun.arch.data.model"));
        args.add("-Xms256M");
        String ram = profile.getMaxRam().split(" ")[0];
        args.add("-Xmx" + Integer.toString(Integer.parseInt(ram) < 100 ? Integer.parseInt(ram) * 1024
                : Integer.parseInt(ram)) + "M");
        args.add("-XX:+UseConcMarkSweepGC");
        args.add("-XX:+ExplicitGCInvokesConcurrent");
        args.add("-XX:+CMSIncrementalMode");
        args.add("-XX:+AggressiveOpts");
        args.add("-XX:+CMSClassUnloadingEnabled");
        args.add("-XX:MaxPermSize=256m");
        log.info("Getting java lib path");
        args.add("-Djava.library.path=" + nativeDir.toAbsolutePath().toString());
        args.add("-Duser.home=" + instance.getPath().toString());
        StringBuilder builder = new StringBuilder();
        Collection<Path> classpath = new ArrayList<>();
        if (!instance.isLower1_6()) {
            if (instance.getForge() != null) {
                classpath.add(instance.getForge().getPath());
                if (instance.getForge().getLibraries() != null) {
                    for (Library library : instance.getForge().getLibraries()) {
                        classpath.add(profile.getPath().resolve("libraries").resolve(library.getPath()));
                    }
                }
            }
            classpath.add(instance.getPath().resolve(version.getId() + ".jar"));
            classpath.addAll(instance.getVersion().getClassPath(profile.getPath()));
        } else {
            classpath.addAll(instance.getVersion().getClassPath(profile.getPath()));
            if (instance.getForge() != null) {
                classpath.add(instance.getForge().getPath());
            }
            classpath.add(instance.getPath().resolve(version.getId() + ".jar"));
            if (Files.exists(instance.getPath().resolve("lib")) && Files.isDirectory(instance
                    .getPath().resolve("lib")) && instance.getForge() != null) {
                for (Path path : Files.newDirectoryStream(instance.getPath().resolve("lib"))) {
                    if (path.toString().endsWith(".jar") || path.toString().endsWith(".zip")) {
                        classpath.add(path);
                    }
                }
            }
        }
        for (Path file : classpath) {
            if (builder.length() > 0) {
                builder.append(System.getProperty("path.separator"));
            }
            builder.append(file.toAbsolutePath().toString());
        }
        log.info("Building classpath");
        args.add("-cp");
        if (instance.isLower1_6()) {
            args.add(builder.toString() + ";" + System.getProperty("java.class.path"));
        } else {
            args.add(builder.toString());
        }
        if (instance.getForge() == null) {
            args.add(version.getMainClass());
        } else {
            args.add(instance.getForge().getMainClass());
        }
        log.info("Building Map properties into args");
        Map<String, String> map = new HashMap();
        map.put("auth_access_token", profile.getAccessToken());
        map.put("user_properties", new GsonBuilder().registerTypeAdapter(PropertyMap.class,
                new LegacyPropertyMapSerializer()).create().toJson(new PropertyMap()));
        map.put("user_property_map", new GsonBuilder().registerTypeAdapter(PropertyMap.class,
                new PropertyMap.Serializer()).create().toJson(new PropertyMap()));
        if (profile.isPremium()) {
            map.put("auth_session", profile.getSessionToken());
            map.put("auth_uuid", profile.getSessionID());
        } else {
            map.put("auth_session", "-");
            map.put("auth_uuid", new UUID(0L, 0L).toString());
        }
        map.put("auth_player_name", profile.getUsername());
        map.put("user_type", profile.getType().getName());
        map.put("profile_name", profile.getProfilename());
        map.put("version_name", version.getId());
        map.put("game_directory", instance.getPath().toAbsolutePath().toString());
        map.put("game_assets", virtualFolder.toAbsolutePath().toString());
        map.put("assets_root", assets.toAbsolutePath().toString());
        map.put("assets_index_name", version.getAssets() == null ? "legacy" : version.getAssets());
        String[] mc_args = (instance.getForge() != null ? instance.getForge() : version)
                .getMinecraftArguments().split(" ");
        for (int i = 0; i < mc_args.length; i++) {
            for (Map.Entry<String, String> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                if (mc_args[i].contains(key)) {
                    mc_args[i] = value;
                }
            }
        }
        args.addAll(Arrays.asList(mc_args));
        if (instance.getForge() != null && instance.isLower1_6()) {
            args.add(nativeDir.toAbsolutePath().toString());
            args.add(instance.getForge().getPath().toAbsolutePath().toString());
            args.add(instance.getPath().resolve(version.getId() + ".jar").toAbsolutePath()
                    .toString());
            args.add(instance.getPath().resolve("jars").toAbsolutePath().toString());
        }
        args.addAll(Arrays.asList(profile.getJavaArgs().split(" ")));
        log.info("Starting process");
        Process proces = new ProcessBuilder(args).directory(instance.getPath()
                .toAbsolutePath().toFile()).redirectErrorStream(true).start();
        MinecraftStream st = new MinecraftStream(proces.getInputStream());
        st.start();
        runnable.setProcess(proces, nativeDir);
        MainFrame.getWindow().printMsg("Minecraft is executing...");
        try {
            runnable.startListening();
        } finally {
            MainFrame.getWindow().printMsg("Minecraft exited");
            MainFrame.getWindow().setBusy(false);
            MainFrame.getWindow().setWorking(false);
        }
    }

    @Override
    public void run() {
        if (profile.isTimeout()) {
            MessageControl.showErrorMessage("Your demo has finished. If you want to continue playing,"
                    + " buy the game.", "Demo timeout");
            MainFrame.getWindow().printWarningMsg("Demo finished");
            return;
        }
        try {
            load();
        } catch (Exception e) {
            MainFrame.getWindow().setWorking(false);
            MainFrame.getWindow().setBusy(false);
            if (e.toString().contains("Minecraft is alive")) {
                MessageControl.showErrorMessage(e.getMessage(), null);
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Failed to load Minecraft", e);
            }
        }
    }

    private class MinecraftStream extends Thread {

        private InputStream stream;

        public MinecraftStream(InputStream input) {
            super("MinecraftStream");
            stream = input;
            setDaemon(true);
        }

        @Override
        public void run() {
            byte[] buffer = new byte[4096];
            String builder = "";
            try {
                while (stream.read(buffer) > 0) {
                    builder = builder + new String(buffer).replace("\r\n", "\n");
                    int nullIndex = builder.indexOf(0);
                    if (nullIndex != -1) {
                        builder = builder.substring(0, nullIndex);
                    }
                    int line;
                    while ((line = builder.indexOf("\n")) != -1) {
                        if (!builder.substring(0, line).contains("Session ID is")) {
                            CoreHandler.INSTANCE.sendEvent(new MinecraftLogEvent(builder.substring(0, line)
                                    + "\n"));
                        }
                        builder = builder.substring(line + 1);
                    }
                    Arrays.fill(buffer, (byte) 0);
                }
            } catch (Exception e) {
                log.log(Level.SEVERE, "MC Stream failed", e);
            }
            try {
                if (stream != null) {
                    stream.close();
                }
            } catch (Exception e) {
                //Ignore
            }
        }
    }

    /*
     Not used yet.
     */
    private static class Property {

        private final String name;
        private final String value;
        private final String signature;

        public Property(String value, String name) {
            this(value, name, null);
        }

        public Property(String name, String value, String signature) {
            this.name = name;
            this.value = value;
            this.signature = signature;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }

        public String getSignature() {
            return signature;
        }

        public boolean hasSignature() {
            return signature != null;
        }

        public boolean isSignatureValid(PublicKey publicKey) {
            try {
                Signature signature = Signature.getInstance("SHA1withRSA");
                signature.initVerify(publicKey);
                signature.update(value.getBytes());
                return signature.verify(Base64.decodeBase64(this.signature));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            } catch (SignatureException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    private static class PropertyMap
            extends ForwardingMultimap<String, Property> {

        private final Multimap<String, Property> properties;

        public PropertyMap() {
            properties = LinkedHashMultimap.create();
        }

        protected Multimap<String, Property> delegate() {
            return properties;
        }

        public static class Serializer
                implements JsonSerializer<PropertyMap>, JsonDeserializer<PropertyMap> {

            public PropertyMap deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                    throws JsonParseException {
                PropertyMap result = new PropertyMap();
                Iterator i$;
                Map.Entry<String, JsonElement> entry;
                if ((json instanceof JsonObject)) {
                    JsonObject object = (JsonObject) json;
                    for (i$ = object.entrySet().iterator(); i$.hasNext();) {
                        entry = (Map.Entry) i$.next();
                        if ((entry.getValue() instanceof JsonArray)) {
                            for (JsonElement element : (JsonArray) entry.getValue()) {
                                result.put(entry.getKey(), new Property((String) entry.getKey(), element.getAsString()));
                            }
                        }
                    }
                } else if ((json instanceof JsonArray)) {
                    for (JsonElement element : (JsonArray) json) {
                        if ((element instanceof JsonObject)) {
                            JsonObject object = (JsonObject) element;
                            String name = object.getAsJsonPrimitive("name").getAsString();
                            String value = object.getAsJsonPrimitive("value").getAsString();
                            if (object.has("signature")) {
                                result.put(name, new Property(name, value, object.getAsJsonPrimitive("signature").getAsString()));
                            } else {
                                result.put(name, new Property(name, value));
                            }
                        }
                    }
                }
                return result;
            }

            public JsonElement serialize(PropertyMap src, Type typeOfSrc, JsonSerializationContext context) {
                JsonArray result = new JsonArray();
                for (Property property : src.values()) {
                    JsonObject object = new JsonObject();

                    object.addProperty("name", property.getName());
                    object.addProperty("value", property.getValue());
                    if (property.hasSignature()) {
                        object.addProperty("signature", property.getSignature());
                    }
                    result.add(object);
                }
                return result;
            }
        }
    }

    private static class LegacyPropertyMapSerializer
            implements JsonSerializer<PropertyMap> {

        public JsonElement serialize(PropertyMap src, Type typeOfSrc, JsonSerializationContext context) {
            JsonObject result = new JsonObject();
            for (String key : src.keySet()) {
                JsonArray values = new JsonArray();
                for (Property property : src.get(key)) {
                    values.add(new JsonPrimitive(property.getValue()));
                }
                result.add(key, values);
            }
            return result;
        }
    }
}
