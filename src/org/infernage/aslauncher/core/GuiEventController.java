/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.SecureRandom;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JProgressBar;
import org.infernage.aslauncher.common.Job;
import org.infernage.aslauncher.core.CoreHandler.Priority;
import org.infernage.aslauncher.gui.MainFrame;

/**
 * Class used to controll all events sent to GUI.
 *
 * @author Infernage
 */
public final class GuiEventController extends Thread {

    private final static Logger log = Logger.getLogger(GuiEventController.class.getName());

    private final Map<Key, Value> mapping = new TreeMap<>();
    private volatile boolean finished = false, running = false;

    /**
     * Default constructor.
     */
    GuiEventController() {
        super("ASL Gui Event Controller");
    }

    /**
     * Checks if the controller is running an event.
     *
     * @return {@code true} if an event is running.
     */
    public boolean isBusy() {
        return running;
    }

    @Override
    public void run() {
        while (!finished) {
            try {
                System.gc();
                Key k;
                Value v;
                synchronized (this) {
                    running = false;
                    while (mapping.isEmpty() && !finished) { // No events
                        log.config("Waiting for new jobs...");
                        wait();
                    }
                    if (finished) { // Shutdown
                        break;
                    }
                    k = new ArrayDeque<>(mapping.keySet()).getFirst();
                    v = mapping.get(k);
                    v.result.inProcess = true;
                    running = true;
                }
                log.log(Level.CONFIG, "Getting job {0}", v.job.getName());
                MainFrame.getWindow().setBusy(true);
                MainFrame.getWindow().printMsg(v.msg);
                JProgressBar bar = v.job.getProgressBar();
                if (bar == null) {
                    bar = MainFrame.getWindow().getProgressBar();
                }
                bar.setValue(0);
                bar.setMaximum(100);
                bar.setMinimum(0);
                List result = new ArrayList<>();
                List<Future<?>> futures = (List<Future<?>>) v.job.startJob();
                if (futures.isEmpty()) {
                    synchronized (this) {
                        mapping.remove(k);
                    }
                    v.result.setResult(result);
                    MainFrame.getWindow().printMsg("");
                    MainFrame.getWindow().setBusy(false);
                    continue;
                }
                while (bar.getValue() < 100 && !v.job.isDone()) {
                    if (v.job.getJobProgress().setScale(2, RoundingMode.HALF_UP)
                            .equals(v.job.getTotalProgress().setScale(2, RoundingMode.HALF_UP))) {
                        continue;
                    }
                    if (v.job.getJobProgress().intValue() == -1) {
                        bar.setIndeterminate(true);
                    } else {
                        bar.setIndeterminate(false);
                        BigDecimal value = v.job.getJobProgress().divide(v.job.getTotalProgress()
                                .intValue() == 0 ? BigDecimal.ONE : v.job.getTotalProgress(), 4,
                                RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).setScale(2);
                        bar.setValue(value.intValue());
                        bar.setString(value + "%");
                    }
                }
                if (bar.isIndeterminate()) {
                    bar.setIndeterminate(false);
                }
                bar.setValue(0);
                bar.setString("");
                for (Future<?> future : futures) {
                    result.add(future.get());
                }
                v.result.setResult(result);
                log.log(Level.CONFIG, "Job {0} finished", v.job.getName());
                MainFrame.getWindow().printMsg("");
                synchronized (this) {
                    mapping.remove(k);
                }
            } catch (Exception e) {
                log.throwing(this.getClass().getName(), "run", e);
            }
            MainFrame.getWindow().setBusy(false);
        }
        synchronized (this) {
            mapping.clear();
            notifyAll();
        }
    }

    /**
     * Adds a Job to the controller. This will cause to add into a queue and
     * wait to be executed. While the Job is being executed, the controller will
     * show a progressbar and label with all messages sent from the Job.
     *
     * @param job The Job to add.
     * @param message The default message to set.
     * @param priority The Job priority.
     * @return A Result object which will contains the result or null if the Job
     * is already added.
     */
    public Result addJob(Job<?, ?> job, String message, Priority priority) {
        if (containsJob(job) || finished) {
            return null;
        }
        log.log(Level.CONFIG, "Adding job {0}", job.getName());
        Result res = new Result();
        synchronized (this) {
            mapping.put(new Key(System.nanoTime(), priority), new Value(job, message, res));
            notifyAll();
        }
        return res;
    }

    synchronized void quit() {
        finished = true;
        notify();
        try {
            while (!mapping.isEmpty()) {
                wait();
            }
        } catch (InterruptedException e) {
            // Interrupted, wait abort!
        }
    }

    private synchronized boolean containsJob(Job<?, ?> job) {
        for (Value j : mapping.values()) {
            if (job.equals(j.job)) {
                return true;
            }
        }
        return false;
    }

    private boolean containsResult(Result res) {
        if (res.inProcess) {
            return true;
        }
        synchronized (this) {
            for (Value j : mapping.values()) {
                if (res.equals(j.result)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Class used to store the computed result.
     */
    public class Result {

        private List<?> future;
        private final String id = new BigDecimal(new SecureRandom().nextDouble()).toString();
        private volatile boolean inProcess = false;

        private Result() {
            future = null;
        }

        private synchronized void setResult(List result) {
            future = result;
            notifyAll();
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof Result) {
                Result res = (Result) o;
                return res.id.equals(id);
            }
            return false;
        }

        @Override
        public int hashCode() {
            int hash = 5;
            hash = 67 * hash + Objects.hashCode(this.id);
            return hash;
        }

        public List get() {
            if (!CoreHandler.getGuiController().containsResult(this)) { // Job already finished.
                return future;
            }
            synchronized (this) {
                while (future == null) {
                    try {
                        wait();
                    } catch (Exception e) {
                        // Ignore
                    }
                }
            }
            return future;
        }
    }

    private class Key implements Comparable<Key> {

        public long time;
        public Priority priority;

        public Key(long time, Priority prio) {
            this.time = time;
            priority = prio;
        }

        @Override
        public int compareTo(Key o) {
            long delta = time - o.time;
            int deltaPriority = priority.getPriority() - o.priority.getPriority();
            // Base cases.
            if (deltaPriority > 0 && delta > 0) {
                return 1;
            } else if (deltaPriority < 0 && delta < 0) {
                return -1;
            } else if (deltaPriority == 0 && delta == 0) {
                return 0;
            } // Too priority.
            else if (deltaPriority > 1 && delta < 0) {
                return 1;
            } else if (deltaPriority < 1 && delta > 0) {
                return -1;
            } // Too waiting time.
            else if (deltaPriority == 1 && (delta / 1000000000L) < -60L) {
                return -1;
            } else if (deltaPriority == 1 && (delta / 1000000000L) > 60L) {
                return 1;
            } // Other cases.
            else if (deltaPriority > 0) {
                return 1;
            } else if (deltaPriority < 0) {
                return -1;
            } else if (delta > 0) {
                return 1;
            } else if (delta < 0) {
                return -1;
            } else {
                return 0; // In any other case.
            }
        }

    }

    private class Value {

        public Job<?, ?> job;
        public String msg;
        public Result result;

        public Value(Job<?, ?> job, String msg, Result res) {
            this.job = job;
            this.msg = msg;
            result = res;
        }
    }
}
