/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.events;

/**
 * Event used to send a Minecraft log.
 *
 * @author Infernage
 */
public class MinecraftLogEvent {

    public final String log;

    /**
     * Default constructor.
     * @param message The message log.
     */
    public MinecraftLogEvent(String message) {
        log = message;
    }
}
