/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.events;

/**
 * Event used to animate the MainFrame resize.
 *
 * @author Infernage
 */
public class TweenEvent {

    public final boolean reverse;

    public TweenEvent(boolean mayReverse) {
        reverse = mayReverse;
    }
}
