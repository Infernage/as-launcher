/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */

package org.infernage.aslauncher.core.events;

import java.util.logging.Level;

/**
 * Event used to send a log.
 * @author Infernage
 */
public final class LogEvent {
    public final String log;
    public final Level type;
    
    /**
     * Default constructor.
     * @param message The log message.
     * @param logType The log type.
     */
    public LogEvent(String message, Level logType){
        log = message;
        type = logType;
    }
}
