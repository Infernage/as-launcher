/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core.events;

import org.infernage.aslauncher.core.CoreHandler;

/**
 * Empty event used to indicates that the JVM is going to shutdown.
 *
 * @author Infernage
 */
public final class ShutdownEvent {

    public ShutdownEvent() {
        if (!CoreHandler.INSTANCE.isGoingShutdown()) {
            throw new IllegalAccessError("JVM is not going to shutdown");
        }
    }
}
