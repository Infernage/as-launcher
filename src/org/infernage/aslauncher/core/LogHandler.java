/*
 * Copyright 2014 Infernage.
 *
 * Redistribution: You may distribute copies of this software in any medium without any modification.
 * This software and its names are property of Infernage.
 *
 */
package org.infernage.aslauncher.core;

import com.google.common.eventbus.Subscribe;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import org.infernage.aslauncher.core.events.LogEvent;
import org.infernage.aslauncher.core.events.MinecraftLogEvent;
import org.infernage.aslauncher.gui.MainFrame;
import org.infernage.aslauncher.util.MessageControl;

/**
 * Log class used to display root messages.
 *
 * @author Infernage
 */
final class LogHandler {

    private final static List<LogEvent> events = new ArrayList<>();
    private final static boolean debug = true;

    static class MinecraftLogRecord extends LogRecord {

        public MinecraftLogRecord(Level level, String msg) {
            super(level, msg);
        }

    }

    static class ConsoleLogHandler extends ConsoleHandler {

        public ConsoleLogHandler() {
            CoreHandler.INSTANCE.subscribe(this);
        }

        @Subscribe
        public void publish(MinecraftLogEvent event) {
            MinecraftLogRecord record = new MinecraftLogRecord(Level.FINE, event.log);
            log(record, this);
            super.publish(record);
        }

        @Override
        public void publish(LogRecord record) {
            if (log(record, this)) {
                super.publish(record);
            }
        }
    }

    static class FileLogHandler extends FileHandler {

        public FileLogHandler() throws IOException {
            super();
            CoreHandler.INSTANCE.subscribe(this);
        }

        public FileLogHandler(String file) throws IOException {
            super(file);
            CoreHandler.INSTANCE.subscribe(this);
        }

        @Subscribe
        public void publish(MinecraftLogEvent event) {
            super.publish(new MinecraftLogRecord(Level.FINE, event.log));
        }

        @Override
        public void publish(LogRecord record) {
            super.publish(record);
        }
    }

    static boolean log(LogRecord record, Handler handler) {
        if (record.getLevel() == Level.WARNING) {
            if (record.getThrown() == null) {
                MessageControl.showWarningMessage(handler.getFormatter().formatMessage(record),
                        null);
            } else {
                MessageControl.showExceptionMessage(record.getThrown(), handler
                        .getFormatter().formatMessage(record));
            }
        } else if (record.getLevel() == Level.SEVERE) {
            if (record.getThrown() != null) {
                MessageControl.showExceptionMessage(record.getThrown(), handler
                        .getFormatter().formatMessage(record));
            } else {
                MessageControl.showErrorMessage(handler.getFormatter()
                        .formatMessage(record), null);
            }
        }
        if (record.getLevel().intValue() >= Level.INFO.intValue() || record.getLevel().intValue()
                == Level.FINE.intValue()) {
            LogEvent evt = new LogEvent(handler.getFormatter().format(record), record.getLevel());
            if (MainFrame.getWindow() != null) {
                String msg = handler.getFormatter().formatMessage(record);
                switch (record.getLevel().intValue()) {
                    case 800:
                        MainFrame.getWindow().printMsg(msg);
                        break;
                    case 1000:
                        MainFrame.getWindow().printErrorMsg(msg);
                        break;
                    case 900:
                        MainFrame.getWindow().printWarningMsg(msg);
                        break;
                }
                if (!events.isEmpty()) {
                    for (LogEvent event : events) {
                        CoreHandler.INSTANCE.sendEvent(event);
                    }
                    events.clear();
                }
                CoreHandler.INSTANCE.sendEvent(evt);
            } else {
                events.add(evt);
            }
            return true;
        }
        return debug;
    }
}
